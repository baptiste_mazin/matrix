/* global describe test expect */
import Matrix from "../src/Matrix.js";

describe("Testing Matrix.elementary_math", () => {

    // Math object function
    // cos acos cosh acosh
    // sin sinh asin asinh
    // tan tanh atan atanh
    // exp log log2 log10 sqrt
    // round sign ceil floor trunc
    // hypot atan2 pow
    // cbrt clz32 fround imul expm1 log1p
    // abs
    // random max min
    test("Matrix.elementary_math: sqrt", () => {
        {
            const mat = Matrix.from([9, 4, 1, 0]);
            mat.sqrt();
            expect(mat.hasImagPart()).toBe(false);
            expect(mat.getData()).toStrictEqual(Float64Array.from([3, 2, 1, 0]));
        } {
            const mat = Matrix.from([9, 4, 1, 0, -1]);
            mat.sqrt();
            expect(mat.isreal()).toBe(false);
            expect(mat.getRealData()).toStrictEqual(Float64Array.from([3, 2, 1, 0, 0]));
            expect(mat.getImagData()).toStrictEqual(Float64Array.from([0, 0, 0, 0, 1]));
        } {
            const mat = Matrix.complex([0, -3, -3, 0, 0, 1], [2, 4, -4, 0, 2, 0]);
            const out = Matrix.sqrt(mat);
            expect(out.isreal()).toBe(false);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([1, 1, 1, 0, 1, 1]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([1, 2, -2, 0, 1, 0]));
            expect(mat.isreal()).toBe(false);
            expect(mat.getRealData()).toStrictEqual(Float64Array.from([0, -3, -3, 0, 0, 1]));
            expect(mat.getImagData()).toStrictEqual(Float64Array.from([2, 4, -4, 0, 2, 0]));
        }
    });

    test("Matrix.elementary_math: exp", () => {
        {
            const mat = Matrix.from([-1, 0, 1]);
            mat.exp();
            expect(mat.hasImagPart()).toBe(false);
            expect(mat.getData()).toStrictEqual(Float64Array.from([1 / Math.E, 1, Math.E]));
        } {
            const expectedReal = Float64Array.from([2.718281828459045, 1, 0.36787944117144233, 1.4686939399158851, 0.5403023058681398, 0.19876611034641298, 1.4686939399158851, 0.5403023058681398, 0.19876611034641298]);
            const expectedImag = Float64Array.from([0, 0, 0, 2.2873552871788423, 0.8414709848078965, 0.3095598756531122, -2.2873552871788423, -0.8414709848078965, -0.3095598756531122]);
            const mat = Matrix.complex([1, 0, -1, 1, 0, -1, 1, 0, -1], [0, 0, 0, 1, 1, 1, -1, -1, -1]);
            mat.exp();
            expect(mat.isreal()).toBe(false);
            expect(mat.getRealData()).toStrictEqual(expectedReal);
            expect(mat.getImagData()).toStrictEqual(expectedImag);
        } {
            const expectedReal = Float64Array.from([2.718281828459045, 1, 0.36787944117144233, 1.4686939399158851, 0.5403023058681398, 0.19876611034641298, 1.4686939399158851, 0.5403023058681398, 0.19876611034641298]);
            const expectedImag = Float64Array.from([0, 0, 0, 2.2873552871788423, 0.8414709848078965, 0.3095598756531122, -2.2873552871788423, -0.8414709848078965, -0.3095598756531122]);
            const mat = Matrix.complex([1, 0, -1, 1, 0, -1, 1, 0, -1], [0, 0, 0, 1, 1, 1, -1, -1, -1]);
            const out = Matrix.exp(mat);
            expect(out.isreal()).toBe(false);
            expect(out.getRealData()).toStrictEqual(expectedReal);
            expect(out.getImagData()).toStrictEqual(expectedImag);
            expect(mat.isreal()).toBe(false);
            expect(mat.getRealData()).toStrictEqual(Float64Array.from([1, 0, -1, 1, 0, -1, 1, 0, -1]));
            expect(mat.getImagData()).toStrictEqual(Float64Array.from([0, 0, 0, 1, 1, 1, -1, -1, -1]));
        }
    });

    test("Matrix.elementary_math: cos", () => {
        const input = Matrix.linspace(-Math.PI, Math.PI, 15);
        const refAngles = input.clone().getData();
        let output = input.clone().cos();
        expect(output.getData()).toStrictEqual(refAngles.map(Math.cos));
        output = Matrix.cos(input);
        expect(output.getData()).toStrictEqual(refAngles.map(Math.cos));
        expect(input.getData()).toStrictEqual(refAngles);
    });

    test("Matrix.elementary_math: sin", () => {
        const input = Matrix.linspace(-Math.PI, Math.PI, 15);
        const refAngles = input.clone().getData();
        let output = input.clone().sin();
        expect(output.getData()).toStrictEqual(refAngles.map(Math.sin));
        output = Matrix.sin(input);
        expect(output.getData()).toStrictEqual(refAngles.map(Math.sin));
        expect(input.getData()).toStrictEqual(refAngles);
    });

    test("Matrix.elementary_math: tan", () => {
        const input = Matrix.linspace(-Math.PI, Math.PI, 15);
        const refAngles = input.clone().getData();
        let output = input.clone().tan();
        expect(output.getData()).toStrictEqual(refAngles.map(Math.tan));
        output = Matrix.tan(input);
        expect(output.getData()).toStrictEqual(refAngles.map(Math.tan));
        expect(input.getData()).toStrictEqual(refAngles);
    });

    test("Matrix.elementary_math: acos", () => {
        const input = Matrix.linspace(-Math.PI, Math.PI, 15);
        const refAngles = input.clone().getData();
        let output = input.clone().acos();
        expect(output.getData()).toStrictEqual(refAngles.map(Math.acos));
        output = Matrix.acos(input);
        expect(output.getData()).toStrictEqual(refAngles.map(Math.acos));
        expect(input.getData()).toStrictEqual(refAngles);
    });

    test("Matrix.elementary_math: asin", () => {
        const input = Matrix.linspace(-Math.PI, Math.PI, 15);
        const refAngles = input.clone().getData();
        let output = input.clone().asin();
        expect(output.getData()).toStrictEqual(refAngles.map(Math.asin));
        output = Matrix.asin(input);
        expect(output.getData()).toStrictEqual(refAngles.map(Math.asin));
        expect(input.getData()).toStrictEqual(refAngles);
    });

    test("Matrix.elementary_math: atan", () => {
        const input = Matrix.linspace(-Math.PI, Math.PI, 15);
        const refAngles = input.clone().getData();
        let output = input.clone().atan();
        expect(output.getData()).toStrictEqual(refAngles.map(Math.atan));
        output = Matrix.atan(input);
        expect(output.getData()).toStrictEqual(refAngles.map(Math.atan));
        expect(input.getData()).toStrictEqual(refAngles);
    });

    test("Matrix.elementary_math: atan2", () => {
        const refAngles = Matrix.linspace(-Math.PI, Math.PI, 15);
        const x = Matrix.cos(refAngles), y = Matrix.sin(refAngles);
        const output = Matrix.atan2(y, x);
        expect(output.getData()).toStrictEqual(refAngles.getData());
    });

    test("Matrix.elementary_math: log", () => {
        const input = Matrix.linspace(0, 10, 41);
        const reference = input.clone().getData();
        let output = input.clone().log();
        expect(output.getData()).toStrictEqual(reference.map(Math.log));
        output = Matrix.log(input);
        expect(output.getData()).toStrictEqual(reference.map(Math.log));
        expect(input.getData()).toStrictEqual(reference);
    });

    test("Matrix.elementary_math: log10", () => {
        const input = Matrix.linspace(-10, 10, 41);
        const reference = input.clone().getData();
        let output = input.clone().log10();
        expect(output.getData()).toStrictEqual(reference.map(Math.log10));
        output = Matrix.log10(input);
        expect(output.getData()).toStrictEqual(reference.map(Math.log10));
        expect(input.getData()).toStrictEqual(reference);
    });

    test("Matrix.elementary_math: log2", () => {
        const input = Matrix.linspace(-10, 10, 41);
        const reference = input.clone().getData();
        let output = input.clone().log2();
        expect(output.getData()).toStrictEqual(reference.map(Math.log2));
        output = Matrix.log2(input);
        expect(output.getData()).toStrictEqual(reference.map(Math.log2));
        expect(input.getData()).toStrictEqual(reference);
    });

    test("Matrix.elementary_math: floor", () => {
        const input = Matrix.linspace(-10, 10, 41);
        const reference = input.clone().getData();
        let output = input.clone().floor();
        expect(output.getData()).toStrictEqual(reference.map(Math.floor));
        output = Matrix.floor(input);
        expect(output.getData()).toStrictEqual(reference.map(Math.floor));
        expect(input.getData()).toStrictEqual(reference);
    });

    test("Matrix.elementary_math: ceil", () => {
        const input = Matrix.linspace(-10, 10, 41);
        const reference = input.clone().getData();
        let output = input.clone().ceil();
        expect(output.getData()).toStrictEqual(reference.map(Math.ceil));
        output = Matrix.ceil(input);
        expect(output.getData()).toStrictEqual(reference.map(Math.ceil));
        expect(input.getData()).toStrictEqual(reference);
    });

    test("Matrix.elementary_math: round", () => {
        const input = Matrix.linspace(-10, 10, 41);
        const reference = input.clone().getData();
        let output = input.clone().round();
        expect(output.getData()).toStrictEqual(reference.map(Math.round));
        output = Matrix.round(input);
        expect(output.getData()).toStrictEqual(reference.map(Math.round));
        expect(input.getData()).toStrictEqual(reference);
    });

    test("Matrix.elementary_math: sign", () => {
        const input = Matrix.from([-1, -0, 0, 1]);
        const reference = input.clone().getData();
        let output = input.sign().round();
        expect(output.getData()).toStrictEqual(reference.map(Math.sign));
        output = Matrix.sign(input);
        expect(output.getData()).toStrictEqual(reference.map(Math.sign));
        expect(input.getData()).toStrictEqual(reference);
    });

});

describe("Testing Matrix.elementary_math.complex_numbers", () => {

    test("Matrix.elementary_math.complex_numbers: real / imag", () => {
        const real = Matrix.randi(255, 5), imag = Matrix.randi(255, 5);
        const cplx = Matrix.complex(real, imag);
        expect(Matrix.real(real).getData()).toStrictEqual(real.getData());
        expect(Matrix.real(cplx).getData()).toStrictEqual(real.getData());
        expect(Matrix.imag(real).getData()).toStrictEqual(new Float64Array(25));
        expect(Matrix.imag(cplx).getData()).toStrictEqual(imag.getData());
    });
    test("Matrix.elementary_math.complex_numbers: angle", () => {
        const real = Matrix.randi(255, 5), imag = Matrix.randi([-10, 10], 5);
        const cplx = Matrix.complex(real, imag);
        expect(Matrix.angle(real).getData()).toStrictEqual(new Float64Array(25));
        expect(Matrix.angle(cplx).getData()).toStrictEqual(Matrix.atan2(imag, real).getData());
    });

    test("Matrix.elementary_math.complex_numbers: abs", () => {
        const real = Matrix.randi([-10, 10], 5), imag = Matrix.randi([-10, 10], 5);
        const cplx = Matrix.complex(real, imag);
        expect(Matrix.abs(real).getData()).toStrictEqual(real.getData().map(Math.abs));
        expect(Matrix.abs(cplx).getData()).toStrictEqual(real.getData().map((v, i) => Math.sqrt(v ** 2 + imag.getData()[i] ** 2)));
        expect(real.clone().abs().getData()).toStrictEqual(real.getData().map(Math.abs));
        expect(cplx.abs().getData()).toStrictEqual(real.getData().map((v, i) => Math.sqrt(v ** 2 + imag.getData()[i] ** 2)));
    });

    test("Matrix.elementary_math.complex_numbers: conj", () => {
        const real = Matrix.randi([-10, 10], 5), imag = Matrix.randi([-10, 10], 5);
        const cplx = Matrix.complex(real, imag);
        expect(Matrix.conj(real).getData()).toStrictEqual(real.getData());
        expect(Matrix.conj(cplx).getImagData()).toStrictEqual(imag.getData().map(v => -v));
        expect(real.clone().conj().getData()).toStrictEqual(real.getData());
        expect(cplx.conj().getImagData()).toStrictEqual(imag.getData().map(v => -v));
    });

    test("Matrix.elementary_math.complex_numbers: ctranspose", () => {
        const real = Matrix.from([-3, -2, -1, 0, 1, 2], [3, 2]), imag = Matrix.from([-6, -5, -4, 3, 4, 5], [3, 2]);
        const cplx = Matrix.complex(real, imag);
        expect(real.ctranspose().getRealData()).toStrictEqual(real.transpose().getData());
        expect(real.ctranspose().getImagData()).toStrictEqual(new Float64Array(6));
        expect(cplx.ctranspose().getRealData()).toStrictEqual(real.transpose().getData());
        expect(cplx.ctranspose().getImagData()).toStrictEqual(imag.transpose().getData().map(v => -v));
        expect(Matrix.ctranspose(cplx).getRealData()).toStrictEqual(real.transpose().getData());
        expect(Matrix.ctranspose(cplx).getImagData()).toStrictEqual(imag.transpose().getData().map(v => -v));
    });


});
