/* global describe test expect */
import Matrix, {} from "../src/Matrix.js";
import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

describe("Testing Matrix.statistics.random", () => {

    test("Matrix.statistics.random: poissrnd", () => {
        {
            const a = Matrix.poissrnd(10, 1e6, 1);
            expect(a.mean()).toEqualMatrix(10, 1e-1);
            expect(a.var()).toEqualMatrix(10, 1e-1);
        } {
            const a = Matrix.poissrnd(Matrix.ones(1e6, 1).times(10));
            expect(a.mean()).toEqualMatrix(10, 1e-1);
            expect(a.var()).toEqualMatrix(10, 1e-1);
        }
    });

    test("Matrix.statistics.random: exprnd", () => {
        expect(Matrix.exprnd(10, 1e6, 1).mean()).toEqualMatrix(10, 1e-1);
        expect(Matrix.exprnd(10, 1e6, 1).var()).toEqualMatrix(100, 1e-1);
    });

    test("Matrix.statistics.random: randperm", () => {
        {
            const a = Matrix.randperm(10);
            expect(a.getSize()).toStrictEqual([10, 1]);
            expect(Matrix.sort(a)).toEqualMatrix(Matrix.colon(0, 9).uint32());
        } {
            const a = Matrix.randperm(1000, 999);
            expect(a.getSize()).toStrictEqual([999, 1]);
            expect(a.unique().numel()).toBe(999);
            expect(a.max().asScalar()).toBeLessThan(1000);
            expect(a.min().asScalar()).toBeGreaterThan(-1);
        }
    });

});
