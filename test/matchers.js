/* istanbul ignore file */
import Matrix, {Check} from "../src/Matrix.js";

// const diff = require('jest-diff');

export function toEqualMatrix(received, expected, threshold = 0) {
    expected = Matrix.from(expected);
    let pass = Check.areSizeEquals(received.getSize(), expected.getSize());
    if (!pass) {
        return {
            message: () => `Expected "size" to be ${"[" + expected.getSize().join(", ") + "]"} but bot ${"[" + received.getSize().join(", ") + "]"}`,
            pass: false,
        };
    }
    if (received.type() !== expected.type()) {
        return {
            message: () => `Expected "type" to be ${received.type()}, but got ${expected.type()}`,
            pass: false,
        };
    }
    const rd = received.getData(), ed = expected.getData();
    let i, ie;
    for (i = 0, ie = rd.length; i < ie; i++) {
        // if (Math.abs(rd[i] - ed[i]) > threshold) {
        if (ed[i] === 0) {
            if (rd[i] > threshold) {
                pass = false;
                break;
            }
        } else if (Math.abs((rd[i] - ed[i]) / rd[i]) > threshold) {
            pass = false;
            break;
        }
    }
    if (!pass) {
        return {
            // message: () => `Value ${rd[i]} at index ${i} must be close to ${ed[i]} with a precision of ${threshold}:\n\n ${diff(rd, ed)}`,
            message: () => `Value ${rd[i]} at index ${i} must be close to ${ed[i]} with a precision of ${threshold}:\n\n ${Matrix.cat(1, received, expected)}`,
            pass: false,
        };
    }
    return {
        message: () => `OK`,
        pass: true,
    };
}
