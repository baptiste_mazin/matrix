/* global describe test expect */
import Matrix from "../src/Matrix.js";

describe("Testing Matrix.information", () => {

    test("Matrix.information: numel / ndims / size / length", () => {
        const size = [5, 4, 3, 2, 1];
        const mat1 = new Matrix(size);
        expect(mat1.numel()).toBe(120);
        expect(Matrix.numel(mat1)).toBe(120);

        expect(mat1.ndims()).toBe(4);
        expect(Matrix.ndims(mat1)).toBe(4);

        expect(mat1.size()).toEqual([5, 4, 3, 2]);
        expect(Matrix.size(mat1)).toEqual([5, 4, 3, 2]);
        expect(mat1.size(3)).toBe(2);
        expect(mat1.size(10)).toBe(1);
        expect(mat1.length()).toBe(5);
    });

    test("Matrix.information: type", () => {
        const types = {
            float64          : "double",
            float32          : "single",
            float            : "single",
            int8             : "int8",
            bool             : "logical",
            boolean          : "logical",
            uint8c           : "uint8c",
            double           : "double",
            single           : "single",
            logical          : "logical",
            uint8            : "uint8",
            int16            : "int16",
            uint16           : "uint16",
            int32            : "int32",
            uint32           : "uint32",
            array            : "double",
            float64array     : "double",
            float32array     : "single",
            int8array        : "int8",
            uint8clampedarray: "uint8c",
            canvaspixelarray : "uint8c",
            uint8array       : "uint8",
            int16array       : "int16",
            uint16array      : "uint16",
            int32array       : "int32",
            uint32array      : "uint32",
        };
        let input, output;
        for ([input, output] of Object.entries(types)) {
            const mat = new Matrix([5, 5], input);
            expect(mat.type()).toEqual(output);
            expect(Matrix.type(mat)).toEqual(output);
        }
    });

    test("Matrix.information: isrow / iscolumn / isvector / ismatrix / issquare", () => {
        const mat = new Matrix([5, 5, 5]);
        expect(mat.ndims()).toBe(3);
        expect(mat.ismatrix()).toBe(false);
        expect(mat.isrow()).toBe(false);
        expect(mat.iscolumn()).toBe(false);
        expect(mat.isvector()).toBe(false);
        expect(mat.issquare()).toBe(false);
        expect(Matrix.ismatrix(mat)).toBe(false);
        expect(Matrix.isrow(mat)).toBe(false);
        expect(Matrix.iscolumn(mat)).toBe(false);
        expect(Matrix.isvector(mat)).toBe(false);
        expect(Matrix.issquare(mat)).toBe(false);
        const mat1 = mat.get([], [], 0);
        expect(mat1.ismatrix()).toBe(true);
        expect(mat1.isvector()).toBe(false);
        expect(mat1.isrow()).toBe(false);
        expect(mat1.iscolumn()).toBe(false);
        expect(mat1.issquare()).toBe(true);
        const mat2 = mat.get([], 0, 0);
        expect(mat2.ismatrix()).toBe(true);
        expect(mat2.isvector()).toBe(true);
        expect(mat2.isrow()).toBe(false);
        expect(mat2.iscolumn()).toBe(true);
        expect(mat2.issquare()).toBe(false);
        const mat3 = mat.get(0, [], 0);
        expect(mat3.ismatrix()).toBe(true);
        expect(mat3.isvector()).toBe(true);
        expect(mat3.isrow()).toBe(true);
        expect(mat3.iscolumn()).toBe(false);
        const mat4 = new Matrix([1, 1, 5]);
        expect(mat4.isvector()).toBe(false);
        expect(mat4.isrow()).toBe(false);
        expect(mat4.iscolumn()).toBe(false);
        const mat5 = new Matrix([1, 5]);
        expect(mat5.ismatrix()).toBe(true);
        expect(mat5.isvector()).toBe(true);
        expect(mat5.isrow()).toBe(true);
        const mat6 = new Matrix([5, 1]);
        expect(mat6.iscolumn()).toBe(true);

    });

    test("Matrix.information: isinteger / isfloat / islogical", () => {
        const integerTypes = {
            int8             : "int8",
            uint8c           : "uint8c",
            uint8            : "uint8",
            int16            : "int16",
            uint16           : "uint16",
            int32            : "int32",
            uint32           : "uint32",
            int8array        : "int8",
            uint8clampedarray: "uint8c",
            canvaspixelarray : "uint8c",
            uint8array       : "uint8",
            int16array       : "int16",
            uint16array      : "uint16",
            int32array       : "int32",
            uint32array      : "uint32",
        };
        const floatTypes = {
            array            : "double",
            float64          : "double",
            float32          : "single",
            float            : "single",
            double           : "double",
            single           : "single",
            float64array     : "double",
            float32array     : "single",
        };
        const logicalTypes = {
            bool             : "logical",
            boolean          : "logical",
            logical          : "logical"
        };
        let input;
        for (input of Object.keys(integerTypes)) {
            const mat = new Matrix([5, 5], input);
            expect(mat.isinteger()).toEqual(true);
            expect(Matrix.isinteger(mat)).toEqual(true);
            expect(mat.isfloat()).toEqual(false);
            expect(Matrix.isfloat(mat)).toEqual(false);
            expect(mat.islogical()).toEqual(false);
            expect(Matrix.islogical(mat)).toEqual(false);
        }
        for (input of Object.keys(floatTypes)) {
            const mat = new Matrix([5, 5], input);
            expect(mat.isinteger()).toEqual(false);
            expect(Matrix.isinteger(mat)).toEqual(false);
            expect(mat.isfloat()).toEqual(true);
            expect(Matrix.isfloat(mat)).toEqual(true);
            expect(mat.islogical()).toEqual(false);
            expect(Matrix.islogical(mat)).toEqual(false);
        }
        for (input of Object.keys(logicalTypes)) {
            const mat = new Matrix([5, 5], input);
            expect(mat.isinteger()).toEqual(false);
            expect(Matrix.isinteger(mat)).toEqual(false);
            expect(mat.isfloat()).toEqual(false);
            expect(Matrix.isfloat(mat)).toEqual(false);
            expect(mat.islogical()).toEqual(true);
            expect(Matrix.islogical(mat)).toEqual(true);
        }
    });

    test("Matrix.information: intmin / intmax / realmin / realmax", () => {
        const integerTypes = new Map([
            ["int8", {min: -128, max: 127}],
            ["int8array", {min: -128, max: 127}],
            [Int8Array, {min: -128, max: 127}],
            ["uint8", {min: 0, max: 255}],
            ["uint8array", {min: 0, max: 255}],
            [Uint8Array, {min: 0, max: 255}],
            ["uint8c", {min: 0, max: 255}],
            ["canvaspixelarray", {min: 0, max: 255}],
            ["uint8clampedarray", {min: 0, max: 255}],
            [Uint8ClampedArray, {min: 0, max: 255}],
            ["int16", {min: -(2 ** 16 / 2), max: 2 ** 16 / 2 - 1}],
            ["int16array", {min: -(2 ** 16 / 2), max: 2 ** 16 / 2 - 1}],
            [Int16Array, {min: -(2 ** 16 / 2), max: 2 ** 16 / 2 - 1}],
            ["uint16", {min: 0, max: 2 ** 16 - 1}],
            ["uint16array", {min: 0, max: 2 ** 16 - 1}],
            [Uint16Array, {min: 0, max: 2 ** 16 - 1}],
            ["int32", {min: -(2 ** 32 / 2), max: 2 ** 32 / 2 - 1}],
            ["int32array", {min: -(2 ** 32 / 2), max: 2 ** 32 / 2 - 1}],
            [Int32Array, {min: -(2 ** 32 / 2), max: 2 ** 32 / 2 - 1}],
            [undefined, {min: -(2 ** 32 / 2), max: 2 ** 32 / 2 - 1}],
            ["uint32", {min: 0, max: 2 ** 32 - 1}],
            ["uint32array", {min: 0, max: 2 ** 32 - 1}],
            [Uint32Array, {min: 0, max: 2 ** 32 - 1}],
        ]);
        let input, data;
        for ([input, data] of integerTypes.entries()) {
            expect(Matrix.intmin(input)).toBe(data.min);
            expect(Matrix.intmax(input)).toBe(data.max);
        }
        const floatTypes = new Map([
            ["array", {min: Number.MIN_VALUE, max: Number.MAX_VALUE}],
            ["double", {min: Number.MIN_VALUE, max: Number.MAX_VALUE}],
            ["float64", {min: Number.MIN_VALUE, max: Number.MAX_VALUE}],
            ["float64array", {min: Number.MIN_VALUE, max: Number.MAX_VALUE}],
            [Float64Array, {min: Number.MIN_VALUE, max: Number.MAX_VALUE}],
            [undefined, {min: Number.MIN_VALUE, max: Number.MAX_VALUE}],
            ["float", {min: 1.1755e-38, max: 3.4028e+38}],
            ["single", {min: 1.1755e-38, max: 3.4028e+38}],
            ["float32", {min: 1.1755e-38, max: 3.4028e+38}],
            ["float32array", {min: 1.1755e-38, max: 3.4028e+38}],
            [Float32Array, {min: 1.1755e-38, max: 3.4028e+38}]
        ]);
        for ([input, data] of floatTypes.entries()) {
            expect(Matrix.realmin(input)).toBe(data.min);
            expect(Matrix.realmax(input)).toBe(data.max);
        }

    });

});
