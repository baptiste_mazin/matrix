/* global describe test expect */
import Matrix from "../src/Matrix.js";

describe("Testing Matrix.tools", () => {

    test("Matrix.tools: dlmread / dlmwrite", () => {
        const testValues = [
            ["0.001", "2e3", "4"],
            ["-10.314e-1", "-.14e+3", -Math.PI + ""]
        ];
        const mat1 = Matrix.dlmread(testValues.map(v => v.join("; ")).join("\n"));
        expect(mat1.getSize()).toEqual([2, 3]);
        expect(mat1.getData()).toEqual(Float64Array.from([ 0.001, -1.0314, 2000, -140, 4, -Math.PI]));

        const mat2 = Matrix.dlmread(testValues.map(v => v.join("; ")).join("\n") + "\n", ";");
        expect(mat2.getSize()).toEqual([2, 3]);
        expect(mat2.getData()).toEqual(Float64Array.from([ 0.001, -1.0314, 2000, -140, 4, -Math.PI]));

        expect(Matrix.dlmread(mat1.dlmwrite("\t")).getData()).toStrictEqual(mat1.getData());
        expect(Matrix.dlmread(mat1.dlmwrite()).getData()).toStrictEqual(mat1.getData());
    });

    test("Matrix.tools: toMatlab", () => {
        const str1 = Buffer.from(
            "Wy0zLjE0MTU5MjY1MzU4OTc5MyAtIDMuMTQxNTkyNjUzNTg5NzkzICogaSwtMC43OD" +
            "UzOTgxNjMzOTc0NDgzIC0gMi4zNTYxOTQ0OTAxOTIzNDUgKiBpLDEuNTcwNzk2MzI2" +
            "Nzk0ODk2NiAtIDEuNTcwNzk2MzI2Nzk0ODk2NiAqIGk7IC0yLjM1NjE5NDQ5MDE5Mj" +
            "M0NSAtIDAuNzg1Mzk4MTYzMzk3NDQ4MyAqIGksMCAtIDAgKiBpLDIuMzU2MTk0NDkw" +
            "MTkyMzQ1ICsgMC43ODUzOTgxNjMzOTc0NDgzICogaTsgLTEuNTcwNzk2MzI2Nzk0OD" +
            "k2NiArIDEuNTcwNzk2MzI2Nzk0ODk2NiAqIGksMC43ODUzOTgxNjMzOTc0NDgzICsg" +
            "Mi4zNTYxOTQ0OTAxOTIzNDUgKiBpLDMuMTQxNTkyNjUzNTg5NzkzICsgMy4xNDE1OT" +
            "I2NTM1ODk3OTMgKiBpXQ==",
            "base64"
        ).toString();
        let mat1 = Matrix.linspace(-Math.PI, Math.PI, 9).reshape(3, 3);
        mat1 = Matrix.complex(mat1, mat1.permute([1, 0]));
        expect(mat1.toMatlab()).toEqual(str1);

        const str2 = Buffer.from(
            "Wy0zLjE0MTU5MjY1MzU4OTc5MywtMC43ODUzOTgxNjMzOTc0NDgzLDEuNTcwNzk2" +
            "MzI2Nzk0ODk2NjsgLTIuMzU2MTk0NDkwMTkyMzQ1LDAsMi4zNTYxOTQ0OTAxOTIz" +
            "NDU7IC0xLjU3MDc5NjMyNjc5NDg5NjYsMC43ODUzOTgxNjMzOTc0NDgzLDMuMTQx" +
            "NTkyNjUzNTg5NzkzXQ==",
            "base64"
        ).toString();
        const mat2 = Matrix.linspace(-Math.PI, Math.PI, 9).reshape(3, 3);
        expect(mat2.toMatlab()).toEqual(str2);
    });

    test("Matrix.tools: toString", () => {
        // To get reference
        // console.log(Buffer.from(mat.toString()).toString('base64').match(/.{1,66}/g));

        const str1 = Buffer.from(
            "KDosOiwwKSA9IFsKCSAgIDMuMTQxNiAgICAzLjE0MTYgICAgMy4xNDE2IAoJICAg" +
            "My4xNDE2ICAgIDMuMTQxNiAgICAzLjE0MTYgCgkgICAzLjE0MTYgICAgMy4xNDE2" +
            "ICAgIDMuMTQxNiAKXQooOiw6LDEpID0gWwoJICAgMy4xNDE2ICAgIDMuMTQxNiAg" +
            "ICAzLjE0MTYgCgkgICAzLjE0MTYgICAgMy4xNDE2ICAgIDMuMTQxNiAKCSAgIDMu" +
            "MTQxNiAgICAzLjE0MTYgICAgMy4xNDE2IApdCig6LDosMikgPSBbCgkgICAzLjE0" +
            "MTYgICAgMy4xNDE2ICAgIDMuMTQxNiAKCSAgIDMuMTQxNiAgICAzLjE0MTYgICAg" +
            "My4xNDE2IAoJICAgMy4xNDE2ICAgIDMuMTQxNiAgICAzLjE0MTYgCl0K",
            "base64"
        ).toString();
        const mat1 = Matrix.from(new Array(27).fill(Math.PI), [3, 3, 3]);
        expect(mat1.toString()).toEqual(str1);

        const str2 = Buffer.from(
            "VGVzdCg6LDosMCkgPSBbCgkgIC0zLjE0MTU5IC0gMy4xNDE1OWkgICAtMi4wMzI4MC" +
            "AtIDIuNzcxOTlpICAgLTAuOTI0MDAgLSAyLjQwMjM5aSAKCSAgLTIuNzcxOTkgLSAy" +
            "LjAzMjgwaSAgIC0xLjY2MzIwIC0gMS42NjMyMGkgICAtMC41NTQ0MCAtIDEuMjkzNj" +
            "BpIAoJICAtMi40MDIzOSAtIDAuOTI0MDBpICAgLTEuMjkzNjAgLSAwLjU1NDQwaSAg" +
            "IC0wLjE4NDgwIC0gMC4xODQ4MGkgCl0KVGVzdCg6LDosMSkgPSBbCgkgICAwLjE4ND" +
            "gwICsgMC4xODQ4MGkgICAgMS4yOTM2MCArIDAuNTU0NDBpICAgIDIuNDAyMzkgKyAw" +
            "LjkyNDAwaSAKCSAgIDAuNTU0NDAgKyAxLjI5MzYwaSAgICAxLjY2MzIwICsgMS42Nj" +
            "MyMGkgICAgMi43NzE5OSArIDIuMDMyODBpIAoJICAgMC45MjQwMCArIDIuNDAyMzlp" +
            "ICAgIDIuMDMyODAgKyAyLjc3MTk5aSAgICAzLjE0MTU5ICsgMy4xNDE1OWkgCl0K",
            "base64"
        ).toString();
        let mat2 = Matrix.linspace(-Math.PI, Math.PI, 18).reshape(3, 3, 2);
        mat2 = Matrix.complex(mat2, mat2.permute([1, 0, 2]));
        expect(mat2.toString("Test", 5)).toEqual(str2);

        const mat3 = new Matrix([0, 5, 3, 2]);
        expect(mat3.toString()).toEqual("Empty array: 0-by-5-by-3-by-2");

        const mat4 = new Matrix([125, 80, 1000]);
        expect(mat4.toString()).toEqual("Array: 125-by-80-by-1000");

        const str5 = Buffer.from(
            "VGVzdCA9IFsKCSAgICAgICAgMSAgIDMxLjYyMjggICAgICAxMDAwIAoJICAgMy4xNj" +
            "IzICAgICAgIDEwMCAzMTYyLjI3NzcgCgkgICAgICAgMTAgIDMxNi4yMjc4ICAgICAx" +
            "MDAwMCAKXQo=",
            "base64"
        ).toString();
        const mat5 = Matrix.logspace(0, 4, 9).reshape(3, 3);
        expect(mat5.toString("Test")).toEqual(str5);

        const str6 = Buffer.from(
            'ID0gWwoJICAgICAgICAxICsgMWkgICAzMS42MjI4ICsgMzEuNjIyOGkgICAgICAxMD' +
            'AwICsgMTAwMGkgCgkgICAzLjE2MjMgLSBOYU5pICAgICAgIDEwMCAgICAgICAgICAg' +
            'MzE2Mi4yNzc3ICsgMzE2Mi4yNzc3aSAKCSAgICAgICAxMCArIDEwaSAgMzE2LjIyNz' +
            'ggKyAzMTYuMjI3OGkgICAgIDEwMDAwICsgMTAwMDBpIApdCg==',
            "base64"
        ).toString();
        const imag6 = new Matrix([3, 3], [1, NaN, 10, 31.622776601683793, 0, 316.2277660168379, 1000, 3162.277660168379, 10000]);
        const mat6 = Matrix.complex(mat5, imag6);
        expect(mat6.toString("")).toEqual(str6);
    });

    test("Matrix.tools: from", () => {
        const input = [
            [
                [1, 2, 3],
                [4, 5, 6]
            ], [
                [7, 8, 9],
                [10, 11, 12]
            ]
        ];
        const mat1 = Matrix.from(input);
        expect(mat1.getSize()).toEqual([3, 2, 2]);
        expect(mat1.getData()).toEqual(Float64Array.from(input.flat().flat()));


        expect(Matrix.from(mat1.clone(), [12, 1]).getSize()).toEqual([12, 1]);
        expect(Matrix.from(mat1.clone()).getSize()).toEqual([3, 2, 2]);

        const mat3 = Matrix.from(input, [12, 1]);
        expect(mat3.getSize()).toEqual([12, 1]);
        expect(mat3.getData()).toEqual(Float64Array.from(input.flat().flat()));

        expect(Matrix.from(3).getSize()).toEqual([1, 1]);
        expect(Matrix.from(3).asScalar()).toEqual(3);

        expect(Matrix.from(true).getSize()).toEqual([1, 1]);
        expect(Matrix.from(true).asScalar()).toEqual(1);
        expect(Matrix.from(true).type()).toBe("logical");

        const mat4 = Matrix.from([[true, false, true], [false, true, false]], [2, 3]);
        expect(mat4.type()).toBe("logical");
    });

    test("Matrix.tools: read / write", () => {
        const types = ["float64", "double", "float32", "float", "single", "int8", "bool", "boolean", "logical", "uint8c", "uint8", "int16", "uint16", "int32", "uint32"];
        for (let type of types) {
            const mat1 = Matrix.randi([0, 128], 10, type);
            const buffer = mat1.write();
            const mat2 = Matrix.read(buffer);
            expect(mat2.getData()).toStrictEqual(mat1.getData());
            expect(mat2.getSize()).toStrictEqual(mat1.getSize());
            // expect(mat2.type()).toStrictEqual(mat1.type());
        }
    });

    test("Matrix.tools: toArray", () => {
        const input = [[0, 1], [2, 3]];
        expect(Matrix.from(input).toArray()).toEqual(input);
    });

});
