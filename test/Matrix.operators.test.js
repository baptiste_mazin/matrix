/* global describe test expect */
import Matrix from "../src/Matrix.js";
import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

describe("Testing Matrix.operators.arithmetic", () => {

    test("Matrix.operators.arithmetic: plus", () => {
        const realMatrix = Matrix.colon(0, 3);
        const cplxScalar = Matrix.complex(1, 1);
        const cplxMatrix = Matrix.complex([0, 1, 2, 3], [4, 5, 6, 7]);

        let input;
        input = realMatrix.clone();
        expect(input.plus(1).getData()).toStrictEqual(Float64Array.from([1, 2, 3, 4]));
        expect(input.plus(input).getData()).toStrictEqual(Float64Array.from([2, 4, 6, 8]));
        input.plus(cplxScalar);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([3, 5, 7, 9]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([1, 1, 1, 1]));
        input = Matrix.plus(1, input);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([4, 6, 8, 10]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([1, 1, 1, 1]));
        input["+="](cplxMatrix);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([4, 7, 10, 13]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([5, 6, 7, 8]));
        let input2 = input["+"](realMatrix);
        // Check that operation is not done in place.
        expect(input.getRealData()).toStrictEqual(Float64Array.from([4, 7, 10, 13]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([5, 6, 7, 8]));
        expect(input2.getRealData()).toStrictEqual(Float64Array.from([4, 8, 12, 16]));
        expect(input2.getImagData()).toStrictEqual(Float64Array.from([5, 6, 7, 8]));

        input = Matrix.plus(cplxMatrix, cplxScalar);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([1, 2, 3, 4]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([5, 6, 7, 8]));

        input = Matrix.plus(realMatrix, cplxMatrix);
        expect(realMatrix.getData()).toStrictEqual(Float64Array.from([0, 1, 2, 3]));
        expect(input.getRealData()).toStrictEqual(Float64Array.from([0, 2, 4, 6]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([4, 5, 6, 7]));
    });

    test("Matrix.operators.arithmetic: minus", () => {
        const realMatrix = Matrix.colon(0, 3);
        const cplxScalar = Matrix.complex(1, 1);
        const cplxMatrix = Matrix.complex([0, 1, 2, 3], [4, 5, 6, 7]);
        let input;
        input = realMatrix.clone();
        expect(input.minus(1).getData()).toStrictEqual(Float64Array.from([-1, 0, 1, 2]));
        expect(input.minus(input).getData()).toStrictEqual(Float64Array.from([0, 0, 0, 0]));
        input.minus(cplxScalar);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([-1, -1, -1, -1]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([-1, -1, -1, -1]));
        input = Matrix.minus(1, input);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([2, 2, 2, 2]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([1, 1, 1, 1]));
        input["-="](cplxMatrix);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([2, 1, 0, -1]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([-3, -4, -5, -6]));
        input["-"](realMatrix);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([2, 1, 0, -1]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([-3, -4, -5, -6]));
        input = input["-"](realMatrix);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([2, 0, -2, -4]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([-3, -4, -5, -6]));

        input = Matrix.minus(cplxMatrix, cplxScalar);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([-1, 0, 1, 2]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([3, 4, 5, 6]));

        input = realMatrix.clone().minus(cplxMatrix);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([0, 0, 0, 0]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([-4, -5, -6, -7]));

        input = cplxMatrix.clone().minus(1);
        expect(input.getRealData()).toStrictEqual(Float64Array.from([-1, 0, 1, 2]));
        expect(input.getImagData()).toStrictEqual(Float64Array.from([4, 5, 6, 7]));
    });

    test("Matrix.operators.arithmetic: uminus", () => {
        const realMatrix = Matrix.colon(0, 3);
        const cplxMatrix = Matrix.complex([0, 1, 2, 3], [4, 5, 6, 7]);
        expect(Matrix.uminus(realMatrix).getData()).toStrictEqual(Float64Array.from([-0, -1, -2, -3]));
        expect(realMatrix.getData()).toStrictEqual(Float64Array.from([0, 1, 2, 3]));
        let cplx = cplxMatrix.clone().uminus();
        expect(cplx.getRealData()).toStrictEqual(Float64Array.from([-0, -1, -2, -3]));
        expect(cplx.getImagData()).toStrictEqual(Float64Array.from([-4, -5, -6, -7]));
    });

    test("Matrix.operators.arithmetic: times", () => {
        const realScalarRef = Matrix.from(2);
        const realMatrixRef = Matrix.from([2, -1]);
        const cplxScalarRef = Matrix.complex(-1, 1);
        const cplxMatrixRef = Matrix.complex([-1, 2], [-2, 1]);
        let out;
        { // Scalar real / scalar real
            const realScalar = realScalarRef.clone();
            out = realScalar[".*"](realScalar);
            expect(out.getData()).toStrictEqual(Float64Array.from([4]));
            expect(out.getData()).toStrictEqual(Float64Array.from([4]));
            out = realScalar["*="](realScalar);
            expect(out.getData()).toStrictEqual(Float64Array.from([4]));
        } { // Scalar real / scalar cplx
            const realScalar = realScalarRef.clone(),
                cplxScalar = cplxScalarRef.clone();
            out = Matrix.times(realScalar, cplxScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-2]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([2]));
            realScalar.times(cplxScalar);
            expect(realScalar.getRealData()).toStrictEqual(Float64Array.from([-2]));
            expect(realScalar.getImagData()).toStrictEqual(Float64Array.from([2]));
        } { // Scalar cplx / scalar real
            const realScalar = realScalarRef.clone(),
                cplxScalar = cplxScalarRef.clone();
            out = Matrix.times(cplxScalar, realScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-2]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([2]));
            cplxScalar.times(realScalar);
            expect(cplxScalar.getRealData()).toStrictEqual(Float64Array.from([-2]));
            expect(cplxScalar.getImagData()).toStrictEqual(Float64Array.from([2]));
        } { // Scalar cplx / scalar cplx
            const cplxScalar = cplxScalarRef.clone();
            out = Matrix.times(cplxScalar, cplxScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([0]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([-2]));
            cplxScalar.times(cplxScalar);
            expect(cplxScalar.getRealData()).toStrictEqual(Float64Array.from([0]));
            expect(cplxScalar.getImagData()).toStrictEqual(Float64Array.from([-2]));
        } { // Matrix real / matrix real
            const realMatrix = realMatrixRef.clone();
            out = Matrix.times(realMatrix, realMatrix);
            expect(out.getData()).toStrictEqual(Float64Array.from([4, 1]));
            realMatrix.times(realMatrix);
            expect(realMatrix.getData()).toStrictEqual(Float64Array.from([4, 1]));
        } { // Matrix real / matrix cplx
            const realMatrix = realMatrixRef.clone(),
                cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.times(realMatrix, cplxMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-2, -2]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([-4, -1]));
            realMatrix.times(cplxMatrix);
            expect(realMatrix.getRealData()).toStrictEqual(Float64Array.from([-2, -2]));
            expect(realMatrix.getImagData()).toStrictEqual(Float64Array.from([-4, -1]));
        } { // Matrix cplx / matrix real
            const realMatrix = realMatrixRef.clone(),
                cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.times(cplxMatrix, realMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-2, -2]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([-4, -1]));
            cplxMatrix.times(realMatrix);
            expect(cplxMatrix.getRealData()).toStrictEqual(Float64Array.from([-2, -2]));
            expect(cplxMatrix.getImagData()).toStrictEqual(Float64Array.from([-4, -1]));
        } { // Matrix cplx / matrix cplx
            const cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.times(cplxMatrix, cplxMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-3, 3]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([4, 4]));
            cplxMatrix.times(cplxMatrix);
            expect(cplxMatrix.getRealData()).toStrictEqual(Float64Array.from([-3, 3]));
            expect(cplxMatrix.getImagData()).toStrictEqual(Float64Array.from([4, 4]));
        }



        // Symetric cases
        // Scalar real / matrix real
        // Scalar real / matrix cplx
        // Scalar cplx / matrix real
        // Scalar cplx / matrix cplx
        // Matrix real / scalar real
        // Matrix real / scalar cplx
        // Matrix cplx / scalar real
        // Matrix cplx / scalar imag
    });

    test("Matrix.operators.arithmetic: rdivide", () => {
        const realScalarRef = Matrix.from(2);
        const realMatrixRef = Matrix.from([2, -1]);
        const cplxScalarRef = Matrix.complex(-1, 1);
        const cplxMatrixRef = Matrix.complex([-1, 2], [-2, 1]);
        let out;
        { // Scalar real / scalar real
            const realScalar = realScalarRef.clone();
            out = realScalar["./"](realScalar);
            expect(out.getData()).toStrictEqual(Float64Array.from([1]));
            out = realScalar["/="](realScalar);
            expect(out.getData()).toStrictEqual(Float64Array.from([1]));
        } { // Scalar real / scalar cplx
            const realScalar = realScalarRef.clone(),
                cplxScalar = cplxScalarRef.clone();
            out = Matrix.rdivide(realScalar, cplxScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-1]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([-1]));
            realScalar.rdivide(cplxScalar);
            expect(realScalar.getRealData()).toStrictEqual(Float64Array.from([-1]));
            expect(realScalar.getImagData()).toStrictEqual(Float64Array.from([-1]));
        } { // Scalar cplx / scalar real
            const realScalar = realScalarRef.clone(),
                cplxScalar = cplxScalarRef.clone();
            out = Matrix.rdivide(cplxScalar, realScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-0.5]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([0.5]));
            cplxScalar.rdivide(realScalar);
            expect(cplxScalar.getRealData()).toStrictEqual(Float64Array.from([-0.5]));
            expect(cplxScalar.getImagData()).toStrictEqual(Float64Array.from([0.5]));
        } { // Scalar cplx / scalar cplx
            const cplxScalar = cplxScalarRef.clone();
            out = Matrix.rdivide(cplxScalar, cplxScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([1]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([0]));
            cplxScalar.rdivide(cplxScalar);
            expect(cplxScalar.getRealData()).toStrictEqual(Float64Array.from([1]));
            expect(cplxScalar.getImagData()).toStrictEqual(Float64Array.from([0]));
        } { // Matrix real / matrix real
            const realMatrix = realMatrixRef.clone();
            out = Matrix.rdivide(realMatrix, realMatrix);
            expect(out.getData()).toStrictEqual(Float64Array.from([1, 1]));
            realMatrix.rdivide(realMatrix);
            expect(realMatrix.getData()).toStrictEqual(Float64Array.from([1, 1]));
        } { // Matrix real / matrix cplx
            const realMatrix = realMatrixRef.clone(),
                cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.rdivide(realMatrix, cplxMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-0.4, -0.4]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([0.8, 0.2]));
            realMatrix.rdivide(cplxMatrix);
            expect(realMatrix.getRealData()).toStrictEqual(Float64Array.from([-0.4, -0.4]));
            expect(realMatrix.getImagData()).toStrictEqual(Float64Array.from([0.8, 0.2]));
        } { // Matrix cplx / matrix real
            const realMatrix = realMatrixRef.clone(),
                cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.rdivide(cplxMatrix, realMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-0.5, -2]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([-1, -1]));
            cplxMatrix.rdivide(realMatrix);
            expect(cplxMatrix.getRealData()).toStrictEqual(Float64Array.from([-0.5, -2]));
            expect(cplxMatrix.getImagData()).toStrictEqual(Float64Array.from([-1, -1]));
        } { // Matrix cplx / matrix cplx
            const cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.rdivide(cplxMatrix, cplxMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([1, 1]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([0, 0]));
            cplxMatrix.rdivide(cplxMatrix);
            expect(cplxMatrix.getRealData()).toStrictEqual(Float64Array.from([1, 1]));
            expect(cplxMatrix.getImagData()).toStrictEqual(Float64Array.from([0, 0]));
        } { // Test added after bug finding
            const a = Matrix.from([1, 1, 1]), b = Matrix.from([0.5, 0.5, 0.5]);
            const res = a["./"](b);
            expect(res.getData()).toStrictEqual(Float64Array.from([2, 2, 2]));
        }
    });

    test("Matrix.operators.arithmetic: ldivide", () => {
        const realScalarRef = Matrix.from(2);
        const realMatrixRef = Matrix.from([2, -1]);
        const cplxScalarRef = Matrix.complex(-1, 1);
        const cplxMatrixRef = Matrix.complex([-1, 2], [-2, 1]);
        let out;
        { // Scalar real / scalar real
            const realScalar = realScalarRef.clone();
            out = realScalar[".\\"](realScalar);
            expect(out.getData()).toStrictEqual(Float64Array.from([1]));
            out = realScalar["\\="](realScalar);
            expect(out.getData()).toStrictEqual(Float64Array.from([1]));
        }
        { // Scalar real / scalar cplx
            const realScalar = realScalarRef.clone(),
                cplxScalar = cplxScalarRef.clone();
            out = Matrix.ldivide(realScalar, cplxScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-0.5]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([0.5]));
            realScalar.ldivide(cplxScalar);
            expect(realScalar.getRealData()).toStrictEqual(Float64Array.from([-0.5]));
            expect(realScalar.getImagData()).toStrictEqual(Float64Array.from([0.5]));
        }
        { // Scalar cplx / scalar real
            const realScalar = realScalarRef.clone(),
                cplxScalar = cplxScalarRef.clone();
            out = Matrix.ldivide(cplxScalar, realScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-1]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([-1]));
            cplxScalar.ldivide(realScalar);
            expect(cplxScalar.getRealData()).toStrictEqual(Float64Array.from([-1]));
            expect(cplxScalar.getImagData()).toStrictEqual(Float64Array.from([-1]));
        }
        { // Scalar cplx / scalar cplx
            const cplxScalar = cplxScalarRef.clone();
            out = Matrix.ldivide(cplxScalar, cplxScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([1]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([0]));
            cplxScalar.ldivide(cplxScalar);
            expect(cplxScalar.getRealData()).toStrictEqual(Float64Array.from([1]));
            expect(cplxScalar.getImagData()).toStrictEqual(Float64Array.from([0]));
        } { // Matrix real / matrix real
            const realMatrix = realMatrixRef.clone();
            out = Matrix.ldivide(realMatrix, realMatrix);
            expect(out.getData()).toStrictEqual(Float64Array.from([1, 1]));
            realMatrix.ldivide(realMatrix);
            expect(realMatrix.getData()).toStrictEqual(Float64Array.from([1, 1]));
        }
        { // Matrix real / matrix cplx
            const realMatrix = realMatrixRef.clone(),
                cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.ldivide(realMatrix, cplxMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-0.5, -2]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([-1, -1]));
            realMatrix.ldivide(cplxMatrix);
            expect(realMatrix.getRealData()).toStrictEqual(Float64Array.from([-0.5, -2]));
            expect(realMatrix.getImagData()).toStrictEqual(Float64Array.from([-1, -1]));
        }
         { // Matrix cplx / matrix real
            const realMatrix = realMatrixRef.clone(),
                cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.ldivide(cplxMatrix, realMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([-0.4, -0.4]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([0.8, 0.2]));
            cplxMatrix.ldivide(realMatrix);
            expect(cplxMatrix.getRealData()).toStrictEqual(Float64Array.from([-0.4, -0.4]));
            expect(cplxMatrix.getImagData()).toStrictEqual(Float64Array.from([0.8, 0.2]));
        } { // Matrix cplx / matrix cplx
            const cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.ldivide(cplxMatrix, cplxMatrix);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([1, 1]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([0, 0]));
            cplxMatrix.ldivide(cplxMatrix);
            expect(cplxMatrix.getRealData()).toStrictEqual(Float64Array.from([1, 1]));
            expect(cplxMatrix.getImagData()).toStrictEqual(Float64Array.from([0, 0]));
        }
    });

    test("Matrix.operators.arithmetic: power", () => {
        const realScalarRef = Matrix.from(3);
        const realMatrixRef = Matrix.from([2, -2]);
        const cplxScalarRef = Matrix.complex(-1, 1);
        const cplxMatrixRef = Matrix.complex([-1, 2], [-2, 1]);
        let out;
        { // Scalar real / scalar real
            const realScalar = realScalarRef.clone();
            out = Matrix.power(realScalar, realScalar);
            expect(out.getData()).toStrictEqual(Float64Array.from([27]));
            out = realScalar.power(realScalar);
            expect(out.getData()).toStrictEqual(Float64Array.from([27]));
        } { // NOT SUPPORTED: Scalar real / scalar cplx
        } { // Scalar cplx / scalar real
            const realScalar = realScalarRef.clone(),
                cplxScalar = cplxScalarRef.clone();
            out = Matrix.power(cplxScalar, realScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([2.000000000000001]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([2]));
            cplxScalar.power(realScalar);
            expect(cplxScalar.getRealData()).toStrictEqual(Float64Array.from([2.000000000000001]));
            expect(cplxScalar.getImagData()).toStrictEqual(Float64Array.from([2]));
        } { // NOT SUPPORTED: Scalar cplx / scalar cplx
        } { // Matrix real / matrix real
            const realMatrix = realMatrixRef.clone();
            out = Matrix.power(realMatrix, realMatrix);
            expect(out.getData()).toStrictEqual(Float64Array.from([4, 0.25]));
            realMatrix.power(realMatrix);
            expect(realMatrix.getData()).toStrictEqual(Float64Array.from([4, 0.25]));
        } { // NOT SUPPORTED: Matrix real / matrix cplx
        } { // Matrix cplx / matrix real
            const realMatrix = realMatrixRef.clone(),
                cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.power(cplxMatrix, realMatrix);
            expect(Matrix.real(out)).toEqualMatrix([-3, 0.12], 1e-12);
            expect(Matrix.imag(out)).toEqualMatrix([4, -0.16], 1e-12);
            cplxMatrix.power(realMatrix);
            expect(Matrix.real(cplxMatrix)).toEqualMatrix([-3, 0.12], 1e-12);
            expect(Matrix.imag(cplxMatrix)).toEqualMatrix([4, -0.16], 1e-12);
        } { // NOT SUPPORTED: Matrix cplx / matrix cplx
        }
    });

});

describe("Testing Matrix.operators.booleans", () => {

    test("Matrix.operators.booleans: eq / ne", () => {
        const real1 = new Matrix([3, 3], [NaN, Infinity, -Infinity, 3, 3, 3, 3, 3, 3]);
        const real2 = new Matrix([3, 3], [NaN, 3, 3, Infinity, 3, 3, -Infinity, 3, 3]);
        const scalar = Matrix.from(3);
        expect(real1.eq(scalar).getData()).toStrictEqual(Uint8ClampedArray.from([0, 0, 0, 1, 1, 1, 1, 1, 1]));
        expect(Matrix.eq(real1, real2).getData()).toStrictEqual(Uint8ClampedArray.from([0, 0, 0, 0, 1, 1, 0, 1, 1]));
        expect(real1.ne(scalar).getData()).toStrictEqual(Uint8ClampedArray.from([1, 1, 1, 0, 0, 0, 0, 0, 0]));
        expect(Matrix.ne(real1, real2).getData()).toStrictEqual(Uint8ClampedArray.from([1, 1, 1, 1, 0, 0, 1, 0, 0]));

    });

    test("Matrix.operators.booleans: gt / ge", () => {
        const real1 = new Matrix([3, 3], [NaN, Infinity, -Infinity, -2, -1, 0, 1, 2, 3]);
        const real2 = new Matrix([3, 3], [NaN, -2, 1, Infinity, -1, 2, -Infinity, 0, 3]);
        const scalar = Matrix.from(2);
        expect(real1.gt(scalar).getData()).toStrictEqual(Uint8ClampedArray.from([0, 1, 0, 0, 0, 0, 0, 0, 1]));
        expect(Matrix.gt(real1, real2).getData()).toStrictEqual(Uint8ClampedArray.from([0, 1, 0, 0, 0, 0, 1, 1, 0]));
        expect(real1.ge(scalar).getData()).toStrictEqual(Uint8ClampedArray.from([0, 1, 0, 0, 0, 0, 0, 1, 1]));
        expect(Matrix.ge(real1, real2).getData()).toStrictEqual(Uint8ClampedArray.from([0, 1, 0, 0, 1, 0, 1, 1, 1]));
    });

    test("Matrix.operators.booleans: lt / le", () => {
        const real1 = new Matrix([3, 3], [NaN, Infinity, -Infinity, -2, -1, 0, 1, 2, 3]);
        const real2 = new Matrix([3, 3], [NaN, -2, 1, Infinity, -1, 2, -Infinity, 0, 3]);
        const scalar = Matrix.from(2);
        expect(real1.lt(scalar).getData()).toStrictEqual(Uint8ClampedArray.from([0, 0, 1, 1, 1, 1, 1, 0, 0]));
        expect(Matrix.lt(real1, real2).getData()).toStrictEqual(Uint8ClampedArray.from([0, 0, 1, 1, 0, 1, 0, 0, 0]));
        expect(real1.le(scalar).getData()).toStrictEqual(Uint8ClampedArray.from([0, 0, 1, 1, 1, 1, 1, 1, 0]));
        expect(Matrix.le(real1, real2).getData()).toStrictEqual(Uint8ClampedArray.from([0, 0, 1, 1, 1, 1, 0, 0, 1]));
    });

    test("Matrix.operators.booleans: and / or", () => {
        const log1 = Matrix.from([true, true, true, false, false, false, true, true, true], [3, 3]);
        const log2 = Matrix.from([true, false, true, true, false, true, true, false, true], [3, 3]);
        expect(Matrix.and(log1, true).getData()).toStrictEqual(Uint8ClampedArray.from([1, 1, 1, 0, 0, 0, 1, 1, 1]));
        expect(log1.and(log2).getData()).toStrictEqual(Uint8ClampedArray.from([1, 0, 1, 0, 0, 0, 1, 0, 1]));
        expect(Matrix.or(false, log1).getData()).toStrictEqual(Uint8ClampedArray.from([1, 1, 1, 0, 0, 0, 1, 1, 1]));
        expect(log1.or(log2).getData()).toStrictEqual(Uint8ClampedArray.from([1, 1, 1, 1, 0, 1, 1, 1, 1]));
    });

    test("Matrix.operators.booleans: not", () => {
        const mat1 = Matrix.from([
            false, true, false,
            true, false, true,
            false, true, false
        ], [3, 3]);
        const mat2 = Matrix.from([
            true, false, true,
            false, true, false,
            true, false, true
        ], [3, 3]);
        expect(mat1.clone().not().getData()).toStrictEqual(mat2.getData());
        expect(Matrix.not(mat1).getData()).toStrictEqual(mat2.getData());
    });
    test("Matrix.operators.booleans: isequal", () => {
        const mat1 = Matrix.randi(255, 5, 5),
            mat2 = Matrix.randi(255, 5, 5);
        const cplx = Matrix.complex(mat1, mat2);
        expect(cplx.isequal(cplx.clone(), cplx.clone())).toBe(true);
        expect(Matrix.isequal(cplx, cplx.clone(), cplx.clone().reshape(25, 1))).toBe(false);
        expect(Matrix.isequal(true, 1)).toBe(true);
        expect(Matrix.isequal(new Matrix([2, 1], [NaN, 1]), new Matrix([2, 1], [NaN, 1]))).toBe(false);
    });
    test("Matrix.operators.booleans: isequaln", () => {
        const mat1 = Matrix.randi(255, 5, 5),
            mat2 = Matrix.randi(255, 5, 5);
        const cplx = Matrix.complex(mat1, mat2);
        expect(cplx.isequaln(cplx.clone(), cplx.clone())).toBe(true);
        expect(Matrix.isequaln(cplx, cplx.clone(), cplx.clone().reshape(25, 1))).toBe(false);
        expect(Matrix.isequaln(true, 1)).toBe(true);
        expect(Matrix.isequaln(new Matrix([1, 1], [NaN]), 1)).toBe(false);
        expect(Matrix.isequaln(new Matrix([1, 1], [NaN]), new Matrix([1, 1], [NaN]))).toBe(true);
        expect(Matrix.isequaln(new Matrix([2, 1], [NaN, 1]), new Matrix([2, 1], [NaN, 1]))).toBe(true);
    });
});

describe("Testing Matrix.operators.misc", () => {

    test("Matrix.operators.misc: arrayfun", () => {
        const input = Float64Array.from([-4, 3, 2, 1]);
        const mat = Matrix.from(input.slice()).arrayfun(v => v * 2);
        expect(mat.getData()).toStrictEqual(input.map(v => v * 2));
    });

    test("Matrix.operators.misc: norm", () => {
        const mat = Matrix.from([-4, -3, -2, -1, 0, -0, 1, 2, 3, 4]);
        expect(mat.norm()).toStrictEqual(7.745966692414834);
        expect(mat.norm(-1)).toStrictEqual(Infinity);
        expect(mat.norm(0)).toStrictEqual(8);
        expect(mat.norm(1)).toStrictEqual(20);
        expect(mat.norm(3)).toStrictEqual(5.848035476425732);
        expect(mat.norm(Infinity)).toStrictEqual(4);
        expect(mat.norm(-Infinity)).toStrictEqual(0);
        expect(Matrix.norm(mat, 2)).toStrictEqual(7.745966692414834);
    });

    test("Matrix.operators.misc: diag", () => {
        const real = Matrix.colon(1, 20).reshape(5, 4);
        const imag = Matrix.colon(21, 40).reshape(5, 4);
        const cplx = Matrix.complex(real, imag);

        {
            const input = real.clone();
            const diag = input.diag();
            expect(diag.getData()).toStrictEqual(Float64Array.from([1, 7, 13, 19]));
        } {
            const input = real.clone().reshape(4, 5);
            const diag = input.diag();
            expect(diag.getData()).toStrictEqual(Float64Array.from([1, 6, 11, 16]));
        } {
            const input = real.clone();
            const diag = input.diag(-1);
            expect(diag.getData()).toStrictEqual(Float64Array.from([2, 8, 14, 20]));
        } {
            const input = real.clone();
            const diag = input.diag(1);
            expect(diag.getData()).toStrictEqual(Float64Array.from([6, 12, 18]));
        } {
            const input = cplx.clone();
            const output = input.diag();
            expect(output.getRealData()).toStrictEqual(Float64Array.from([1, 7, 13, 19]));
            expect(output.getImagData()).toStrictEqual(Float64Array.from([21, 27, 33, 39]));
        }
    });

    test("Matrix.operators.misc: triu", () => {
        const real = Matrix.colon(1, 20).reshape(5, 4);
        const imag = Matrix.colon(21, 40).reshape(5, 4);
        const cplx = Matrix.complex(real, imag);

        {
            const input = real.clone();
            input.triu();
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 0, 0, 0, 0, 6, 7, 0, 0, 0, 11, 12, 13, 0, 0, 16, 17, 18, 19, 0]));
        } {
            const input = real.clone().reshape(4, 5);
            input.triu();
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 0, 0, 0, 5, 6, 0, 0, 9, 10, 11, 0, 13, 14, 15, 16, 17, 18, 19, 20]));
        } {
            const input = real.clone();
            input.triu(-1);
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 2, 0, 0, 0, 6, 7, 8, 0, 0, 11, 12, 13, 14, 0, 16, 17, 18, 19, 20]));
        } {
            const input = real.clone();
            input.triu(1);
            expect(input.getData()).toStrictEqual(Float64Array.from([0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 11, 12, 0, 0, 0, 16, 17, 18, 0, 0]));
        } {
            const input = cplx.clone();
            const output = Matrix.triu(input);
            expect(output.getRealData()).toStrictEqual(Float64Array.from([1, 0, 0, 0, 0, 6, 7, 0, 0, 0, 11, 12, 13, 0, 0, 16, 17, 18, 19, 0]));
            expect(output.getImagData()).toStrictEqual(Float64Array.from([21, 0, 0, 0, 0, 26, 27, 0, 0, 0, 31, 32, 33, 0, 0, 36, 37, 38, 39, 0]));
        }

        {
            const input = real.clone();
            input.tril();
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 2, 3, 4, 5, 0, 7, 8, 9, 10, 0, 0, 13, 14, 15, 0, 0, 0, 19, 20]));
        } {
            const input = real.clone().reshape(4, 5);
            input.tril();
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 2, 3, 4, 0, 6, 7, 8, 0, 0, 11, 12, 0, 0, 0, 16, 0, 0, 0, 0]));
        } {
            const input = real.clone();
            input.tril(-1);
            expect(input.getData()).toStrictEqual(Float64Array.from([0, 2, 3, 4, 5, 0, 0, 8, 9, 10, 0, 0, 0, 14, 15, 0, 0, 0, 0, 20]));
        } {
            const input = real.clone();
            input.tril(1);
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 12, 13, 14, 15, 0, 0, 18, 19, 20]));
        }  {
            const input = cplx.clone();
            const output = Matrix.tril(input);
            expect(output.getRealData()).toStrictEqual(Float64Array.from([1, 2, 3, 4, 5, 0, 7, 8, 9, 10, 0, 0, 13, 14, 15, 0, 0, 0, 19, 20]));
            expect(output.getImagData()).toStrictEqual(Float64Array.from([21, 22, 23, 24, 25, 0, 27, 28, 29, 30, 0, 0, 33, 34, 35, 0, 0, 0, 39, 40]));
        }
    });

    test("Matrix.operators.misc: tril", () => {
        const real = Matrix.colon(1, 20).reshape(5, 4);
        const imag = Matrix.colon(21, 40).reshape(5, 4);
        const cplx = Matrix.complex(real, imag);

        {
            const input = real.clone();
            input.tril();
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 2, 3, 4, 5, 0, 7, 8, 9, 10, 0, 0, 13, 14, 15, 0, 0, 0, 19, 20]));
        } {
            const input = real.clone().reshape(4, 5);
            input.tril();
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 2, 3, 4, 0, 6, 7, 8, 0, 0, 11, 12, 0, 0, 0, 16, 0, 0, 0, 0]));
        } {
            const input = real.clone();
            input.tril(-1);
            expect(input.getData()).toStrictEqual(Float64Array.from([0, 2, 3, 4, 5, 0, 0, 8, 9, 10, 0, 0, 0, 14, 15, 0, 0, 0, 0, 20]));
        } {
            const input = real.clone();
            input.tril(1);
            expect(input.getData()).toStrictEqual(Float64Array.from([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 12, 13, 14, 15, 0, 0, 18, 19, 20]));
        }  {
            const input = cplx.clone();
            const output = Matrix.tril(input);
            expect(output.getRealData()).toStrictEqual(Float64Array.from([1, 2, 3, 4, 5, 0, 7, 8, 9, 10, 0, 0, 13, 14, 15, 0, 0, 0, 19, 20]));
            expect(output.getImagData()).toStrictEqual(Float64Array.from([21, 22, 23, 24, 25, 0, 27, 28, 29, 30, 0, 0, 33, 34, 35, 0, 0, 0, 39, 40]));
        }
    });

    test("Matrix.operators.misc: bsxfun", () => {
        const A = Matrix.colon(0, 8).reshape(3, 3);
        const B = Matrix.from(10);
        const C = Matrix.from([10, 11, 12], [3, 1]);
        const D = Matrix.from([10, 11, 12], [1, 3]);
        const E = Matrix.from([2, -2]);
        const F = Matrix.from([-2, 2]);
        expect(Matrix.bsxfun("plus", A, B).getData()).toStrictEqual(Matrix.colon(10, 18).getData());
        expect(Matrix.bsxfun("plus", A, C).getData()).toStrictEqual(Matrix.from([10, 12, 14, 13, 15, 17, 16, 18, 20]).getData());
        expect(Matrix.bsxfun("plus", A, D).getData()).toStrictEqual(Matrix.from([10, 11, 12, 14, 15, 16, 18, 19, 20]).getData());
        expect(Matrix.bsxfun("plus", C, D).getData()).toStrictEqual(Matrix.from([20, 21, 22, 21, 22, 23, 22, 23, 24]).getData());

        expect(Matrix.bsxfun("minus", C, D).getData()).toStrictEqual(Matrix.from([0, 1, 2, -1, 0, 1, -2, -1, 0]).getData());
        expect(Matrix.bsxfun("times", C, D).getData()).toStrictEqual(Matrix.from([100, 110, 120, 110, 121, 132, 120, 132, 144]).getData());
        expect(Matrix.bsxfun("rdivide", C, D).getData()).toStrictEqual(Matrix.from([1, 11 / 10, 12 / 10, 10 / 11, 1, 12 / 11, 10 / 12, 11 / 12, 1]).getData());
        expect(Matrix.bsxfun("ldivide", C, D).getData()).toStrictEqual(Matrix.from([1, 10 / 11, 10 / 12, 11 / 10, 1, 11 / 12, 12 / 10, 12 / 11, 1]).getData());

        expect(Matrix.bsxfun("min", E, F).getData()).toStrictEqual(Matrix.from([-2, -2]).getData());
        expect(Matrix.bsxfun("max", E, F).getData()).toStrictEqual(Matrix.from([2, 2]).getData());
        expect(Matrix.bsxfun("power", E, F).getData()).toStrictEqual(Matrix.from([0.25, 4]).getData());
        expect(Matrix.bsxfun("hypot", E, F).getData()).toStrictEqual(Matrix.from([2.8284271247461903, 2.8284271247461903]).getData());
        expect(Matrix.bsxfun("atan2", E, F).getData()).toStrictEqual(Matrix.from([2.356194490192345, -0.7853981633974483]).getData());
        expect(Matrix.bsxfun("eq", [2, -2], [-2, -2]).getData()).toStrictEqual(Matrix.from([0, 1]).getData());
        expect(Matrix.bsxfun("ne", [2, -2], [-2, -2]).getData()).toStrictEqual(Matrix.from([1, 0]).getData());
        expect(Matrix.bsxfun("lt", [2, -2], [-2, 0]).getData()).toStrictEqual(Matrix.from([0, 1]).getData());
        expect(Matrix.bsxfun("le", [2, -2], [-2, -2]).getData()).toStrictEqual(Matrix.from([0, 1]).getData());
        expect(Matrix.bsxfun("gt", [2, -2], [-2, 0]).getData()).toStrictEqual(Matrix.from([1, 0]).getData());
        expect(Matrix.bsxfun("ge", [-3, -2], [-2, -2]).getData()).toStrictEqual(Matrix.from([0, 1]).getData());
        expect(Matrix.bsxfun("and", [1, 1], [0, 1]).getData()).toStrictEqual(Matrix.from([0, 1]).getData());
        expect(Matrix.bsxfun("or", [0, 1], [0, 1]).getData()).toStrictEqual(Matrix.from([0, 1]).getData());
        expect(Matrix.bsxfun((a, b) => a + b, [0, 1], [0, 1]).getData()).toStrictEqual(Matrix.from([0, 2]).getData());
    });

});
