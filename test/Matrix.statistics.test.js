/* global describe test expect */
import Matrix from "../src/Matrix.js";
import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

describe("Testing Matrix.statistics.base", () => {

    test("Matrix.statistics.base: min / amin", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const amin = mat.amin(), amin0 = mat.amin(0), amin1 = mat.amin(1);
            expect(amin.getSize()).toStrictEqual([1, 1]);
            expect(amin0.getSize()).toStrictEqual([1, 5]);
            expect(amin1.getSize()).toStrictEqual([5, 1]);
            expect(amin.getData()).toStrictEqual(Uint32Array.from([5]));
            expect(amin0.getData()).toStrictEqual(Uint32Array.from([0, 5, 13, 17, 24]));
            expect(amin1.getData()).toStrictEqual(Uint32Array.from([5, 16, 17, 3, 24]));
        } {
            const min = mat.min(), min0 = mat.min(0), min1 = mat.min(1);
            expect(min.getSize()).toStrictEqual([1, 1]);
            expect(min0.getSize()).toStrictEqual([1, 5]);
            expect(min1.getSize()).toStrictEqual([5, 1]);
            expect(min.getData()).toStrictEqual(Float64Array.from([1]));
            expect(min0.getData()).toStrictEqual(Float64Array.from([2, 1, 3, 1, 1]));
            expect(min1.getData()).toStrictEqual(Float64Array.from([1, 4, 1, 2, 1]));
        } {
            const input = Matrix.complex(mat, mat.transpose());
            expect(input.min()).toEqualMatrix(input.get(Matrix.abs(input).amin()));
            expect(input.min(0)).toEqualMatrix(input.get(Matrix.abs(input).amin(0)));
            expect(input.min(1)).toEqualMatrix(input.get(Matrix.abs(input).amin(1)));
        }
    });

    test("Matrix.statistics.base: max / amax", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const amax = mat.amax(), amax0 = mat.amax(0), amax1 = mat.amax(1);
            expect(amax.getSize()).toStrictEqual([1, 1]);
            expect(amax0.getSize()).toStrictEqual([1, 5]);
            expect(amax1.getSize()).toStrictEqual([5, 1]);
            expect(amax.getData()).toStrictEqual(Uint32Array.from([10]));
            expect(amax0.getData()).toStrictEqual(Uint32Array.from([1, 6, 10, 15, 22]));
            expect(amax1.getData()).toStrictEqual(Uint32Array.from([10, 11, 12, 23, 14]));
        } {
            const max = mat.max(), max0 = mat.max(0), max1 = mat.max(1);
            expect(max.getSize()).toStrictEqual([1, 1]);
            expect(max0.getSize()).toStrictEqual([1, 5]);
            expect(max1.getSize()).toStrictEqual([5, 1]);
            expect(max.getData()).toStrictEqual(Float64Array.from([9]));
            expect(max0.getData()).toStrictEqual(Float64Array.from([6, 7, 9, 6, 8]));
            expect(max1.getData()).toStrictEqual(Float64Array.from([9, 8, 9, 7, 9]));
        } {
            const input = Matrix.complex(mat, mat.transpose());
            expect(input.max()).toEqualMatrix(input.get(Matrix.abs(input).amax()));
            expect(input.max(0)).toEqualMatrix(input.get(Matrix.abs(input).amax(0)));
            expect(input.max(1)).toEqualMatrix(input.get(Matrix.abs(input).amax(1)));
        }
    });

    test("Matrix.statistics.base: sum", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const sum = mat.sum(), sum0 = mat.sum(0), sum1 = mat.sum(1);
            expect(sum.getSize()).toStrictEqual([1, 1]);
            expect(sum0.getSize()).toStrictEqual([1, 5]);
            expect(sum1.getSize()).toStrictEqual([5, 1]);
            expect(sum.getData()).toStrictEqual(Float64Array.from([128]));
            expect(sum0.getData()).toStrictEqual(Float64Array.from([17, 22, 38, 21, 30]));
            expect(sum1.getData()).toStrictEqual(Float64Array.from([25, 32, 25, 22, 24]));
        } {
            const input = Matrix.complex(mat, mat);
            const sum = input.sum(), sum0 = input.sum(0), sum1 = input.sum(1);
            expect(Matrix.real(sum)).toEqualMatrix(Matrix.from([128]));
            expect(Matrix.real(sum0)).toEqualMatrix(Matrix.from([17, 22, 38, 21, 30], [1, 5]));
            expect(Matrix.real(sum1)).toEqualMatrix(Matrix.from([25, 32, 25, 22, 24]));
            expect(Matrix.imag(sum)).toEqualMatrix(Matrix.from([128]));
            expect(Matrix.imag(sum0)).toEqualMatrix(Matrix.from([17, 22, 38, 21, 30], [1, 5]));
            expect(Matrix.imag(sum1)).toEqualMatrix(Matrix.from([25, 32, 25, 22, 24]));
        }
    });

    test("Matrix.statistics.base: prod", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const prod = mat.prod(), prod0 = mat.prod(0), prod1 = mat.prod(1);
            expect(prod.getSize()).toStrictEqual([1, 1]);
            expect(prod0.getSize()).toStrictEqual([1, 5]);
            expect(prod1.getSize()).toStrictEqual([5, 1]);
            expect(prod.getData()).toStrictEqual(Float64Array.from([5807171543040000]));
            expect(prod0.getData()).toStrictEqual(Float64Array.from([288, 700, 17496, 600, 2744]));
            expect(prod1.getData()).toStrictEqual(Float64Array.from([756, 9408, 864, 1050, 900]));
        }
    });

    test("Matrix.statistics.base: mean", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const mean = mat.mean(), mean0 = mat.mean(0), mean1 = mat.mean(1);
            expect(mean.getSize()).toStrictEqual([1, 1]);
            expect(mean0.getSize()).toStrictEqual([1, 5]);
            expect(mean1.getSize()).toStrictEqual([5, 1]);
            expect(mean.getData()).toStrictEqual(Float64Array.from([5.12]));
            expect(mean0.getData()).toStrictEqual(Float64Array.from([3.4, 4.4, 7.6, 4.2, 6.0]));
            expect(mean1.getData()).toStrictEqual(Float64Array.from([5, 6.4, 5, 4.4, 4.8]));
        } {
            const input = Matrix.complex(mat, mat);
            const mean = input.mean(), mean0 = input.mean(0), mean1 = input.mean(1);
            expect(Matrix.real(mean)).toEqualMatrix(Matrix.from([5.12]));
            expect(Matrix.real(mean0)).toEqualMatrix(Matrix.from([3.4, 4.4, 7.6, 4.2, 6.0], [1, 5]));
            expect(Matrix.real(mean1)).toEqualMatrix(Matrix.from([5, 6.4, 5, 4.4, 4.8]));
            expect(Matrix.imag(mean)).toEqualMatrix(Matrix.from([5.12]));
            expect(Matrix.imag(mean0)).toEqualMatrix(Matrix.from([3.4, 4.4, 7.6, 4.2, 6.0], [1, 5]));
            expect(Matrix.imag(mean1)).toEqualMatrix(Matrix.from([5, 6.4, 5, 4.4, 4.8]));
        }
    });

    test("Matrix.statistics.base: variance", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const variance = mat.variance(), variance0 = mat.variance(0), variance1 = mat.variance(1);
            expect(variance.getSize()).toStrictEqual([1, 1]);
            expect(variance0.getSize()).toStrictEqual([1, 5]);
            expect(variance1.getSize()).toStrictEqual([5, 1]);
            expect(variance.getData()).toStrictEqual(Float64Array.from([6.693333333333334]));
            expect(variance0.getData()).toStrictEqual(Float64Array.from([2.8, 4.8, 6.8, 3.7, 8]));
            expect(variance1.getData()).toStrictEqual(Float64Array.from([11.5, 2.3, 11.5, 3.8, 8.2]));
        } {
            const variance = mat.variance(undefined, true), variance0 = mat.variance(0, 1), variance1 = mat.variance(1, 1);
            expect(variance.getSize()).toStrictEqual([1, 1]);
            expect(variance0.getSize()).toStrictEqual([1, 5]);
            expect(variance1.getSize()).toStrictEqual([5, 1]);
            expect(variance.getData()).toStrictEqual(Float64Array.from([6.4256]));
            expect(variance0.getData()).toStrictEqual(Float64Array.from([2.2399999999999998, 3.84, 5.4399999999999995, 2.96, 6.4]));
            expect(variance1.getData()).toStrictEqual(Float64Array.from([9.2, 1.84, 9.2, 3.04, 6.56]));
        }

    });

    test("Matrix.statistics.base: std", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const std = mat.std(), std0 = mat.std(0), std1 = mat.std(1);
            expect(std.getSize()).toStrictEqual([1, 1]);
            expect(std0.getSize()).toStrictEqual([1, 5]);
            expect(std1.getSize()).toStrictEqual([5, 1]);
            expect(std.getData()).toStrictEqual(Float64Array.from([6.693333333333334]).map(Math.sqrt));
            expect(std0.getData()).toStrictEqual(Float64Array.from([2.8, 4.8, 6.8, 3.7, 8]).map(Math.sqrt));
            expect(std1.getData()).toStrictEqual(Float64Array.from([11.5, 2.3, 11.5, 3.8, 8.2]).map(Math.sqrt));
        }
    });

    test("Matrix.statistics.base: cumsum", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const cumsum = mat.clone().cumsum();
            expect(cumsum.getSize()).toStrictEqual([5, 5]);
            expect(cumsum.getData()).toStrictEqual(Float64Array.from([2, 8, 11, 13, 17, 18, 25, 29, 34, 39, 48, 56, 65, 68, 77, 83, 87, 88, 93, 98, 105, 112, 120, 127, 128]));
        } {
            const cumsum0 = mat.clone().cumsum(0);
            expect(cumsum0.getSize()).toStrictEqual([5, 5]);

            expect(cumsum0.getData()).toStrictEqual(Float64Array.from([2, 8, 11, 13, 17, 1, 8, 12, 17, 22, 9, 17, 26, 29, 38, 6, 10, 11, 16, 21, 7, 14, 22, 29, 30]));
        } {
            const cumsum1 = mat.clone().cumsum(1);
            expect(cumsum1.getSize()).toStrictEqual([5, 5]);
            expect(cumsum1.getData()).toStrictEqual(Float64Array.from([2, 6, 3, 2, 4, 3, 13, 7, 7, 9, 12, 21, 16, 10, 18, 18, 25, 17, 15, 23, 25, 32, 25, 22, 24]));
        } {
            const cumsum = Matrix.complex(mat, mat).cumsum(1);
            expect(Matrix.real(cumsum)).toEqualMatrix(Matrix.cumsum(mat, 1));
            expect(Matrix.imag(cumsum)).toEqualMatrix(Matrix.cumsum(mat, 1));
        }
    });

    test("Matrix.statistics.base: cumprod", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const cumprod = mat.clone().cumprod();
            expect(cumprod.getSize()).toStrictEqual([5, 5]);
            expect(cumprod.getData()).toStrictEqual(Float64Array.from([2, 12, 36, 72, 288, 288, 2016, 8064, 40320, 201600, 1814400, 14515200, 130636800, 391910400, 3527193600, 21163161600, 84652646400, 84652646400, 423263232000, 2116316160000, 14814213120000, 103699491840000, 829595934720000, 5807171543040000, 5807171543040000]));
        } {
            const cumprod0 = mat.clone().cumprod(0);
            expect(cumprod0.getSize()).toStrictEqual([5, 5]);
            expect(cumprod0.getData()).toStrictEqual(Float64Array.from([2, 12, 36, 72, 288, 1, 7, 28, 140, 700, 9, 72, 648, 1944, 17496, 6, 24, 24, 120, 600, 7, 49, 392, 2744, 2744]));
        } {
            const cumprod1 = Matrix.cumprod(mat, 1);
            expect(cumprod1.getSize()).toStrictEqual([5, 5]);
            expect(cumprod1.getData()).toStrictEqual(Float64Array.from([2, 6, 3, 2, 4, 2, 42, 12, 10, 20, 18, 336, 108, 30, 180, 108, 1344, 108, 150, 900, 756, 9408, 864, 1050, 900]));
        }
    });

    test("Matrix.statistics.base: cov", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const cov = Matrix.cov(mat);
            const expected = Matrix.from([
                2.8, 2.8, 1.45, -0.6, -1.0,
                2.8, 4.8, -1.55, -1.1, -1.0,
                1.45, -1.55, 6.8, -1.15, -1.75,
                -0.6, -1.1, -1.15, 3.7, -2.0,
                -1.0, -1.0, -1.75, -2.0, 8.0
            ], [5, 5]);
            expect(cov).toEqualMatrix(expected, 1e-12);
        } {
            const cov = mat.clone().cov(mat.transpose());
            const expected = Matrix.from([
                -2, 0, -0.5, -3.75, -2,
                1.30, 0.3, 3.45, -1.85, -0.5,
                1.5, 0.5, 6.25, -4.25, -3.5,
                1.55, 3.05, -1.05, 0.65, -4.25,
                -0.65, -0.15, -0.35, -4.45, 6.75
            ], [5, 5]);
            expect(cov).toEqualMatrix(expected, 1e-12);
        }
    });

    test("Matrix.statistics.base: corrcoef", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const corrcoef = Matrix.corrcoef(mat);
            const expected = Matrix.from([
                1.000000000000000, 0.763762615825973, 0.332303205172727, -0.186410929800360, -0.211288563682129,
                0.763762615825973, 1.000000000000000, -0.271304266280427, -0.261018448831937, -0.161374306091976,
                0.332303205172727, -0.271304266280427, 1.000000000000000, -0.229267514063309, -0.237267688239155,
                -0.186410929800360, -0.261018448831937, -0.229267514063309, 1.000000000000000, -0.367607311046904,
                -0.211288563682129, -0.161374306091976, -0.237267688239155, -0.367607311046904, 1.000000000000000],
                [5, 5]
            );
            expect(corrcoef).toEqualMatrix(expected, 1e-12);
        } {
            // const corrcoef = mat.clone().corrcoef(mat.transpose());
            // const expected = Matrix.from([
            //     1.000000000000000, 0.763762615825973, 0.332303205172727, -0.186410929800360, -0.211288563682129,
            //     0.763762615825973, 1.000000000000000, -0.271304266280427, -0.261018448831937, -0.161374306091976,
            //     0.332303205172727, -0.271304266280427, 1.000000000000000, -0.229267514063309, -0.237267688239155,
            //     -0.186410929800360, -0.261018448831937, -0.229267514063309, 1.000000000000000, -0.367607311046904,
            //     -0.211288563682129, -0.161374306091976, -0.237267688239155, -0.367607311046904, 1.000000000000000],
            //     [5, 5]
            // );
            // expect(corrcoef).toEqualMatrix(expected);
        }
    });

    test("Matrix.statistics.base: sort", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const expectedData = [
                1, 1, 1, 2, 2,
                3, 3, 4, 4, 4,
                5, 5, 5, 5, 6,
                6, 7, 7, 7, 7,
                8, 8, 9, 9, 9
            ];
            let input;
            input = Matrix.sort(mat);
            expect(input).toEqualMatrix(Matrix.from(expectedData, [5, 5]));
            input = Matrix.sort(mat, undefined, "descend");
            expect(input).toEqualMatrix(Matrix.from(expectedData.reverse(), [5, 5]));
        } {
            const input = mat.clone();
            const expected = Matrix.from([
                1, 1, 1, 2, 2,
                3, 3, 4, 4, 4,
                5, 5, 5, 5, 6,
                6, 7, 7, 7, 7,
                8, 8, 9, 9, 9
            ], [5, 5]);
            input.sort();
            expect(input).toEqualMatrix(expected);
        } {
            const expected = Matrix.from([
                2, 2, 3, 4, 6,
                1, 4, 5, 5, 7,
                3, 8, 9, 9, 9,
                1, 4, 5, 5, 6,
                1, 7, 7, 7, 8
            ], [5, 5]);
            const input = Matrix.sort(mat, 0);
            expect(input).toEqualMatrix(expected);
        } {
            const expected = Matrix.from([
                9, 8, 9, 7, 9,
                7, 7, 8, 5, 5,
                6, 7, 4, 5, 5,
                2, 6, 3, 3, 4,
                1, 4, 1, 2, 1,
             ], [5, 5]);
            const input = Matrix.sort(mat, 1, "descend");
            expect(input).toEqualMatrix(expected);
        }
    });

    test("Matrix.statistics.base: asort", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 5]);
        {
            const expectedData = [
                6, 18, 25, 1, 4,
                3, 14, 5, 8, 17,
                9, 10, 19, 20, 2,
                16, 7, 21, 22, 24,
                12, 23, 11, 13, 15
            ].map(v => v - 1);
            const input = Matrix.asort(mat);
            expect(input).toEqualMatrix(Matrix.from(expectedData, [5, 5]).uint32());
        } {
            const expectedData = [
                11, 13, 15, 12, 23,
                 7, 21, 22, 24,  2,
                16,  9, 10, 19, 20,
                 5,  8, 17,  3, 14,
                 1,  4,  6, 18, 25
            ].map(v => v - 1);
            const input = mat.asort(undefined, "descend");
            expect(input).toEqualMatrix(Matrix.from(expectedData, [5, 5]).uint32());
        } {
            const expectedData = [
                1, 4, 2, 0, 3,
                6, 8, 9, 7, 5,
                10, 12, 14, 11, 13,
                15, 18, 19, 16, 17,
                22, 20, 21, 23, 24
            ];
            const input = Matrix.asort(mat, 0, "descend");
            expect(input).toEqualMatrix(Matrix.from(expectedData, [5, 5]).uint32());
        } {
            const expectedData = [
                 5, 16, 17,  3, 24,
                 0,  1,  2, 13,  4,
                15,  6,  7,  8,  9,
                20, 21, 22, 18, 19,
                10, 11, 12, 23, 14
            ];
            const input = Matrix.asort(mat, 1, "ascend");
            // expect(mat.get(input)).toEqualMatrix(Matrix.sort(mat, 1, "ascend"));
            expect(input).toEqualMatrix(Matrix.from(expectedData, [5, 5]).uint32());
        }
    });

    test("Matrix.statistics.base: median", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
        ], [5, 4]);
        {
            const input = Matrix.median(mat);
            expect(input).toEqualMatrix(Matrix.from([5], [1, 1]));
        } {
            const input = Matrix.median(mat, 0);
            expect(input).toEqualMatrix(Matrix.from([3, 5, 9, 5], [1, 4]));
        } {
            const input = Matrix.median(mat, 1);
            expect(input).toEqualMatrix(Matrix.from([4, 6.5, 3.5, 4, 5], [5, 1]));
        }
    });

    test("Matrix.statistics.base: unique / union", () => {
        const mat1 = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5
        ], [5, 2]);
        const mat2 = Matrix.from([
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ], [5, 3]);
        const unique1 = Matrix.unique(mat1);
        expect(unique1).toEqualMatrix(Matrix.from([1, 2, 3, 4, 5, 6, 7]));
        const unique2 = Matrix.unique(mat2);
        expect(unique2).toEqualMatrix(Matrix.from([1, 3, 4, 5, 6, 7, 8, 9]));
        const union = Matrix.union(unique1, unique2);
        expect(union).toEqualMatrix(Matrix.from([1, 2, 3, 4, 5, 6, 7, 8, 9]));
    });

    test("Matrix.statistics.base: accumarray", () => {
        const mat = Matrix.from([
            2, 6, 3, 2, 4,
            1, 7, 4, 5, 5,
            9, 8, 9, 3, 9,
            6, 4, 1, 5, 5,
            7, 7, 8, 7, 1
        ]);
        const output1 = Matrix.accumarray(mat, 1);
        expect(output1).toEqualMatrix(Matrix.from([0, 3, 2, 2, 3, 4, 2, 4, 2, 3]));
        const output2 = Matrix.accumarray(mat, Matrix.colon(1, 25));
        expect(output2).toEqualMatrix(Matrix.from([0, 49, 5, 17, 30, 58, 18, 74, 35, 39]));
        const output3 = Matrix.accumarray(mat, Matrix.from(1), [11, 1]);
        expect(output3).toEqualMatrix(Matrix.from([0, 3, 2, 2, 3, 4, 2, 4, 2, 3, 0]));
    });

    test("Matrix.statistics.base: pca", () => {
        // Test data from https://octave.sourceforge.io/statistics/function/pcacov.html
        const X = Matrix.from([
            [7, 26,  6, 60],
            [1, 29, 15, 52],
            [11, 56,  8, 20],
            [11, 31,  8, 47],
            [7, 52,  6, 33],
            [11, 55,  9, 22],
            [3, 71, 17,  6],
            [1, 31, 22, 44],
            [2, 54, 18, 22],
            [21, 47,  4, 26],
            [1, 40, 23, 34],
            [11, 66,  9, 12],
            [10, 68,  8, 12]
        ]).transpose();
        const expectedCoeff = Matrix.from([
            0.0677999856954739,     0.646018286568728,    -0.567314540990512,     0.506179559977705,
             0.678516235418647,    0.0199933404840989,     0.543969276583817,     0.493268092159297,
            -0.029020832106229,    -0.755309622491133,    -0.403553469172668,     0.515567418476836,
            -0.730873909451461,     0.108480477171676,     0.468397518388289,     0.484416225289198
        ], [4, 4]).transpose();
        const expectedLatent = Matrix.from([
            517.796878073906,
             67.4964360487231,
             12.405430048081,
              0.237153265187813,
        ]);
        const [coeff, scores, latent] = Matrix.pca(X);
        const reconstruct = Matrix.bsxfun("plus", coeff.mtimes(scores.transpose()), X.mean(0).transpose()).transpose();
        expect(reconstruct).toEqualMatrix(X, 1e-14);
        expect(coeff.abs()).toEqualMatrix(expectedCoeff.abs(), 1e-14);
        expect(latent).toEqualMatrix(expectedLatent, 1e-14);
    });

});
