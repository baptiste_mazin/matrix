/* global describe test expect */
import Matrix, {} from "../src/Matrix.js";
import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

describe("Testing Matrix.elementaryMath.special_functions", () => {

    test(`Matrix.elementaryMath.special_functions: erf`, () => {
        const expected = Matrix.from([
            -0.999977909503001, -0.999899378077880, -0.999593047982555, -0.998537283413319, -0.995322265018953,
            -0.986671671219182, -0.966105146475311, -0.922900128256458, -0.842700792949715, -0.711155633653515,
            -0.520499877813047, -0.276326390168237, 0.000000000000000, 0.276326390168237, 0.520499877813047,
            0.711155633653515, 0.842700792949715, 0.922900128256458, 0.966105146475311, 0.986671671219182,
            0.995322265018953, 0.998537283413319, 0.999593047982555, 0.999899378077880, 0.999977909503001
        ]);
        const a = Matrix.linspace(-3, 3, 25);
        expect(Matrix.erf(a)).toEqualMatrix(expected, 1e-12);
        expect(a.erf()).toEqualMatrix(expected, 1e-12);
    });

    test(`Matrix.elementaryMath.special_functions: erfc`, () => {
        const expected = Matrix.from([
            1.99999999999846e+00, 1.99999999990937e+00, 1.99999999619734e+00, 1.99999988627274e+00, 1.99999757153253e+00,
            1.99996289261123e+00, 1.99959304798255e+00, 1.99678377068987e+00, 1.98157787454590e+00, 1.92290012825646e+00,
            1.76140717068356e+00, 1.44431020971720e+00, 1.00000000000000e+00, 5.55689790282794e-01, 2.38592829316435e-01,
            7.70998717435418e-02, 1.84221254540990e-02, 3.21622931012744e-03, 4.06952017444959e-04, 3.71073887694882e-05,
            2.42846747297583e-06, 1.13727256569797e-07, 3.80265988725934e-09, 9.06274279083390e-11, 1.53745979442804e-12,
        ]);
        const a = Matrix.linspace(-5, 5, 25);
        expect(Matrix.erfc(a)).toEqualMatrix(expected, 1e-12);
        expect(a.erfc()).toEqualMatrix(expected, 1e-12);
    });

    test(`Matrix.elementaryMath.special_functions: erfcx`, () => {
        const expected = Matrix.from([
            Infinity, 1.35498994318491e-05, 6.77250993036231e-06, 4.51446470582578e-06, 3.38564534802779e-06,
            2.70841876073675e-06, 2.25696146070443e-06, 1.93450522896190e-06, 1.69267031085430e-06, 1.50458078516126e-06,
            1.35411187320939e-06, 1.23100273601645e-06, 1.12841301948384e-06, 1.04160721042327e-06, 9.67202868999068e-07,
            9.02719582627742e-07, 8.46297069776729e-07, 7.96512780753919e-07, 7.52260300666013e-07, 7.12666152897804e-07,
            6.77031562446195e-07, 6.44790858872124e-07, 6.15481224273252e-07, 5.88720333336903e-07, 5.64189583547474e-07
        ]);
        const a = Matrix.linspace(-30, 1e6, 25);
        expect(Matrix.erfcx(a)).toEqualMatrix(expected, 1e-12);
        expect(a.erfcx()).toEqualMatrix(expected, 1e-12);
    });

    test(`Matrix.elementaryMath.special_functions: gamma`, () => {
        const expected = Matrix.from([
            2.36327180120735e+00, Infinity, -3.54490770181103e+00, Infinity, 1.77245385090552e+00,
            1.00000000000000e+00, 8.86226925452758e-01, 1.00000000000000e+00, 1.32934038817914e+00, 2.00000000000000e+00,
            3.32335097044784e+00, 6.00000000000000e+00, 1.16317283965674e+01, 2.40000000000000e+01, 5.23427777845535e+01,
            1.20000000000000e+02, 2.87885277815044e+02, 7.20000000000000e+02, 1.87125430579779e+03, 5.04000000000000e+03,
            1.40344072934834e+04, 4.03200000000000e+04, 1.19292461994609e+05, 3.62880000000000e+05, 1.13327838894879e+06,
            3.62880000000000e+06, 1.18994230839622e+07, 3.99168000000000e+07, 1.36843365465566e+08, 4.79001600000000e+08,
            1.71054206831957e+09, 6.22702080000000e+09, 2.30923179223142e+10, 8.71782912000000e+10, 3.34838609873557e+11,
            1.30767436800000e+12, 5.18999845304013e+12, 2.09227898880000e+13, 8.56349744751621e+13, 3.55687428096000e+14
        ]).plus(Number.EPSILON);
        const a = Matrix.linspace(-1.5, 18, 40);
        expect(Matrix.gamma(a)).toEqualMatrix(expected, 1e-12);
        expect(a.gamma()).toEqualMatrix(expected, 1e-12);
    });

    test(`Matrix.elementaryMath.special_functions: betainc`, () => {
        const x = 0.5, X = Matrix.linspace(0, 1, 8);
        const z = 1,   Z = Matrix.logspace(-1, 1, 8);
        const w = 3,   W = Matrix.colon(0.5, 0.5, 4);
        {
            expect(Matrix.betainc(x, z, w, "lower")).toEqualMatrix(Matrix.from(0.875));
        } {
            const expected = Matrix.from([
                0.292893218813452, 0.5, 0.646446609406726, 0.75, 0.823223304703363, 0.875, 0.911611652351682, 0.9375
            ]);
            expect(Matrix.betainc(x, z, W, "lower")).toEqualMatrix(expected, 1e-12);
        } {
            const expected = Matrix.from([
                0.992513844747279, 0.984372269350295, 0.965645052098717, 0.919677949999234, 0.805296258003796, 0.557007525930523, 0.209457306353604, 0.019287109375
            ]);
            expect(Matrix.betainc(x, Z, w)).toEqualMatrix(expected, 1e-12);
        } {
            const expected = Matrix.from([
                0.84800171239977, 0.874742458817522, 0.864763287180198, 0.825736995575967, 0.736959185175805, 0.557007525930523, 0.27403069309419, 0.046142578125
            ]);
            expect(Matrix.betainc(x, Z, W)).toEqualMatrix(expected, 1e-12);
        } {
            const expected = Matrix.from([
                0, 0.370262390670554, 0.635568513119533, 0.813411078717201, 0.921282798833819, 0.97667638483965, 0.997084548104956, 1
            ]);
            expect(Matrix.betainc(X, z, w)).toEqualMatrix(expected, 1e-12);
        } {
            const expected = Matrix.from([
                0, 0.142857142857143, 0.396318389479631, 0.673469387755102, 0.879757489053637, 0.97667638483965, 0.998898062760906, 1
            ]);
            expect(Matrix.betainc(X, z, W)).toEqualMatrix(expected, 1e-12);
        } {
            const expected = Matrix.from([
                0, 0.858585725899194, 0.875641046016352, 0.876779953755823, 0.87326870277605, 0.879810089677594, 0.93001652286605, 1
            ]);
            expect(Matrix.betainc(X, Z, w)).toEqualMatrix(expected, 1e-12);
        } {
            const expected = Matrix.from([
                0, 0.68681079618225, 0.727239974278405, 0.766965825881283, 0.815467660323429, 0.879810089677594, 0.962315261492709, 1
            ]);
            expect(Matrix.betainc(X, Z, W)).toEqualMatrix(expected, 1e-12);
        } {
            const expected = Matrix.from([
                0, 0.68681079618225, 0.727239974278405, 0.766965825881283, 0.815467660323429, 0.879810089677594, 0.962315261492709, 1
            ]);
            expect(Matrix.betainc(X, Z, W)).toEqualMatrix(expected, 1e-12);
        }

    });

    test(`Matrix.elementaryMath.special_functions: nchoosek`, () => {
        expect(Matrix.nchoosek(5, 4)).toEqualMatrix(5);
    });

});
