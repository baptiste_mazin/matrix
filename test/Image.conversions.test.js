/* global describe test expect */
import Matrix, {} from "../src/Matrix.js";
import {
    toEqualMatrix
} from "./matchers.js";
expect.extend({
    toEqualMatrix
});

describe("Testing Image.conversions", () => {

    test("Image.conversions: convertImage", () => {});

    test("Image.conversions: im2double / im2single", () => {
            const im = Matrix.colon(0, 255).uint8();
            expect(im.type()).toBe("uint8");
        {
            const test = im.im2double();
            expect(test.type()).toBe("double");
            expect(test.min().asScalar()).toBe(0);
            expect(test.max().asScalar()).toBe(1);
        } {
            const test = im.im2single();
            expect(test.type()).toBe("single");
            expect(test.min().asScalar()).toBe(0);
            expect(test.max().asScalar()).toBe(1);
        } {
            const im = Matrix.from(new Uint8Array([0, 1, 254, 255]));
            expect(im.im2double()).toEqualMatrix([0, 1, 254, 255].map(v => v / 255));
        } { // uint16 to double
            const im = Matrix.from(new Uint16Array([0, 1, 65534, 65535]));
            expect(im.im2double()).toEqualMatrix([0, 1 / 65535, 65534 / 65534, 1]);
        }
    });

    test("Image.conversions: im2uint8", () => {
        { // Double to uint8
            const nbit = 8;
            const input = Matrix.from([
                0,
                0 + 1 / (2 * (2 ** nbit - 1)) - Number.EPSILON,
                0 + 1 / (2 * (2 ** nbit - 1)),
                0 + 1 / (2 * (2 ** nbit - 1)) + Number.EPSILON,
                1 - 1 / (2 * (2 ** nbit - 1)) - Number.EPSILON,
                1 - 1 / (2 * (2 ** nbit - 1)),
                1 - 1 / (2 * (2 ** nbit - 1)) + Number.EPSILON,
                1
            ]);
            const expected = Matrix.from([0, 0, 1, 1, 2 ** nbit - 2, 2 ** nbit - 1, 2 ** nbit - 1, 2 ** nbit - 1]).uint8();
            const test = input.im2uint8();
            expect(test).toEqualMatrix(expected);
        } { // uint16 to uint8
            const input = Matrix.colon(0, 64, 0xffff - 1).uint16();
            const test = input.im2uint8();
            expect(test).toEqualMatrix(input.arrayfun(v => v >> 8).uint8());
        } { // uint32 to uint8
            const input = Matrix.colon(0, 1024, 0xffffffff - 1).uint32();
            const test = input.im2uint8();
            expect(test).toEqualMatrix(input.arrayfun(v => v >> 24).uint8());
        }
    });

    // test("Image.conversions: im2uint8c", () => {
    //     {
    //         // const input = Matrix.from([
    //         //     0,
    //         //     0 + 1 / 510 - Number.EPSILON,
    //         //     0 + 1 / 510,
    //         //     0 + 1 / 510 + Number.EPSILON,
    //         //     1 - 1 / 510 - Number.EPSILON,
    //         //     1 - 1 / 510,
    //         //     1 - 1 / 510 + Number.EPSILON,
    //         //     1,
    //         //     1 + 1 / 510 - Number.EPSILON,
    //         // ]);
    //         // const expected = Matrix.from([0, 0, 1, 1, 254, 255, 255, 255, 255]).uint8c();
    //         // const test = input.im2uint8c();
    //         // expect(test).toEqualMatrix(expected);
    //     }
    // });
    //
        test("Image.conversions: im2uint16", () => {
            { // Double to uint32
                const nbit = 16;
                const input = Matrix.from([
                    0,
                    0 + 1 / (2 * (2 ** nbit - 1)) - Number.EPSILON,
                    0 + 1 / (2 * (2 ** nbit - 1)),
                    0 + 1 / (2 * (2 ** nbit - 1)) + Number.EPSILON,
                    1 - 1 / (2 * (2 ** nbit - 1)) - Number.EPSILON,
                    1 - 1 / (2 * (2 ** nbit - 1)),
                    1 - 1 / (2 * (2 ** nbit - 1)) + Number.EPSILON,
                    1
                ]);
                const expected = Matrix.from([0, 0, 1, 1, 2 ** nbit - 2, 2 ** nbit - 1, 2 ** nbit - 1, 2 ** nbit - 1]).uint16();
                const test = input.im2uint16();
                expect(test).toEqualMatrix(expected);
            } { // uint8 to uint16
                const input = Matrix.colon(0, 0xff - 1).uint8();
                const test = input.im2uint16();
                expect(test).toEqualMatrix(input.uint16().arrayfun(v => v * 257));
            } { // uint32 to uint16
                const input = Matrix.colon(0, 0xff - 1).uint8();
                const test = input.im2uint16();
                expect(test).toEqualMatrix(input.uint16().arrayfun(v => v * 257));
            }
        });

        test("Image.conversions: im2uint32", () => {
            { // Double to uint32
                const nbit = 32;
                const input = Matrix.from([
                    0,
                    0 + 1 / (2 * (2 ** nbit - 1)) - Number.EPSILON,
                    0 + 1 / (2 * (2 ** nbit - 1)),
                    0 + 1 / (2 * (2 ** nbit - 1)) + Number.EPSILON,
                    1 - 1 / (2 * (2 ** nbit - 1)) - Number.EPSILON,
                    1 - 1 / (2 * (2 ** nbit - 1)),
                    1 - 1 / (2 * (2 ** nbit - 1)) + Number.EPSILON,
                    1
                ]);
                const expected = Matrix.from([0, 0, 1, 1, 2 ** nbit - 2, 2 ** nbit - 1, 2 ** nbit - 1, 2 ** nbit - 1]).uint32();
                const test = input.im2uint32();
                expect(test).toEqualMatrix(expected);
            }
        });
    //
    // test("Image.conversions: getImageData", () => {});
    //
    // test("Image.conversions: toImage", () => {});
    //
    // test("Image.conversions: toBlob", () => {});
    //
    // test("Image.conversions: toFile", () => {});

});
