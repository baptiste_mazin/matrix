/* global describe test expect */
import Matrix, {} from "../src/Matrix.js";
import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

import {solveLU} from "../src/Linalg/Linalg.class.js";

describe("Testing Matrix.Linalg.class", () => {
    const sz = 10;
    const C = Matrix.complex(Matrix.randi(9, sz), Matrix.randi(9, sz));
    const C_real = Matrix.real(C);
    const CCt = C.mtimes(C.ctranspose());
    const CCt_real = C_real.mtimes(C_real.transpose());

    test("Matrix.Linalg: CHOLESKY Upper Decomposition", () => {
        const func = A => {
            const G = A.chol('upper');
            const residual = G.ctranspose().mtimes(G).minus(A).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: CHOLESKY Lower Decomposition", () => {
        const func = A => {
            const G = A.chol('lower');
            const residual = G.mtimes(G.ctranspose()).minus(A).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: LU Decomposition", () => {
        const func = A => {
            const [L, U] = A.lu();
            const residual = L.mtimes(U).minus(A).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: LUP Decomposition", () => {
        const func = A => {
            const [L, U, P] = A.lup();
            const residual = L.mtimes(U).minus(P.mtimes(A)).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: QR Decomposition", () => {
        const func = A => {
            const [Q, R] = A.qr();
            const residual = Q.mtimes(R).minus(A).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: QRP Decomposition", () => {
        const func = A => {
            const [Q, R, P] = A.qrp();
            const residual = Q.mtimes(R).minus(A.mtimes(P)).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: LU Inversion", () => {
        const func = A => {
            const eye = Matrix.eye(A.getSize(0));
            const iA = solveLU(A, eye);
            const residual = A.mtimes(iA).minus(eye).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: QR Inversion", () => {
        const func = A => {
            const eye = Matrix.eye(A.getSize(0));
            const iA = A.mldivide(eye);
            const residual = A.mtimes(iA).minus(eye).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: BIDIAG Decomposition", () => {
        const func = A => {
            const [U, B, V] = A.bidiag();
            const residual = U.mtimes(B).mtimes(V).minus(A).reshape().norm();
            return residual;
        };
        expect(func(CCt)).toBeLessThan(1e-6);
        expect(func(CCt_real)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: SVD Decomposition", () => {
        const func = A => {
            const [U, S, V] = A.svd();
            const residual = U.mtimes(S).mtimes(V.transpose()).minus(A).reshape().norm();
            return residual;
        };
        const A = Matrix.rand(sz);
        expect(func(A)).toBeLessThan(1e-6);
    });

    test("Matrix.Linalg: polyfit / polyval", () => {
        for (let np = 1; np < 10; np++) {
            for (let i = 0; i < 10; i++) {
                const N = 400 + Math.floor(Math.random() * 200);
                const p = Matrix.randi([-100, 100], np, 1);
                const x = Matrix.colon(Math.floor(-N / 2), Math.ceil(N / 2)),
                    y = Matrix.polyval(p, x);
                const pp = Matrix.polyfit(x, y, p.numel() - 1);
                const errMean = p['-'](pp).abs().mean().asScalar();
                expect(errMean).toBeLessThan(1e-7);
            }
        }
    });

});
