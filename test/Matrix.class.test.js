/* global describe test expect */
import Matrix, {MatrixView} from "../src/Matrix.js";

describe("Testing Matrix.class", () => {

    test("Matrix.class: Constructor", () => {
        const A = new Matrix([3, 3]);
        expect(A.getSize()).toEqual([3, 3]);
        expect(A.getView()).toBeInstanceOf(MatrixView);
        // Chack that a copy is returned and not the internal view.
        expect(A.getView()).not.toBe(A.getView());

        const B = new Matrix([2, 2, 2], [
            1, 1, 1, 1, 1, 1, 1, 2,
            2, 2, 2, 2, 2, 2, 2, 6
        ], true);
        expect(B.getSize()).toEqual([2, 2, 2]);
        expect(B.getRealData()).toStrictEqual(Float64Array.from([1, 1, 1, 1, 1, 1, 1, 2]));
        expect(B.getImagData()).toStrictEqual(Float64Array.from([2, 2, 2, 2, 2, 2, 2, 6]));
        expect(B.isreal()).toBe(false);
        expect(B.value([1, 1, 1])).toEqual([2, 6]);
        B.value([1, 1, 1], [3, 4]);
        expect(B.value([1, 1, 1])).toEqual([3, 4]);
        B.value(7, [5, 8]);
        expect(B.value(7)).toEqual([5, 8]);
        B.reshape();
        expect(B.getSize()).toEqual([8, 1]);
        B.reshape([4, 2]);
        expect(B.getSize()).toEqual([4, 2]);
        const C = new Matrix([4, 2], Uint8Array);
        expect(C.getData()).toBeInstanceOf(Uint8Array);
        expect(C.getDataType()).toEqual("Uint8Array");

        const D = new Matrix([4, 2], "single");
        expect(D.getData()).toBeInstanceOf(Float32Array);
        expect(D.getDataType()).toEqual("Float32Array");

        const E = new Matrix([4, 2], [true, false, true, false, true, false, true, false], false, true);
        // expect(E.getData()).toBeInstanceOf(Uint8ClampedArray);
        expect(E.getDataType()).toEqual("logical");

        const F = new Matrix([2, 2, 2], [
            1, 1, 1, 1, 3, 1, 1, 1,
            0, 0, 0, 0, 0, 0, 0, 0
        ], true);
        expect(F.hasImagPart()).toBe(true);
        expect(F.isreal()).toBe(true);
        expect(F.value([0, 0, 1])).toBe(3);
        F.value([1, 1, 1], 0);
        expect(F.value([1, 1, 1])).toEqual(0);
        F.toComplex([1, 1, 1, 1, 1, 1, 1, 1]);
        expect(F.isreal()).toBe(false);
        expect(F.getImagData()).toStrictEqual(Float64Array.from([1, 1, 1, 1, 1, 1, 1, 1]));

        const G = new Matrix();
        expect(G.isempty()).toBe(true);

        const H = new Matrix(1, [3]);
        expect(H.getSize()).toEqual([1, 1]);
        expect(H.isscalar()).toBe(true);
        expect(H.asScalar()).toBe(3);

        const real = Float64Array.from([0, 1, 2, 3, 4, 5, 6, 7]),
              imag = real.slice().reverse();
        const I = new Matrix([2, 2, 2], real);
        const J = new Matrix([2, 2, 2], imag);
        I.toComplex(J);
        expect(I.isreal()).toBe(false);
        expect(I.getRealData()).not.toBe(real);
        expect(I.getImagData()).not.toBe(imag);
        const ICopy = I.clone();
        expect(ICopy.getImagData()).not.toBe(I.getImagData());
        expect(ICopy.getRealData()).not.toBe(I.getRealData());


        const K = new Matrix([3, 1], [3, 3, 3]);
        const L = new Matrix(K);
        expect(L.getSize()).toEqual([3, 3, 3]);

        const M = new Matrix([3, 1], "Array");
        expect(M.getData()).toBeInstanceOf(Float64Array);

        const N = new Matrix([3, 1], "logical");
        expect(N.getDataType()).toEqual("logical");

        const O = new Matrix([3, 1], "double", true);
        expect(O.getRealData().length).toEqual(3);
        expect(O.getImagData().length).toEqual(3);

    });

    test("Matrix.class: real / imag", () => {
        const real = Matrix.randi(255, 5), imag = Matrix.randi(255, 5);
        const cplx = Matrix.complex(real, imag);
        expect(real.clone().real().getData()).toStrictEqual(real.getData());
        expect(cplx.clone().real().getData()).toStrictEqual(real.getData());
        expect(real.imag().getData()).toStrictEqual(new Float64Array(25));
        expect(cplx.imag().getData()).toStrictEqual(imag.getData());
    });

});
