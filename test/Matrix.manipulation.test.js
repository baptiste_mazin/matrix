/* global describe test expect */
import Matrix, {MatrixView} from "../src/Matrix.js";

describe("Testing Matrix.manipulation", () => {

    test("Matrix.manipulation: extractViewFrom", () => {
        const data = [
             0,  1,  2,  3,  4,
             5,  6,  7,  8,  9,
            10, 11, 12, 13, 14,
            15, 16, 17, 18, 19,
            20, 21, 22, 23, 24
        ];
        const expectedData = [6, 8, 16, 18];
        const view = new MatrixView([5, 5]);
        view.select([[1, 3]], [[1, 3]]);

        const input1 = new Matrix([5, 5], data);
        const input2 = Matrix.complex(input1, new Matrix([5, 5], data.slice().reverse()));

        const mat1 = input1.extractViewFrom(view, new Matrix([2, 2]));
        expect(mat1.getData()).toStrictEqual(Float64Array.from(expectedData));

        const mat2 = input1.extractViewFrom(view, new Matrix([2, 2], "double", true));
        expect(mat2.getData()).toStrictEqual(Float64Array.from([...expectedData, ...new Array(4).fill(0)]));

        const mat3 = input2.extractViewFrom(view);
        expect(mat3.getData()).toStrictEqual(Float64Array.from([...expectedData, ...expectedData.slice().reverse()]));

        const mat4 = input2.extractViewFrom(view, new Matrix([2, 2], "double"));
        expect(mat4.getData()).toStrictEqual(Float64Array.from([...expectedData, ...expectedData.slice().reverse()]));
    });

    test("Matrix.manipulation: extractViewTo", () => {
        const data = [6, 8, 16, 18];
        const expectedData = [
             0,  0,  0,  0,  0,
             0,  6,  0,  8,  0,
             0,  0,  0,  0,  0,
             0, 16,  0, 18,  0,
             0,  0,  0,  0,  0
        ];
        const view = new MatrixView([5, 5]);
        view.select([[1, 3]], [[1, 3]]);

        const input1 = new Matrix([2, 2], data);
        const input2 = Matrix.complex(input1, new Matrix([2, 2], data.slice().reverse()));

        const mat1 = input1.extractViewTo(view, new Matrix([5, 5]));
        expect(mat1.getData()).toStrictEqual(Float64Array.from(expectedData));

        const mat2 = input1.extractViewTo(view, new Matrix([5, 5], "double", true));
        expect(mat2.getData()).toStrictEqual(Float64Array.from([...expectedData]));

        const mat3 = input2.extractViewTo(view);
        expect(mat3.getData()).toStrictEqual(Float64Array.from([...expectedData, ...expectedData.slice().reverse()]));

        const mat4 = input2.extractViewTo(view, new Matrix([5, 5], "double"));
        expect(mat4.getData()).toStrictEqual(Float64Array.from([...expectedData, ...expectedData.slice().reverse()]));
    });

    test("Matrix.manipulation: get", () => {
        const input = Matrix.colon(0, 24).reshape(5, 5);

        expect(input.get().getData()).toStrictEqual(input.getData());
        expect(input.get().getData()).not.toBe(input.getData());

        const sel1 = Matrix.from([
            true, false, true, false, true,
            false, true, false, true, false,
            true, false, true, false, true,
            false, true, false, true, false,
            true, false, true, false, true
        ], [5, 5]);
        expect(input.get(sel1).getData()).toStrictEqual(Matrix.colon(0, 2, 25).getData());

        const sel2 = Matrix.colon(0, 2, 23);
        expect(input.get(sel2).getSize()).toStrictEqual([12, 1]);
        expect(input.get(sel2).getData()).toStrictEqual(sel2.getData());
        expect(input.get(sel2.reshape(2, 3, 2)).getSize()).toStrictEqual([2, 3, 2]);
        expect(input.get(sel2.reshape(2, 3, 2)).getData()).toStrictEqual(sel2.getData());

        const sel3 = [true, true, false, false, false], sel4 = [[3, 4]];
        expect(input.get(Matrix.from(sel3), Matrix.from(sel4)).getData()).toStrictEqual(Float64Array.from([15, 16, 20, 21]));
        expect(input.get(sel3, sel4).getData()).toStrictEqual(Float64Array.from([15, 16, 20, 21]));
        expect(input.get([0, 2, -1], 2).getData()).toStrictEqual(Float64Array.from([10, 12, 14]));
    });

    test("Matrix.manipulation: set", () => {
        const input = Matrix.colon(0, 24).reshape(5, 5);
        expect(input.get().getData()).toStrictEqual(input.getData());
        expect(input.get().getData()).not.toBe(input.getData());

        const sel1 = Matrix.from([
            true, false, true, false, true,
            false, true, false, true, false,
            true, false, true, false, true,
            false, true, false, true, false,
            true, false, true, false, true
        ], [5, 5]);
        const output1 = [
            0,  1,  0,  3,  0,
            5,  0,  7,  0,  9,
            0, 11,  0, 13,  0,
           15,  0, 17,  0, 19,
            0, 21,  0, 23,  0
        ];
        expect(Matrix.set(input, sel1, new Array(13).fill(0)).getData()).toStrictEqual(Float64Array.from(output1));
        expect(Matrix.set(input, sel1, new Array(13).fill(0)).getSize()).toStrictEqual([5, 5]);

        const sel2 = Matrix.colon(0, 2, 24);
        expect(Matrix.set(input, sel2, new Array(13).fill(0)).getData()).toStrictEqual(Float64Array.from(output1));
        expect(Matrix.set(input, sel2, new Array(13).fill(0)).getSize()).toStrictEqual([5, 5]);
        // expect(input.set(sel2.reshape(2, 3, 2)).getSize()).toStrictEqual([2, 3, 2]);

        const expectedOutput2 = [
             0,  1,  2,  3,  4,
             5,  6,  7,  8,  9,
            10, 11, 12, 13, 14,
             0,  0, 17, 18, 19,
             0,  0, 22, 23, 24
        ];
        const sel3 = [true, true, false, false, false], sel4 = [[3, 4]];
        const output2 = Matrix.set(input, Matrix.from(sel3), Matrix.from(sel4), Matrix.from([0, 0, 0, 0], [2, 2]));
        expect(output2.getData()).toStrictEqual(Float64Array.from(expectedOutput2));
        expect(Matrix.set(input, sel3, sel4, Matrix.from([0, 0, 0, 0], [2, 2])).getData()).toStrictEqual(Float64Array.from(expectedOutput2));
        const expectedOutput3 = [
             0,  1,  2,  3,  4,
             5,  6,  7,  8,  9,
             0, 11,  0, 13,  0,
            15, 16, 17, 18, 19,
            20, 21, 22, 23, 24
        ];
        expect(Matrix.set(input, [0, 2, -1], 2, [0, 0, 0]).getData()).toStrictEqual(Float64Array.from(expectedOutput3));
        { // Set real matrix to complex matrix
            const cplxMatrix = Matrix.complex([0, 1, 2, 3], [4, 5, 6, 7]);
            const realMatrix = Matrix.colon(10, 13);
            cplxMatrix.set(realMatrix);
            expect(cplxMatrix.isreal()).toBe(true);
        }
        {
            const mat = Matrix.colon(0, 3).reshape([2, 2]);
            const setData = Matrix.from([false, false, false, false], [2, 2]);
            mat.set(setData, 0); // scalar set
            expect(mat.getData()).toStrictEqual(Float64Array.from([0, 1, 2, 3]));
        } {
            const mat = Matrix.colon(0, 3).reshape([2, 2]);
            const setData = Matrix.from([false, false, false, false], [2, 2]);
            mat.set(setData, 0); // empty selection
            expect(mat.getData()).toStrictEqual(Float64Array.from([0, 1, 2, 3]));
        } {
            const mat = Matrix.colon(0, 3).reshape([2, 2]);
            mat.set(9); // global set
            expect(mat.getData()).toStrictEqual(Float64Array.from([9, 9, 9, 9]));
        }

    });

    test("Matrix.manipulation: reshape", () => {
        const input = Matrix.colon(0, 24).reshape(5, 5);
        expect(Matrix.reshape(input, 25).getSize()).toStrictEqual([25, 1]);
        // Check that it is really done on a copy.
        expect(input.getSize()).toStrictEqual([5, 5]);
    });

    test("Matrix.manipulation: transpose", () => {
        const real = Matrix.from([0, 1, 2, 3, 4, 5], [3, 2]);
        const imag = Matrix.from([6, 7, 8, 9, 0, 1], [3, 2]);
        const cplx = Matrix.complex(real, imag);
        {
            const output = real.transpose();
            expect(output.getSize()).toStrictEqual([2, 3]);
            expect(output.getData()).toStrictEqual(Float64Array.from([0, 3, 1, 4, 2, 5]));
            expect(real.getSize()).toStrictEqual([3, 2]);
            expect(real.getData()).toStrictEqual(Float64Array.from([0, 1, 2, 3, 4, 5]));
        } {
            const output = Matrix.transpose(cplx);
            expect(output.getSize()).toStrictEqual([2, 3]);
            expect(output.getRealData()).toStrictEqual(Float64Array.from([0, 3, 1, 4, 2, 5]));
            expect(output.getImagData()).toStrictEqual(Float64Array.from([6, 9, 7, 0, 8, 1]));
        }
    });

    test("Matrix.manipulation: permute / ipermute", () => {
        {
            const m = Matrix.colon(0, 7).reshape([2, 2, 2]);
            const m2 = m.permute([2, 1, 0]);
            expect(m2.getData()).toEqual(Float64Array.from([0, 4, 2, 6, 1, 5, 3, 7]));

            const m3 = m2.ipermute([2, 1, 0]);
            expect(m3.getData()).toStrictEqual(m.getData());
        } {
            const m = Matrix.colon(0, 7).reshape([2, 2, 2]);
            const m2 = Matrix.permute(m, [2, 1, 0]);
            expect(m2.getData()).toEqual(Float64Array.from([0, 4, 2, 6, 1, 5, 3, 7]));

            const m3 = Matrix.ipermute(m2, [2, 1, 0]);
            expect(m3.getData()).toStrictEqual(m.getData());
        }
    });

    test("Matrix.manipulation: shiftdim", () => {
        const input = Matrix.colon(0, 7).reshape([1, 1, 2, 2, 2]);
        const [output1, n1] = input.shiftdim();
        expect(n1).toBe(2);
        expect(output1.getSize()).toEqual([2, 2, 2]);
        expect(input.getSize()).toEqual([1, 1, 2, 2, 2]);

        const [output2, n2] = input.shiftdim(2);
        expect(n2).toBe(2);
        expect(output2.getSize()).toEqual([2, 2, 2]);
        expect(input.getSize()).toEqual([1, 1, 2, 2, 2]);

        const [output3, n3] = input.shiftdim(-2);
        expect(n3).toBe(-2);
        expect(output3.getSize()).toEqual([1, 1, 1, 1, 2, 2, 2]);
        expect(input.getSize()).toEqual([1, 1, 2, 2, 2]);
    });

    test("Matrix.manipulation: circshift", () => {
        const d = [
            1, 1, 0, 0,
            1, 1, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        ];
        const input = new Matrix([4, 4], d);
        const m1 = input.circshift([-1, -1]);
        expect(m1.getData()).toStrictEqual(Float64Array.from([
            1, 0, 0, 1,
            0, 0, 0, 0,
            0, 0, 0, 0,
            1, 0, 0, 1
        ]));
        // Shift 2 on dimension 2;
        const m2 = input.circshift(2, 0);
        expect(m2.getData()).toStrictEqual(Float64Array.from([
            0, 0, 1, 1,
            0, 0, 1, 1,
            0, 0, 0, 0,
            0, 0, 0, 0
        ]));
    });

    test("Matrix.manipulation: rot90", () => {
        const d = [0, 1, 2, 3];
        const mat = new Matrix([2, 2], d);
        let mat1 = mat.rot90();
        expect(mat1.getData()).toStrictEqual(Float64Array.from([2, 0, 3, 1]));
        const mat2 = mat.rot90(2);
        expect(mat2.getData()).toStrictEqual(Float64Array.from([3, 2, 1, 0]));
        const mat3 = mat.rot90(3);
        expect(mat3.getData()).toStrictEqual(Float64Array.from([1, 3, 0, 2]));
        const mat4 = mat.rot90(-1);
        expect(mat4.getData()).toStrictEqual(Float64Array.from([1, 3, 0, 2]));
        const mat5 = mat.rot90(4);
        expect(mat5.getData()).toStrictEqual(Float64Array.from(d));
    });

    test("Matrix.manipulation: flipdim / fliplr / flipud", () => {
        const input = Matrix.colon(0, 3).reshape(2, 2);
        const flipud = input.flipud(), fliplr = input.fliplr();
        expect(flipud.getData()).toStrictEqual(Float64Array.from([1, 0, 3, 2]));
        expect(fliplr.getData()).toStrictEqual(Float64Array.from([2, 3, 0, 1]));
    });

    test("Matrix.manipulation: repmat", () => {
        const input1 = Matrix.from([0, 1, 2]);
        const output1 = input1.repmat(2, 2, 2);
        expect(output1.getSize()).toStrictEqual([6, 2, 2]);

        const input2 = Matrix.complex(
            Matrix.from([0, 1, 2], [1, 1, 3]),
            Matrix.from([3, 4, 5], [1, 1, 3])
        );
        const output2 = Matrix.repmat(input2, 2, 2);
        expect(output2.getSize()).toStrictEqual([2, 2, 3]);
        expect(output2.isreal()).toBe(false);
        expect(output2.getData()).toStrictEqual(Float64Array.from([0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5]));
    });

    test("Matrix.manipulation: cat", () => {
        const input1 = Matrix.from([0, 1, 2]);
        const input2 = Matrix.from([3, 4, 5, 6]);
        const input3 = Matrix.from([3, 4, 5]);

        const mat1 = Matrix.cat(0, input1, input2);
        expect(mat1.getSize()).toEqual([7, 1]);
        expect(mat1.getData()).toStrictEqual(Float64Array.from([0, 1, 2, 3, 4, 5, 6]));

        const mat2 = Matrix.cat(2, input1, input3);
        expect(mat2.getSize()).toEqual([3, 1, 2]);
        expect(mat2.getData()).toStrictEqual(Float64Array.from([0, 1, 2, 3, 4, 5]));
    });

});
