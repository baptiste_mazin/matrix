/* global describe test expect */
import Matrix, {} from "../src/Matrix.js";
import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

describe("Testing Matrix.fourier", () => {
    test("Matrix.fourier: fft / ifft / fft2 / ifft2", () => {
        const test1 = function (s) {
            const fft = Matrix.fft(s);
            const out = Matrix.ifft(fft);
            return Matrix.psnr(s, out).asScalar();
        };
        const test2 = function (s) {
            const fft = Matrix.fft2(s);
            const out = Matrix.ifft2(fft);
            return Matrix.psnr(s, out).asScalar();
        };
        for (let sz = 1; sz < 6; sz += 2) {
            const s = Matrix.rand(sz, sz + 1);
            let res;
            res = test1(s);
            expect(res).toBeGreaterThan(200);
            res = test2(s);
            expect(res).toBeGreaterThan(200);
        }
    });
});
