/* global describe test expect */
import Matrix, {} from "../src/Matrix.js";
import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

describe("Testing Matrix.interpolation", () => {
    test(`Matrix.interpolation: `, () => {
        const values = [-0.25, 0, 0.25, 0.5, 0.75, 1.0, 1.25];
        {
            const interpolated = Matrix.interp1([0, 0.5, 1], [0, 0.5, 1], values);
            expect(interpolated.getData()).toStrictEqual(new Matrix(7, [NaN, ...values.slice(1, 6), NaN]).getData());
        } {
            const interpolated = Matrix.interp1([0, 0.5, 1], [0, 0.5, 1], values, "linear", "extrap");
            expect(interpolated.getData()).toStrictEqual(new Matrix(7, values).getData());
        } {
            const interpolated = Matrix.interp1([0, 0.5, 1], [0, 0.5, 1], values, "linear", 2);
            expect(interpolated.getData()).toStrictEqual(new Matrix(7, [2, ...values.slice(1, 6), 2]).getData());
        }
        {
            const interpolated = Matrix.interp1([0, 0.5, 1], [0, 0.5, 1], values);
            expect(interpolated.getData()).toStrictEqual(new Matrix(7, [NaN, ...values.slice(1, 6), NaN]).getData());
        }
        {
            const interpolated = Matrix.interp1([0, 0.5, 1], [0, 0.5, 1], values, "nearest");
            expect(interpolated.getData()).toStrictEqual(new Matrix(7, [NaN, 0, 0.5, 0.5, 1, 1, NaN]).getData());
        } {
            const interpolated = Matrix.interp1([0, 0.5, 1], [0, 0.5, 1], values, "previous");
            expect(interpolated.getData()).toStrictEqual(new Matrix(7, [NaN, 0, 0, 0.5, 0.5, 1, NaN]).getData());
        } {
            const interpolated = Matrix.interp1([0, 0.5, 1], [0, 0.5, 1], values, "next");
            expect(interpolated.getData()).toStrictEqual(new Matrix(7, [NaN, 0, 0.5, 0.5, 1, 1, NaN]).getData());
        } {
            const x = Matrix.from([0, 1]), values = Matrix.from([0, 1, 0, 4], [2, 2]), request = Matrix.from([0, 0.25, 0.5, 1], [2, 2]);
            const interpolated = Matrix.interp1(x, values, request);
            expect(interpolated).toEqualMatrix(Matrix.from([0, 0.25, 0.5, 1, 0, 1, 2, 4], [2, 2, 2]));
        }
    });
});
