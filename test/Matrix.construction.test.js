/* global describe test expect */
import Matrix, {Check} from "../src/Matrix.js";

describe("Testing Matrix.construction", () => {

    test("Matrix.construction: colon", () => {
        const col1 = Matrix.colon(0, 1, 10);
        expect(col1.getData()).toEqual(Float64Array.from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));

        const col2 = Matrix.colon(-3, 3, 4);
        expect(col2.getData()).toEqual(Float64Array.from([-3, 0, 3]));

        const col3 = Matrix.colon(0.8, -0.3, 0.0);
        expect(col3.getData()).toEqual(Float64Array.from([0.8, 0.5, 0.20000000000000007]));

        const col4 = Matrix.colon(0.8, -0.4, -0.4);
        expect(col4.getData()).toEqual(Float64Array.from([0.8, 0.4, 0.0, -0.4]));
    });

    test("Matrix.construction: linspcace", () => {
        const lin1 = Matrix.linspace(0, 0.2, 3);
        expect(lin1.getData()).toEqual(Float64Array.from([0, 0.1, 0.2]));

        const lin2 = Matrix.linspace(0.2, -0.1, 4);
        expect(lin2.getData()).toEqual(Float64Array.from([0.2, 0.09999999999999999, 1.3877787807814457e-17, -0.1]));

        const lin3 = Matrix.linspace(1, 99);
        expect(lin3.getSize()).toEqual([100, 1]);
    });

    test("Matrix.construction: logspace", () => {
        const log1 = Matrix.logspace(-1, 2, 4);
        expect(log1.getData()).toEqual(Float64Array.from([0.1, 1, 10, 100]));

        const log2 = Matrix.logspace(0, 2, 3);
        expect(log2.getData()).toEqual(Float64Array.from([1, 10, 100]));

        const log3 = Matrix.logspace(1, 2);
        expect(log3.getSize()).toEqual([50, 1]);
    });

    test("Matrix.construction: zeros / ones / true / false", () => {
        const zeros1 = Matrix.zeros([3, 3], "uint8");
        expect(zeros1.getData()).toEqual(new Uint8Array(9));
        expect(zeros1.getSize()).toEqual([3, 3]);

        const ones2 = Matrix.ones(3);
        expect(ones2.getData()).toEqual(new Float64Array(9).fill(1));
        expect(ones2.getSize()).toEqual([3, 3]);

        const true1 = Matrix.true([3, 3, 3]);
        expect(true1.getData()).toEqual(new Uint8ClampedArray(27).fill(1));
        expect(true1.getSize()).toEqual([3, 3, 3]);

        const false1 = Matrix.false([3, 1]);
        expect(false1.getData()).toEqual(new Uint8ClampedArray(3).fill(0));
        expect(false1.getSize()).toEqual([3, 1]);
    });

    test("Matrix.construction: rand / randi / randn", () => {
        const rand1 = Matrix.rand(100);
        expect(rand1.getSize()).toEqual([100, 100]);
        expect(Check.isArrayOfNumbers(rand1.getData(), 0, 1)).toBe(true);

        const rand2 = Matrix.rand("uint8");
        expect(rand2.getSize()).toEqual([1, 1]);
        expect(Check.isArrayOfNumbers(rand2.getData(), 0, 1)).toBe(true);

        const randn1 = Matrix.randn(100);
        expect(randn1.getSize()).toEqual([100, 100]);
        expect(Check.isArrayOfNumbers(randn1.getData())).toBe(true);

        const randn2 = Matrix.randn("uint8");
        expect(randn2.getSize()).toEqual([1, 1]);
        expect(Check.isArrayOfNumbers(randn2.getData())).toBe(true);

        const randi1 = Matrix.randi([10, 500], 1000);
        expect(randi1.getSize()).toEqual([1000, 1000]);
        expect(Check.isArrayOfNumbers(randi1.getData(), 10, 500)).toBe(true);

        const randi2 = Matrix.randi([10, 11], "uint8");
        expect(randi2.getSize()).toEqual([1, 1]);
        expect(Check.isArrayOfNumbers(randi2.getData(), 10, 11)).toBe(true);

    });

    test("Matrix.construction: eye", () => {
        const eye1 = Matrix.eye("uint8");
        expect(eye1.getSize()).toEqual([1, 1]);
        expect(eye1.asScalar()).toBe(1);
        expect(eye1.type()).toBe("uint8");

        const eye2 = Matrix.eye([2, 2, 2]);
        expect(eye2.getSize()).toEqual([2, 2, 2]);
        expect(eye2.getData()).toStrictEqual(Float64Array.from([1, 0, 0, 1, 1, 0, 0 ,1]));
        expect(eye2.type()).toBe("double");
    });

    // test("Matrix.construction: Imread", () => {
    //     expect(true).toBe(false);
    // });

    test("Matrix.construction: diag", () => {
        const diag1 = Matrix.diag([1, 3, 2]);
        expect(diag1.getSize()).toEqual([3, 3]);
        expect(diag1.getData()).toStrictEqual(Float64Array.from([1, 0, 0, 0, 3, 0, 0, 0, 2]));
        expect(diag1.type()).toBe("double");
    });

});
