/* global describe test expect */
import Matrix from "../src/Matrix.js";

describe("Testing Matrix.numeric_types", () => {

    test("Matrix.numeric_types: cast / all types", () => {
        const types = new Map([
            ["float64", "double"],
            ["float32", "single"],
            ["float", "single"],
            ["int8", "int8"],
            ["bool", "logical"],
            ["boolean", "logical"],
            ["uint8c", "uint8c"],
            ["double", "double"],
            ["single", "single"],
            ["logical", "logical"],
            ["uint8", "uint8"],
            ["int16", "int16"],
            ["uint16", "uint16"],
            ["int32", "int32"],
            ["uint32", "uint32"],
            ["array", "double"],
            ["float64array", "double"],
            [Float64Array, "double"],
            ["float32array", "single"],
            [Float32Array, "single"],
            ["int8array", "int8"],
            [Int8Array, "int8"],
            ["uint8clampedarray", "uint8c"],
            [Uint8ClampedArray, "uint8c"],
            ["uint8array", "uint8"],
            [Uint8Array, "uint8"],
            ["int16array", "int16"],
            [Int16Array, "int16"],
            ["uint16array", "uint16"],
            [Uint16Array, "uint16"],
            ["int32array", "int32"],
            [Int32Array, "int32"],
            ["uint32array", "uint32"],
            [Uint32Array, "uint32"],
        ]);
        const input = Matrix.colon(0, 24).reshape(5, 5);
        for (let [type, matlabType] of types.entries()) {
            const output1 = input.cast(type);
            expect(output1.type()).toBe(matlabType);
            const output2 = Matrix.cast(input, type);
            expect(output2.type()).toBe(matlabType);
        }
        for (let type of ["double", "single", "int8", "int16", "int32", "uint8", "uint8c", "uint16", "uint32", "logical"]) {
            const output1 = input[type]();
            expect(output1.type()).toBe(type);
            const output2 = Matrix[type](input);
            expect(output2.type()).toBe(type);
        }
    });

    test("Matrix.numeric_types: isnan", () => {
        const input1 = new Matrix([2, 2], Float64Array.from([NaN, NaN, 3, 4]));
        expect(input1.isnan().getSize()).toStrictEqual([2, 2]);
        expect(input1.isnan().getData()).toStrictEqual(Uint8ClampedArray.from([true, true, false, false]));

        const input2 = Matrix.complex(input1, new Matrix([2, 2], Float64Array.from([3, 4, NaN, NaN])));
        expect(input2.isnan().getSize()).toStrictEqual([2, 2]);
        expect(input2.isnan().getData()).toStrictEqual(Uint8ClampedArray.from([true, true, true, true]));
    });

    test("Matrix.numeric_types: isinf", () => {
        const input1 = new Matrix([3, 2], Float64Array.from([Infinity, -Infinity, 3, 4, 5, 6]));
        expect(input1.isinf().getSize()).toStrictEqual([3, 2]);
        expect(input1.isinf().getData()).toStrictEqual(Uint8ClampedArray.from([true, true, false, false, false, false]));

        const input2 = Matrix.complex(input1, new Matrix([3, 2], Float64Array.from([3, 4, -Infinity, Infinity, 6, 6])));
        expect(input2.isinf().getSize()).toStrictEqual([3, 2]);
        expect(input2.isinf().getData()).toStrictEqual(Uint8ClampedArray.from([true, true, true, true, false, false]));
    });

    test("Matrix.numeric_types: isfinite", () => {
        const input1 = new Matrix([3, 2], Float64Array.from([Infinity, -Infinity, NaN, 4, 5, 6]));
        expect(input1.isfinite().getSize()).toStrictEqual([3, 2]);
        expect(input1.isfinite().getData()).toStrictEqual(Uint8ClampedArray.from([false, false, false, true, true, true]));

        const input2 = Matrix.complex(input1, new Matrix([3, 2], Float64Array.from([0, 4, 0, NaN, 6, 6])));
        expect(input2.isfinite().getSize()).toStrictEqual([3, 2]);
        expect(input2.isfinite().getData()).toStrictEqual(Uint8ClampedArray.from([false, false, false, false, true, true]));

    });
});
