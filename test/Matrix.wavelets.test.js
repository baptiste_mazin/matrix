/* global describe test expect */
import Matrix, {Check} from "../src/Matrix.js";
import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

// Template
// describe("Testing Matrix.<module_name>", () => {
//     test("Matrix.<module_name>: <func name>", () => {
//     });
// });



describe("Testing Matrix.wavelet.class", () => {
    const wNames = [
        "haar",
        "sym2", "sym4", "sym8",
        "db2", "db4", "db8",
        "coif1", "coif2", "coif4", "coif4",
        "bi13", "bi31", "bi68",
        "rbio31", "rbio33", "rbio35", "rbio39",
        "cdf97"
    ];
    const wModes = [
        "sym", "symw", "per", "zpd", "nn"
    ];
    const test_wavedecrec = function (s, N, name) {
        const test = function (dim) {
            const wt = Matrix.wavedec(s, N, name, dim);
            const iwt = Matrix.waverec(wt, name, dim);
            const psnr = Matrix.psnr(s, iwt).asScalar();
            return psnr;
        };
        const res0 = test(0), res1 = test(1);
        return Math.min(res0, res1);
    };
    const test_upwlev = function (s, N, name) {
        const test = function (dim) {
            let wt = Matrix.wavedec(s, N, name, dim);
            for (let n = 0; n < N; n++) {
                wt = Matrix.upwlev(wt, name, dim);
            }
            return Matrix.psnr(wt[0], s).asScalar();
        };
        const res0 = test(0), res1 = test(1);
        return Math.min(res0, res1);
    };
    const test_wrcoef = function (s, N, name) {
        let M = 0;
        const test = function (dim) {
            const wt = Matrix.wavedec(s, N, name, dim);
            let rec = Matrix.wrcoef('l', wt, name, dim, N - M);
            for (let n = N - M; n > 0; n--) {
                rec["+="](Matrix.wrcoef('h', wt, name, dim, n));
            }
            return Matrix.psnr(s, rec).asScalar();
        };
        const res0 = test(0), res1 = test(1);
        return Math.min(res0, res1);
    };
    const test_wavedecrec2 = function (s, N, name) {
        const wt2 = Matrix.wavedec2(s, N, name);
        const iwt2 = Matrix.waverec2(wt2, name);
        const psnr = Matrix.psnr(s, iwt2).asScalar();
        return psnr;
    };
    const test_upwlev2 = function (s, N, name) {
        let wt = Matrix.wavedec2(s, N, name);
        for (let n = 0; n < N; n++) {
            wt = Matrix.upwlev2(wt, name);
        }
        const rec = Matrix.appcoef2(wt, name, 0);
        return Matrix.psnr(rec, s).asScalar();
    };
    const test_wrcoef2 = function (s, N, name) {
        let M = 0;
        const wt = Matrix.wavedec2(s, N, name);
        let rec = Matrix.wrcoef2('a', wt, name, N - M);
        for (let n = N - M; n > 0; n--) {
            rec["+="](Matrix.wrcoef2('h', wt, name, n));
            rec["+="](Matrix.wrcoef2('v', wt, name, n));
            rec["+="](Matrix.wrcoef2('d', wt, name, n));
        }
        return Matrix.psnr(s, rec).asScalar();
    };
    for (let n = 0; n < wNames.length; n++) {
        const name = wNames[n];
        test(`Matrix.wavelet: ${name}`, () => {
            for (let m = 0; m < wModes.length; m++) {
                Matrix.dwtmode(wModes[m]);
                for (let sz = 1; sz < 10; sz += 2) {
                    const s = Matrix.rand(sz, sz + 1, 2);
                    let N = Matrix.dwtmaxlev([sz, sz + 1], name);
                    N = N < 1 ? 1 : N;

                        let res;
                        // 1D tests
                        res = test_wavedecrec(s, N, name);
                        expect(res).toBeGreaterThan(200);
                        res = test_upwlev(s, N, name);
                        expect(res).toBeGreaterThan(200);
                        res = test_wrcoef(s, N, name);
                        expect(res).toBeGreaterThan(200);
                        // 2D tests
                        res = test_wavedecrec2(s, N, name);
                        expect(res).toBeGreaterThan(200);
                        res = test_upwlev2(s, N, name);
                        expect(res).toBeGreaterThan(200);
                        res = test_wrcoef2(s, N, name);
                        expect(res).toBeGreaterThan(200);
                }
            }
        });
    }
});
