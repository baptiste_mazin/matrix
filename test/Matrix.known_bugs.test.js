/* global describe test expect */
import Matrix, {MatrixView, Check} from "../src/Matrix.js";

import {toEqualMatrix} from "./matchers.js";
expect.extend({toEqualMatrix});

// Template
// describe("Testing Matrix.<module_name>", () => {
//     test("Matrix.<module_name>: <func name>", () => {
//     });
// });


describe("Known bugs", () => {

    // test("Matrix.manipulation: set empty selection", () => {
    //     {
    //         const mat = Matrix.colon(0, 3).reshape([2, 2]);
    //         const setData = Matrix.from([false, false, true, false], [2, 2]);
    //         mat.set(setData, []); // empty set
    //         expect(mat.getData()).toStrictEqual(Float64Array.from([0, 1, 3]));
    //     }
    // });

    test("Matrix.operators.arithmetic: power complex numbers to real power", () => {
        const realScalarRef = Matrix.from(3);
        const realMatrixRef = Matrix.from([2, -2]);
        const cplxScalarRef = Matrix.complex(-1, 1);
        const cplxMatrixRef = Matrix.complex([-1, 2], [-2, 1]);
        let out;
        { // Matrix cplx / matrix real
            const realMatrix = realMatrixRef.clone(),
                cplxMatrix = cplxMatrixRef.clone();
            out = Matrix.power(cplxMatrix, realMatrix);
            expect(Matrix.real(out)).toEqualMatrix([-3, 0.12], 1e-12);
            expect(Matrix.imag(out)).toEqualMatrix([4, -0.16], 1e-12);
            cplxMatrix.power(realMatrix);
            expect(Matrix.real(cplxMatrix)).toEqualMatrix([-3, 0.12], 1e-12);
            expect(Matrix.imag(cplxMatrix)).toEqualMatrix([4, -0.16], 1e-12);
        } {
            const real = Matrix.from([
                1, -4, -8, -6, 4, 9, 9,
                3, 6, -2, 6, 1, 9, -6,
                -6, -6, 5, 6, -4, -8, 9,
                3, -3, 8, -9, -2, -9, -4,
                9, -7, 5, 8, -2, 2, 2,
                5, -4, 4, -3, 2, -4, 1,
                -6, 2, -7, 8, -3, -9, -6
            ], [7, 7]);
            const imag = Matrix.from([
                8, -4, 8, -6, 9, -4, 4,
                5, -1, -3, 2, -6, -7, 3,
                2, 6, 8, 3, -6, -6, -8,
                7, 0, 7, 3, -7, 0, -7,
                9, 7, 8, 4, 7, 0, 5,
                6, 0, -5, 0, 7, -1, 2,
                -8, -2, -8, -6, 4, 9, 5
            ], [7, 7]);
            const cplx = Matrix.complex(real, imag);
            const expectedReal = Matrix.from([
                1.00000000000000e+00, -9.76562500000000e-04, 3.72529029846191e-09, -0.00000000000000e+00, -9.59000000000000e+02, -7.08399351000000e+08, -7.08399351000000e+08,
                -1.98000000000000e+02, 2.77550000000000e+04, -2.95857988165680e-02, -2.25280000000000e+04, 1.00000000000000e+00, 3.07671566400000e+09, -1.02716049382716e-05,
                -5.50000000000000e-06, 0.00000000000000e+00, 2.55250000000000e+04, -8.52930000000000e+04, -2.60407548755296e-04, 4.21972480000000e-09, 5.14871712900000e+09,
                -4.14000000000000e+02, -3.70370370370370e-02, 1.40468161000000e+08, 1.55829903978052e-09, -1.60199359202563e-02, -2.58117479171320e-09, -1.14673855957424e-04,
                6.19872782400000e+09, -7.58916049313758e-08, 2.55250000000000e+04, -3.45374720000000e+07, -1.60199359202563e-02, 4.00000000000000e+00, -2.10000000000000e+01,
                -9.47500000000000e+03, 3.90625000000000e-03, -1.51900000000000e+03, -3.70370370370370e-02, -4.50000000000000e+01, 1.92765891212988e-03, 1.00000000000000e+00,
                7.52192000000000e-07, 0.00000000000000e+00, -6.18988569100242e-08, 4.21972480000000e+07, 7.48800000000000e-03, -8.06617122410374e-11, -2.28004942621151e-06
            ], [7, 7]);
            const expectedImag = Matrix.from([
                8.00000000000000e+00, -0.00000000000000e+00, 0.00000000000000e+00, 2.67918381344307e-06, -9.36000000000000e+03, 5.08331996000000e+08, -5.08331996000000e+08,
                1.00000000000000e+01, -4.23720000000000e+04, -7.10059171597633e-02, 5.99040000000000e+04, -6.00000000000000e+00, 1.06692084800000e+09, 3.86282578875171e-06,
                1.46250000000000e-05, -2.67918381344307e-06, -7.02320000000000e+04, 3.20760000000000e+04, 2.62595847484332e-04, 9.06608640000000e-09, -1.35091872800000e+09,
                -1.54000000000000e+02, -0.00000000000000e+00, -8.27836800000000e+07, -3.90997307321038e-10, -9.96796012815949e-03, -0.00000000000000e+00, 2.07051573824446e-04,
                6.19872782400000e+09, 7.58916049313758e-08, -7.02320000000000e+04, -2.20200960000000e+07, 9.96796012815949e-03, 0.00000000000000e+00, 2.00000000000000e+01, -2.74740000000000e+04, 0.00000000000000e+00, 7.20000000000000e+02, -0.00000000000000e+00, 2.80000000000000e+01, -2.87352881311287e-03, 2.00000000000000e+00, 6.58944000000000e-07, -8.00000000000000e+00, -2.04728588793497e-08, 9.06608640000000e+07, -2.81600000000000e-03, -8.06617122410374e-11, -3.76977074427124e-06
            ], [7, 7]);
            const expectedCplx = Matrix.complex(expectedReal, expectedImag);
            expect(cplx.power(real)).toEqualMatrix(expectedCplx, 1e-12);
        } { // Scalar cplx / scalar real
            const realScalar = realScalarRef.clone(),
                cplxScalar = cplxScalarRef.clone();
            out = Matrix.power(cplxScalar, realScalar);
            expect(out.getRealData()).toStrictEqual(Float64Array.from([2.000000000000001]));
            expect(out.getImagData()).toStrictEqual(Float64Array.from([2]));
            cplxScalar.power(realScalar);
            expect(cplxScalar.getRealData()).toStrictEqual(Float64Array.from([2.000000000000001]));
            expect(cplxScalar.getImagData()).toStrictEqual(Float64Array.from([2]));
        }

    });


    test("Matrix.linalg: solveUnderdeterminedQR", () => {
        const a = Matrix.rand(6, 7);
        const b = a.mtimes(a.mldivide(Matrix.eye(6)));
        expect(b).toEqualMatrix(Matrix.eye(6), 1e-10);
    });

});
