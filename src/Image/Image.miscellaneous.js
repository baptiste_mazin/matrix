/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

export default function imageMiscExtension (Matrix, Matrix_prototype) {
    (function () {
        var computeCDF = function (src, n, norm) {
            norm = norm === undefined ? 1 : norm;
            var i, nPixels = src.length, cst = n / norm;
            // Compute histogram and histogram sum
            var hist = new Float32Array(n);//, indices = new Uint16Array(nPixels);
            for (i = 0; i < nPixels; ++i) {
                var bin = Math.floor(src[i] * cst);
                bin = bin >= n ? n - 1 : (bin < 0 ? 0 : bin);
                // indices[i] = bin;
                hist[bin]++;
            }
            // Compute bias
            var bias = 0 * hist[0], inPixels = 1 / (nPixels - bias);
            // Compute integral histogram
            for (i = 1, hist[0] -= bias; i < n; ++i) {
                hist[i] += hist[i - 1];
                hist[i - 1] *= inPixels;
            }
            hist[n - 1] *= inPixels;
            return hist;//[hist, indices];
        };

        // var computeContrastChangeLUT = function (H1, H2, f) {
        //     f = f === undefined ? 1 : f;
        //     // We use a lookup table to compute all the contrast changes values at once
        //     var lut = new Float32Array(H2.length);
        //     // For each position on the 1st histogram we find the position in the 2nd histogram
        //     // with the same cumulative value
        //     for (var pos1 = 0, pos2; pos1 < H1.length; pos1++) {
        //         var currentSum = H1[pos1];
        //         for (pos2 = 0; pos2 < H2.length - 1; pos2++) {
        //             if (H2[pos2] < currentSum && H2[pos2 + 1] >= currentSum) {
        //                 var d1 = currentSum - H2[pos2], d2 = H2[pos2 + 1] - currentSum;
        //                 var pos =  pos2 + d1 / (d2 + d1) + 0.5;
        //                 lut[pos1] = pos1 * (1 - f) / (H1.length - 1) + pos * f / (H2.length - 1);
        //                 break;
        //             }
        //         }
        //     }
        //     return lut;
        // };
        //
        // var computeContrastChangeLUT = function (H1, H2, f) {
        //     console.log(H1);
        //     f = f === undefined ? 1 : f;
        //     // We use a lookup table to compute all the contrast changes values at once
        //     var lut = new Float32Array(H2.length);
        //     for (var pos1 = 0, pos2; pos1 < H1.length; pos1++) {
        //         var currentSum = H1[pos1];
        //         var dmin = 1, posmin = 0;
        //         for (pos2 = 0; pos2 < H2.length - 1; pos2++) {
        //             var dist = Math.abs(currentSum - H2[pos2]);
        //             if (dist < dmin && currentSum > H2[pos2]) {
        //                 dmin = dist;
        //                 posmin = pos2;
        //             }
        //         }
        //         // console.log(posmin, currentSum, H2[posmin]);
        //         var dist2 = Math.abs(H2[posmin + 1] - currentSum);
        //         var pos =  posmin + dmin / (dist2 + dmin);
        //         lut[pos1] = pos1 * (1 - f) / (H1.length - 1) + pos * f / (H2.length - 1);
        //     }
        //     return lut;
        // };
        var computeContrastChangeLUT = function (H1, H2) {
            // We use a lookup table to compute all the contrast changes values at once
            var lut = new Float32Array(H2.length);
            for (var pos1 = 0, pos2; pos1 < H1.length; pos1++) {
                var currentSum = H1[pos1];
                var dmin = Math.abs(currentSum - H2[0]), posmin = 0;
                for (pos2 = 1; pos2 < H2.length; pos2++) {
                    var dist = Math.abs(currentSum - H2[pos2]);
                    if (dist < dmin && currentSum > H2[pos2]) {
                        // if (currentSum >= H2[pos2] && currentSum <= H2[pos2 + 1]) {
                        dmin = dist;
                        posmin = pos2;
                    }
                }
                console.log(pos1, posmin, dmin, currentSum.toFixed(8));
                lut[pos1] = posmin / H1.length;
            }
            console.log(lut);
            return lut;
        };

        var applyCDF = function (src, CDF, norm) {
            // Equalize image:
            // var indices = CDF[1];
            // CDF = CDF[0];
            var n = CDF.length, cst = (n - 1) / norm;
            // var lastVal = CDF[n - 1] + (CDF[n - 1] - CDF[n - 2]) / 2;
            var lastVal = CDF[n - 1];
            for (var i = 0, ie = src.length; i < ie; ++i) {
                var ind = src[i] * cst;
                var p1 = Math.floor(ind), diff = (ind - p1), p2 = p1 + 1;
                var v1 = ind >= n ? lastVal : CDF[p1], v2 = ind >= n ? lastVal : CDF[p2];
                var newVal = v1 + (v2 - v1) * diff;
                src[i] = newVal * norm;
                //src[i] = CDF[indices[i]] * norm;
            }
        };

        /** Perform an histogram specification.
        *
        * @param {Matrix} arg1
        * If it's a scalar then it is the number of bin used to perform full
        * histogram equalization.
        * If it's avVector then it provides the target histogram to be matched.
        *
        * @param {Number} perc
        * limit the specification to a given percentage
        * perc = 1   : histogram specification
        * perc = 0.5 : midway specification
        * perc = 0   : Identity
        *
        * @return {Matrix}
        */
        Matrix_prototype.histeq = function (arg1, f) {
            arg1 = Matrix.from(arg1);
            var HTarget;
            if (arg1.isscalar()) {
                HTarget = Matrix.ones(arg1.asScalar(), 1).cumsum();
            } else if (arg1.isvector()) {
                HTarget = arg1.clone().cumsum();
            } else {
                throw new Error("Matrix.histeq: argument 1 must be a bin number or an histogram.");
            }
            HTarget["/="](HTarget.get(-1));

            var src = this.getData(), norm = 1, channel, space;
            // If the input is a RGB image then convert to an opponent colorspace
            // to  work on the luminance channel
            if (this.getSize(2) === 3) {
                // Colorspace parameters;
                // channel = 0;
                // space = "Ohta";
                // norm = 3 / Math.sqrt(3);

                channel = 2;
                space = "HSV";
                norm = 1;
                src = this.applycform("RGB to " + space)
                .getData()
                .subarray(channel * src.length / 3, (channel + 1) * src.length / 3);
            } else if (this.getSize(2) !== 1) {
                throw new Error("Matrix.histeq: Input must be grey level image.");
            }

            var CDF = computeCDF(src, HTarget.numel(), norm);
            // CDF[0] = computeContrastChangeLUT(CDF[0], HTarget.getData(), f);
            CDF = computeContrastChangeLUT(CDF, HTarget.getData(), f);
            // window.CDFT = CDF;
            applyCDF(src, CDF, norm);
            if (this.getSize(2) > 1) {
                this.applycform(space + " to RGB");
            }
            return this;
        };

        Matrix.histeq = function (im, n, f) {
            return im.clone().histeq(n, f);
        };
    })();


    //////////////////////////////////////////////////////////////////
    //                  REGION AND IMAGE PROPERTIES                 //
    //////////////////////////////////////////////////////////////////


    (function () {

        /* Class for Tree creation */
        var Node = function (x, y, parent) {
            this.x = x;
            this.y = y;
            this.parent = parent;
        };
        Node.prototype.initChildren = function (w, h) {
            var x = this.x, y = this.y;
            if (y + 1 < h) {
                this.top = new Node(x, y + 1, this);
            }
            if (y - 1 >= 0) {
                this.bottom = new Node(x, y - 1, this);
            }
            if (x + 1 < w) {
                this.left = new Node(x + 1, y, this);
            }
            if (x - 1 >= 0) {
                this.right = new Node(x - 1, y, this);
            }
        };
        Node.prototype.remove = function (n) {
            if (this.bottom === n) {
                this.bottom = undefined;
            } else if (this.top === n) {
                this.top = undefined;
            } else if (this.right === n) {
                this.right = undefined;
            } else if (this.left === n) {
                this.left = undefined;
            }
            return this;
        };
        Node.prototype.getNext = function () {
            if (this.top) {
                return this.top;
            }
            if (this.bottom) {
                return this.bottom;
            }
            if (this.left) {
                return this.left;
            }
            if (this.right) {
                return this.right;
            }
            if (this.parent) {
                return this.parent.remove(this).getNext();
            }
            return undefined;
        };

        /** From an image and a pixel request select neighbour pixels with a similar values
        * (RGB or grey level).
        * @param{Number} xRef
        *  x coordinate of the pixel.
        * @param{Number} yRef
        *  x coordinate of the pixel.
        * @param{Number} t
        *  threshold on the distance
        * @return{Matrix}
        *  Return a Matrix with boolean values.
        */
        Matrix_prototype.getConnectedComponent = function (xRef, yRef, t) {

            // Get image height, width and depth
            var h = this.getSize(0), w = this.getSize(1), d = this.getSize(2);

            // Squared threshold
            var t2 = t * t;

            // Connected component and visited pixels
            var cc = new Matrix([h, w], 'logical'), isVisited = new Matrix([h, w], 'logical');
            var ccd = cc.getData(), imd = this.getData(), ivd = isVisited.getData();

            // For debug, has to be removed
            window.CC = cc;
            window.IV = isVisited;

            var cRef = yRef + h * xRef;
            var compare_pixels;
            if (d === 1) {
                if (this.type() !== "logical") {
                    // Grey value of pixel request
                    var v = imd[cRef];
                    compare_pixels = function (c) {
                        var dTmp = imd[c] - v;
                        return dTmp * dTmp < t2;
                    };
                }
            } else if (d === 3) {
                // RGB values of pixel request
                var rRef = imd[cRef], gRef = imd[cRef + h * w], bRef = imd[cRef + h * w * 2];
                // Image channel subarrays
                var rd = imd, gd = imd.subarray(h * w), bd = imd.subarray(h * w * 2);
                compare_pixels = function (c) {
                    var dTmp1 = rd[c] - rRef, dTmp2 = gd[c] - gRef, dTmp3 = bd[c] - bRef;
                    return dTmp1 * dTmp1 + dTmp2 * dTmp2 + dTmp3 * dTmp3 < t2;
                };
            } else {
                throw new Error("Matrix.getConnectedComponent: This function only support " +
                "images with depth 1 or 3.");
            }

            var root = new Node(xRef, yRef), current = root;

            while (current !== undefined) {
                var x = current.x, y = current.y, c = y + h * x;

                if (ivd[c] === 1) {
                    current = current.parent.remove(current).getNext();
                    continue;
                }
                ivd[c] = 1;
                if (compare_pixels(c)) {
                    ccd[c] = 1;
                    current.initChildren(w, h);
                }
                current = current.getNext();
            }

            return cc;
        };

        Matrix_prototype.bwconncomp = function () {};

    })();

    /**  Implementation of the Guided filter.
    * @param{Matrix} guidance
    *  Guidance image
    * @param{Number} radius
    *  Half of the patch size
    * @param{Numer} epsilon
    *  Parameter that determine "what is an edge/a high variance patch
    *  that should be preserved”
    */
    Matrix_prototype.guidedFilter = function (p, r, eps) {
        var d = 2 * Math.round(r) + 1;
        var mI = this.boxFilter(d),
        mII = this[".*"](this).boxFilter(d),
        vI = mII["-"](mI[".*"](mI));
        var mp = mI, cIp = vI;
        if (this !== p) {
            mp = p.boxFilter(d);
            const mIp = this[".*"](p).boxFilter(d);
            cIp = mIp["-"](mI[".*"](mp));
        }
        var A = cIp["./"](vI["+"](eps)), B = mp["-"](A[".*"](mI));
        return A.boxFilter(d)[".*"](this)["+"](B.boxFilter(d));
    };


    Matrix_prototype.bin = function (wy, wx) {
        var is = this.getSize();
        var os = [Math.ceil(is[0] / wy), Math.ceil(is[1] / wx), this.getSize(2)];

        var output = Matrix.zeros(os), od = output.getData();
        var input = this.padarray(
            "nn",
            [0, os[0] * wx - is[0]],
            [0, os[1] * wx - is[1]]
        ), id = input.getData();

        var iView = input.getView(), oView = output.getView();
        var dic = iView.getStep(2), lic = iView.getEnd(2), doc = oView.getStep(2);
        var dix = iView.getStep(1), lix = iView.getEnd(1), dox = oView.getStep(1);
        var diy = iView.getStep(0), liy = iView.getEnd(0), doy = oView.getStep(0);

        var ic, oc, ix, nix, iy, niy, v, nv, u, nu, ox, oy, sum;
        var cst = 1 / (wx * wy);
        for (ic = 0, oc = 0; ic !== lic; ic += dic, oc += doc) {
            for (ix = ic, nix = ic + lix, ox = oc; ix !== nix; ix += dix * wx, ox += dox) {
                for (iy = ix, oy = ox, niy = ix + liy; iy < niy; iy += wy, oy++) {
                    for (sum = 0, v = iy, nv = iy + wx * dix; v !== nv; v += dix) {
                        for (u = v, nu = v + wy; u < nu; u++) {
                            sum += id[u];
                        }
                    }
                    od[oy] = sum * cst;
                }
            }
        }
        return output;
    };

    Matrix_prototype.expand = function (wx, wy) {
        var is = this.getSize();
        var os = [is[0] * wy, is[1] * wx, this.getSize(2)];

        var output = Matrix.zeros(os), od = output.getData();
        var input = this, id = input.getData();

        var iView = input.getView(), oView = output.getView();
        var dic = iView.getStep(2), lic = iView.getEnd(2), doc = oView.getStep(2);
        var dix = iView.getStep(1), lix = iView.getEnd(1), dox = oView.getStep(1);
        var diy = iView.getStep(0), liy = iView.getEnd(0), doy = oView.getStep(0);

        var ic, oc, ix, nix, iy, niy, v, nv, u, nu, ox, oy, val;
        for (ic = 0, oc = 0; ic !== lic; ic += dic, oc += doc) {
            for (ix = ic, nix = ic + lix, ox = oc; ix !== nix; ix += dix, ox += dox * wx) {
                for (iy = ix, oy = ox, niy = ix + liy; iy < niy; iy++, oy += wy) {
                    for (val = id[iy], v = oy, nv = oy + wx * dox; v !== nv; v += dox) {
                        for (u = v, nu = v + wy; u < nu; u++) {
                            od[u] = val;
                        }
                    }
                }
            }
        }
        return output;
    };


    /**  Convert an image to a set of patches represented as a 2D matrix.
    * @param{Matrix} psize
    *  patchSize
    * @param{String} type
    *  Can be distinct or sliding.
    * @matlike
    */
    Matrix_prototype.im2col = function (psize, type) {
        psize = Matrix.from(psize);
        var [wy, wx] = psize.getData(), is = this.getSize();

        // # patches for distinct type
        var nx = Math.floor(is[1] / wx), ny = Math.floor(is[0] / wy), dwx = wx, dwy = wy;
        if (type === "sliding") {
            nx = is[1] - wx + 1;
            ny = is[0] - wy + 1;
            dwx = 1;
            dwy = 1;
        } else if (type !== undefined && type !== "distinct") {
            throw new Error("Matrix.imcol: Valid types are distinct or sliding.");
        }

        var id = this.getData();
        var output = Matrix.zeros([wx * wy * this.getSize(2), nx * ny]), od = output.getData();

        var iView = this.getView().select([0, ny * dwy - 1], [0, nx * dwx - 1]),
        oView = output.getView();

        var dic = iView.getStep(2), lic = iView.getEnd(2), doc = oView.getStep(2);
        var dix = iView.getStep(1), lix = iView.getEnd(1), dox = oView.getStep(1);
        var diy = iView.getStep(0), liy = iView.getEnd(0), doy = oView.getStep(0);

        var ic, nic, oc, ix, nix, iy, niy, v, nv, u, nu, ox, oy, o;
        for (o = 0, ix = 0, nix = lix; ix < nix; ix += dix * dwx) {
            for (iy = ix, niy = ix + liy; iy < niy; iy += dwy) {
                for (ic = iy, nic = iy + lic; ic < nic; ic += dic) {
                    for (v = ic, nv = ic + wx * dix; v < nv; v += dix) {
                        for (u = v, nu = v + wy; u < nu; u++) {
                            od[o++] = id[u];
                        }
                    }
                }
            }
        }
        return output;
    };

    /**  Apply a function to all block of psize of the image.
    * @param{Matrix} psize
    *  patchSize
    * @param{Function} fun
    *  A function which a patch as an argument and return a value as output.
    * @matlike
    */
    Matrix_prototype.nlfilter = function (psize, fun) {
        // Patch size
        psize = Matrix.from(psize);
        var wy = psize.asScalar(0), wx = psize.asScalar(1),
        hwy = Math.floor(wy / 2), hwx = Math.floor(wx / 2),
        ny = this.getSize(0), nx = this.getSize(1), nPatches = ny * nx;
        var input = this.padarray("symw", [hwy, hwy], [hwx, hwx]);

        // # Patches
        var is = input.getSize(), id = input.getData(), iView = input.getView();

        var dic = iView.getStep(2), lic = iView.getEnd(2),
        dix = iView.getStep(1), lix = iView.getEnd(1),
        liy = iView.getEnd(0);

        if (is[2] === 1) { // For grey level images
            var patch = Matrix.zeros(wy, wx), pd = patch.getData();
            var output = Matrix.zeros(ny, nx), od = output.getData();
            var ic, nic, oc, ix, nix, iy, niy, v, nv, u, nu, ox, oy, o, n;
            for (n = 0, ix = 0, nix = lix - (wx - 1) * dix; ix < nix; ix += dix) {
                for (iy = ix, niy = ix + liy - (wy - 1); iy < niy; iy++, n++) {
                    // Copy patch into patch data variable
                    for (v = iy, nv = iy + wx * dix; v < nv; v += dix) {
                        for (u = v, nu = v + wy; u < nu; u++) {
                            pd[o++] = id[u];
                        }
                    }
                    od[n] = fun(patch); // Expect a value output
                }
            }
        } else if (is[2] === 3) { // For color images
            var patch = Matrix.zeros(wy, wx, 3), pd = patch.getData();
            var output = Matrix.zeros(ny, nx, 3), od = output.getData();
            var rod = od.subarray(0,                nPatches),
            god = od.subarray(nPatches,     2 * nPatches),
            bod = od.subarray(2 * nPatches, 3 * nPatches);
            var ic, nic, oc, ix, nix, iy, niy, v, nv, u, nu, ox, oy, o, n;
            for (n = 0, ix = 0, nix = lix - (wx - 1) * dix; ix < nix; ix += dix) {
                for (iy = ix, niy = ix + liy - (wy - 1); iy < niy; iy++, n++) {
                    // Copy patch into patch data variable
                    for (o = 0, ic = iy, nic = iy + lic; ic < nic; ic += dic) {
                        for (v = ic, nv = ic + wx * dix; v < nv; v += dix) {
                            for (u = v, nu = v + wy; u < nu; u++) {
                                pd[o++] = id[u];
                            }
                        }
                    }
                    var patchVal = fun(patch); // Expect a color output;
                    rod[n] = patchVal[0];
                    god[n] = patchVal[1];
                    bod[n] = patchVal[2];
                }
            }
        }
        return output;
    };

    Matrix_prototype.imadjust = function (th) {
        var nBins = 65535;
        var size = this.getSize();
        var hist = this.reshape().imhist(nBins).cumsum();
        this.reshape(size);
        var hd = hist["/="](hist.get(-1)).getData();
        //console.log(hd);
        var iMin = 0, iMax = hd.length - 1;
        for (var i = 0, ie = hd.length; i < ie; i++) {
            // console.log(hd[i], th);
            if (hd[i] > th) {
                iMin = i;
                break;
            }
        }
        for (var i = hd.length - 1, ie = -1; i > ie; i--) {
            // console.log(hd[i], 1 - th);
            if (hd[i] < 1 - th) {
                iMax = i;
                break;
            }
        }
        console.log(iMin, hd[iMin], iMax, hd[iMax]);
        var id = this.getData();
        iMax = nBins / (iMax - iMin);
        iMin = iMin / nBins;
        for (var i = 0, ie = id.length; i < ie; i++) {
            var v = (id[i] - iMin) * iMax;
            id[i] = v < 0 ? 0 : (v > 1 ? 1 : v);
        }
        return this;
    };

    (function () {
        var spatial_map = function (img) {
            var blk_size = 8;
            var [nr, nc] = img.size(), id = img.getData();
            var res = Matrix.zeros(nr, nc, img.type()), rd = res.getData();
            var tv_tmp = new Float64Array((blk_size - 1) * (blk_size - 1));
            var abs = Math.abs;
            for (var r_ = 0, r_e = nr * nc; r_ < r_e; r_ += blk_size * nr) {
                for (var rc = r_, rce = r_ + nr; rc < rce; rc += blk_size) {

                    // Measure local total variation for every 2x2 block
                    for (var i_ = rc, i_e = rc + (blk_size - 1) * nr, ind = 0; i_ < i_e; i_ += nr) {
                        for (var ij = i_, ije = i_ + (blk_size - 1); ij < ije; ij++, ind++) {
                            var v00 = id[ij],     v01 = id[ij + nr],
                            v10 = id[ij + 1], v11 = id[ij + nr + 1];
                            tv_tmp[ind] =
                            abs(v00 - v10) + abs(v00 - v01) + abs(v00 - v11)
                            + abs(v11 - v01) + abs(v01 - v10) + abs(v11 - v10);
                        }
                    }

                    let tv_max = tv_tmp[0];
                    for (let i = 1, ie = tv_tmp.length; i < ie; i++) {
                        if (tv_tmp[i] > tv_max) {
                            tv_max = tv_tmp[i];
                        }
                    }
                    // Normalize tv_max to be from 0 -1.
                    tv_max *= 0.25;

                    for (let i_ = rc, i_e = rc + blk_size * nr; i_ < i_e; i_ += nr) {
                        for (let ij = i_, ije = i_ + blk_size; ij < ije; ij++) {
                            rd[ij] = tv_max;
                        }
                    }

                }
            }
            return res;
        };

        Matrix_prototype.computeSharpnessMap = function () {
            var im = this.im2single().padarray("sym", [8, 8], [8, 8]);
            var map1 = spatial_map(im).get([8, -9], [8, -9]);
            im = im.get([4, -5], [4, -5]);
            var map2 = spatial_map(im).get([4, -5], [4, -5]);
            var map = Matrix.zeros(map1.size(), im.type()), md = map.getData();
            var m1d = map1.getData(), m2d = map2.getData();
            var max = Math.max;
            for (var i = 0, ie = md.length; i < ie; i++) {
                md[i] = max(m1d[i], m2d[i]);
            }
            return map;
        };

    })();

    Matrix_prototype.imregionalmax = function () {
        let [nx, ny] = this.size();
        let out  = Matrix.zeros(nx, ny, "single"), od = out.getData(), id = this.getData();

        for (let _y = 1, _ye = nx * (ny - 1); _y < _ye; _y += nx) {
            let c00 = id[_y - 1 - nx], c10 = id[_y - 1], c20 = id[_y - 1 + nx],
                c01 = id[_y     - nx], c11 = id[_y    ], c21 = id[_y     + nx];
            let m0 = c00 > c10 ?
                    (c00 > c20 ? c00 : (c10 > c20 ? c10 : c20)) :
                    (c10 > c20 ? c10 : (c20 > c00 ? c20 : c00)),
                m1 = c01 > c11 ?
                    (c01 > c21 ? c01 : (c11 > c21 ? c11 : c21)) :
                    (c11 > c21 ? c11 : (c21 > c01 ? c21 : c01)),
                m2;
            // let m0 = Math.max(c00, c10, c20),
            //     m1 = Math.max(c01, c11, c21);
            for (let xy = _y + 1, xye = _y + nx - 1; xy < xye; xy++) {
                let c11 = id[xy],
                    c02 = id[xy + 1 - nx], c12 = id[xy + 1], c22 = id[xy + 1 + nx];
                m2 = c02 > c12 ?
                    (c02 > c22 ? c02 : (c12 > c22 ? c12 : c22)) :
                    (c12 > c22 ? c12 : (c22 > c02 ? c22 : c02));
                let m = m0 > m1 ?
                    (m0 > m2 ? m0 : (m1 > m2 ? m1 : m2)) :
                    (m1 > m2 ? m1 : (m2 > m0 ? m2 : m0));
                // let m2 = Math.max(c02, c12, c22);
                // let m = Math.max(m0, m1, m2);
                od[xy] = c11 === m ? 1 : 0;
                m0 = m1;
                m1 = m2;
            }
        }
        return out;
    };

    Matrix_prototype.cornermetric = function ({method = "Harris", FilterCoefficients = Matrix.fspecial("gaussian", [5, 1], 1.5), SensitivityFactor = 0.04} = {}) {
        let image = this;
        if (image.size(2) === 3) {
            image = image.rgb2gray();
        }
        image = image.im2double();
        let gradient = image.gradient(true, true);
        let Ix2 = Matrix.times(gradient.x, gradient.x),
            Iy2 = Matrix.times(gradient.y, gradient.y),
            Ixy = Matrix.times(gradient.x, gradient.y);

        let Sx2 = Ix2.separableFilter(FilterCoefficients, FilterCoefficients).getData(),
            Sy2 = Iy2.separableFilter(FilterCoefficients, FilterCoefficients).getData(),
            Sxy = Ixy.separableFilter(FilterCoefficients, FilterCoefficients).getData();

        let R = Matrix.zeros(image.size()), rd = R.getData();
        if (method === "Harris") {
            for (let i = 0, ie = rd.length; i < ie; i++) {
                let det = Sx2[i] * Sy2[i] - Sxy[i] * Sxy[i];
                let trace = (Sx2[i] + Sy2[i]) * (Sx2[i] + Sy2[i]);
                rd[i] = det - SensitivityFactor * trace * trace;
            }
        } else if (method === "MinimumEigenvalue") {
            for (let i = 0, ie = rd.length; i < ie; i++) {
                let ApB = Sx2[i] + Sy2[i], AmB = Sx2[i] - Sy2[i];
                let C4 = 4 * Sxy[i] * Sxy[i];
                AmB *= AmB;
                rd[i] = (ApB - Math.sqrt(AmB + C4)) / 2;
            }
        }
        return R;
    };

}
