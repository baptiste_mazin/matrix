/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */
import colorExtension from "./Image.color.js";
import conversionsExtension from "./Image.conversions.js";
import miscExtension from "./Image.miscellaneous.js";
import filteringExtension from "./Image.filtering.js";
import morphoFilteringExtension from "./Image.filtering.morphological.js";


export default function imageExtension (Matrix, Matrix_prototype) {

    // Check if nodejs or browser
    const isNode = (typeof module !== 'undefined' && module.exports) ? true : false;
    let Canvas;
    if (isNode) {
        // Do not forget: export NODE_PATH=/usr/local/lib/node_modules
        import("canvas").then(mod => Canvas = mod);
    }

    const createCanvas = function (width = 0, height = 0) {
        let canvas;
        if (isNode) {
            canvas = new Canvas();
        } else {
            canvas = document.createElement("canvas");
        }
        canvas.width = width;
        canvas.height = height;
        return canvas;
    };

    conversionsExtension(Matrix, Matrix_prototype, isNode, createCanvas);
    colorExtension(Matrix, Matrix_prototype, isNode, createCanvas);
    miscExtension(Matrix, Matrix_prototype, isNode, createCanvas);
    filteringExtension(Matrix, Matrix_prototype, isNode, createCanvas);
    morphoFilteringExtension(Matrix, Matrix_prototype, isNode, createCanvas);


    //////////////////////////////////////////////////////////////////
    //                     MISCELLANEOUS FUNCTIONS                  //
    //////////////////////////////////////////////////////////////////


    /** @class Matrix */

    /** Display an image into an HTML5 canvas element.
    *
    * __Also see:__
    *  {@link Matrix#imagesc},
    *  {@link Matrix#imread}.
    *
    * @param {String|HTMLCanvasElement} canvas
    *  Can be either a canvas `id` or a canvas object.
    *
    * @param {Number|String} [scale=1]
    *  Can be a number providing the magnification factor or `fit`
    *  specifying that the image will fit the canvas dimension.
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.imshow = function (canvas, scale) {
        if (isNode) {
            console.warn("Matrix.imshow: function not available in nodejs.");
            return;
        }

        var errMsg = this.constructor.name + '.imshow: ';
        // TODO: We should check the image configuration
        var [width, height] = this.getSize();

        // Optional parameters
        if (typeof canvas === 'string' && document.getElementById(canvas)) {
            canvas = document.getElementById(canvas);
        }
        var w;
        if (canvas === undefined || canvas === null) {
            canvas = document.createElement("canvas");
            w = window.open("", "", "width=" + width, "height=" + height);
            w.document.body.appendChild(canvas);
        }

        if (!(canvas instanceof HTMLCanvasElement)) {
            throw new Error(errMsg + 'Invalid canvas.');
        }

        var imageData = this.getImageData();
        if (scale === undefined || scale === 1) {
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').putImageData(imageData, 0, 0);
        } else {
            if (scale === 'fit') {
                // Compute the scale
                var hScale = canvas.width / width;
                var vScale = canvas.height / height;
                scale = Math.min(hScale, vScale);
                scale = scale > 1 ? 1 : scale;
            } else if (typeof scale !== 'number') {
                throw new Error(errMsg + 'scale must be a number or \'fit\'');
            }
            canvas.width = Math.round(width * scale);
            canvas.height = Math.round(height * scale);

            var canvasTmp = document.createElement('canvas');
            canvasTmp.width = width;
            canvasTmp.height = height;
            var ctxTmp = canvasTmp.getContext('2d');
            ctxTmp.putImageData(imageData, 0, 0);
            canvas.getContext('2d')
            .drawImage(
                canvasTmp,
                0, 0, width, height,
                0, 0, canvas.width, canvas.height
            );
        }

        return w || this;
    };
    /** Display a Matrix into an HTML5 canvas element in a popup
    * and open the the print menu.
    *
    * __Also see:__
    *  {@link Matrix#imshow}.
    *
    * @chainable
    */

    Matrix_prototype.print = function () {
        var w = this.imshow();
        w.print();
        w.close();
        return this;
    };

    /** Display a Matrix into an HTML5 canvas element by streching
    * the values in order to fit the display range.
    *
    * __Also see:__
    *  {@link Matrix#imshow}.
    *
    * @param {String|HTMLCanvasElement} canvas
    *  Can be either a canvas `id` or a canvas object.
    *
    * @param {Number|String} [scale=1]
    *  Can be a number providing the magnification factor or `fit`
    *  specifying that the image will fit the canvas dimension.
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.imagesc = function (canvas, scale) {
        var min = this.min().asScalar(), max = this.max().asScalar();
        if (min - max === 0) {
            return this['-'](min).imshow(canvas, scale);
        }
        return this['-'](min)['./'](max - min).imshow(canvas, scale);
    };

    /** Apply an affine transformation to an Image.
    * __Only works with `uint8` images.__
    *
    * @param {Matrix} transform
    *  3x3 Matrix.
    *
    * @return {Matrix}
    */
    Matrix_prototype.imtransform = function (transform) {

        transform = Matrix.from(transform);

        // TODO: we should get matrix image transpoition information to do that.
        var [w, h] = this.getSize();
        var c1 = createCanvas(w, h), ctx1 = c1.getContext("2d");
        var c2 = createCanvas(), ctx2 = c2.getContext("2d");

        var p = Matrix.from([0, 0, 1, w, 0, 1, w, h, 1, 0, h, 1]).reshape([3, 4]);
        var pp = transform.mtimes(p).transpose();
        var max = pp.max(0).getData(), min = pp.min(0).getData();
        var imageData = this.getImageData();
        ctx1.putImageData(imageData, 0, 0);

        var d = transform.getData();
        c2.width = max[0] - min[0];
        c2.height = max[1] - min[1];
        ctx2.translate(-min[0], -min[1]);
        ctx2.transform(d[0], d[1], d[3], d[4], d[6], d[7]);
        ctx2.drawImage(c1, 0, 0);
        return Matrix.imread(c2).convertImage(this.type());
    };

    /** Compute the histogram of a grey-level image.
    *
    * @param {Matrix} [bins=256]
    * number of bins used for the histogram.
    *
    * @return {Matrix}
    */
    Matrix_prototype.imhist = function (bins) {

        if (!this.ismatrix()) {
            throw new Error("Matrix.imhist: This function only works on grey-level images.");
        }

        var M;
        if (this.isinteger()) {
            M = Matrix.intmax(this.type());
            bins = bins ? bins : M;
        } else if (this.islogical()) {
            M = 1;
            bins = 2;
        } else if (this.isfloat()) {
            M = 1;
            bins = bins ? bins : 100;
        } else {
            throw new Error("Matrix.imhist: unknow data type.");
        }
        var data = this.getData();
        var hist = Matrix.zeros(bins, 1), hd = hist.getData();

        var i, ie, cst = bins / M;
        for (i = 0, ie = data.length; i < ie; i++) {
            var indice = data[i] * cst | 0;
            if (indice < 0) {
                hd[0]++;
            } else if(indice >= bins) {
                hd[bins - 1]++;
            } else {
                hd[indice]++;
            }
        }
        return hist;
    };

    /** Transform a RGB image to a gray level image.
    *  Grey level is computed as 0.3 * R + 0.59 * G + 0.11 * B.
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.rgb2gray = function () {
        if (this.ndims() !== 3 || this.getSize(2) < 3) {
            throw new Error('Matrix.rgb2gray: Matrix must be an RGB image.');
        }

        // Scaning the from the second dimension (dim = 1)
        var sizeOut = this.getSize();
        sizeOut[2] -= 2; // this is to deal when alpha channel is there
        var imOut = new Matrix(sizeOut, this.getDataType());
        var id = this.getData(), od = imOut.getData();

        // Iterator to scan the view
        var view = this.getView();
        var ly = view.getEnd(0), ny;
        var dx = view.getStep(1), lx = view.getEnd(1), nx;
        var dc = view.getStep(2);

        var x, y0, y1, y2;
        for (x = 0, nx = lx; x !== nx; x += dx) {
            y0 = x;
            y1 = x + dc;
            y2 = x + 2 * dc;
            for (ny = x + ly; y0 < ny; y0++, y1++, y2++) {
                od[y0] = 0.3 * id[y0] + 0.59 * id[y1] + 0.11 * id[y2];
            }
        }

        // Copy alpha channel
        if (this.getSize(2) === 4) {
            var alphaOut = od.subarray(dc);
            var alphaIn = id.subarray(3 * dc);
            alphaOut.set(alphaIn);
        }

        return imOut;
    };

    /** Compute the PSNR of two signal of the same size.
    * __See also :__
    * {@link Matrix#norm}.
    * @param {Matrix} signal
    * @param {Matrix} ref
    * @return {Matrix}
    *  Scalar Matrix containing the PSNR value.
    * @method psnr
    */
    Matrix.psnr = function (A, B, peakval) {
        A = Matrix.from(A);
        B = Matrix.from(B);
        Check.checkSizeEquals(A.size(), B.size(), Matrix.ignoreTrailingDims);
        if (!Check.isSet(peakval)) {
            var tA = A.type(), tB = B.type();
            var peakval1 = A.isfloat() ? 1 : Matrix.intmax(tA) - Matrix.intmin(tB);
            var peakval2 = B.isfloat() ? 1 : Matrix.intmax(tB) - Matrix.intmin(tB);
            peakval = Math.max(peakval1, peakval2);
        } else {
            peakval = 1;
        }
        var dRef = B.getData(), d2 = A.getData();
        var i, ie, ssd = 0;
        for (i = 0, ie = d2.length; i < ie; i++) {
            var tmp = dRef[i] - d2[i];
            ssd += tmp * tmp;
        }
        return Matrix.from(10 * Math.log10(peakval * peakval * ie / ssd));
    };

}
