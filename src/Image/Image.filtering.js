import {Check} from "@etsitpab/matrixview";

export default function imageFilteringExtension (Matrix, Matrix_prototype) {

    //////////////////////////////////////////////////////////////////
    //                        FILTERING MODULE                      //
    //////////////////////////////////////////////////////////////////


    /** Return some kernels.
    *
    * __Also see:__
    * {@link Matrix#imfilter}
    *
    * @param {String} type
    *  Can be 'average', 'disk', 'gaussian', 'log', 'unsharp', 'prewitt'
    *  or'sobel'.
    *
    * @param {String} parameter1
    *
    * @param {String} parameter2
    *
    * @return {Matrix}
    *
    * @todo
    *  Not every filter works add documentation on filter parameters.
    *  Meanwhile, have a look to the matlab documentation.
    */
    Matrix.fspecial = function (type, p1, p2) {
        var a, xsize, ysize, sigma;
        var data, n1, n2, _j, ij, e_j, eij, sum;
        var tmp, i, ei;
        var gaussian = function (n1, n2) {
            return Math.exp(-(n1 * n1 + n2 * n2) / (2 * sigma * sigma));
        };
        // var log = function (n1, n2) {
        //     return (n1 * n1 + n2 * n2 - 2 * sigma * sigma) * gaussian(n1, n2) / (2 * Math.PI * Math.pow(sigma, 6));
        // };
        switch (type.toLowerCase()) {
            case 'average':
            if (Check.isArrayLike(p1)) {
                ysize = p1[0];
                xsize = p1[1];
            } else {
                ysize = (p1 === undefined) ? 3 : p1;
                xsize = ysize;
            }
            return this.ones([ysize, xsize])['./'](ysize * xsize);
            case 'disk':
            break;
            case 'gaussian':
            if (Check.isArrayLike(p1)) {
                ysize = p1[0];
                xsize = p1[1];
            } else {
                ysize = (p1 === undefined) ? 3 : p1;
                xsize = ysize;
            }
            sigma = (p2 === undefined) ? 0.5 : p2;
            data = [];
            sum = 0;
            for (_j = 0, n1 = -(xsize - 1) / 2, e_j = xsize * ysize; _j < e_j; _j += ysize, n1++) {
                for (ij = _j, eij = _j + ysize, n2 = -(ysize - 1) / 2; ij < eij; ij++, n2++) {
                    tmp = gaussian(n1, n2);
                    data[ij] = tmp;
                    sum += tmp;
                }
            }
            for (i = 0, ei = data.length; i < ei; i++) {
                data[i] /= sum;
            }

            return new this([ysize, xsize], data);
            case 'laplacian':
            a = (p1 === undefined) ? 0.2 : p1;
            return new this([3, 3], [a / 4, (1 - a) / 4, a / 4, (1 - a) / 4, -1, (1 - a) / 4, a / 4, (1 - a) / 4, a / 4])['.*'](4 / (a + 1));
            case 'log':
            if (Check.isArrayLike(p1)) {
                ysize = p1[0];
                xsize = p1[1];
            } else {
                ysize = (p1 === undefined) ? 3 : p1;
                xsize = ysize;
            }
            sigma = (p2 === undefined) ? 0.5 : p2;
            data = [];
            sum = 0;
            for (_j = 0, n1 = -(xsize - 1) / 2, e_j = xsize * ysize; _j < e_j; _j += ysize, n1++) {
                for (ij = _j, eij = _j + ysize, n2 = -(ysize - 1) / 2; ij < eij; ij++, n2++) {
                    tmp = gaussian(n1, n2);
                    data[ij] = (n1 * n1 + n2 * n2 - 2 * sigma * sigma) * tmp / Math.pow(sigma, 4);
                    sum += tmp;
                }
            }
            for (i = 0, ei = data.length; i < ei; i++) {
                data[i] /= sum;
            }
            sum = 0;
            for (i = 0, ei = data.length; i < ei; i++) {
                sum += data[i];
            }
            sum /= xsize * ysize;
            for (i = 0, ei = data.length; i < ei; i++) {
                data[i] -= sum;
            }
            return new this([ysize, xsize], data);
            case 'unsharp':
            a = (p1 === undefined) ? 0.2 : p1;
            return new this([3, 3], [-a, a - 1, -a, a - 1, a + 5, a - 1, -a, a - 1, -a])['.*'](1 / (a + 1));
            case 'prewitt':
            return new this([3, 3], [1, 0, -1, 1, 0, -1, 1, 0, -1]);
            case 'sobel':
            return new this([3, 3], [1, 0, -1, 2, 0, -2, 1, 0, -1]);
            default:
            return;
        }
    };

    Matrix_prototype.filter1d = function (kernel, origin) {

        // 1. ARGUMENTS
        var errMsg = this.constructor.name + '.filter1d: ';

        // origin
        if (origin === undefined) {
            origin = 'C';
        }

        if (!kernel.isvector()) {
            throw new Error("Matrix.filter1d: Kernel must be a vector");
        }
        var K = kernel.getLength(), kd = kernel.getData();
        if (typeof origin === 'string') {
            switch (origin.toUpperCase()) {
                case 'C':
                case 'CL':
                origin = Math.floor((K - 1) / 2);
                break;
                case 'CR':
                origin = Math.ceil((K - 1) / 2);
                break;
                case 'L':
                origin = 0;
                break;
                case 'R':
                origin = K - 1;
                break;
                default:
                throw new Error(errMsg + "unknown origin position '" + origin + "'");
            }
        } else if (typeof origin  === 'number') {
            if (origin < 0) {
                origin += K;
            }
            if (origin < 0 || origin >= K) {
                throw new Error(errMsg + "origin value must satisfy : |origin| < kernel.length");
            }
        }

        // 2. Filtering
        var output = new this.constructor(this.getSize(), this.getDataType());
        var id = this.getData(), od = output.getData();

        // Iterator to scan the view
        var view = output.getView();
        var dc = view.getStep(2), lc = view.getEnd(2);
        var dx = view.getStep(1), lx = view.getEnd(1);
        var ly = view.getEnd(0);

        if (origin >= ly / 2) {
            throw new Error('Matrix.filter1d: Kernel is too large.');
        }

        var sum, stop = ly - origin;
        var c, x, nx, y, ny, j, k;
        for (c = 0; c !== lc; c += dc) {
            for (x = c, nx = c + lx; x !== nx; x += dx) {
                for (y = x, ny = x + origin; y < ny; y++) {
                    // This loop code the symmetry
                    for (sum = 0, k = 0, j = 2 * x + origin - y; j > x; k++, j--) {
                        sum += kd[k] * id[j];
                    }
                    for (j = x; k < K; k++, j++) {
                        sum += kd[k] * id[j];
                    }
                    od[y] = sum;
                }
                for (y = x + origin, ny = x + stop; y < ny; y++) {
                    for (sum = 0, k = 0, j = y - origin; k < K; k++, j++) {
                        sum += kd[k] * id[j];
                    }
                    od[y] = sum;
                }
                for (y = x + stop, ny = x + ly; y < ny; y++) {
                    for (sum = 0, k = 0, j = y - origin; j < ny; k++, j++) {
                        sum += kd[k] * id[j];
                    }
                    // This loop code the symmetry
                    for (j = ny - 2; k < K; k++, j--) {
                        sum += kd[k] * id[j];
                    }
                    od[y] = sum;
                }
            }
        }

        // Return the result
        return output;
    };
    /** Apply different filters on rows and columns.
    *
    * @param {Matrix} filterX
    *
    * @param {Matrix} [filterY=filterX]
    *
    * @return {Matrix}
    */
    Matrix_prototype.separableFilter = function (hKernel, vKernel) {
        if (vKernel === undefined) {
            vKernel = hKernel;
        }
        return this
        .filter1d(hKernel).permute([1, 0, 2])
        .filter1d(vKernel).permute([1, 0, 2]);
    };
    /** 2D gaussian blur.
    *
    * __Also see:__
    * {@link Matrix#fastBlur}.
    *
    * @param {Number} sigmaX
    *
    * @param {Number} [sigmaY=sigmaX]
    *
    * @param {Integer} [precision=3]
    *  High number increases the computational time as well as the
    *  quality of the filtering.
    *
    * @return {Matrix}
    */
    Matrix_prototype.gaussian = function (sigmaX, sigmaY, precision) {
        precision = precision || 3;
        var kernelX = Kernel.gaussian(sigmaX, 0, precision);
        var kernelY;
        if (typeof sigmaY === "number") {
            kernelY = Kernel.gaussian(sigmaY, 0, precision);
        } else {
            kernelY = kernelX;
        }
        return this.separableFilter(kernelX, kernelY);
    };
    /** Compute image derivative using a gaussian kernel.
    * Gaussian kernel is computed with 'kernel.gaussian (sigma, 3)'
    * which ensures a good accuracy but takes time.
    *
    * @param {Number} sigma
    *  Derivative order (0, 1) for the X kernel.
    *
    * @returns {Object}
    *  Out image derivatives (Object.{x, y, norm, phase}).
    *
    *     // Compute the gradient
    *     var gradient = im.gaussianGradient(1);
    */
    Matrix_prototype.gaussianGradient = function (sigma) {
        if (!sigma) {
            sigma = 2;
        }
        var kernel1 = Kernel.gaussian(sigma, 1, 3);
        var kernel2 = Kernel.gaussian(sigma, 0, 3);

        var x = this.separableFilter(kernel2, kernel1);
        var y = this.separableFilter(kernel1, kernel2);

        var IPI2 = 0.5 / Math.PI;

        var n = this.constructor.zeros(this.getSize()), p = this.constructor.zeros(this.getSize());
        var xData = x.getData(), yData = y.getData();
        var nData = n.getData(), pData = p.getData();

        var i, ie;
        for (i = 0, ie = xData.length; i < ie; i++) {
            var a = xData[i], b = yData[i];
            nData[i] = Math.sqrt(a * a + b * b);
            var ph = Math.atan2(b, a) * IPI2;
            pData[i] = ph < 0 ? ph + 1 : ph;
        }

        return {x: x, y: y, norm: n, phase: p};
    };
    /** Compute various differential operators on an image
    * with discret schemes.
    *
    * @param {Boolean} gradx
    *
    * @param {Boolean} grady
    *
    * @param {Boolean} norm
    *
    * @param {Boolean} phase
    *
    * @param {Boolean} laplacian
    *
    * @return {Object}
    *  Return an object with the requested properties.
    *
    * @author
    *  This function was imported from the [Megawave library][1].
    *  And then adapted to work with the Matrix class.
    *  [1]: http://megawave.cmla.ens-cachan.fr/
    */
    Matrix_prototype.gradient = function (gradx, grady, norm, phase, laplacian) {
        Matrix = this.constructor;
        var IRAC2   = 0.70710678, RAC8P4  = 6.8284271, IRAC8 = 0.35355339;
        var IRAC2P2 = 0.29289322, IRAC8P4 = 1 / RAC8P4, IPI2 =  0.5 / Math.PI;

        var gradient = {}, xData, yData, nData, pData, lData;
        var size = this.getSize();
        var type = this.getDataType();
        if (gradx) {
            gradient.x = new Matrix(size, type);
            xData = gradient.x.getData();
        }
        if (grady) {
            gradient.y = new Matrix(size, type);
            yData = gradient.y.getData();
        }
        if (norm) {
            gradient.norm = new Matrix(size, type);
            nData = gradient.norm.getData();
        }
        if (phase) {
            gradient.phase = new Matrix(size, type);
            pData = gradient.phase.getData();
        }
        if (laplacian) {
            gradient.laplacian = new Matrix(size, type);
            lData = gradient.laplacian.getData();
        }

        var id = this.getData();

        // Iterator to scan the view
        var view = this.getView();
        var dc = view.getStep(2), lc = view.getEnd(2);
        var dx = view.getStep(1), lx = view.getEnd(1);
        var ly = view.getEnd(0);

        var c, x, nx, y, ny, y0, y1;
        for (c = 0; c !== lc; c += dc) {
            for (x = c + dx, nx = c + lx - dx; x !== nx; x += dx) {
                for (y0 = x - dx, y = x, y1 = x + dx, ny = x + ly - 2; y < ny; y) {
                    var a00 = id[y0], a01 = id[++y0], a02 = id[y0 + 1];
                    var a10 = id[y],  a11 = id[++y],  a12 = id[y  + 1];
                    var a20 = id[y1], a21 = id[++y1], a22 = id[y1 + 1];

                    var c1 = a22 - a00, d1 = a02 - a20;
                    var ax = IRAC2P2 * (a21 - a01 + IRAC8 * (c1 - d1));
                    var ay = IRAC2P2 * (a10 - a12 - IRAC8 * (c1 + d1));

                    if (gradx) {
                        xData[y] = ax;
                    }
                    if (grady) {
                        yData[y] = ay;
                    }
                    if (norm) {
                        nData[y] = Math.sqrt(ax * ax + ay * ay);
                    }
                    if (phase) {
                        var ap = Math.atan2(-ay, ax) * IPI2;
                        pData[y] = (ap < 0) ? (ap + 1) : ap;
                    }
                    if (laplacian) {
                        lData[y] =
                        (IRAC2 * (a00 + a02 + a20 + a22) + (a10 + a01 + a21 + a12)) *
                        IRAC8P4 - a11;
                    }
                }
            }
        }

        return gradient;
    };

    /** Performs an 1D convolution between two vectors.
    *
    * @param {Function} vect
    *
    * @param {String} shape
    *  Can be "full", "same" or "valid".
    *
    * @return {Matrix}
    * @fixme This function must not be in image processing group.
    */
    Matrix_prototype.conv = function (vect, shape) {
        if (!this.isvector() || !vect.isvector()) {
            throw new Error("Matrix.conv: Input must be vector.");
        }

        var id1 = this.getData(), n1 = id1.length;
        var id2 = vect.getData(), n2 = id2.length;

        if (n1 < n2) {
            return vect.conv(this, shape);
        }
        var Type = Check.getTypeConstructor(this.getDataType());
        var od = new Type(n1 + n2 - 1), no = od.length;

        var j0, j, nj, i, x, nx, sum;

        // Initial zero padding
        for (x = 0, nx = n2 - 1; x < nx; x++) {
            for (sum = 0, j = 0, nj = x + 1, i = x; j < nj; j++, i--) {
                sum += id1[j] * id2[i];
            }
            od[x] = sum;
        }
        // Central part
        for (x = n2 - 1, j0 = 0, nx = n1; x < nx; x++, j0++) {
            for (sum = 0, j = j0, nj = x + 1, i = n2 - 1; j < nj; j++, i--) {
                sum += id1[j] * id2[i];
            }
            od[x] = sum;
        }
        // Final zero padding
        for (x = n1, j0 = n1 - n2 + 1; x < no; x++, j0++) {
            for (sum = 0, j = j0, i = n2 - 1; j < n1; j++, i--) {
                sum += id1[j] * id2[i];
            }
            od[x] = sum;
        }
        let Matrix = this.constructor;
        switch (shape) {
            case undefined:
            case 'full':
            return new Matrix(no, od);
            case 'same':
            var orig = Math.ceil(n2 / 2);
            return new Matrix(n1, od.subarray(orig, orig + n1));
            case 'valid':
            return new Matrix(n1 - n2 + 1, od.subarray(n2 - 1, n1));
            default:
            throw new Error("Matrix.conv: Invalid shape parameter.");
        }
    };

    (function () {
        var computeImageIntegral = function(im) {
            var view = im.getView(), d = im.getData();
            var dc = view.getStep(2), lc = view.getEnd(2);
            var dx = view.getStep(1), lx = view.getEnd(1), nx;
            var ly = view.getEnd(0), ny;
            var c, y, x;
            for (c = 0; c < lc; c += dc) {
                for (y = c + 1, ny = c + ly; y < ny; y++) {
                    d[y] += d[y - 1];
                }
                for (x = c + dx, nx = c + lx; x < nx; x += dx) {
                    var sum = d[x];
                    d[x] += d[x - dx];
                    for (y = x + 1, ny = x + ly; y < ny; y++) {
                        sum += d[y];
                        d[y] = d[y - dx] + sum;
                    }
                }
            }
        };
        var meanFilter = function (imcum, imout, wx, wy) {
            var view = imcum.getView();
            var dc = view.getStep(2), lc = view.getEnd(2);
            var dx = view.getStep(1), lx = view.getEnd(1);
            var ly = view.getEnd(0);

            var sy = (wy / 2) | 0;
            var sx = ((wx / 2) | 0) * dx;
            var sx2 = ((wx / 2) | 0);

            var din = imcum.getData(), dout = imout.getData();
            var e = (sx + sy + dx + 1), f = sx + sy, g = -(sx + dx) + sy, h = sx - sy - 1;
            var dinf = din.subarray(f), dinh = din.subarray(h);
            var nx, ny, c, x, y, y_, yx;
            var cst, csty, cste;

            for (c = 0; c < lc; c += dc) {

                // First rows
                for (y_ = c, y = 0, ny = c + sy + 1; y_ < ny; y_++, y++) {
                    csty = (y + sy + 1);
                    // First columns
                    for (yx = y_, x = 0, nx = y_ + sx + dx; yx < nx; yx += dx, x++) {
                        dout[yx] = dinf[yx] / ((x + sx2 + 1) * csty);
                    }
                    cst = 1 / (wx * csty);
                    // Central columns
                    for (yx = y_ + sx + dx, nx = y_ + lx - sx; yx < nx; yx += dx) {
                        dout[yx] = (dinf[yx] - din[yx + g]) * cst;
                    }
                    cste = din[y_ + lx - dx + sy];
                    // Last columns
                    for (yx = y_ + lx - sx, nx = y_ + lx; yx < nx; yx += dx) {
                        dout[yx] =  cste - din[yx + g];
                        dout[yx] /= ((sx + nx - yx) / dx) * csty;
                    }
                }

                // First columns
                for (x = c, nx = c + sx + dx; x < nx; x += dx) {
                    // Central part
                    cst = 1 / (((x - c + sx) / dx + 1) * wy);
                    for (y = x + sy + 1, ny = x + ly - sy; y < ny; y++) {
                        dout[y] = dinf[y] - dinh[y];
                        dout[y] *= cst;
                    }
                }
                // Central part
                cst = 1 / (wx * wy);
                for (x = c + sx + dx, nx = c + lx - sx; x < nx; x += dx) {
                    // Central part
                    for (y = x + sy + 1, ny = x + ly - sy; y < ny; y++) {
                        dout[y] = (din[y - e] + dinf[y] - din[y + g] - dinh[y]) * cst;
                    }
                }
                // Last columns
                for (x = c + lx - sx, nx = c + lx; x < nx; x += dx) {
                    // Central part
                    cst = 1 / (((c + lx - x + sx) / dx) * wy);
                    for (y = x + sy + 1, ny = x + ly - sy; y < ny; y++) {
                        dout[y] = din[y - e] + din[c + lx - dx + y - x + sy] - din[c + lx - dx + y - x - sy - 1] - din[y + g];
                        dout[y] *= cst;
                    }
                }

                // last rows
                for (y = c + ly - sy, ny = c + ly; y < ny; y++) {
                    // First columns
                    for (x = y, nx = y + sx + dx; x < nx; x += dx) {
                        dout[x] = din[x - y + c + ly - 1 + sx] - dinh[x];
                        dout[x] /= ((x - y + sx) / dx + 1) * (sy + (ly - (y - c)));
                    }
                    cst = 1 / (wx * (sy + (ly - (y - c))));
                    // Central columns
                    for (x = y + sx + dx, nx = y + lx - sx; x < nx; x += dx) {
                        dout[x] = din[x - y + c + ly - 1 + sx] - din[x - y + c + ly - 1 - sx - dx] + din[x - e] - dinh[x];
                        dout[x] *= cst;
                    }
                    // Last columns
                    for (x = y + lx - sx, nx = y + lx; x < nx; x += dx) {
                        dout[x] = din[c + lx - dx + ly - 1] + din[x - e] - din[c + x - y + ly - 1 - sx - dx] - din[c + lx - dx + y - c - sy - 1];
                        dout[x] /= ((lx - (x - y) + sx) / dx) * (sy + (ly - (y - c)));
                    }
                }
            }
        };

        /** Gaussian bluring based on box filtering.
        * It computes a fast approximation of gaussian blur
        * in constant time.
        *
        * @param {Number} sigmaX
        *  Standard deviation of the gausian.
        * @param {Number} [sigmaY=sigmaX]
        * @param {Number} [k=2]
        *  Number of times than the image is boxfiltered.
        * @return {Matrix}
        */
        Matrix_prototype.fastBlur = function (sx, sy, k) {
            k = k || 3;
            sy = sy || sx;
            var wx = Math.round(Math.sqrt(12 / k * sx * sx + 1) / 2) * 2 + 1;
            var wy = Math.round(Math.sqrt(12 / k * sy * sy + 1) / 2) * 2 + 1;
            if (wy > this.getSize(0)) {
                throw new Error("Matrix.fastBlur: sigma on y axis is too large.");
            }
            if (wx > this.getSize(1)) {
                throw new Error("Matrix.fastBlur: sigma on x axis is too large.");
            }
            var imout = this.constructor.zeros(this.getSize(), this.getDataType()),
            imcum = this.im2double();
            for (var p = 0; p < k; p++) {
                computeImageIntegral(imcum);
                meanFilter(imcum, imout, wx, wy);
                var tmp = imout;
                imout = imcum;
                imcum = tmp;
            }
            return tmp;
        };

        /** Box filtering. Compute the average of square patches in constant time
        * using integral image.
        * @param {Number} wx
        *  x size of the box filter
        * @param {Number} [wy=wx]
        *  y size of the box filter
        * @return {Matrix}
        */
        Matrix_prototype.boxFilter = function (wx, wy, imcum) {
            wy = wy === undefined ? wx : wy;
            var imout = this.constructor.zeros(this.getSize(), this.getDataType());
            if (imcum === undefined) {
                imcum = this.im2double();
                computeImageIntegral(imcum);
            }
            meanFilter(imcum, imout, wx, wy);
            return imout;
        };
    })();
}

//////////////////////////////////////////////////////////////////
//                          KERNEL TOOLS                        //
//////////////////////////////////////////////////////////////////


/** Holds kernels generation for filtering.
* @private
*/
var Kernel = {};
/** Normalize a kernel.
* Normalization such that its L1 norm is 1.
*
* @param {Array} kernel
*  The kernel.
*
* @return {Array}
*  The same array, but normalized.
*/
Kernel.normalize = function (kernel) {
    var i;
    var N = kernel.length;

    // L1 norm of the kernel
    var sum = 0;
    for (i = 0; i < N; i++) {
        sum += Math.abs(kernel[i]);
    }

    // Normalize
    if (sum !== 0) {
        for (i = 0; i < N; i++) {
            kernel[i] /= sum;
        }
    }

    // Return it
    return kernel;
};
/** Compute a gaussian kernel and its derivatives.
*
* @param {Number} sigma
*   Standard deviation of kernel
*
* @param {Integer} [order=0]
*   Derivative order: 0, 1 or 2
*
* @param {Number} [precision=3.0]
*   Precision of the kernel
*
* @return {Float32Array}
*   The gaussian Kernel
*/
Kernel.gaussian = function (sigma, order, precision) {
    var i, x;

    // Kernel parameters
    if (precision === undefined) {
        precision = 3;
    }
    if (order === undefined) {
        order = 0;
    }

    var size = 1 + 2 * Math.ceil(sigma * Math.sqrt(precision * 2 * Math.log(10)));
    if (size === 1) {
        return new Matrix([1, 1], [1]);
    }
    var kerOut = new Matrix.dataType(size);
    var shift = (size - 1) / 2;
    var sum = 0, abs = Math.abs;
    for (i = 0; i < (size + 1) / 2; i++) {
        x = i - shift;
        var tmp = 1 / (Math.sqrt(2 * Math.PI) * sigma);
        tmp *= Math.exp(-(x * x) / (2 * sigma * sigma));
        kerOut[i] = kerOut[size - 1 - i] = tmp;
    }

    // Generate the kernel
    switch (order) {

        case 0:
        for (i = 0, sum = 0; i < kerOut.length; i++) {
            sum += abs(kerOut[i]);
        }
        break;

        case 1:
        for (i = 0, sum = 0; i < kerOut.length; i++) {
            x = i - shift;
            kerOut[i] *= -x / Math.pow(sigma, 2);
            sum += abs(x * kerOut[i]);
        }
        break;

        case 2:
        for (i = 0, sum = 0; i < kerOut.length; i++) {
            x = i - shift;
            kerOut[i] *= (x * x / Math.pow(sigma, 4) - 1 / Math.pow(sigma, 2));
            sum += abs(kerOut[i]);
        }
        sum /= kerOut.length;
        for (i = 0; i < kerOut.length; i++) {
            kerOut[i] -= abs(sum);
        }
        for (i = 0, sum = 0; i < kerOut.length; i++) {
            x = i - shift;
            sum += abs(0.5 * x * x * kerOut[i]);
        }
        break;

        default:
        throw new Error('Kernel.gaussian: Derive order can be 0,1 or 2 but not ' + order);
    }

    if (sum !== 0) {
        for (i = 0; i < kerOut.length; i++) {
            kerOut[i] /= sum;
        }
    }

    return new Matrix(kerOut.length, kerOut);
};
