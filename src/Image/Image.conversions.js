export default function imageExtension (Matrix, Matrix_prototype, isNode, createCanvas) {


    //////////////////////////////////////////////////////////////////
    //                    IMAGES CONVERSION MODULE                  //
    //////////////////////////////////////////////////////////////////


    /** Image cast function.
    * @param {Object} image
    * @param {String} type
    * @return {Matrix}
    * @private
    */
    Matrix_prototype.convertImage = function (type) {
        let inputRange, outputRange, cstMin = 0, cstMax = 0;
        if (this.isfloat() || this.islogical()) {
            [cstMin, cstMax] = [-0.5, -0.5]; // Constant use to properly convert to integer
            inputRange = [0, 1];
        } else if (this.isinteger()) {
            [cstMin, cstMax] = [0, 1];
            inputRange = [Matrix.intmin(this.type()), Matrix.intmax(this.type())];
        } else {
            throw new Error("Matrix.convertImage: Unknown input range type.");
        }
        const a = inputRange[0], b = 1 / (inputRange[1] - inputRange[0]);

        const output = new Matrix(this.getSize(), type);
        if (output.isfloat() || this.islogical()) {
            outputRange = [0, 1];
        } else if (output.isinteger()) {
            if (output.type() === "uint8c") {
                // we should do something special here.
            }
            outputRange = [Matrix.intmin(output.type()) + cstMin, Matrix.intmax(output.type()) + cstMax];
        } else {
            throw new Error("Matrix.convertImage: Unknown output range type.");
        }
        const c = outputRange[0], d = b * (outputRange[1] - outputRange[0]);

        const id = this.getData(), od = new output.getData();
        if (a === 0 && c === 0) {
            console.log(this.type(), output.type(), inputRange, outputRange, a, d, c);
            for (let i = 0, ie = id.length; i < ie; i++) {
                od[i] = id[i] * d;
            }
        } else {
            console.log(this.type(), output.type(), inputRange, outputRange, a, d, c);
            for (let i = 0, ie = id.length; i < ie; i++) {
                od[i] = (id[i] - a) * d - c;
            }
        }
        return new Matrix(this.getSize(), od);
    };
    // double uint8 [ 0, 1 ] [ -0.5, 254.5 ] 0 255 -0.5
    // uint16 uint8 [ 0, 65535 ] [ 0, 256 ] 0 0.003906309605554284 0
    // uint32 uint8 [ 0, 4294967295 ] [ 0, 256 ] 0 5.960464478926841e-8 0

    const float2uint8 = function (input, output) {
        // double uint8 [ 0, 1 ] [ -0.5, 254.5 ] 0 255 -0.5
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] * 255 + 0.5;
        }
        return output;
    };
    const float2uint16 = function (input, output) {
        // double uint16 [ 0, 1 ] [ -0.5, 65534.5 ] 0 65535 -0.5
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] * 65535 + 0.5;
        }
        return output;
    };
    const float2uint32 = function (input, output) {
        // double uint32 [ 0, 1 ] [ -0.5, 4294967294.5 ] 0 4294967295 -0.5
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] * 4294967295 + 0.5;
        }
        return output;
    };

    const uint322float = function (input, output) {
        // uint8 double [ 0, 255 ] [ 0, 1 ] 0 0.00392156862745098 0
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] * 2.3283064370807974e-10;
        }
        return output;
    };
    const uint322uint16 = function (input, output) {
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] >> 16;
        }
        return output;    };
    const uint322uint8 = function (input, output) {
        // uint32 uint8 [ 0, 4294967295 ] [ 0, 256 ] 0 5.960464478926841e-8 0
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] >> 24;
        }
        return output;
    };

    const uint162float = function (input, output) {
        // uint8 double [ 0, 255 ] [ 0, 1 ] 0 0.00392156862745098 0
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] * 0.00392156862745098;
        }
        return output;
    };
    const uint162uint32 = function (input, output) {
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] >> 16; // * 0.000015259021896696422;
        }
        return output;
    };
    const uint162uint8 = function (input, output) {
        // uint16 uint8 [ 0, 65535 ] [ 0, 256 ] 0 0.003906309605554284 0
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] >> 8; // * 0.003906309605554284;
        }
        return output;
    };

    const uint82float = function (input, output) {
        // uint8 double [ 0, 255 ] [ 0, 1 ] 0 0.00392156862745098 0
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] * 0.00392156862745098;
        }
        return output;
    };
    const uint82uint32 = function (input, output) {
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] * 16777217;
        }
        return output;
    };
    const uint82uint16 = function (input, output) {
        // uint8 uint16 [ 0, 255 ] [ 0, 65536 ] 0 257.00392156862745 0
        for (let i = 0, ie = input.length; i < ie; i++) {
            output[i] = input[i] * 257;
        }
        return output;
    };


    /** Cast image to `double` type
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.im2double = function () {
        const type = this.type(), size = this.size();
        if (type === "double" || type === "single") {
            return this[type]();
        }
        const output = new Matrix(size, "double");
        if (type === "uint32") {
            uint322float(this.getData(), output.getData());
        } else if (type === "uint16") {
            uint162float(this.getData(), output.getData());
        } else if (type === "uint8" || type === "uint8c") {
            uint82float(this.getData(), output.getData());
        } else {
            throw new Error(`Matrix.im2double: Conversion from type ${type} is not implemented.`);
        }
        return output;
    };
    /** Cast image to `single` type
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.im2single = function () {
        const type = this.type(), size = this.size();
        if (type === "double" || type === "single") {
            return this[type]();
        }
        const output = new Matrix(size, "single");
        if (type === "uint32") {
            uint322float(this.getData(), output.getData());
        } else if (type === "uint16") {
            uint162float(this.getData(), output.getData());
        } else if (type === "uint8" || type === "uint8c") {
            uint82float(this.getData(), output.getData());
        } else {
            throw new Error(`Matrix.im2single: Conversion from type ${type} is not implemented.`);
        }
        return output;
    };
    /** Cast image to uint8 type
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.im2uint8 = function () {
        const type = this.type();
        if (type === "uint8" || type === "uint8c") {
            return this[type]();
        }
        const size = this.size(), output = new Matrix(size, "uint8");
        if (type === "single" || type === "double") {
            float2uint8(this.getData(), output.getData());
        } else if (type === "uint32") {
            uint322uint8(this.getData(), output.getData());
        } else if (type === "uint16") {
            uint162uint8(this.getData(), output.getData());
        } else {
            throw new Error(`Matrix.im2uint8: Conversion from type ${type} is not implemented.`);
        }
        return output;
    };

    /** Cast image to uint8c type
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.im2uint8c = function () {
        const type = this.type(), size = this.size(), output = new Matrix(size, "uint8c");
        if (type === "uint8" || type === "uint8c") {
            return this[type]();
        }
        if (type === "single" || type === "double") {
            float2uint8(this.getData(), output.getData());
        } else if (type === "uint32") {
            uint322uint8(this.getData(), output.getData());
        } else if (type === "uint16") {
            uint162uint8(this.getData(), output.getData());
        } else {
            throw new Error(`Matrix.im2uint8: Conversion from type ${type} is not implemented.`);
        }
        return output;
    };

    /** Cast image to uint16 type
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.im2uint16 = function () {
        const type = this.type(), size = this.size(), output = new Matrix(size, "uint16");
        if (type === "single" || type === "double") {
            float2uint16(this.getData(), output.getData());
        } else if (type === "uint32") {
            uint322uint16(this.getData(), output.getData());
        } else if (type === "uint8") {
            uint82uint16(this.getData(), output.getData());
        } else {
            throw new Error(`Matrix.im2uint8: Conversion from type ${type} is not implemented.`);
        }
        return output;
    };
    /** Cast image to uint16 type
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.im2uint32 = function () {
        const type = this.type(), size = this.size(), output = new Matrix(size, "uint32");
        if (type === "single" || type === "double") {
            float2uint32(this.getData(), output.getData());
        } else if (type === "uint16") {
            uint162uint32(this.getData(), output.getData());
        } else if (type === "uint8") {
            uint82uint32(this.getData(), output.getData());
        } else {
            throw new Error(`Matrix.im2uint8: Conversion from type ${type} is not implemented.`);
        }
        return output;
    };

    (function () {
        // Transpose the image
        // const channels1 = function (dI, dO, width, height, a, b) {
        //     var nx = height * width, nx0;
        //     var xo, y, x0, vTmp;
        //     for (y = 0, xo = 0; y < height; y++) {
        //         for (x0 = y, nx0 = y + nx; x0 < nx0; x0 += height) {
        //             vTmp = (dI[x0] - a) * b | 0;
        //             dO[xo++] = vTmp;
        //             dO[xo++] = vTmp;
        //             dO[xo++] = vTmp;
        //             dO[xo++] = 255;
        //         }
        //     }
        // };
        // const channels2 = function (dI, dO, width, height, a, b) {
        //     var nx = height * width, nx0;
        //     var xo, y, x0, x1, vTmp;
        //     for (y = 0, xo = 0; y < height; y++) {
        //         for (x0 = y, x1 = y + nx, nx0 = y + nx; x0 < nx0; x0 += height, x1 += height) {
        //             vTmp = (dI[x0] - a) * b | 0;
        //             dO[xo++] = vTmp;
        //             dO[xo++] = vTmp;
        //             dO[xo++] = vTmp;
        //             dO[xo++] = (dI[x1] - a) * b | 0;
        //         }
        //     }
        // };
        // const channels3 = function (dI, dO, width, height, a, b) {
        //     var nx = height * width, nx0;
        //     var xo, y, x0, x1, x2;
        //     for (y = 0, xo = 0; y < height; y++) {
        //         for (x0 = y, x1 = y + nx, x2 = x1 + nx, nx0 = x1; x0 < nx0; x0 += height, x1 += height, x2 += height) {
        //             dO[xo++] = (dI[x0] - a) * b | 0;
        //             dO[xo++] = (dI[x1] - a) * b | 0;
        //             dO[xo++] = (dI[x2] - a) * b | 0;
        //             dO[xo++] = 255;
        //         }
        //     }
        // };
        // const channels4 = function (dI, dO, width, height, a, b) {
        //     var nx = height * width, nx0;
        //     var xo, y, x0, x1, x2, x3;
        //     for (y = 0, xo = 0; y < height; y++) {
        //         for (x0 = y, x1 = x0 + nx, x2 = x1 + nx, x3 = x2 + nx, nx0 = x1; x0 < nx0; x0 += height, x1 += height, x2 += height, x3 += height) {
        //             dO[xo++] = (dI[x0] - a) * b | 0;
        //             dO[xo++] = (dI[x1] - a) * b | 0;
        //             dO[xo++] = (dI[x2] - a) * b | 0;
        //             dO[xo++] = (dI[x3] - a) * b | 0;
        //         }
        //     }
        // };
        const channels1 = function (dI, dO, width, height, a, b) {
            const xye = height * width;
            let xy, xyo;
            for (xy = 0, xyo = 0; xy < xye; xy++) {
                const vTmp = (dI[xy] - a) * b | 0;
                dO[xyo++] = vTmp;
                dO[xyo++] = vTmp;
                dO[xyo++] = vTmp;
                dO[xyo++] = 255;
            }
        };
        const channels2 = function (dI, dO, height, width, a, b) {
            const xye = height * width;
            const dI0 = dI.subarray(0 * xye, 1 * xye),
                dI1 = dI.subarray(1 * xye, 2 * xye);
            let xy, xyo;
            for (xy = 0, xyo = 0; xy < xye; xy++) {
                const vTmp = (dI0[xy] - a) * b | 0;
                dO[xyo++] = vTmp;
                dO[xyo++] = vTmp;
                dO[xyo++] = vTmp;
                dO[xyo++] = (dI1[xy] - a) * b | 0;
            }
        };
        const channels3 = function (dI, dO, width, height, a, b) {
            const xye = height * width;
            const dI0 = dI.subarray(0 * xye, 1 * xye),
                dI1 = dI.subarray(1 * xye, 2 * xye),
                dI2 = dI.subarray(2 * xye, 3 * xye);
            let xy, xyo;
            for (xy = 0, xyo = 0; xy < xye; xy++) {
                dO[xyo++] = (dI0[xy] - a) * b | 0;
                dO[xyo++] = (dI1[xy] - a) * b | 0;
                dO[xyo++] = (dI2[xy] - a) * b | 0;
                dO[xyo++] = 255;
            }
        };
        const channels4 = function (dI, dO, width, height, a, b) {
            const xye = height * width;
            let xy, xyo;
            const dI0 = dI.subarray(0 * xye, 1 * xye),
                dI1 = dI.subarray(1 * xye, 2 * xye),
                dI2 = dI.subarray(2 * xye, 3 * xye),
                dI3 = dI.subarray(3 * xye, 4 * xye);
            for (xy = 0, xyo = 0; xy < xye; xy++) {
                dO[xyo++] = (dI0[xy] - a) * b | 0;
                dO[xyo++] = (dI1[xy] - a) * b | 0;
                dO[xyo++] = (dI2[xy] - a) * b | 0;
                dO[xyo++] = (dI3[xy] - a) * b | 0;
            }
        };

        let canvasCtx;
        /** Return the image in a array who can be displayed in
        * a `Canvas` element.
        *
        * @return {Uint8ClampedArray}
        */
        Matrix_prototype.getImageData = function () {
            let Matrix = this.constructor;
            // Input image range
            var range;
            if (this.isfloat()  || this.islogical()) {
                range = [0, 1];
            } else if (this.isinteger()) {
                range = [Matrix.intmin(this.type()), Matrix.intmax(this.type())];
            } else {
                throw new Error("Matrix.getImageData: Invalid matrix. should be either 'float', 'integer' or 'logical'.");
            }
            var a = range[0], b = 255 / (range[1] - range[0]);

            // Ouptut iterator
            // TODO: Check Matrix transposition option
            var width = this.getSize(0), height = this.getSize(1), nChannels = this.getSize(2);
            if (!canvasCtx) {
                canvasCtx = createCanvas().getContext('2d');
            }
            var imageData = canvasCtx.createImageData(width, height);
            var dI = this.getData(), dO = imageData.data;

            if (nChannels === 1) {
                channels1(dI, dO, width, height, a, b);
            } else if (nChannels === 2) {
                channels2(dI, dO, width, height, a, b);
            } else if (nChannels === 3) {
                channels3(dI, dO, width, height, a, b);
            } else if (nChannels === 4) {
                channels4(dI, dO, width, height, a, b);
            } else {
                throw new Error("Matrix.getImageData: Channels must be <= 4.");
            }
            return imageData;
        };
    })();

    if (isNode) {
        // WARNING: FOR NODEJS USE ONLY, THESE FUNCTIONS ARE NOT AVAILABLE IN A BROWSER.
        let fs;
        import("fs").then(mod => fs = mod);

        /** Allow to save an image on the Disk.
        * File extension must be be either a png or jpg valid extension.
        *
        *
        * @param {String} name
        *  Name of the file
        * @param {Function} callback
        *  Function to call when the conversion is done.
        * @matlike
        */
        Matrix.prototype.imwrite = function (name, callback, quality) {
            (function (image, name, callback, quality) {
                quality = quality === undefined ? 95 : quality;
                var canvas = createCanvas(image.getSize(1), image.getSize(0));
                var imageData = image.getImageData();
                canvas.getContext('2d').putImageData(imageData, 0, 0);
                var out = fs.createWriteStream(name), stream;
                if ((/\.(png)$/i).test(name.toLowerCase())) {
                    stream = canvas.pngStream();
                } else if ((/\.(jpeg|jpg)$/i).test(name.toLowerCase())) {
                    stream = canvas.syncJPEGStream({"quality": quality});
                } else if ((/\.(ppm)$/i).test(name.toLowerCase())) {
                    var header = "P6\n" + image.getSize(1) + " " + image.getSize(0) + "\n255\n";
                    var headerBin = new Uint8Array(header.length);
                    for (var c in header) {
                        headerBin[c] = header.charCodeAt(c);
                    }
                    var size = imageData.data.length + headerBin.length;
                    var data = new Uint8Array(size);
                    data.subarray(0, headerBin.length).set(headerBin);
                    var id = imageData.data;
                    for (var o1 = 0, o1e = id.length, o2 = headerBin.length; o1 < o1e; o1 += 4, o2 += 3) {
                        data[o2]     = id[o1];
                        data[o2 + 1] = id[o1 + 1];
                        data[o2 + 2] = id[o1 + 2];
                    }
                    fs.writeFileSync(name, new Buffer(data));
                    return;
                } else {
                    throw new Error("Matrix.imwrite: invalid file extension.");
                }
                stream.on('data', function (chunk) {
                    out.write(chunk);
                });
                if (callback === undefined) {
                    callback = function () {};
                }
                stream.on('end', function () {
                    callback.bind(image);
                });
            })(this, name, callback, quality);
        };
    } else {
        /** Transform a Matrix into an `Image` element.
        *
        * @async
        * @param {Function} callback
        *  Function to call when the conversion is done.
        */
        Matrix_prototype.toImage = async function (mime = "image/jpeg", ...args) {
            var canvas = createCanvas(this.getSize(0), this.getSize(1));
            var id = this.getImageData();
            canvas.getContext('2d').putImageData(id, 0, 0);
            var im = new Image();
            im.src = canvas.toDataURL(mime, ...args);
            return new Promise(resolve => im.onload = async () => resolve(im));
        };

        /** Transform a Matrix into an `Blob`.
        *
        * @async
        * @param {Function} callback
        *  Function to call when the conversion is done.
        */
        Matrix_prototype.toBlob = async function (mime = "image/jpeg", ...args) {
            var canvas = createCanvas(this.getSize(0), this.getSize(1));
            var id = this.getImageData();
            canvas.getContext('2d').putImageData(id, 0, 0);
            // async blob => resolve(new File([blob], filename, {type: "image/jpeg"})),
            return new Promise(
                resolve => canvas.toBlob(blob => resolve(blob), mime, ...args)
            );
        };

        /** Transform a Matrix into an `File element`.
        *
        * @async
        * @param {Function} callback
        *  Function to call when the conversion is done.
        */
        Matrix_prototype.toFile = async function (filename, ...args) {
            let blob = await this.toBlob(...args);
            return new File(
                [blob], filename,
                {lastModified: new Date(), type: blob.type}
            );
        };
    }

}
