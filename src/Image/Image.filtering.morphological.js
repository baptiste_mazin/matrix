export default function imageMorphoFilteringExtension (Matrix, Matrix_prototype) {

    //////////////////////////////////////////////////////////////////
    //                   MORPHOLOGICAL OPERATIONS                   //
    //////////////////////////////////////////////////////////////////


    var getLoopIndices = function (FX, FY, w, h) {
        var HFY = FY >> 1, HFX = FX >> 1;
        return {
            xS: new Int32Array([0, 0, 0, HFX, HFX, HFX, w - HFX, w - HFX, w - HFX]),
            xE: new Int32Array([HFX, HFX, HFX, w - HFX, w - HFX, w - HFX, w, w, w]),
            yS: new Int32Array([0, HFY, h - HFY, 0, HFY, h - HFY, 0, HFY, h - HFY]),
            yE: new Int32Array([HFY, h - HFY, h, HFY, h - HFY, h, HFY, h - HFY, h]),

            jS: new Int32Array([0, 0, 0, -HFX, -HFX, -HFX, -HFX, -HFX, -HFX]),
            jE: new Int32Array([HFX + 1, HFX + 1, HFX + 1, HFX + 1, HFX + 1, HFX + 1, w, w, w]),
            iS: new Int32Array([0, -HFY, -HFY, 0, -HFY, -HFY, 0, -HFY, -HFY]),
            iE: new Int32Array([HFY + 1, HFY + 1, h, HFY + 1, HFY + 1, h, HFY + 1, HFY + 1, h]),

            lS: new Int32Array([HFX, HFX, HFX,  0,  0,  0 ,      0,       0,       0]),
            lE: new Int32Array([ FX,  FX,  FX, FX, FX, FX, HFX + w, HFX + w, HFX + w]),
            kS: new Int32Array([HFY,  0,       0, HFY,  0,       0, HFY,  0,       0]),
            kE: new Int32Array([ FY, FY, HFY + h,  FY, FY, HFY + h,  FY, FY, HFY + h]),

            jxS: new Int32Array([0, 0, 0, 1, 1, 1, 1, 1, 1]),
            jxE: new Int32Array([1, 1, 1, 1, 1, 1, 0, 0, 0]),
            iyS: new Int32Array([0, 1, 1, 0, 1, 1, 0, 1, 1]),
            iyE: new Int32Array([1, 1, 0, 1, 1, 0, 1, 1, 0])
        };
    };

    var f_dilate = function (d, m, h, fh, yx, is, js, ks, ie, ls, _je) {
        var max = -Infinity;
        for (var _j = js, _l = ls; _j < _je; _j += h, _l += fh) {
            for (var ij = is + _j, kl = ks + _l, ije = ie + _j; ij < ije; ij++, kl++) {
                if (d[ij] > max && m[kl]) {
                    max = d[ij];
                }
            }
        }
        return max;
    };
    var f_erode = function (d, m, h, fh, yx, is, js, ks, ie, ls, _je) {
        var min = Infinity;
        for (var _j = js, _l = ls; _j < _je; _j += h, _l += fh) {
            for (var ij = is + _j, kl = ks + _l, ije = ie + _j; ij < ije; ij++, kl++) {
                if (d[ij] < min && m[kl]) {
                    min = d[ij];
                }
            }
        }
        return min;
    };
    var f_filt = function (d, m, h, fh, yx, is, js, ks, ie, ls, _je) {
        var sum = 0;
        for (var _j = js, _l = ls; _j < _je; _j += h, _l += fh) {
            for (var ij = is + _j, kl = ks + _l, ije = ie + _j; ij < ije; ij++, kl++) {
                sum += d[ij] * m[kl];
            }
        }
        return sum;
    };
    var applyFilter = function (im, mask, f) {
        mask = Matrix.from(mask);
        var h = im.getSize(0), w = im.getSize(1), id = im.getData();
        var out = new Matrix(im.getSize(), im.type()), od = out.getData();

        // Filter size and data
        var FY = mask.getSize(0), FX = mask.getSize(1), md = mask.getData();

        // Loop indices
        var li = getLoopIndices(FX, FY, w, h);
        // Loop start (S) and end (E) indices
        var xS  = li.xS,  xE  = li.xE,   yS  = li.yS,  yE  = li.yE,
        jS  = li.jS,  jE  = li.jE,   iS  = li.iS,  iE  = li.iE,
        lS  = li.lS,  kS  = li.kS,
        jxS = li.jxS, jxE = li.jxE, iyS = li.iyS, iyE = li.iyE;

        // Loop indices
        var b, c, x, y, _x, yx;
        // Loop end indices
        var ce, xe, ye;
        for (c = 0, ce = id.length; c < ce; c += w * h) {
            var idc = id.subarray(c, c + w * h), odc = od.subarray(c, c + w * h);
            for (b = 0; b < 9; b++) {
                for (x = xS[b], xe = xE[b], _x = x * h; x < xe; x++, _x += h) {
                    var js = (jS[b] + (jxS[b] ? x : 0)) * h, _je = (jE[b] + (jxE[b] ? x : 0)) * h;
                    var ls = (lS[b] - (jxS[b] ? 0 : x)) * FY;
                    for (y = yS[b], ye = yE[b], yx = y + _x; y < ye; y++, yx++) {
                        var is = iS[b] + (iyS[b] ? y : 0), ie = iE[b] + (iyE[b] ? y : 0);
                        var ks = kS[b] - (iyS[b] ? 0 : y);
                        odc[yx] = f(idc, md, h, FY, yx, is, js, ks, ie, ls, _je);
                    }
                }
            }
        }
        return out;
    };

    /** Perform an image dilation with a given structuring element.
    *
    * __Also see:__
    * {@link Matrix#imerode},
    * {@link Matrix#imopen},
    * {@link Matrix#imclose}.
    * @param{Matrix} elem
    *  The structuring element
    * @return{Matrix}
    * @matlike
    */
    Matrix_prototype.imdilate = function (mask) {
        return applyFilter(this, mask, f_dilate);
    };
    /** Perform an image erosion with a given structuring element.
    *
    * __Also see:__
    * {@link Matrix#imdilate},
    * {@link Matrix#imopen},
    * {@link Matrix#imclose}.
    * @param{Matrix} elem
    *  The structuring element
    * @return{Matrix}
    * @matlike
    */
    Matrix_prototype.imerode = function (mask) {
        return applyFilter(this, mask, f_erode);
    };
    /** Perform an image opening with a given structuring element.
    *
    * __Also see:__
    * {@link Matrix#imdilate},
    * {@link Matrix#imerode},
    * {@link Matrix#imclose}.
    *
    * @param{Matrix} elem
    *  The structuring element
    * @return{Matrix}
    */
    Matrix_prototype.imopen = function (mask) {
        return applyFilter(applyFilter(this, mask, f_erode), mask, f_dilate);
    };
    /** Perform an image closing with a given structuring element.
    *
    * __Also see:__
    * {@link Matrix#imdilate},
    * {@link Matrix#imerode},
    * {@link Matrix#imopen}.
    *
    * @param{Matrix} elem
    *  The structuring element
    * @return{Matrix}
    * @matlike
    */
    Matrix_prototype.imclose = function (mask) {
        return applyFilter(applyFilter(this, mask, f_dilate), mask, f_erode);
    };
    /** Filter an image.
    * @param{Matrix} filter
    *  The filter to apply (2D kernel).
    * @return{Matrix}
    * @matlike
    * @todo should check if the kernel is separable with an SVD.
    */
    Matrix_prototype.imfilter = function (mask) {
        return applyFilter(this, mask, f_filt);
    };
    /** Median filter.
    *
    * /!\ This function si currently Very slow.
    *
    * @param{Matrix} mask
    *  Boolean mask.
    * @return {Matrix}
    */
    Matrix_prototype.immedian = function (mask) {
        var arg = (mask.length * 0.5) | 0;
        var f_med = function (d, m, h, fh, yx, is, js, ks, ie, ls, _je) {
            var values = [];
            for (var _j = js, _l = ls; _j < _je; _j += h, _l += fh) {
                for (var ij = is + _j, kl = ks + _l, ije = ie + _j; ij < ije; ij++, kl++) {
                    if (m[kl]) {
                        values.push(d[ij]);
                    }
                }
            }
            return values.sort()[arg];
        };
        return applyFilter(this, mask, f_med);
    };
    /** Bilateral filtering.
    *
    * __Also see:__
    * {@link Matrix#imfilter}.
    *
    * @param {Number} sigma_s
    *  Value for spacial sigma.
    *
    * @param {Number} sigma_i
    *  Value for intensity sigma.
    *
    * @param {Number} [precision=3]
    *  used to compute the window size (size = precision * sigma_s).
    *
    * @return {Matrix}
    */
    Matrix_prototype.imbilateral = function (sigma_s, sigma_i, prec) {
        prec = prec || 3;
        var mask = Matrix.fspecial('gaussian', Math.round(prec * sigma_s / 2) * 2 + 1, sigma_s);
        var cst = -1 / (2 * sigma_i);
        var f_bilat = function (d, m, h, fh, yx, is, js, ks, ie, ls, _je) {
            var sum = 0, val = 0, v = d[yx];
            for (var _j = js, _l = ls; _j < _je; _j += h, _l += fh) {
                for (var ij = is + _j, kl = ks + _l, ije = ie + _j; ij < ije; ij++, kl++) {
                    var tmp = v - d[ij];
                    var weight = m[kl] * Math.exp(cst * tmp * tmp);
                    sum += weight;
                    val += d[ij] * weight;
                    //sum += d[ij] * m[kl];
                }
            }
            return val / sum;
        };
        return applyFilter(this, mask, f_bilat);
    };

}
