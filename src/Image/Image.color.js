/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

/** @class Matrix */

import CIE from "@etsitpab/cie";
import ColorConversions from "@etsitpab/colorconversions";

export default function colorExtension (Matrix, Matrix_prototype) {

    /** This object provides tools for colorspace conversion.
    * It works on array storing color information in different ways.
    * the way they are stored is specified by three parameters:
    *
    * + `sc` specify the space between 2 channels for the same pixel position,
    * + `sp` specify the space between 2 pixels for the same channel,
    * + `N` specify the number of pixels.
    *
    * For instance they can be stored as :
    *
    * + RGBRGB...RGB, `sc = 1, sp = 3` (default)
    * + RGBARGBA...RGBA, `sc = 1, sp = 4`
    * + RRR...GGG...BBB, `sc = N, sp = 1`
    * + RRR...GGG...BBB...AAA, `sc = N, sp = 1`
    *
    * Despite that these functions are designed for work on images,
    * they can be used to work with every kind of data.
    *
    * **Warning:** The data are always converted on place.
    *
    * @class Matrix.Colorspaces
    * @singleton
    */
    // Matrix.Colorspaces = CS;

    //////////////////////////////////////////////////////////////////
    //                       COLOR IMAGE MODULE                     //
    //////////////////////////////////////////////////////////////////


    var matlabEquivalence = {
        "lab2lch":   "Lab to Lch",
        "lab2srgb":  "Lab to RGB",
        "lab2xyz":   "Lab to XYZ",
        "lch2lab":   "Lch to Lab",
        "srgb2cmyk": "RGB to CMY",
        "srgb2lab":  "RGB to Lab",
        "srgb2xyz":  "RGB to XYZ",
        "upvpl2xyz": "1976 u'v'Y to XYZ",
        "uvl2xyz":   "1960 uvY to XYZ",
        "xyl2xyz":   "xyY to XYZ",
        "xyz2lab":   "XYZ to Lab",
        "xyz2srgb":  "XYZ to RGB",
        "xyz2upvpl": "XYZ to 1976 u'v'",
        "xyz2uvl":   "XYZ to 1960 uv",
        "xyz2xyl":   "XYZ to xyY"
    };


    /** @class Matrix */


    /** Apply a transformation to each RGB triplet of an image.
    *
    * @param {String | Function | Matrix} cform
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.applycform = function (cform) {
        if (this.ndims() !== 3 || this.getSize(2) < 3) {
            throw new Error("Matrix.applycform: Matrix must be an " +
            "image with RGB components.");
        }
        var N = this.getSize(0) * this.getSize(1);
        if (typeof(cform) === "string") {
            if (ColorConversions[cform]) {
                ColorConversions[cform](this.getData(), N, N, 1);
            } else if (ColorConversions[matlabEquivalence[cform]]) {
                ColorConversions[matlabEquivalence[cform]](this.getData(), N, N, 1);
            } else {
                throw new Error("Matrix.applycform: Unknown color transformation " + cform);
            }
        } else if (typeof(cform) === "function") {
            if (cform.length === 3) {
                ColorConversions.applyFunctionRGB(this.getData(), cform, N, N, 1);
            } else if (cform.length === 1) {
                ColorConversions.applyFunctionColor(this.getData(), cform, N, N, 1);
            }
        } else {
            cform = Matrix.from(cform);
            if (!Check.checkSizeEquals(cform.size(), [3, 3], Matrix.ignoreTrailingDims)) {
                throw new Error("Matrix.applycform: Matrix argument must be 3x3.");
            }
            ColorConversions.matrix(this.getData(), cform.getData(), N, N, 1);
        }
        return this;
    };

    Matrix.applycform = function (im, cform) {
        return im.clone().applycform(cform);
    };


    /** Convert an gray-level image to a color image given a colormap.
    *
    * @param {String} colormap
    *  Can be "JET", or "HUE".
    *
    * @return {Matrix}
    */
    {
        const palletes = {
            glowbow: "AAEECAsPEhUYGx4hJSgrLjI1ODw/QURIS05SVltgZWlrbnJ1eHx/goWJjI+SlZibn6KlqKyvsra5vL/CxcjMzs/P0NDR0tPU1NXW19jZ2dra29zd3t/f4OHi4+Pk5ebn5+jp6err7O3t7u/w8fLy8/P09fb3+Pj5+vv8/P3+/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wAAAQECAgICAgMDBAQFBQUFBgYHBwgICAgICQkKCwwMDAwNDQ4ODw8PDw8QEBEREhISEhMTFBQVFRUVFRYWFxcYGhscHR4fICEjJCUmJygpKissLS4vMDIzNDU2Nzg5Ozs8PT4/QEJDREVGR0hJS0xMTU5PUFJTVFVWV1haW1xdX2FjZWdpa21vcXN1dnh6fX+Bg4WGiIqMjpCTlZaYmpyeoKKkpqiqrK6wsrS1t7m8vsDCxMXHycvNz9LU1dfZ2tvb3Nzd3t/f3+Dg4eLj4+Tk5eXm5ufo6enp6urr7O3t7u7v7/Dw8fHy8/P09PX19vb3+Pn5+vr6+/z9/f7+//8AAAEBAgMEBAQFBgcHCAgJCQoKCwwNDQ0ODxAQERITFBUVFhYXFxgZGhoaGxwdHR4eHx8gISIiIyMjJCUmJicnJiYlJCMjIiIhICAfHx4eHR0cGxoaGRkYFxcWFhUVFBMTEhEREBAPDg0NDQwLCgoJCQgIBwcGBQQEBAMCAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIGCg4SFhoeIiYqLjI2Oj5CRkpOUlZaXmJmam5ydnp+goaKjZGVmp2hpaqusra6vcHFyc7S1trd4ubq7fL2+v7/",
            greyred: "6+ro5uXj4N7c2tjW09HPzcvIxcPAvry5trSxr6ypp6WioJyZl5WSj42Kh4WCgH57eXZzcW5samdlY2FfXVtZV1VTUU9NTEpIR0VDQUA/Pj08Ozo5ODc2NTQ0MzMyMjIyMTExMTExMjIyMzM0NDU2Nzc4ODk6PD0/QEFDREZHSEpMTlBSU1VXWVtdX2FkZmhqbG5xdHd5e36AgoWHio2PkZSWmZyeoKOmqKutr7K1uLq8v8HDxsjKzM7Q0tTW2Nrc3uDi5OXn6err7e7w8fL09fX29/j5+vr7/Pz9/f7+/v7+/v7+/v79/f38/Pv7+vr5+fj49/f29vX19fT08/Py8sfHxsbFxMPCwcDAv769vLu7urq5uLe3tbS0s7KxsbCvr66trayrqqmop6alpaSjoqKhoaCfnp6cm5uamZiYl5aWlZSTkpKRkI+OjYyMi4qJiYiIh4aFhYOCgoGAf359fXx8e3p5eXd2dXV0c3NycXBwb29ubWxramlpaGdmZWRkY2NiYWBgXl1cXFtaWllYV1dWVVRUU1JRUE9OTk1MS0tKSklIR0dFRENDQkFBQD8+Pj08Ozs6OTg3NjU1NDMyMjExMC8uLSwrKyopKCcmJiUlJCMiIiEgHx4dHBwbGhkZGBgXFhUUExIREBAPDg0NDAwLCgkJCAcGBQQDAwIBAAC5ubi4t7a1tLSzsrGxsLCvr66trKyrqqmoqKempaWko6OioaGgoJ6dnJycm5qZmZiYl5aVlZSTkpGRkI+Ojo2NjIyLiomJiIeGhYWEg4KCgYGAf35+fX17enl5eXh3dnZ1dXRzcnJxcG9ubm1sa2tqamlpaGdmZmVkY2JiYWBfX15eXV1cW1pZWVhXVlZVVFNTUlJRUE9PTk1MS0tKSklISEdGRkVEQ0JCQUA/Pz49PDw7Ozo6OTg3NjY1NDMzMjExMC8vLi0sKysqKSgoJycmJiUkIyMiISAfHx4dHBwbGhoZGBgXFxUUExMTEhEQEA8PDg0MDAsKCQgIBwYFBQQE",
            iron: "AAAAAAAAAAAAAAECAwQFBggKCw0PERQYGx4hJCgrLzI1ODs+QEJFSEtOUVNXWl1gY2ZpbG9xc3d6fYCChYiKjZCTlpmbnaCipKepq6yusLGztbe4ubu8vr/AwcLDxMXHyMnKy83Oz9DR0tPU1dbX2Nna29zd3t/g4OHi4+Tk5ebm5+jo6err6+vs7O3t7u7v7/Dw8fHx8fHy8vPz9PT09PX19vb39/j4+Pj4+fn5+fr6+/v8/P39/f39/f3+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/////////////////////////////////////////////////////////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAQICAwQFBQUGBwgJCwwNDhASExQWFxkaGx0fISIkJigqLS8wMTM1Nzg6PD4/QUNFR0lLTE1OUFJUVlhaW1xeX2FjZWZnaWpsbW9xcnR1d3l8foCCg4WHiImLjI6PkJKUlpibnZ+ho6aoqqyur7GytLe5ury9v8HDxcfIysvMzs/R09XX2Nrb3N3e4OHj5OXm5+nq6+3u7+/w8fHy8/T09fX29/j4+fn6+/z9/f7+//8ABxgmLjQ7QklQVVldYWVobHB0dnd5e36AgoWHiIqLjY6QkZKUlZaWlpeXmJiZmpubm5ucnJ2dnZ2dnZ2dnZ2dnJybm5ubm5ubmpqZmZiYl5eWlZWVlJOSkpGQj46NjIqIhoWDgX98eXd0cnBsaGViX1xXUk1HQTw2MSslIBwaGBUTEQ8NDAsKCQgIBwYFBQQEAwMDAwICAQEBAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAgMEBggKCwwOEBMXGx8jJiowNTxBR01TWWBmbXN8hIuSmaCor7W7wcbM0tje4+jt8/f5",
            midgreen: "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAAAAAAAAAAACAwUGCAkKDA0PEBETFBYXGBobHR4fISIkJSYoKSssLi8wMjM1Njc5Ojw+QkVIS09SVVlcX2JmaWxwdHd6foGEh4uOkZWYm56ipaisr7K1ubzAxMfKzdDT19ve4eXm5ubo6evs7u/w8vP19vf5+/3+/////////////////////////////////////////////////////////////////////////////////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAwYKDxQZHiMpLzQ5P0RJTlNYXWJnbHJ3fIKHjJKXnKGmq7C2u8DFys/V2uDl6vD09fX19fX19fT08/Py8vHx8fDw7+/u7u3t7Ozr6+rq6unp6Ojn5+bm5eXk5OPj4uLi4eHh4eHh4uLi4uLi4+Pj4+Pj4+Pj5OTk5OTk5eXl5eXl5ebm5ubm5ubm5ubm5ubm5ubm5ubm5ubl4t/c2dbT0M3Kx8TBvru4tbKvrKmmo6CdmpeUkY6LiIWCf3x5dnNwbWpnZGFeW1hVUk9MSUZDQD07NzQxLisoJSIfHRoWExANCgcEAQCUlpqeoqaqrrK2ub3BxcnN0dXZ3eHl6Ozx+P7///////////////////////////////////////////////////////////769vPv6N7W0MrEvriyq6WgmpSOiIJ8dnBpY11XUkxGQDo0LSciHBYQCgMAAAAAAAAAAAAAAAABAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            rain: "AAMKERkgKC83PkZNVVxka3N6gomRmKCnr7a+xc3U3OPr8/v99u7m39fQyMG5sqqjm5SMhX12bmdfWFBJQToyKyMcFA0FAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBg0UGyIpMTg/Rk1UW2JpcHd+hYyTmqGor7a9xMvS2eDn7vb9/vz6+ff29fPy8O7t6+ro5+bk4uDf3tzb2djW1NPR0M/NzMrJyMnKzM3Oz9DS09TV1tjY2tvc3d7g4eLj5Obn6Onq6+zu7/Dx8vT19vf4+vv8/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIDxceJi01PERLU1piaXF4gIePlp6lrbS8w8vS2uHp8Pn++/bx7enl4NzY1M/Lx8O/urayrqmloZ2ZlJCMiIR/e3dyb3F1eX2BhYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ent8fX6/vv07OXe19DJwru0raafmJGKg3x1bmdgWVJLRD02LyghGhMLBAIGDBIXHSInLTI3PUNITlNYXmNpbnN5foSKj5San6Wqr7W6wMXL0NXb4OXr8fMAAwoRGSAoLzc+Rk1VXGRrc3qCiZGYoKevtr7FzdTc4+vy+v79+vf18/Du7Oro5ePh393b2NbU0tDOy8nHxcPBvry6t7W0tri6vb/Bw8XHyszP0dPV19nc3uDi5Obp6+3v8fP2+Pr9/vjw6OHZ0srDu7SspZ2Wjod/eHBpYVpSS0M8NC0lHhYPBwEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACBwwSFx0iJy0yNz1DSE5TWF5jaW5zeX6Eio+Ump+lqq+1usDFy9DV2+Dl6/Hz",
            yellow: "NTU1NTQ0MzMzMzMzMzIyMjIyMjIyMzMzMzM0NDQ0NTU2Njc3ODg5OTo7Ozw9Pj9AQEFBQkNFRkhJSktMTk9QUVJUVVZYWVtdXmBhY2RmaGlrbG5wcnR1d3l7fX+AgoSGiIqMjo+Rk5WXmZudn6GjpKaoqqyusLK0tbe5u7y+wMLDxcfJyszOz9DS1NbX2Nrb3d7f4OHj5Obn6Onq7O3u7+/w8fHy8/T19vf3+Pj5+fr6+/v8/Pz8/f39/f3+/v7+/v7+/v7+/f39/f38/Pz8+/v6+vr6+fn4+Pj39/b29fX09PPz8vHw8PDv7+7u7e3s7Ovq6eno6Ofn5+bm5eXl5QEBAgMEBQcICQoLDA0ODxAQERITFBUWGBkaGxwdHh8gICEiIyQlJygpKissLS4vMDAxMjM0NTY4OTo7PD0+P0BAQUJDREVHSElKS0xNTk9QUFFSU1RVVlhZWltcXV5fYGBhYmNkZWdoaWprbG1ub3BwcXJzdHV2eHl6e3x9fn+AgIGCg4SFh4iJiouMjY6PkJCRkpOUlZaYmZqbnJ2en6CgoaKjpKWnqKmqq6ytrq+wsLGys7S1tri5uru8vb6/wMDBwsPExcfIycrLzM3Oz9DQ0dLT1NXW2Nna29zd3t/g4OHi4+Tl5+jp6uvs7e7v8PDx8vP09fb4+fr7/P3+//8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        };
        Matrix_prototype.toColormap = function (cMap) {
            var data = this.getData(), size = this.getSize(), dc = data.length;
            size[2] = 3;
            var out = new Matrix(size), dOut = out.getData();
            var R = dOut.subarray(0, dc), G = dOut.subarray(dc, 2 * dc), B = dOut.subarray(2 * dc, 3 * dc);
            var i, t, floor = Math.floor;
            if (cMap === "JET") {
                for (i = 0; i < dc; i++) {

                    t = data[i] * 4;

                    if (t >= 4) {
                        t = 3.99;
                    } else if (t < 0) {
                        t = 0;
                    }
                    switch (floor(t * 2) % 8) {
                        case 0:
                        R[i] = 0;
                        G[i] = 0;
                        B[i] = t + 0.5;
                        break;
                        case 1:
                        case 2:
                        R[i] = 0;
                        B[i] = 1;
                        G[i] = t - 0.5;
                        break;
                        case 3:
                        case 4:
                        R[i] = t - 1.5;
                        G[i] = 1;
                        B[i] = 2.5 - t;
                        break;
                        case 5:
                        case 6:
                        R[i] = 1;
                        G[i] = 3.5 - t;
                        B[i] = 0;
                        break;
                        case 7:
                        R[i] = 4.5 - t;
                        G[i] = 0;
                        B[i] = 0;
                        break;
                    }
                }
            } else if (cMap === "HUE") {

                for (i = 0; i < dc; i++) {
                    var H = data[i];
                    t = floor(H * 6) % 6;
                    var f = H * 6 - t;
                    switch (t) {
                        case 0:
                        R[i] = 1;
                        G[i] = f;
                        B[i] = 0;
                        break;
                        case 1:
                        R[i] = 1 - f;
                        G[i] = 1;
                        B[i] = 0;
                        break;
                        case 2:
                        R[i] = 0;
                        G[i] = 1;
                        B[i] = f;
                        break;
                        case 3:
                        R[i] = 0;
                        G[i] = 1 - f;
                        B[i] = 1;
                        break;
                        case 4:
                        R[i] = f;
                        G[i] = 0;
                        B[i] = 1;
                        break;
                        case 5:
                        R[i] = 1;
                        G[i] = 0;
                        B[i] = 1 - f;
                        break;
                    }
                }

            } else if (cMap === "HUE") {
                dOut.set(data);
            } else if (cMap.toLowerCase() in palletes) {
                // Determines input image range
                let min, max;
                if (this.isfloat() || this.islogical()) {
                    [min, max] = [0, 1];
                } else if (this.isinteger()) {
                    [min, max] = [Matrix.intmin(this.type()), Matrix.intmax(this.type())];
                }
                // Set palette to match input image range
                let pallete = Tools.decodeB64(palletes[cMap.toLowerCase()]);
                pallete = Float64Array.from(pallete).map(v => min + v / 255 * (max - min));

                // Parameters to normalize input image range to [0, #color]
                let nColors = pallete.length / 3;
                let a = min, b = 1 / (max - min) * nColors - 1;

                // Apply pallete
                for (var i = 0; i < dc; i++) {
                    let d = data[i] < min ? min : (data[i] >= max ? max : data[i]);
                    let index = Math.floor((d - a) * b);
                    R[i] = pallete[              index];
                    G[i] = pallete[    nColors + index];
                    B[i] = pallete[2 * nColors + index];
                }

            }
            return out;
        };
    }


    Matrix_prototype.correctImage = function (ill, illout = CIE.getIlluminant('D65')) {
        const mat = CIE.getIlluminantConversionMatrix(illout, ill);
        this.applycform('sRGB to LinearRGB')
            .applycform(mat)
            .applycform('LinearRGB to sRGB');
        return this;
    };

    Matrix.correctImage = function (im, ill, illout) {
        return im.clone().correctImage(ill, illout);
    };

    Matrix_prototype.im2CCT = function () {
        const cform = CIE['xyY to CCT'];

        const sizeOut = this.getSize();
        sizeOut.pop();
        const imOut = new Matrix(sizeOut, 'single');

        const id = this.getData(), od = imOut.getData();

        // Iterator to scan the view
        const view = this.getView();
        const dy = view.getStep(0), ly = view.getEnd(0);
        const dx = view.getStep(1), lx = view.getEnd(1);
        const dc = view.getStep(2);

        let x, nx, ny, CCT;
        const color = [];
        for (x = 0, nx = lx; x !== nx; x += dx) {
            let y0 = x;
            let y1 = x + dc;
            let y2 = x + 2 * dc;
            for (ny = x + ly; y0 !== ny; y0 += dy, y1 += dy, y2 += dy) {
                color[0] = id[y0];
                color[1] = id[y1];
                color[2] = id[y2];
                CCT = cform(color);
                CCT = CCT < 1668 ? 1668 : (CCT > 20000 ? 20000 : CCT);
                CCT = isNaN(CCT)  ? 24999 : CCT;
                od[y0] = CCT;
            }
        }
        return imOut;
    };

    Matrix_prototype.CCT2im = function () {
        const cform = CIE['CCT to xyY'];

        const sizeOut = this.getSize();
        sizeOut[2] = 3;
        const imOut = new Matrix(sizeOut, 'single');

        const id = this.getData(), od = imOut.getData();

        // Iterator to scan the view
        const view = imOut.getView();
        const dy = view.getStep(0), ly = view.getEnd(0);
        const dx = view.getStep(1), lx = view.getEnd(1);
        const dc = view.getStep(2);

        let x, nx, ny;
        for (x = 0, nx = lx; x !== nx; x += dx) {
            let y0 = x, y1 = x + dc, y2 = x + 2 * dc;
            for (ny = x + ly; y0 !== ny; y0 += dy, y1 += dy, y2 += dy) {
                const color = cform(id[y0]);
                od[y0] = color[0];
                od[y1] = color[1];
                od[y2] = color[2];
            }
        }
        return imOut;
    };
}
