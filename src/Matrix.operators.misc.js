/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */

export default function operatorsExtension (Matrix, Matrix_prototype) {


    //////////////////////////////////////////////////////////////////
    //                        Other operators                       //
    //////////////////////////////////////////////////////////////////


    /** Apply a function to values of Matrix.
    *
    * @param {Function} f
    *  Function to apply to Array elements.
    *
    * @chainable
    * @todo This function should provide a way to deal with complex
    * @matlike
    * @method arrayfun
    */
    {
        const apply_real = function(data, f) {
            const ie = data.length;
            let i;
            for (i = 0; i < ie; i++) {
                data[i] = f(data[i]);
            }
        };

        Matrix_prototype.arrayfun = function (fct) {
            if (!this.isreal()) {
                throw new Error("Matrix.arrayfun: This function doesn't work with complex numbers.");
            }
            if (typeof fct !== "function") {
                throw new Error("Matrix.arrayfun: Argument must be a function.");
            }
            apply_real(this.getData(), fct.bind(this));
            return this;
        };
    }

    /** Compute the p-norm of the Matrix
    * (the sum of all elements at power p).
    *
    * @param {Integer} [power=2]
    *
    * @return {Number} result
    *
    * @chainable
    * @matlike
    * @method norm
    */
    {
        const l0 = function (xd) {
            const n = xd.length;
            let i, norm;
            for (i = 0, norm = 0.0; i < n; i++) {
                if (xd[i] !== 0) {
                    norm++;
                }
            }
            return norm;
        };
        const l1 = function (xd) {
            const n = xd.length;
            let i, norm;
            for (i = 0, norm = 0.0; i < n; i++) {
                const tmp = xd[i];
                norm += tmp > 0 ? tmp : -tmp;
            }
            return norm;
        };
        const l2 = function (xd) {
            const n = xd.length;
            let i, norm;
            for (i = 0, norm = 0.0; i < n; i++) {
                norm += xd[i] ** 2;
            }
            return norm;
        };
        const lp = function (xd, p) {
            const {pow, abs} = Math, n = xd.length;
            let i, norm;
            for (i = 0, norm = 0.0; i < n; i++) {
                norm += pow(abs(xd[i]), p);
            }
            return norm;
        };

        Matrix_prototype.norm = function (p = 2) {
            if (!Check.isNumber(p)) {
                throw new Error("Matrix.norm: Argument p must be a number.");
            }
            if (!this.isvector()) {
                throw new Error("Matrix.norm: Is only implemented for vector.");
            }
            const xd = this.getData();
            if (p === -Infinity) {
                return Matrix.abs(this).min().asScalar();
            }
            if (p < 0) {
                return Infinity;
            }
            if (p === Infinity) {
                return Matrix.abs(this).max().asScalar();
            }
            if (p === 0) {
                return l0(xd);
            }
            let norm;
            if (p === 1) {
                norm = l1(xd);
            } else if (p === 2) {
                norm = l2(xd);
            } else {
                norm = lp(xd, p);
            }
            return Math.pow(norm, 1 / p);
        };
    }
    Matrix.norm = function (A, p) {
        return A.clone().norm(p);
    };

    /** Return a the upper part of the Matrix.
    * The lower part is set to zero.
    *
    * @param {Integer} shift
    *  Define diagonal separing the upper from
    *  the lower part of the Matrix.
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.triu = function (shift = 0) {
        const view = this.getView();
        const dn = view.getStep(1), m = view.getSize(0), n = view.getSize(1);

        let k, _k, lk, elk;
        if (this.isreal()) {
            const ud = this.getData();
            for (k = 0, _k = 0; k < n; _k += dn, k++) {
                for (lk = _k + k + 1 - shift, elk = _k + m; lk < elk; lk++) {
                    ud[lk] = 0;
                }
            }
        } else {
            const urd = this.getRealData(), uid = this.getImagData();
            for (k = 0, _k = 0; k < n; _k += dn, k++) {
                for (lk = _k + k + 1 - shift, elk = _k + m; lk < elk; lk++) {
                    urd[lk] = 0;
                    uid[lk] = 0;
                }
            }
        }
        return this;
    };
    Matrix.triu = function (A) {
        return A.clone().triu();
    };

    /** Return a the lower part of the Matrix.
    * The upper part is set to zero.
    *
    * See also:

    *  {@link Matrix#tril},
    *  {@link Matrix#diag}.
    *
    * @param {Integer} shift
    *  Define diagonal separing the upper from
    *  the lower part of the Matrix.
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.tril = function (shift = 0) {
        const view = this.getView();
        const dn = view.getStep(1), n = view.getSize(1);

        let k, _k, lk, elk;

        if (this.isreal()) {
            const ld = this.getData();
            for (k = 0, _k = 0; k < n; _k += dn, k++) {
                for (lk = _k, elk = _k + k - shift; lk < elk; lk++) {
                    ld[lk] = 0;
                }
            }
        } else {
            const lrd = this.getRealData(), lid = this.getImagData();
            for (k = 0, _k = 0; k < n; _k += dn, k++) {
                for (lk = _k, elk = _k + k - shift; lk < elk; lk++) {
                    lrd[lk] = 0;
                    lid[lk] = 0;
                }
            }
        }
        return this;
    };
    Matrix.tril = function (A) {
        return A.clone().tril();
    };

    /** Return a vector containing the diagonal elements.
    *
    * See also:
    * {@link Matrix#triu},
    * {@link Matrix#tril}.
    *
    * @param {Integer} shift
    *  Define diagonal to be copied.
    *
    * @matlike
    *
    * @todo
    * This function should return a Matrix if a vector is given as input.
    */
    Matrix_prototype.diag = function (shift = 0) {
        const view = this.getView();
        const dm = view.getStep(0), m = view.getSize(0);
        const dn = view.getStep(1), n = view.getSize(1);

        let f, s;
        if (shift > 0) {
            shift = Math.abs(shift);
            f = dn;
            s = Math.min(m, n - shift);
        } else {
            shift = Math.abs(shift);
            f = dm;
            s = Math.min(m - shift, n);
        }
        if (s <= 0) {
            throw new Error("Matrix.diag: Invalid diagonal requirement.");
        }
        const D = new Matrix([1, s], this.type(), !this.isreal());

        const step = dn + dm;
        let k, lk = shift * f;
        if (this.isreal()) {
            const ud = this.getData();
            const dd = D.getData();
            for (k = 0; k < s; k++, lk += step) {
                dd[k] = ud[lk];
            }
        } else {
            const urd = this.getRealData(), uid = this.getImagData();
            const drd = D.getRealData(), did = D.getImagData();
            for (k = 0; k < s; k++, lk += step) {
                drd[k] = urd[lk];
                did[k] = uid[lk];
            }
        }
        return D;
    };

    /** Apply a function on two Matrix by extending the non-singleton
    * dimensions.
    *
    * @param {Function|String} fun
    *  Function to be applied. If string, it should be either:
    *  - "plus", "minus", "times", "rdivide", "ldivide",
    *  - "min", "max"
    *  - "atan2", "hypot"
    *  - "eq", "ne", "lt", "le", "gt", "ge", "and", "or"
    *
    * @param {Matrix} A
    *  First Matrix
    *
    * @param {Matrix} B
    *  Second Matrix
    *
    * @matlike
    */
    Matrix.bsxfun = function (fun, a, b) {
        a = Matrix.from(a);
        b = Matrix.from(b);
        if (!a.isreal() || !b.isreal()) {
            throw new Error("Matrix.bsxfun: This function doesn't work with complex numbers.");
        }

        const aView = a.getView(), bView = b.getView();
        const ei = Math.max(aView.ndims(), bView.ndims());
        let i;
        for (i = 0; i < ei; i++) {
            const asize = a.getSize(i), bsize = b.getSize(i);
            if (asize === 1 && bsize > 1) {
                aView.selectDimByIndices(i, new Uint8Array(bsize));
            } else if (bsize === 1 && asize > 1) {
                bView.selectDimByIndices(i, new Uint8Array(asize));
            } else if (bsize !== asize) {
                throw new Error("Matrix.bsxfun: Incompatiblity on dimension: " + i);
            }
        }
        const out = Matrix.zeros(aView.getSize());
        const od = out.getData(), ad = a.getData(), bd = b.getData();
        const {iterator: ait, begin: ab, isEnd: e} = aView.getIterator(0);
        const {iterator: bit, begin: bb} = bView.getIterator(0);

        let io, ia, ib;

        if (fun instanceof Function) {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = fun(ad[ia], bd[ib]);
            }
        } else if (fun === "plus") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] + bd[ib];
            }
        } else if (fun === "minus") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] - bd[ib];
            }
        } else if (fun === "times") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] * bd[ib];
            }
        } else if (fun === "rdivide") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] / bd[ib];
            }
        } else if (fun === "ldivide") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = bd[ib] / ad[ia];
            }
        } else if (fun === "min") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = bd[ib] > ad[ia] ? ad[ia] : bd[ib];
            }
        } else if (fun === "max") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = bd[ib] > ad[ia] ? bd[ia] : ad[ib];
            }
        } else if (fun === "power") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = Math.pow(ad[ia], bd[ib]);
            }
        } else if (fun === "hypot") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = Math.sqrt(bd[ib] * bd[ib] + ad[ia] * ad[ia]);
            }
        } else if (fun === "atan2") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = Math.atan2(ad[ia], bd[ib]);
            }
        } else if (fun === "eq") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] === bd[ib] ? 1 : 0;
            }
        } else if (fun === "ne") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] !== bd[ib] ? 1 : 0;
            }
        } else if (fun === "lt") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] < bd[ib] ? 1 : 0;
            }
        } else if (fun === "le") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] <= bd[ib] ? 1 : 0;
            }
        } else if (fun === "gt") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] > bd[ib] ? 1 : 0;
            }
        } else if (fun === "ge") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] >= bd[ib] ? 1 : 0;
            }
        } else if (fun === "and") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] && bd[ib] ? 1 : 0;
            }
        } else if (fun === "or") {
            for (ia = ab(), ib = bb(), io = 0; !e(); ia = ait(), ib = bit(), io++) {
                od[io] = ad[ia] || bd[ib] ? 1 : 0;
            }
        } else {
            throw new Error("Matrix.bsxfun: Wrong function argument.");
        }
        return out;
    };


    // /** Returns the remainder after division.
    // * See also:
    // * {@link Matrix#rem},
    // * @matlike
    // */
    // Matrix_prototype.mod = function () {
    //     throw new Error ("Matrix: Function mod has to be implemented!") ;
    // };
    //
    // /** Returns the remainder after division.
    // * See also:
    // * {@link Matrix#mod},
    // * @matlike
    // */
    // Matrix_prototype.rem = function () {
    //     throw new Error ("Matrix: Function rem has to be implemented!") ;
    // };
}
