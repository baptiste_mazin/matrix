/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */

export default function statisticsRandomExtension (Matrix, Matrix_prototype) {
    /** Generate Poisson random numbers.
    *
    * The `lambda` parameter can a number as well as a Matrix.
    * - If it is a number then the function returns an array of
    * dimension `size`.
    * - If `lambda` is a Matrix then the function will return
    * a Matrix of the same size.
    *
    * Note that to avoid copy, you can use the syntax `mat.poissrnd()`.
    *
    * @param {Number} lambda
    * @param {Number} [size]
    * @return {Matrix}
    * @matlike
    */
    Matrix.poissrnd = function (lambda, ...size) {
        lambda = Matrix.from(lambda);
        if (lambda.isscalar()) {
            size = Check.checkSize(size, "square");
            const mat = new Matrix(size), data = mat.getData();
            poissrnd_lambda(data, lambda.asScalar());
            return mat;
        }
        return lambda.clone().poissrnd();
    };
    Matrix_prototype.poissrnd = function() {
        poissrnd_lambdas(this.getData());
        return this;
    };

    /** Generate exponentially distributed random numbers.
    * @param {Number} mu
    * @param {Number} size
    * @return {Matrix}
    * @matlike
    */
    Matrix.exprnd = function (mu, ...size) {
        mu = Matrix.from(mu);
        if (!mu.isscalar()) {
            throw new Error(`Matrix.exprnd: Mu parameter must be a scalar. Got size = []${mu.getSize()}]`);
        }
        size = Check.checkSize(size, "square");
        const mat = new Matrix(size), data = mat.getData();
        exprnd(data, mu.asScalar());
        return mat;
    };

    /** Generate random permutations of integers from 0 to n - 1.
    * @param {n} n
    *  Output size
    * @param {k} k
    *  return only k unique integer between 0 and n - 1;
    * @return {Matrix}
    */
    Matrix.randperm = function (n, k) {
        const out = this.zeros(n, 1, "uint32"), od = out.getData();
        let i;
        for (i = 0; i < n; i++) {
            od[i] = i;
        }
        const {random, round} = Math;
        for (i = 0; i < n - 1; i++) {
            const j = round(random() * (n - i - 1));
            const tmp = od[i + j];
            od[i + j] = od[i];
            od[i] = tmp;
        }
        if (k !== undefined) {
            return out.get([0, k - 1]);
        }
        return out;
    };
}

const poissrnd_lambda = function (data, lambda) {
    const L = Math.exp(-lambda), random = Math.random;
    const ie = data.length;
    let i;
    for (i = 0; i < ie; i++) {
        let p = 1, k = 0;
        do {
            k++;
            p *= random();
        } while (p > L);
        data[i] = k - 1;
    }
};

const poissrnd_lambdas = function (lambda) {
    const {exp, random} = Math;
    const ie = lambda.length;
    let i;
    for (i = 0; i < ie; i++) {
        let p = 1, k = 0, L = exp(-lambda[i]);
        do {
            k++;
            p *= random();
        } while (p > L);
        lambda[i] = k - 1;
    }
};

const exprnd = function (data, mu) {
    const {random, log} = Math;
    const ie = data.length;
    let i;
    for (i = 0, mu = -mu; i < ie; ++i) {
        data[i] = mu * log(random());
    }
};
