/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

let Matrix;

/** @class Matrix */

////////////////////////////////////////////////////////////////////////////////
//                                   TOOLS                                    //
////////////////////////////////////////////////////////////////////////////////


const getRealColumnArray = function (A) {
    const [m, n] = A.getSize(), ad = A.getData(), col = [];
    for (let j = 0, _j = 0; j < n; j++, _j += m) {
        col[j] = ad.subarray(_j, m + _j);
    }
    return col;
};

const getImagColumnArray = function (A) {
    const [m, n] = A.getSize();
    const aid = A.getImagData(), col = [];
    for (let j = 0, _j = 0; j < n; j++, _j += m) {
        col[j] = aid.subarray(_j, m + _j);
    }
    return col;
};

const resizeRealMatrix = function (A, m, n) {
    const o = Math.min(A.getSize(1), n);
    const X = new Matrix([m, n]);
    const Xcol = getRealColumnArray(X);
    const Acol = getRealColumnArray(A);
    for (let i = 0; i < o; i++) {
        Xcol[i].set(Acol[i].subarray(0, m));
    }
    return X;
};

const resizeComplexMatrix = function (A, m, n) {
    const o = Math.min(A.getSize(1), n);
    const X = new Matrix([m, n], undefined, true);
    const Xcolr = getRealColumnArray(X);
    const Xcoli = getImagColumnArray(X);
    const Acolr = getRealColumnArray(A);
    const Acoli = getImagColumnArray(A);
    for (let i = 0; i < o; i++) {
        Xcolr[i].set(Acolr[i].subarray(0, m));
        Xcoli[i].set(Acoli[i].subarray(0, m));
    }
    return X;
};

const swap = function (t, a, b) {
    const v = t[a];
    t[a] = t[b];
    t[b] = v;
};

const swapColumn = function (t, r, k, col) {
    col.set(t[r]);
    t[r].set(t[k]);
    t[k].set(col);
};

const findMax = function (tab, iMax = 0) {
    let vMax = tab[iMax];
    for (let i = iMax + 1, ie = tab.length; i < ie; i++) {
        if (tab[i] > vMax) {
            iMax = i;
            vMax = tab[i];
        }
    }
    return iMax;
};

const normFro = function (c, r) {
    let norm = 0;
    for (let i = r, ei = c.length; i < ei; i++) {
        norm += c[i];
    }
    return Math.sqrt(norm);
};

const getRowVector = function (cols, i) {
    const row = new Array(cols.length);
    for (let j = 0, ej = cols.length; j < ej; j++) {
        row[j] = cols[j][i];
    }
    return row;
};

const setRowVector = function (cols, i, row) {
    for (let j = 0, ej = cols.length; j < ej; j++) {
        cols[j][i] = row[j];
    }
};

const dotproduct = function (cxArray, nx, ria, sk, ek, i, cst) {
    // l = [0, N - 1]
    for (let l = 0; l < nx; l++) {
        let clx = cxArray[l],
            vr = 0;
        // k = [0, i]
        for (let k = sk; k < ek; k++) {
            vr += clx[k] * ria[k];
        }
        clx[i] -= vr;
        clx[i] *= cst;
    }
};

const dotproduct_cplx = function (cxrArray, cxiArray, nx, riar, riai, sk, ek, i, rd, id, cst) {
    let a, b, c, d;
    // l = [0, N - 1]
    for (let l = 0; l < nx; l++) {
        const clxr = cxrArray[l];
        const clxi = cxiArray[l];

        // k = [0, i]
        let vr = 0, vi = 0;
        for (let k = sk; k < ek; k++) {
            a = clxr[k];
            b = clxi[k];
            c = riar[k];
            d = riai[k];
            vr += a * c - b * d;
            vi += b * c + a * d;
        }

        a = clxr[i] - vr;
        b = clxi[i] - vi;
        clxr[i] = (a * rd + b * id) * cst;
        clxi[i] = (b * rd - a * id) * cst;
    }
};


////////////////////////////////////////////////////////////////////////////////
//                          MATRIX MULTIPLICATIONS                            //
////////////////////////////////////////////////////////////////////////////////


const mtimes_real_real = function (a, bcols, out, M, N, K) {
    let i, j, l, ij, il, bl, tmp;
    const rowTmp = new Float64Array(N);
    for (i = 0; i < M; i++) {
        // Copy the i-th row of matrix A.
        for (j = 0, ij = i; j < N; j++, ij += M) {
            rowTmp[j] = a[ij];
        }
        // Dot product between the i-th row of matrix A and the l-th row of matrix B.
        for (il = i, l = 0; l < K; il += M, l++) {
            bl = bcols[l];
            for (tmp = 0, j = 0; j < N; j++) {
                tmp += rowTmp[j] * bl[j];
            }
            out[il] = tmp;
        }
    }
};

const mtimes_real_cplx = function (a, brcols, bicols, outr, outi, M, N, K) {
    let i, j, l, ij, il, brl, bil, tmpr, tmpi, ar;
    const rowTmp = new Float64Array(N);
    for (i = 0; i < M; i++) {
        // Copy the i-th row of matrix A.
        for (j = 0, ij = i; j < N; j++, ij += M) {
            rowTmp[j] = a[ij];
        }
        // Dot product between the i-th row of matrix A and the l-th row of matrix B.
        for (il = i, l = 0; l < K; il += M, l++) {
            brl = brcols[l];
            bil = bicols[l];
            for (tmpr = 0, tmpi = 0, j = 0; j < N; j++) {
                ar = rowTmp[j];
                tmpr += ar * brl[j];
                tmpi += ar * bil[j];
            }
            outr[il] = tmpr;
            outi[il] = tmpi;
        }
    }
};

const mtimes_cplx_real = function (ar, ai, bcols, outr, outi, M, N, K) {
    let i, j, l, ij, il, bl, tmpr, tmpi, br;
    const rowrTmp = new Float64Array(N);
    const rowiTmp = new Float64Array(N);
    for (i = 0; i < M; i++) {
        // Copy the i-th row of matrix A.
        for (j = 0, ij = i; j < N; j++, ij += M) {
            rowrTmp[j] = ar[ij];
            rowiTmp[j] = ai[ij];
        }
        // Dot product between the i-th row of matrix A and the l-th row of matrix B.
        for (l = 0, il = i; l < K; l++, il += M) {
            bl = bcols[l];
            for (tmpr = 0, tmpi = 0, j = 0; j < N; j++) {
                br = bl[j];
                tmpr += rowrTmp[j] * br;
                tmpi += rowiTmp[j] * br;
            }
            outr[il] = tmpr;
            outi[il] = tmpi;
        }
    }
};

const mtimes_cplx_cplx = function (ard, aid, brcols, bicols, outr, outi, M, N, K) {
    let i, j, l, ij, il, brl, bil, tmpr, tmpi, ar, ai, br, bi;
    const rowr = new Matrix.dataType(N);
    const rowi = new Matrix.dataType(N);
    for (i = 0; i < M; i++) {
        // Copy the i-th row of matrix A.
        for (j = 0, ij = i; j < N; j++, ij += M) {
            rowr[j] = ard[ij];
            rowi[j] = aid[ij];
        }
        // Dot product between the i-th row of matrix A and the l-th row of matrix B.
        for (il = i, l = 0; l < K; il += M, l++) {
            brl = brcols[l];
            bil = bicols[l];
            for (tmpr = 0, tmpi = 0, j = 0; j < N; j++) {
                ar = rowr[j];
                ai = rowi[j];
                br = brl[j];
                bi = bil[j];
                tmpr += ar * br - ai * bi;
                tmpi += ar * bi + ai * br;
            }
            outr[il] = tmpr;
            outi[il] = tmpi;
        }
    }
};


////////////////////////////////////////////////////////////////////////////////
//                          GAUSSIAN SUBSTITUTIONS                            //
////////////////////////////////////////////////////////////////////////////////


const gaussianSubstitution_real_real = function (forward, A, X, rdiag) {
    // Matrix A iterators
    var viewA = A.getView();
    var m = viewA.getSize(0),
        n = viewA.getSize(1),
        dn = viewA.getStep(1);

    var rank = Math.min(m, n);
    var Float = Float64Array;

    // Matrix X
    var viewX = X.getView();
    var nx = viewX.getSize(1);

    var cxArray = getRealColumnArray(X);
    var ad = A.getData();

    // Main loop variables
    var i, j;
    // Composed loop variables
    var ij, ii;
    // End loop variables
    var ei, ek, eij;

    // Row i matrix a, real and imaginary parts
    var ria = new Float(rank);

    var si = forward ? 0 : rank - 1;
    ei = forward ? rank : -1;
    var di = forward ? 1 : -1;
    var dii = forward ? dn + 1 : -dn - 1;

    for (i = si, ii = i + i * dn; i !== ei; i += di, ii += dii) {

        var rd = (rdiag === undefined) ? ad[ii] : rdiag[i];
        var cst = 1 / rd;
        var sk = forward ? 0 : i + 1;
        ek = forward ? i + 1 : rank;
        var sj = forward ? 0 : i;
        var sij = forward ? i : ii;
        eij = forward ? ii : i + rank * dn;

        for (j = sj, ij = sij; ij < eij; j++, ij += dn) {
            ria[j] = ad[ij];
        }

        // l = [0, N - 1]
        dotproduct(cxArray, nx, ria, sk, ek, i, cst);
    }
};

const gaussianSubstitution_real_cplx = function (forward, A, X, rdiag) {
    // Matrix A iterators
    var viewA = A.getView();
    var m = viewA.getSize(0),
        n = viewA.getSize(1),
        dn = viewA.getStep(1);
    var rank = Math.min(m, n);
    var Float = Float64Array;

    // Matrix X
    var viewX = X.getView();
    var nx = viewX.getSize(1);

    var cxrArray = getRealColumnArray(X);
    var cxiArray = getImagColumnArray(X);
    var ad = A.getData();

    // Main loop variables
    var i, j, k, l;
    // Composed loop variables
    var ij, ii;
    // End loop variables
    var ei, ek, eij;

    var si = forward ? 0 : rank - 1;
    ei = forward ? rank : -1;
    var di = forward ? 1 : -1;
    var dii = forward ? dn + 1 : -dn - 1;

    var vr, vi;
    var ria = new Float(rank);


    for (i = si, ii = i + i * dn; i !== ei; i += di, ii += dii) {

        var rd = (rdiag === undefined) ? ad[ii] : rdiag[i];
        var cst = 1 / rd;

        var sk = forward ? 0 : i + 1;
        ek = forward ? i + 1 : rank;
        var sj = forward ? 0 : i;
        var sij = forward ? i : ii;
        eij = forward ? ii : i + rank * dn;

        for (j = sj, ij = sij; ij < eij; j++, ij += dn) {
            ria[j] = ad[ij];
        }

        // l = [0, N - 1]
        for (l = 0; l < nx; l++) {
            var clxr = cxrArray[l];
            var clxi = cxiArray[l];

            // k = [0, i]
            for (vr = 0, vi = 0, k = sk; k < ek; k++) {
                var c = ria[k];
                vr += clxr[k] * c;
                vi += clxi[k] * c;
            }

            clxr[i] -= vr;
            clxi[i] -= vi;
            clxr[i] *= cst;
            clxi[i] *= cst;

        }

    }

};

const gaussianSubstitution_cplx_cplx = function (forward, A, X, rdiag, idiag) {
    // Matrix A iterators
    var viewA = A.getView();
    var m = viewA.getSize(0),
        n = viewA.getSize(1),
        dn = viewA.getStep(1);
    var rank = Math.min(m, n);
    var Float = Float64Array;

    // Matrix X
    var viewX = X.getView();
    var nx = viewX.getSize(1);

    if (X.isreal()) {
        X.toComplex();
    }

    var ard = A.getRealData(),
        aid = A.getImagData();
    var cxrArray = getRealColumnArray(X),
        cxiArray = getImagColumnArray(X);

    // Main loop variables
    var i, j;
    // Composed loop variables
    var ij, ii;
    // End loop variables
    var ei, ek, eij;

    var si, di, dii;
    si = forward ? 0 : rank - 1;
    ei = forward ? rank : -1;
    di = forward ? 1 : -1;
    dii = forward ? dn + 1 : -dn - 1;

    var riar = new Float(rank),
        riai = new Float(rank);


    for (i = si, ii = i + i * dn; i !== ei; i += di, ii += dii) {

        var rd = (rdiag === undefined) ? ard[ii] : rdiag[i];
        var id = (idiag === undefined) ? aid[ii] : idiag[i];
        var cst = 1 / (rd * rd + id * id);

        var sk = forward ? 0 : i + 1;
        ek = forward ? i + 1 : rank;
        var sj = forward ? 0 : i;
        var sij = forward ? i : ii;
        eij = forward ? ii : i + rank * dn;

        for (j = sj, ij = sij; ij < eij; j++, ij += dn) {
            riar[j] = ard[ij];
            riai[j] = aid[ij];
        }
        dotproduct_cplx(cxrArray, cxiArray, nx, riar, riai, sk, ek, i, rd, id, cst);
    }
};

const gaussianSubstitution = function (forward, A, X, rdiag, idiag) {
    if (A.isreal()) {
        if (X.isreal()) {
            gaussianSubstitution_real_real(forward, A, X, rdiag);
        } else {
            gaussianSubstitution_real_cplx(forward, A, X, rdiag);
        }
    } else {
        gaussianSubstitution_cplx_cplx(forward, A, X, rdiag, idiag);
    }
    return X;
};


////////////////////////////////////////////////////////////////////////////////
//                          CHOLESKY DECOMPOSITION                            //
////////////////////////////////////////////////////////////////////////////////


const cholesky_real = function (cArray, n) {
    for (let k = 0; k < n; k++) {
        let ck = cArray[k];
        let v = 0;
        for (let p = 0; p < k; p++) {
            v += ck[p] * ck[p];
        }
        if (ck[k] - v < 0) {
            throw new Error("Matrix.chol: Input must be positive definite.");
        }
        ck[k] = Math.sqrt(ck[k] - v);
        let cst = 1 / ck[k];
        for (let p = k + 1; p < n; p++) {
            ck[p] = 0;
        }

        for (let i = k + 1; i < n; i++) {
            let ci = cArray[i];
            let v = 0;
            for (let p = 0; p < k; p++) {
                v += ci[p] * ck[p];
            }
            ci[k] -= v;
            ci[k] *= cst;
        }
    }
};

const cholesky_cplx = function (crArray, ciArray, n) {
    for (let k = 0; k < n; k++) {
        let crk = crArray[k],
            cik = ciArray[k];
        let vr = 0;
        for (let p = 0; p < k; p++) {
            let c = crk[p];
            let d = cik[p];
            vr += c * c + d * d;
        }
        if (crk[k] - vr < 0) {
            throw new Error("Matrix.chol: Input must be positive definite.");
        }
        if (cik[k] !== 0) {
            throw new Error("Matrix.chol: Diagonal must be real positive.");
        }
        crk[k] = Math.sqrt(crk[k] - vr);
        cik[k] = 0;
        let cst = 1 / (crk[k] * crk[k]);
        for (let p = k + 1; p < n; p++) {
            crk[p] = 0;
            cik[p] = 0;
        }

        for (let i = k + 1; i < n; i++) {
            let cri = crArray[i],
                cii = ciArray[i];
            let vr = 0,
                vi = 0;
            let p = 0;
            for (; p < k; p++) {
                let a = cri[p],
                    b = cii[p],
                    c = crk[p],
                    d = cik[p];
                vr += a * c + b * d;
                vi += b * c - a * d;
            }
            cri[k] -= vr;
            cii[k] -= vi;
            let a = cri[p],
                b = cii[p],
                c = crk[p],
                d = cik[p];
            cri[k] = (a * c + b * d) * cst;
            cii[k] = (b * c - a * d) * cst;
        }
    }
};


////////////////////////////////////////////////////////////////////////////////
//                              FULL LU MODULE                                //
////////////////////////////////////////////////////////////////////////////////


const computeLU_real = function (cArray, m, n, piv) {
    let abs = Math.abs,
        pivsign = 1;
    for (let k = 0, p = 0; k < n; k++, p = k) {
        let ck = cArray[k];
        for (let i = k + 1; i < m; i++) {
            if (abs(ck[i]) > abs(ck[p])) {
                p = i;
            }
        }
        if (p !== k) {
            for (let j = 0; j < n; j++) {
                swap(cArray[j], p, k);
            }
            swap(piv, p, k);
            pivsign = -pivsign;
        }

        for (let i = k + 1, v = 1 / ck[k]; i < m; i++) {
            ck[i] *= v;
        }

        for (let j = k + 1; j < n; j++) {
            let cj = cArray[j];
            for (let i = k + 1, v = cj[k]; i < m; i++) {
                cj[i] -= ck[i] * v;
            }
        }

    }
    return pivsign;
};

const computeLU_cplx = function (crArray, ciArray, m, n, piv) {
    let pivsign = 1;

    for (let k = 0, p = 0; k < n; k++, p = k) {
        let crk = crArray[k],
            cik = ciArray[k];
        for (let i = k + 1; i < m; i++) {
            let a = crk[i],
                b = cik[i],
                c = crk[p],
                d = cik[p];
            if (a * a + b * b > c * c + d * d) {
                p = i;
            }
        }
        if (p !== k) {
            for (let j = 0; j < n; j++) {
                swap(crArray[j], p, k);
                swap(ciArray[j], p, k);
            }
            swap(piv, p, k);
            pivsign = -pivsign;
        }
        let vr = crk[k],
            vi = cik[k];
        let imod = 1 / (vr * vr + vi * vi);
        vr *= imod;
        vi *= -imod;
        for (let i = k + 1; i < m; i++) {
            let a = crk[i],
                b = cik[i];
            crk[i] = a * vr - b * vi;
            cik[i] = a * vi + b * vr;
        }
        for (let j = k + 1; j < n; j++) {
            let crj = crArray[j],
                cij = ciArray[j];
            let vr = crj[k],
                vi = cij[k];
            for (let i = k + 1; i < m; i++) {
                let a = crk[i],
                    b = cik[i];
                crj[i] -= a * vr - b * vi;
                cij[i] -= b * vr + a * vi;
            }
        }
    }
    return pivsign;
};

const computeLU = function (A) {

    let LU = A.clone();

    // Scaning the from the second dimension (dim = 1)
    let view = LU.getView();
    let dn = view.getStep(1),
        [m, n] = view.getSize();

    let piv = new Float64Array(A.getSize(0)),
        pivsign = 1;
    for (let i = 0; i < m; i++) {
        piv[i] = i;
    }

    if (LU.isreal()) {

        let lud = LU.getData(),
            cArray = [];
        for (let k = 0, _k = 0; k < n; _k += dn, k++) {
            cArray[k] = lud.subarray(_k, m + _k);
        }
        pivsign = computeLU_real(cArray, m, n, piv);

    } else {

        let lurd = LU.getRealData(),
            crArray = [];
        let luid = LU.getImagData(),
            ciArray = [];
        for (let k = 0, _k = 0; k < n; _k += dn, k++) {
            crArray[k] = lurd.subarray(_k, m + _k);
            ciArray[k] = luid.subarray(_k, m + _k);
        }
        pivsign = computeLU_cplx(crArray, ciArray, m, n, piv);

    }
    return {
        LU,
        piv,
        pivsign
    };
};

const isNonsingular = function (LU) {
    let view = LU.getView(),
        lud = LU.getData();
    let n = view.getSize(0);
    for (let ij = 0, eij = n * n, d = n + 1; ij < eij; ij += d) {
        if (lud[ij] === 0) {
            return false;
        }
    }
    return true;
};

const getLU = function (param, P) {
    let [m, n] = param.LU.getSize();

    let L, U;
    if (m === n) {
        L = param.LU.clone();
        U = param.LU.clone();
    } else if (m < n) {
        L = param.LU.get([], [0, m - 1]);
        U = param.LU.get([0, m - 1], []);
    } else {
        L = param.LU.get([], [0, n - 1]);
        U = param.LU.get([0, n - 1], []);
    }

    // L matrix
    let view = L.getView(),
        lm = view.getEnd(0),
        dn = view.getStep(1),
        ln = view.getEnd(1);
    if (param.LU.isreal()) {

        let ld = L.getData();
        // j = [0, min(M, N) - 1]
        for (let j = 0, _j = 0, e_j = ln; _j < e_j; j++, _j += dn) {
            // i = [0, j - 1], i < j
            let ij = _j;
            for (let eij = j + _j; ij < eij; ij++) {
                ld[ij] = 0;
            }
            // i = j
            ld[ij] = 1;
        }

    } else {

        let ldr = L.getRealData(),
            ldi = L.getImagData();
        // j = [0, min(M, N) - 1]
        for (let j = 0, _j = 0, e_j = ln; _j < e_j; j++, _j += dn) {
            // i = [0, j - 1], i < j
            let ij = _j;
            for (let eij = j + _j; ij < eij; ij++) {
                ldr[ij] = 0;
                ldi[ij] = 0;
            }
            // i = j
            ldr[ij] = 1;
            ldi[ij] = 0;
        }

    }

    // U matrix
    view = U.getView();
    lm = view.getEnd(0);
    dn = view.getStep(1);
    ln = view.getEnd(1);
    if (param.LU.isreal()) {

        let ud = U.getData();
        // j = [0, N - 1]
        for (let j = 0, _j = 0, e_j = ln; _j < e_j; j++, _j += dn) {
            // i = [j + 1, M - 1], i > j
            for (let ij = (j + 1) + _j, eij = lm + _j; ij < eij; ij++) {
                ud[ij] = 0;
            }
        }

    } else {

        let urd = U.getRealData(),
            uid = U.getImagData();
        // j = [0, N - 1]
        for (let j = 0, _j = 0, e_j = ln; _j < e_j; j++, _j += dn) {
            // i = [j + 1, M - 1], i > j
            for (let ij = (j + 1) + _j, eij = lm + _j; ij < eij; ij++) {
                urd[ij] = 0;
                uid[ij] = 0;
            }
        }

    }

    let piv = param.piv;
    if (P === false) {
        let ipiv = new Float64Array(m);
        for (let i = 0, ei = piv.length; i < ei; i++) {
            ipiv[piv[i]] = i;
        }
        L = L.get([ipiv]);
        return [L, U];
    }

    // P matrix
    P = new Matrix([m, m]);
    let pd = P.getData();
    view = P.getView();
    lm = view.getEnd(0);
    dn = view.getStep(1);
    ln = view.getEnd(1);
    for (let i = 0; i < m; i++) {
        pd[i + piv[i] * dn] = 1;
    }

    return [L, U, P];
};

const solveLU = function (A, B) {
    let params = computeLU(A);
    let LU = params.LU, piv = params.piv;

    if (!isNonsingular(LU)) {
        throw new Error("Matrix.mldivide: Matrix is singular.");
    }

    // Data
    B = B.get([piv]);
    let rdiag = Matrix.ones(LU.getSize(0)).getData();
    let idiag = Matrix.zeros(LU.getSize(0)).getData();
    gaussianSubstitution(true, LU, B, rdiag, idiag);
    return gaussianSubstitution(false, LU, B);
};
export {solveLU};


////////////////////////////////////////////////////////////////////////////////
//                              FULL QR MODULE                                //
////////////////////////////////////////////////////////////////////////////////


const house_real = function (real, j) {
    var i, m = real.length;

    // Determiation of x normalization factor
    var M = -Infinity,
        mod;
    for (i = j; i < m; i++) {
        mod = Math.abs(real[i]);
        if (mod > M) {
            M = mod;
        }
    }

    // Vector v computation from x normalized
    var iM = (M !== 0) ? 1 / M : 0;
    var sigma = 0,
        x1 = real[j] * iM;
    for (real[j] = 1, i = j + 1; i < m; i++) {
        real[i] *= iM;
        sigma += real[i] * real[i];
    }

    // Compute sqrt(x1^2 + sigma)
    var mu = Math.sqrt(x1 * x1 + sigma);

    // Compute 2 * v1^2 / (sigma + v1^2)
    var sig = (x1 > 0) ? 1 : -1;
    var v1 = x1 + sig * mu;
    var v12 = v1 * v1;
    var beta = 2 * v12 / (sigma + v12);

    // Compute 1 / V1
    var iv1 = 1 / v1;

    // Normalize vector by 1 / v1
    for (i = j + 1; i < m; i++) {
        real[i] *= iv1;
    }

    real[j] = -sig * mu * M;

    return beta;
};

const update_real = function (v, c, beta, j, start) {
    var i, l, n = c.length,
        m = c[0].length;
    var s, coll;

    for (l = start; l < n; l++) {
        coll = c[l];
        for (s = coll[j], i = j + 1; i < m; i++) {
            s += v[i] * coll[i];
        }
        s *= beta;
        for (coll[j] -= s, i = j + 1; i < m; i++) {
            coll[i] -= v[i] * s;
        }
    }
};

const house_complex = function (real, imag, j) {
    var i, m = real.length,
        a, b;

    // Determiation of x normalization factor
    var M = -Infinity,
        mod;
    for (i = j; i < m; i++) {
        mod = Math.abs(real[i]) + Math.abs(imag[i]);
        if (mod > M) {
            M = mod;
        }
    }

    // Vector v computation from x normalized
    var iM = (M !== 0) ? 1 / M : 0;
    var sigma = 0,
        x1r = real[j] * iM,
        x1i = imag[j] * iM;
    for (real[j] = 1, imag[j] = 0, i = j + 1; i < m; i++) {
        real[i] *= iM;
        imag[i] *= iM;
        a = real[i];
        b = imag[i];
        sigma += a * a + b * b;
    }

    // Compute sqrt(x1^2 + sigma)
    var mu = Math.sqrt(x1r * x1r + x1i * x1i + sigma),
        an = Math.atan2(x1i, x1r);

    // Compute 2 * v1^2 / (sigma + v1^2)
    var v1r = x1r + mu * Math.cos(an),
        v1i = x1i + mu * Math.sin(an);
    var v12 = v1r * v1r + v1i * v1i;
    var beta = 2 * v12 / (sigma + v12);

    // Compute 1 / V1
    mod = 1 / (v1r * v1r + v1i * v1i);
    var iv1r = v1r * mod,
        iv1i = -v1i * mod;

    // Normalize vector by 1 / v1
    for (i = j + 1; i < m; i++) {
        a = real[i];
        b = imag[i];
        real[i] = a * iv1r - b * iv1i;
        imag[i] = b * iv1r + a * iv1i;
    }

    real[j] = -mu * M * Math.cos(an);
    imag[j] = -mu * M * Math.sin(an);

    return beta;
};

const update_complex = function (vr, vi, cr, ci, beta, j, start) {
    var sr, si;
    var i, l, n = cr.length,
        m = cr[0].length;
    var collr, colli;

    for (l = start; l < n; l++) {

        collr = cr[l];
        colli = ci[l];

        for (sr = collr[j], si = colli[j], i = j + 1; i < m; i++) {
            sr += vr[i] * collr[i] + vi[i] * colli[i];
            si += vr[i] * colli[i] - vi[i] * collr[i];
        }

        sr *= beta;
        si *= beta;

        for (collr[j] -= sr, colli[j] -= si, i = j + 1; i < m; i++) {
            collr[i] -= vr[i] * sr - vi[i] * si;
            colli[i] -= vi[i] * sr + vr[i] * si;
        }
    }
};

// Compute A = A * (I - Beta*v*v')
// <=> A_{ij} -= beta * v_{j} * sum_{k=1}^{n} A_{ik} * v_{k}
const update_right_real = function (v, c, beta, j, start) {
    var s, i, k, n = c.length,
        m = c[0].length;
    for (i = start; i < m; i++) {

        // sum_{k=1}^{n} A_{ik} * v_{k}
        for (s = c[j][i], k = j + 1; k < n; k++) {
            s += v[k] * c[k][i];
        }

        // beta * sum_{k=1}^{n} A_{ik} * v_{k}
        s *= beta;

        // A_{ij} -= v_{j} * beta * sum_{k=1}^{n} A_{ik} * v_{k}
        for (c[j][i] -= s, k = j + 1; k < n; k++) {
            c[k][i] -= v[k] * s;
        }

    }

};

const update_right_complex = function (vr, vi, cr, ci, beta, j, start) {
    var sr, si;
    var i, k, n = cr.length,
        m = cr[0].length;
    for (i = start; i < m; i++) {

        // sum_{k=1}^{n} A_{ik} * v_{k}
        for (sr = cr[j][i], si = ci[j][i], k = j + 1; k < n; k++) {
            sr += vr[k] * cr[k][i] + vi[k] * ci[k][i];
            si += vr[k] * ci[k][i] - vi[k] * cr[k][i];
        }

        // beta * sum_{k=1}^{n} A_{ik} * v_{k}
        sr *= beta;
        si *= beta;

        // A_{ij} -= v_{j} * beta * sum_{k=1}^{n} A_{ik} * v_{k}
        for (cr[j][i] -= sr, ci[j][i] -= si, k = j + 1; k < n; k++) {
            cr[k][i] -= vr[k] * sr - vi[k] * si;
            ci[k][i] -= vi[k] * sr + vr[k] * si;
        }

    }

};

const computehouseBidiagonalisation = function (A) {
    A = A.clone();
    var Float = Float64Array;

    var view = A.getView();
    var n = view.getSize(1);
    var j;
    var betac = new Float(n),
        betar = new Float(n);
    if (A.isreal()) {
        var col = getRealColumnArray(A);

        for (j = 0; j < n; j++) {
            betac[j] = house_real(col[j], j);
            update_real(col[j], col, betac[j], j, j + 1);
            if (j < n - 2) {
                var row = getRowVector(col, j);
                betar[j] = house_real(row, j + 1);
                setRowVector(col, j, row);
                update_right_real(row, col, betar[j], j + 1, j + 1);
            }
        }

    } else {

        var colr = getRealColumnArray(A),
            coli = getImagColumnArray(A);

        for (j = 0; j < n; j++) {
            betac[j] = house_complex(colr[j], coli[j], j);
            update_complex(colr[j], coli[j], colr, coli, betac[j], j, j + 1);
            if (j < n - 2) {
                var rowr = getRowVector(colr, j);
                var rowi = getRowVector(coli, j);
                betar[j] = house_complex(rowr, rowi, j + 1);
                setRowVector(colr, j, rowr);
                setRowVector(coli, j, rowi);
                update_right_complex(rowr, rowi, colr, coli, betar[j], j + 1, j + 1);
            }
        }
    }

    return [A, betac, betar];
};

const getU = function ([UBV, betar]) {
    let [m, n] = UBV.getSize();
    let U = Matrix.eye(m);
    let view = U.getView(),
        dc = view.getStep(1);

    if (UBV.isreal()) {

        let ucol = getRealColumnArray(U);
        let ubvd = UBV.getData();
        for (let j = n - 1, _j = dc * j; j >= 0; j--, _j -= dc) {
            let ubvcol = ubvd.subarray(_j, m + _j);
            update_real(ubvcol, ucol, betar[j], j, j);
        }

    } else {

        U.toComplex();
        let ucolr = getRealColumnArray(U),
            ucoli = getImagColumnArray(U);
        let ubvrd = UBV.getRealData(),
            ubvid = UBV.getImagData();
        for (let j = n - 1, _j = dc * j; j >= 0; j--, _j -= dc) {
            let ubvcolr = ubvrd.subarray(_j, m + _j);
            let ubvcoli = ubvid.subarray(_j, m + _j);
            update_complex(ubvcolr, ubvcoli, ucolr, ucoli, betar[j], j, j);
        }
    }
    return U;
};

const getB = function ([UBV]) {
    return UBV.clone().triu().tril(1);
};

const getV = function ([UBV, , betac]) {
    UBV = UBV.transpose();

    let [m, n] = UBV.getSize();
    let U = Matrix.eye(m);
    let view = U.getView(),
        dc = view.getStep(1);

    if (UBV.isreal()) {

        let ucol = getRealColumnArray(U);
        let ubvd = UBV.getData();
        for (let j = n - 1, _j = dc * j; j >= 0; j--, _j -= dc) {
            let ubvcol = ubvd.subarray(_j, m + _j);
            update_real(ubvcol, ucol, betac[j], j + 1, j + 1);
        }

    } else {

        U.toComplex();
        let ucolr = getRealColumnArray(U),
            ucoli = getImagColumnArray(U);
        let ubvrd = UBV.getRealData(),
            ubvid = UBV.getImagData();
        for (let j = m - 3, _j = dc * j; j >= 0; j--, _j -= dc) {
            let ubvcolr = ubvrd.subarray(_j, m + _j);
            let ubvcoli = ubvid.subarray(_j, m + _j);
            update_complex(ubvcolr, ubvcoli, ucolr, ucoli, betac[j], j + 1, j + 1);
        }

    }
    return U.transpose();
};

const getUBV = function (UBV) {
    return [getU(UBV), getB(UBV), getV(UBV)];
};

const getR = function ([QR]) {
    let R = QR.clone();

    let view = R.getView();
    let dc = view.getStep(1),
        lr = view.getEnd(0);
    let m = view.getSize(0),
        n = view.getSize(1);
    let j, _j, ij, jj, eij;

    if (R.isreal()) {
        let rd = R.getData();
        for (j = 0, _j = 0, jj = 0; j < m; j++, _j += dc, jj = j + _j) {
            for (ij = jj + 1, eij = lr + _j; ij < eij; ij++) {
                rd[ij] = 0;
            }
        }
        for (; j < n; j++, _j += dc, jj = j + _j) {
            for (ij = jj + 1, eij = lr + _j; ij < eij; ij++) {
                rd[ij] = 0;
            }
        }
    } else {
        let rrd = R.getRealData(),
            rid = R.getImagData();
        for (j = 0, _j = 0, jj = 0; j < m; j++, _j += dc, jj = j + _j) {
            for (ij = jj + 1, eij = lr + _j; ij < eij; ij++) {
                rrd[ij] = 0;
                rid[ij] = 0;
            }
        }
        for (; j < n; j++, _j += dc, jj = j + _j) {
            for (ij = jj + 1, eij = lr + _j; ij < eij; ij++) {
                rrd[ij] = 0;
                rid[ij] = 0;
            }
        }
    }
    return R;
};

const getQ = function ([QR, beta]) {

    let [m, n] = QR.getSize();
    let Q = Matrix.eye(m);
    let view = Q.getView(),
        dc = view.getStep(1);

    if (QR.isreal()) {

        let qcol = getRealColumnArray(Q);

        let qrd = QR.getData();
        for (let j = n - 1, _j = dc * j; j >= 0; j--, _j -= dc) {
            let qrcol = qrd.subarray(_j, m + _j);
            update_real(qrcol, qcol, beta[j], j, j);
        }

    } else {

        Q.toComplex();
        let qcolr = getRealColumnArray(Q);
        let qcoli = getImagColumnArray(Q);

        let qrrd = QR.getRealData(),
            qrid = QR.getImagData();
        for (let j = n - 1, _j = dc * j; j >= 0; j--, _j -= dc) {
            let qrcolr = qrrd.subarray(_j, m + _j);
            let qrcoli = qrid.subarray(_j, m + _j);
            update_complex(qrcolr, qrcoli, qcolr, qcoli, beta[j], j, j);
        }

    }

    return Q;
};

const getP = function ([, , piv]) {
    let m = piv.length;
    let P = new Matrix([m, m]);
    let view = P.getView(),
        pd = P.getData();
    let dc = view.getStep(1);
    let n = m;
    for (let j = 0, _j = 0; j < n; j++, _j += dc) {
        pd[piv[j] + _j] = 1;
    }
    return P;
};

const computeQR = function (A, pivoting) {

    let norm;
    let eps = 2 ** -52 || 2 ** -23;

    let QR = A.clone(),
        view = QR.getView(),
        [m, n] = view.getSize();

    let colTmp = new Float64Array(m),
        c = new Float64Array(n),
        piv = new Float64Array(n),
        beta = new Float64Array(n);

    let r;
    if (QR.isreal()) {

        let col = getRealColumnArray(QR);

        if (pivoting) {
            for (let j = 0; j < n; j++) {
                let colj = col[j];
                let v = 0;
                for (let i = 0; i < m; i++) {
                    v += colj[i] * colj[i];
                }
                c[j] = v;
                piv[j] = j;
            }
            norm = normFro(c, 0);
        }

        for (r = 0; r < n; r++) {

            if (pivoting) {
                let k = findMax(c, r);
                if (normFro(c, r) <= norm * eps) {
                    return [QR, beta, piv, r];
                } else if (r !== k) {
                    swapColumn(col, r, k, colTmp);
                    swap(c, r, k);
                    swap(piv, r, k);
                }
            }

            beta[r] = house_real(col[r], r);
            update_real(col[r], col, beta[r], r, r + 1);

            if (pivoting) {
                for (let i = r + 1; i < n; i++) {
                    c[i] -= col[i][r] * col[i][r];
                }
            }

        }

    } else {

        let colr = getRealColumnArray(QR);
        let coli = getImagColumnArray(QR);

        if (pivoting) {
            for (let j = 0; j < n; j++) {
                let coljr = colr[j],
                    colji = coli[j];
                let v = 0;
                for (let i = 0; i < m; i++) {
                    v += coljr[i] * coljr[i] + colji[i] * colji[i];
                }
                c[j] = v;
                piv[j] = j;
            }
            norm = normFro(c, 0);
        }

        for (r = 0; r < n; r++) {

            if (pivoting) {
                let k = findMax(c, r);
                if (normFro(c, r) <= norm * eps) {
                    return [QR, beta, piv, r + 1];
                } else if (r !== k) {
                    swapColumn(colr, r, k, colTmp);
                    swapColumn(coli, r, k, colTmp);
                    swap(c, r, k);
                    swap(piv, r, k);
                }
            }

            beta[r] = house_complex(colr[r], coli[r], r);
            update_complex(colr[r], coli[r], colr, coli, beta[r], r, r + 1);

            if (pivoting) {
                for (let i = r + 1; i < n; i++) {
                    c[i] -= colr[i][r] * colr[i][r] + coli[i][r] * coli[i][r];
                }
            }

        }
    }
    return [QR, beta, piv, r];
};

const solveOverdeterminedQR = function (A, B) {
    let [QR, beta, piv] = computeQR(A, true);

    let [m, n] = A.getSize(), n2 = B.getSize(1);
    let rank = Math.min(m, n);

    // Compute Q' * B
    B = B.clone();

    let X;

    if (QR.isreal() && B.isreal()) {

        let bcol = getRealColumnArray(B);
        let qrcol = getRealColumnArray(QR);
        for (let j = 0; j < n; j++) {
            update_real(qrcol[j], bcol, beta[j], j, 0);
        }

        // Solve R * X = Q * B, backward-subsitution B is overwriting by X
        gaussianSubstitution(false, QR, B);

        // Copy B part of interest in X
        X = resizeRealMatrix(B, rank, n2);

    } else {

        if (QR.isreal()) {
            QR.toComplex();
        }
        if (B.isreal()) {
            B.toComplex();
        }

        let brcol = getRealColumnArray(B);
        let bicol = getImagColumnArray(B);

        let qrcolr = getRealColumnArray(QR);
        let qrcoli = getImagColumnArray(QR);

        for (let j = 0; j < n; j++) {
            update_complex(qrcolr[j], qrcoli[j], brcol, bicol, beta[j], j, 0);
        }

        // Solve R * X = Q * B, backward-subsitution B is overwriting by X
        gaussianSubstitution(false, QR, B);

        // Copy B part of interest in X
        X = resizeComplexMatrix(B, rank, n2);

    }

    let ipiv = new Uint32Array(rank);
    for (let i = 0, ei = piv.length; i < ei; i++) {
        ipiv[piv[i]] = i;
    }

    return X.get([ipiv]);

};

const solveUnderdeterminedQR = function (A, B) {
    let [QR, beta, piv] = computeQR(A.ctranspose(), true);

    let [m, n] = QR.getSize(), o = B.getSize(1);
    let rank = Math.min(m, n);
    let ipiv = new Uint32Array(rank);
    for (let i = 0, ei = piv.length; i < ei; i++) {
        ipiv[piv[i]] = i;
    }
    B = B.get([ipiv]);

    let QtX = gaussianSubstitution(true, QR.ctranspose(), B);

    if (QR.isreal() && B.isreal()) {

        QtX = resizeRealMatrix(QtX, m, o);
        let Xcol = getRealColumnArray(QtX);

        let qrcol = getRealColumnArray(QR);
        for (let j = n - 1; j >= 0; j--) {
            update_real(qrcol[j], Xcol, beta[j], j, 0);
        }

    } else {

        if (QR.isreal()) {
            QR.toComplex();
        }
        if (QtX.isreal()) {
            QtX.toComplex();
        }

        QtX = resizeComplexMatrix(QtX, m, o);
        let Xcolr = getRealColumnArray(QtX);
        let Xcoli = getImagColumnArray(QtX);

        let qrcolr = getRealColumnArray(QR);
        let qrcoli = getImagColumnArray(QR);
        for (let j = n - 1; j >= 0; j--) {
            update_complex(qrcolr[j], qrcoli[j], Xcolr, Xcoli, beta[j], j, 0);
        }

    }

    return QtX;
};

const solveQR = function (A, B) {
    if (A.getSize(0) < A.getSize(1)) {
        return solveUnderdeterminedQR(A, B);
    }
    return solveOverdeterminedQR(A, B);
};


////////////////////////////////////////////////////////////////////////////////
//                             MATRIX FUNCTIONS                               //
////////////////////////////////////////////////////////////////////////////////

export default function linalgExtension(M, Matrix_prototype) {
    Matrix = M;
    /** Mtimes operator make a matrix multiplication,
     *
     * __Also see:__
     * {@link Matrix#minus},
     * {@link Matrix#plus},
     * {@link Matrix#rdivide},
     * {@link Matrix#ldivide}.
     *
     * @param {Number|Matrix} rightOp
     *
     * @return {Matrix}
     * @matlike
     */
    Matrix_prototype.mtimes = function (B) {
        // Check if Matrix
        B = Matrix.from(B);
        if (!this.ismatrix() || !B.ismatrix()) {
            throw new Error('Matrix.mtimes: mtimes is undefined for ND array.');
        }

        const [M, N] = this.getSize();
        const K = B.getSize(1);

        // Check if size are compatible
        if (N !== B.getSize(0)) {
            throw new Error('Matrix.mtimes: Matrix sizes must match.');
        }

        const complex = (this.isreal() && B.isreal()) ? false : true;
        const TypeConstructor = Check.getTypeConstructor(this.getDataType());
        const X = new Matrix([M, K], TypeConstructor, complex);

        if (this.isreal()) {
            if (B.isreal()) {
                mtimes_real_real(
                    this.getData(),
                    getRealColumnArray(B),
                    X.getData(),
                    M, N, K
                );
            } else {
                mtimes_real_cplx(
                    this.getData(),
                    getRealColumnArray(B), getImagColumnArray(B),
                    X.getRealData(), X.getImagData(),
                    M, N, K
                );
            }
        } else {
            if (B.isreal()) {
                mtimes_cplx_real(
                    this.getRealData(), this.getImagData(),
                    getRealColumnArray(B),
                    X.getRealData(), X.getImagData(),
                    M, N, K
                );
            } else {
                mtimes_cplx_cplx(
                    this.getRealData(), this.getImagData(),
                    getRealColumnArray(B), getImagColumnArray(B),
                    X.getRealData(), X.getImagData(),
                    M, N, K
                );
            }
        }
        return X;
    };

    Matrix.mtimes = function (A, B) {
        return Matrix.from(A).mtimes(B);
    };

    Matrix_prototype["*"] = Matrix_prototype.mtimes;

    /** Compute the cholesky decomposition.
     *
     * @param {String} [upper='upper']
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.chol = function (lower) {
        if (!this.ismatrix()) {
            throw new Error("Matrix.chol: Input must be a matrix.");
        }
        if (!this.issquare()) {
            throw new Error("Matrix.chol: Matrix must be square.");
        }

        if (lower === 'lower') {
            lower = true;
        } else if (lower === 'upper' || lower === undefined) {
            lower = false;
        } else {
            throw new Error("Matrix.chol: Invalid parameters.");
        }

        let A = this.clone();

        if (A.isreal()) {
            cholesky_real(getRealColumnArray(A), A.getSize(1));
        } else {
            cholesky_cplx(getRealColumnArray(A), getImagColumnArray(A), A.getSize(1));
        }
        return lower ? A.ctranspose(A) : A;
    };

    /** Compute the Matrix inverse.
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.inv = function () {
        return this.mldivide(Matrix.eye(this.getSize()));
    };

    /** Compute the Matrix determinant.
     *
     * @return {Matrix}
     *
     * @todo Check the use (and utility) of pivsign !!
     * @matlike
     */
    Matrix_prototype.det = function () {
        if (!this.issquare()) {
            throw new Error("Matrix.det: Matrix must be square.");
        }
        let paramsLU = computeLU(this);
        let LU = paramsLU.LU;
        let view = LU.getView(),
            N = view.getSize(0);

        if (this.isreal()) {
            let d = paramsLU.pivsign;
            let lud = LU.getData();
            for (let y = 0, yn = N * N, dy = N + 1; y < yn; y += dy) {
                d *= lud[y];
            }
            return new Matrix([1, 1], [d]);
        }

        let lurd = LU.getRealData();
        let luid = LU.getImagData();
        let dr = paramsLU.pivsign,
            di = 0;
        for (let y = 0, yn = N * N, dy = N + 1; y < yn; y += dy) {
            dr = dr * lurd[y] - di * luid[y];
            di = dr * luid[y] + di * lurd[y];
        }
        return new Matrix([1, 1], [dr, di], true);
    };

    /** Compute the Matrix rank.
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.rank = function () {
        let [, , , r] = computeQR(this, true);
        return r;
    };

    /** Operator lu.
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.lu = function () {
        return getLU(computeLU(this), false);
    };

    /** Operator lu with permutations.
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.lup = function () {
        return getLU(computeLU(this), true);
    };

    /** Operator qr with permutations.
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.qrp = function () {
        let QR = computeQR(this, true);
        return [getQ(QR), getR(QR), getP(QR)];
    };

    /** Operator qr.
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.qr = function () {
        let QR = computeQR(this, false);
        return [getQ(QR), getR(QR)];
    };

    /** Operator mldivide.
     *
     * @param {Number|Matrix} rightOp
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.mldivide = function (B) {
        B = Matrix.from(B);
        if (!this.ismatrix() || !B.ismatrix()) {
            throw new Error("1 Matrix.mldivide: Both arguments must be Matrix.");
        }
        if (B.getSize(0) !== this.getSize(0)) {
            throw new Error("2 Matrix.mldivide: Row dimensions must agree.");
        }
        return solveQR(this, B.clone());
    };

    /** Operator mrdivide.
     *
     * @param {Number|Matrix} rightOp
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.mrdivide = function (B) {
        B = Matrix.from(B);
        if (!this.ismatrix() || !B.ismatrix()) {
            throw new Error("Matrix.mrdivide: Both arguments must be Matrix.");
        }
        if (B.getSize(0) !== this.getSize(0)) {
            throw new Error("Matrix.mrdivide: Row dimensions must agree.");
        }
        return solveQR(B.ctranspose(), this.ctranspose()).ctranspose();
    };

    /** Compute the bidiagonal decomposition.
     *
     * @return {Matrix}
     *
     * @matlike
     */
    Matrix_prototype.bidiag = function () {
        let UBV = computehouseBidiagonalisation(this.clone());
        return getUBV(UBV);
    };


    /** Computes an SVD decomposition on a given Matrix.,
     *
     * @return {Matrix[]}
     *  Array containing `U`, `S` and `V` Matrix.
     *
     *     let A = Matrix.rand(300);
     *     let USV = A.svd(); let U = USV[0], S = USV[1], V = USV[2];
     *     let n = U.mtimes(S).mtimes(V.transpose()).minus(A).norm();
     *     console.log("norm:", n);
     *
     * @author
     *  This code was imported from the [numericjs library][1]
     *  and adapted to work with Matrix class.
     *
     *  We report here the comment found in the code:
     *  _Shanti Rao sent me this routine by private email. I had to modify it
     *  slightly to work on Arrays instead of using a Matrix object.
     *  It is apparently translated [from here][2]_
     *
     *  [1]: http://www.numericjs.com/
     *  [2]: http://stitchpanorama.sourceforge.net/Python/svd.py
     *
     * @matlike
     */
    Matrix_prototype.svd = function () {

        // Compute the thin SVD from G. H. Golub and C. Reinsch, Numer. Math. 14, 403-420 (1970)
        let prec = Math.pow(2, -52); // assumes double prec
        let tolerance = 1.e-64 / prec;
        let itmax = 50;
        let i = 0,
            l = 0;

        let u = this.transpose().toArray();
        let m = u.length;

        let n = u[0].length;

        if (m < n) {
            throw "Matrix.svd: Needs more rows than columns";
        }

        let e = new Array(n);
        let q = new Array(n);
        for (i = 0; i < n; i++) {
            e[i] = 0;
            q[i] = 0;
        }
        let v = Matrix.zeros(n).toArray();

        let pythag = function (a, b) {
            a = Math.abs(a);
            b = Math.abs(b);
            if (a > b) {
                return a * Math.sqrt(1.0 + (b * b / a / a));
            } else if (b === 0.0) {
                return a;
            }
            return b * Math.sqrt(1.0 + (a * a / b / b));
        };

        // Householder's reduction to bidiagonal form

        let f = 0, g = 0, h = 0, x = 0, y = 0, z = 0;
        for (let i = 0; i < n; i++) {
            e[i] = g;
            let s = 0;
            let l = i + 1;
            for (let j = i; j < m; j++) {
                s += (u[j][i] * u[j][i]);
            }
            if (s <= tolerance) {
                g = 0;
            } else {
                f = u[i][i];

                g = Math.sqrt(s);

                if (f >= 0) {
                    g = -g;
                }

                let h = f * g - s;

                u[i][i] = f - g;

                for (let j = l; j < n; j++) {
                    let s = 0;
                    for (let k = i; k < m; k++) {
                        s += u[k][i] * u[k][j];
                    }
                    let f = s / h;
                    for (let k = i; k < m; k++) {
                        u[k][j] += f * u[k][i];
                    }
                }
            }

            q[i] = g;
            s = 0;

            for (let j = l; j < n; j++) {
                s = s + u[i][j] * u[i][j];
            }

            if (s <= tolerance) {
                g = 0;
            } else {
                let f = u[i][i + 1];
                g = Math.sqrt(s);
                if (f >= 0) {
                    g = -g;
                }

                let h = f * g - s;
                u[i][i + 1] = f - g;
                for (let j = l; j < n; j++) {
                    e[j] = u[i][j] / h;
                }
                for (let j = l; j < m; j++) {
                    let s = 0;
                    for (let k = l; k < n; k++) {
                        s += (u[j][k] * u[i][k]);
                    }
                    for (let k = l; k < n; k++) {
                        u[j][k] += s * e[k];
                    }
                }
            }
            y = Math.abs(q[i]) + Math.abs(e[i]);
            if (y > x) {
                x = y;
            }
        }

        // accumulation of right hand gtransformations

        for (let i = n - 1; i !== -1; i += -1) {
            if (g !== 0) {
                h = g * u[i][i + 1];
                for (let j = l; j < n; j++) {
                    v[j][i] = u[i][j] / h;
                }
                for (let j = l; j < n; j++) {
                    let s = 0;
                    for (let k = l; k < n; k++) {
                        s += u[i][k] * v[k][j];
                    }
                    for (let k = l; k < n; k++) {
                        v[k][j] += (s * v[k][i]);
                    }
                }
            }
            for (let j = l; j < n; j++) {
                v[i][j] = 0;
                v[j][i] = 0;
            }
            v[i][i] = 1;
            g = e[i];
            l = i;
        }

        // Accumulation of left hand transformations

        for (let i = n - 1; i !== -1; i += -1) {
            let l = i + 1;
            let g = q[i];
            for (let j = l; j < n; j++) {
                u[i][j] = 0;
            }
            if (g !== 0) {
                h = u[i][i] * g;
                for (let j = l; j < n; j++) {
                    let s = 0;
                    for (let k = l; k < m; k++) {
                        s += u[k][i] * u[k][j];
                    }
                    f = s / h;
                    for (let k = i; k < m; k++) {
                        u[k][j] += f * u[k][i];
                    }

                }
                for (let j = i; j < m; j++) {
                    u[j][i] = u[j][i] / g;
                }
            } else {
                for (let j = i; j < m; j++) {
                    u[j][i] = 0;
                }
            }
            u[i][i] += 1;
        }

        // diagonalization of the bidiagonal form

        prec = prec * x;
        for (let k = n - 1; k !== -1; k += -1) {
            let l;
            for (let iteration = 0; iteration < itmax; iteration++) {
                // test f splitting
                let test_convergence = false;
                for (l = k; l !== -1; l += -1) {
                    if (Math.abs(e[l]) <= prec) {
                        test_convergence = true;
                        break;
                    }
                    if (Math.abs(q[l - 1]) <= prec) {
                        break;
                    }
                }
                if (!test_convergence) {
                    // cancellation of e[l] if l>0
                    let c = 0,
                        s = 1;
                    let l1 = l - 1;
                    for (let i = l; i < k + 1; i++) {
                        f = s * e[i];
                        e[i] = c * e[i];
                        if (Math.abs(f) <= prec) {
                            break;
                        }
                        g = q[i];
                        h = pythag(f, g);
                        q[i] = h;
                        c = g / h;
                        s = -f / h;
                        for (let j = 0; j < m; j++) {
                            y = u[j][l1];
                            z = u[j][i];
                            u[j][l1] = y * c + (z * s);
                            u[j][i] = -y * s + (z * c);
                        }
                    }
                }
                // test f convergence
                z = q[k];
                if (l === k) {
                    // convergence
                    if (z < 0) {
                        //q[k] is made non-negative
                        q[k] = -z;
                        for (let j = 0; j < n; j++) {
                            v[j][k] = -v[j][k];
                        }
                    }
                    break;
                    //break out of iteration loop and move on to next k value
                }
                if (iteration >= itmax - 1) {
                    throw 'Error: no convergence.';
                }
                // shift from bottom 2x2 minor
                x = q[l];
                y = q[k - 1];
                g = e[k - 1];
                h = e[k];
                f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2 * h * y);
                g = pythag(f, 1.0);
                if (f < 0.0) {
                    f = ((x - z) * (x + z) + h * (y / (f - g) - h)) / x;
                } else {
                    f = ((x - z) * (x + z) + h * (y / (f + g) - h)) / x;
                }
                // next QR transformation
                let c = 1,
                    s = 1;
                for (let i = l + 1; i < k + 1; i++) {
                    let g = e[i];
                    let y = q[i];
                    let h = s * g;
                    g = c * g;
                    let z = pythag(f, h);
                    e[i - 1] = z;
                    c = f / z;
                    s = h / z;
                    f = x * c + g * s;
                    g = -x * s + g * c;
                    h = y * s;
                    y = y * c;
                    for (let j = 0; j < n; j++) {
                        let x = v[j][i - 1];
                        let z = v[j][i];
                        v[j][i - 1] = x * c + z * s;
                        v[j][i] = -x * s + z * c;
                    }
                    z = pythag(f, h);
                    q[i - 1] = z;
                    c = f / z;
                    s = h / z;
                    f = c * g + s * y;
                    x = -s * g + c * y;
                    for (let j = 0; j < m; j++) {
                        let y = u[j][i - 1];
                        let z = u[j][i];
                        u[j][i - 1] = y * c + z * s;
                        u[j][i] = -y * s + z * c;
                    }
                }
                e[l] = 0;
                e[k] = f;
                q[k] = x;
            }
        }

        for (let i = 0; i < q.length; i++) {
            if (q[i] < prec) {
                q[i] = 0;
            }
        }

        // sort eigenvalues
        for (let i = 0; i < n; i++) {
            for (let j = i - 1; j >= 0; j--) {
                if (q[j] < q[i]) {
                    const c = q[j];
                    q[j] = q[i];
                    q[i] = c;
                    for (let k = 0; k < u.length; k++) {
                        const temp = u[k][i];
                        u[k][i] = u[k][j];
                        u[k][j] = temp;
                    }
                    for (let k = 0; k < v.length; k++) {
                        const temp = v[k][i];
                        v[k][i] = v[k][j];
                        v[k][j] = temp;
                    }
                    i = j;
                }
            }
        }

        return [
            Matrix.from(u).transpose(),
            Matrix.diag(Matrix.from(q)),
            Matrix.from(v).transpose()
        ];
    };


    /** This function returns a Vandermonde Matrix corresponding to
     * to the provided vector.
     * @param {Matrix} x
     * Vector used as basis to compute Vandermonde Matrix
     * @param {Integer} degree
     * used to limit the size of the output.
     */
    Matrix.vander = function (x, degree) {
        x = Matrix.from(x);
        if (!x.isvector()) {
            throw new Error("Matrix.vander: Input must be a vector.");
        }
        // Test if vector is provided
        let id = x.getData(),
            N = id.length;
        degree = degree === undefined ? N - 1 : degree;
        let out = Matrix.zeros(N, degree + 1),
            od = out.getData();
        for (let y = 0; y < N; y++) {
            let tmp = 1;
            for (let oxy = y + N * degree; oxy >= y; oxy -= N) {
                od[oxy] = tmp;
                tmp *= id[y];
            }
        }
        return out;
    };

    Matrix_prototype.vander = function (degree) {
        return Matrix.vander(this, degree);
    };

    /** This function computes the coefficients of a polynomial
     * of a given degree fitting the provided data.
     * @param {Matrix} x
     * @param {Matrix} y
     * @param {Integer} degree
     */
    Matrix.polyfit = function (x, y, degree) {
        return Matrix.from(x).vander(degree).mldivide(y);
    };

    /** This function computes the value of a polynomial
     * function for the provided values.
     * @param {Matrix} coeffs
     *  Coefficients of the polynomial
     * @param {Matrix} x
     *  value used to estimate the function
     */
    Matrix.polyval = function (p, x) {
        x = Matrix.from(x);
        p = Matrix.from(p);
        if (!p.isvector()) {
            throw new Error("Matrix.polyval: Parameter \"p\" must be a vector.");
        }
        if (!x.isvector()) {
            throw new Error("Matrix.polyval: Parameter \"x\" must be a vector.");
        }
        return x.vander(p.numel() - 1).mtimes(p);
    };

}
/*
(function () {
    var getColumnArray = function  (ad, M, N) {
        var j, col;
        if (ad instanceof Array) {
            for (j = 0, col = []; j < N; j++) {
                col[j] = ad.slice(j * M, (j + 1) * M);
            }
        } else {
            for (j = 0, col = []; j < N; j++) {
                col[j] = ad.subarray(j * M, (j + 1) * M);
            }
        }
        return col;
};


    var  getRow = function (ad, M, N, i, out) {
        out = out || new Float64Array(N);
        for (var j = 0, ij = i + j; j < N; j++, ij += M) {
            out[j] = ad[ij];
        }
        return out;
    };

    var rand = function (M, N) {
        var tab = new Float32Array(M * N);
        for (var i = 0; i < M * N; i++) {
            tab[i] = Math.random();
        }
        return getColumnArray(tab, M, N);
    };

    var dotproduct_real = function (a, b, N) {
        for (var i = 0, sum = 0.0; i < N; ++i) {
            sum += a[i] * b[i];
        }
        return sum;
    };

    var dotproduct_cplx = function (ar, ai, br, bi, N) {
        for (var i = 0, sumr = 0.0, sumi = 0.0; i < N; ++i) {
            var a = ar[i], b = ai[i], c = br[i], d = bi[i];
            sumr += a * c - b * d;
            sumi += a * d + b * c;
        }
        return [sumr, sumi];
    };

    var dotproduct_real_cplx = function (ar, br, bi, N) {
        for (var i = 0, sumr = 0.0, sumi = 0.0; i < N; ++i) {
            var a = ar[i];
            sumr += a * br[i];
            sumi += a * bi[i];
        }
        return [sumr, sumi];
    };

    var mtimes_real = function (a, b, c, M, N, K) {
        var i, j, row = new Float64Array(N);
        b = getColumnArray(b, N, K);
        c = getColumnArray(c, M, K);
        for (j = 0; j < M; j++) {
            row = getRow(a, M, N, j, row);
            for (i = 0; i < K; i++) {
                c[i][j] = dotproduct_real(row, b[i], N);
            }
        }
    };

    var mtimes_cplx = function (ar, ai, br, bi, cr, ci, M, N, K) {
        var i, j, dotp;
        var rowr = new Float64Array(N), rowi = new Float64Array(N);
        br = getColumnArray(br, N, K);
        bi = getColumnArray(bi, N, K);
        cr = getColumnArray(cr, M, K);
        ci = getColumnArray(ci, M, K);
        for (j = 0; j < M; j++) {
            rowr = getRow(ar, M, N, j, rowr);
            rowi = getRow(ai, M, N, j, rowi);
            for (i = 0; i < K; i++) {
                dotp = dotproduct_cplx(rowr, rowi, br[i], bi[i], N);
                cr[i][j] = dotp[0];
                ci[i][j] = dotp[1];
            }
        }
    };

    var mtimes_real_cplx = function (a, br, bi, cr, ci, M, N, K) {
        var i, j, row = new Float64Array(N), dotp;
        br = getColumnArray(br, N, K);
        bi = getColumnArray(bi, N, K);
        cr = getColumnArray(cr, M, K);
        ci = getColumnArray(ci, M, K);
        for (j = 0; j < M; j++) {
            row = getRow(a, M, N, j, row);
            for (i = 0; i < K; i++) {
                dotp = dotproduct_real_cplx(row, br[i], bi[i], N);
                cr[i][j] = dotp[0];
                ci[i][j] = dotp[1];
            }
        }
    };

    var mtimes_cplx_real = function (ar, ai, b, cr, ci, M, N, K) {
        var i, j, dotp;
        var rowr = new Float64Array(N), rowi = new Float64Array(N);
        b = getColumnArray(b, N, K);
        cr = getColumnArray(cr, M, K);
        ci = getColumnArray(ci, M, K);
        for (j = 0; j < M; j++) {
            rowr = getRow(ar, M, N, j, rowr);
            rowi = getRow(ai, M, N, j, rowi);
            for (i = 0; i < K; i++) {
                dotp = dotproduct_real_cplx(b[i], rowr, rowi, N);
                cr[i][j] = dotp[0];
                ci[i][j] = dotp[1];
            }
        }
    };

    var dotproduct_check = function () {
        var a = [1, 2, 3, 4], b = [5, 4, 3, 2];
        var t1 = dotproduct_real(a, b, 4);
        var t2 = dotproduct_cplx(a, b, b, a, 4);
        var t3 = dotproduct_real_cplx(a, b, b, 4);
        if (t1 !== 30 || t2[0] !== 0 || t2[1] !== 84 || t3[0] !== 30 || t3[1] !== 30) {
            throw new Error("Dot product change!");
        }
    };

    if (0) {
        Matrix.mtimes = function (A, B) {
            var M = A.getSize(0), N = A.getSize(1), K = B.getSize(1);
            var C = Matrix.zeros(M, K);
            var a, ar, ai, b, br, bi, c, cr, ci;
            if (A.isreal()) {
                a = A.getData();
                if (B.isreal()) {
                    b = B.getData();
                    c = C.getData();
                    mtimes_real(a, b, c, M, N, K);
                } else {
                    br = B.getRealData();
                    bi = B.getImagData();
                    C.toComplex();
                    cr = C.getRealData();
                    ci = C.getImagData();
                    mtimes_real_cplx(a, br, bi, cr, ci, M, N, K);
                }
            } else {
                ar = A.getRealData();
                ai = A.getImagData();
                C.toComplex();
                cr = C.getRealData();
                ci = C.getImagData();
                if (B.isreal()) {
                    b = B.getData();
                    mtimes_cplx_real(ar, ai, b, cr, ci, M, N, K);
                } else {
                    br = B.getRealData();
                    bi = B.getImagData();
                    mtimes_cplx(ar, ai, br, bi, cr, ci, M, N, K);
                }
            }
            return C;
        };
        Matrix_prototype.mtimes = function (B) {
            return Matrix.mtimes(this, B);
        };
    }

    var mtimes_check = function () {
        var c, cr, ci, r, rr, ri, t, tr, ti;
        var a = new Int8Array([6, 6, 1, 7, -8, 2, 0, -2, 1]);
        var b = new Int8Array([-6, 2, 3, -9, -5, 8, 2, -1, 4]);

        c = new Int8Array(9);
        r = [-22, -58, 1, -89, -30, -11, 5, 12, 4];
        mtimes_real(a, b, c, 3, 3, 3);
        if (!Check.areArrayEquals(c, r)) {
            throw new Error("Error mtimes real.");
        }

        cr = new Int16Array(9);
        ci = new Int16Array(9);
        rr = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        ri = [-44, -116, 2, -178, -60, -22, 10, 24, 8];
        mtimes_cplx(a, a, b, b, cr, ci, 3, 3, 3);
        if (!Check.areArrayEquals(cr, rr) || !Check.areArrayEquals(ci, ri)) {
            throw new Error("Error mtimes complex.");
        }

        cr = new Int16Array(9);
        ci = new Int16Array(9);
        mtimes_real_cplx(a, b, b, cr, ci, 3, 3, 3);
        if (!Check.areArrayEquals(cr, r) || !Check.areArrayEquals(ci, r)) {
            throw new Error("Error mtimes real/complex.");
        }

        cr = new Int16Array(9);
        ci = new Int16Array(9);
        mtimes_cplx_real(a, b, b, cr, ci, 3, 3, 3);
        ri = [24, -25, 10, 115, -1, -35, 5, 5, 14];
        if (!Check.areArrayEquals(cr, r) || !Check.areArrayEquals(ci, ri)) {
            throw new Error("Error mtimes complex/real.");
        }
    };

    Matrix._benchmarkMtimes = function (M, N, K) {
        M = M || 1000;
        N = N || M;
        K = K || M;

        var Ar = Matrix.rand(M, N), Br = Matrix.rand(N, K);
        var Ai = Matrix.rand(M, N), Bi = Matrix.rand(N, K);

        var r1, r2, rr1, ri1, rr2, ri2;
        Tools.tic();
        r1 = Matrix.mtimes(Ar, Br).getData();
        console.log("NEW mtimes REAL/REAL:", Tools.toc());
        // Tools.tic();
        // r2 = Ar.mtimes(Br).getData();
        // console.log("OLD mtimes REAL/REAL:", Tools.toc());
        // if (!Check.areArrayEquals(r1, r2)) {
        // throw new Error("Error mtimes complex.");
        // }

        var Ac = Matrix.complex(Ar, Ai), Bc = Matrix.complex(Br, Bi);
        Tools.tic();
        r1 = Matrix.mtimes(Ac, Bc);
        console.log("NEW mtimes CPLX/CPLX:", Tools.toc());

         // rr1 = r1.getRealData();
         // ri1 = r1.getImagData();
         // Tools.tic();
         // r2 = Ac.mtimes(Bc);
         // console.log("OLD mtimes CPLX/CPLX:", Tools.toc());
         // rr2 = r2.getRealData();
         // ri2 = r2.getImagData();
         // if (!Check.areArrayEquals(rr1, rr2) || !Check.areArrayEquals(rr1, rr2)) {
         // throw new Error("Error mtimes complex.");
         // }

    };

    Matrix._testsMtimes = function () {
        dotproduct_check();
        mtimes_check();_
    };

})();
*/
