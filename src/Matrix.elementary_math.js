/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */

export default function trigonometryExtension (Matrix, Matrix_prototype) {


    ////////////////////////////////////////////////////////////////////////////
    //                         MATH OBJECT OPERATORS                          //
    ////////////////////////////////////////////////////////////////////////////


    /** Apply the square root function to values of Matrix.
    *
    * @chainable
    * @matlike
    * @method sqrt
    */
    {
        const sqrt_real = function (data) {
            const ie = data.length;
            let i;
            for (i = 0; i < ie; i++) {
                if (data[i] < 0) {
                    break;
                }
                data[i] = Math.sqrt(data[i]);
            }
            if (i !== ie) {
                return i;
            }
        };

        const sqrt_cplx = function (datar, datai, offset = 0) {
            const ie = datar.length;
            let i;
            for (i = offset; i < ie; i++) {
                const a = datar[i], b = datai[i];
                if (b !== 0 || a < 0) {
                    const m = Math.sqrt(a * a + b * b);
                    datar[i] = Math.sqrt((a + m) * 0.5);
                    datai[i] = b < 0 ? -Math.sqrt((m - a) * 0.5) : Math.sqrt((m - a) * 0.5);
                } else {
                    datar[i] = Math.sqrt(datar[i]);
                }
            }
        };

        Matrix_prototype.sqrt = function () {
            if (!this.hasImagPart()) {
                const offset = sqrt_real(this.getData());
                if (offset) {
                    this.toComplex();
                    sqrt_cplx(this.getRealData(), this.getImagData(), offset);
                }
            } else {
                sqrt_cplx(this.getRealData(), this.getImagData());
            }
            return this;
        };
    }
    Matrix.sqrt = function (A) {
        return Matrix.from(A).clone().sqrt();
    };


    /** Apply the exponential function to values of Matrix.
    *
    * @chainable
    * @todo This function must be tested on complex numbers
    * @matlike
    * @method exp
    */
    {
        const exp_real = function (data) {
            const ie = data.length;
            let i;
            for (i = 0; i < ie; i++) {
                data[i] = Math.exp(data[i]);
            }
        };
        const exp_cplx = function (datar, datai) {
            const ie = datar.length;
            let i;
            for (i = 0; i < ie; i++) {
                const a = Math.exp(datar[i]), b = datai[i];
                if (b === 0) {
                    datar[i] = a;
                } else {
                    datar[i] = a * Math.cos(b);
                    datai[i] = a * Math.sin(b);
                }
            }
        };
        Matrix_prototype.exp = function () {
            if (this.isreal()) {
                exp_real(this.getData());
            } else {
                exp_cplx(this.getRealData(), this.getImagData());
            }
            return this;
        };
    }
    Matrix.exp = function (A) {
        return Matrix.from(A).clone().exp();
    };

    /** Apply the cosine function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.cos = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.cos(data[i]);
        }
        return this;
    };

    Matrix.cos = function (A) {
        return Matrix.from(A).clone().cos();
    };

    /** Apply the arccosine function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.acos = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.acos(data[i]);
        }
        return this;
    };

    Matrix.acos = function (A) {
        return Matrix.from(A).clone().acos();
    };

    /** Apply the sine function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.sin = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.sin(data[i]);
        }
        return this;
    };

    Matrix.sin = function (A) {
        return Matrix.from(A).clone().sin();
    };

    /** Apply the arcsine function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.asin = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.asin(data[i]);
        }
        return this;
    };

    Matrix.asin = function (A) {
        return Matrix.from(A).clone().asin();
    };

    /** Apply the tangent function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.tan = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.tan(data[i]);
        }
        return this;
    };

    Matrix.tan = function (A) {
        return Matrix.from(A).clone().tan();
    };

    /** Apply the arctan function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.atan = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.atan(data[i]);
        }
        return this;
    };

    Matrix.atan = function (A) {
        return Matrix.from(A).clone().atan();
    };

    /** Apply the arctan2 function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix.atan2 = function (A, B) {
        A = Matrix.from(A);
        B = Matrix.from(B);
        if (!Check.checkSizeEquals(A.size(), B.size(), Matrix.ignoreTrailingDims)) {
            throw new Error("Matrix.atan2: Side of of elements must be equal.");
        }
        const dataA = A.getData(), dataB = B.getData();
        const out = new Matrix(A.getSize()), od = out.getData();
        const ie = dataA.length;
        let i;
        for (i = 0; i < ie; i++) {
            od[i] = Math.atan2(dataA[i], dataB[i]);
        }
        return out;
    };

    /** Apply the natural logarithm function to the values of the Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.log = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.log(data[i]);
        }
        return this;
    };

    Matrix.log = function (A) {
        return Matrix.from(A).clone().log();
    };

    /** Apply the base 10 logarithm to the values of the Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.log10 = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.log10(data[i]);
        }
        return this;
    };

    Matrix.log10 = function (A) {
        return Matrix.from(A).clone().log10();
    };

    /** Apply the base 2 logarithm to the values of the Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.log2 = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.log2(data[i]);
        }
        return this;
    };

    Matrix.log2 = function (A) {
        return Matrix.from(A).clone().log2();
    };

    /** Apply the floor function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.floor = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.floor(data[i]);
        }
        return this;
    };

    Matrix.floor = function (A) {
        return Matrix.from(A).clone().floor();
    };

    /** Apply the ceil function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.ceil = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.ceil(data[i]);
        }
        return this;
    };

    Matrix.ceil = function (A) {
        return Matrix.from(A).clone().ceil();
    };

    /** Apply the round function to values of Matrix.
    *
    * @chainable
    * @todo This function should work with complex
    * @matlike
    */
    Matrix_prototype.round = function () {
        var data = this.getData(), i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = Math.round(data[i]);
        }
        return this;
    };

    Matrix.round = function (A) {
        return Matrix.from(A).clone().round();
    };

    /** Return a Matrix containg of the same where values are
    * either -1, 0, 1 depending on the sign of the elements.
    *
    * @matlike
    */
    Matrix_prototype.sign = function () {
        const d = this.getData();
        const out = new Matrix(this.getSize()), od = out.getData();
        const ie = d.length;
        let i;
        for (i = 0; i < ie; i++) {
            const v = d[i];
            if (v > 0) {
                od[i] = 1;
            } else if (v === 0) {
                od[i] = v; // To handle difference between -0 and 0
            } else {
                od[i] = -1;
            }
            // od[i] = Math.sign(d[i]);
        }
        return out;
    };
    Matrix.sign = function (A) {
        return Matrix.from(A).sign();
    };

}
