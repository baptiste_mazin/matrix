/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import MatrixView, {Check} from "@etsitpab/matrixview";

/** @class Matrix */

export default function manipulationExtension (Matrix, Matrix_prototype) {


    //////////////////////////////////////////////////////////////////
    //          Primitives Extraction/Insertion Functions           //
    //////////////////////////////////////////////////////////////////


    /** Extracts Matrix data given a MatrixView into a another matrix.
    * - The size of the returned (or provided) matrix will (or has to)
    *   match the current view size.
    * - and the size of the input matrix has to match the initial view size.
    *
    * @param {MatrixView} view
    *   View on input matrix where the data comes from.
    *
    * @param {Matrix} [output]
    *   Output Matrix where data will be extracted. If provided, its size must
    * match the view current view size.
    *
    * @return {Matrix}
    *
    * @private
    */
    Matrix_prototype.extractViewFrom = function (v, mat) {
        if (mat === undefined) {
            mat = new Matrix(v.getSize(), this.getDataType(), !this.isreal());
        } else if (!Check.areSizeEquals(v.getInitialSize(), this.size())) {
            throw new Error("Matrix.extractTo: View initial size must match input size.");
        } else if (!Check.areSizeEquals(v.getSize(), mat.size())) {
            throw new Error("Matrix.extractTo: View current size must match output size.");
        }

        if (this.isreal()) {
            if (mat.hasImagPart()) {
                v.extractFrom(this.getData(), mat.getRealData());
            } else {
                v.extractFrom(this.getData(), mat.getData());
            }
        } else {
            if (!mat.hasImagPart()) {
                mat.toComplex();
            }
            v.extractFrom(this.getRealData(), mat.getRealData());
            v.extractFrom(this.getImagData(), mat.getImagData());
        }
        return mat;
    };

    /** Extracts Matrix data into another matrix with a specified view.
    * - The size of the returned (or provided) matrix will (or has to)
    *   match the view initial size.
    * - and the size of the input matrix has to match the current view size.
    *
    * @param {MatrixView} view
    * View on matrix where data will be extracted.
    *
    * @param {Matrix} [output]
    * Output Matrix where data will be extracted. If provided, its size must
    * match the view initial size.
    *
    * @return {Matrix}
    *
    * @private
    */
    Matrix_prototype.extractViewTo = function (v, mat) {
        if (mat === undefined) {
            mat = new Matrix(v.getInitialSize(), this.getDataType(), !this.isreal());
        } else if (!Check.areSizeEquals(v.getInitialSize(), mat.size())) {
            throw new Error(`Matrix.extractTo: View initial size must match output size. Got [${v.getInitialSize()}] and [${mat.size()}]`);
        } else if (!Check.areSizeEquals(v.getSize(), this.size()) && !Check.areSizeEquals(this.getSize(), [1, 1])) {
            throw new Error(`Matrix.extractTo: Input size must match View current size or be a scalar. Got [${this.getSize()}] and [${v.getSize()}]`);
        }
        if (this.isreal()) {
            if (mat.isreal()) {
                v.extractTo(this.getData(), mat.getData());
            } else {
                v.extractTo(this.getData(), mat.getRealData());
                v.extractTo(0, mat.getImagData());
            }
        } else {
            if (!mat.hasImagPart()) {
                mat.toComplex();
            }
            v.extractTo(this.getRealData(), mat.getRealData());
            v.extractTo(this.getImagData(), mat.getImagData());
        }
        return mat;
    };

    const arrayToBoolean = function (array) {
        array = Array.from(array);
        let i, ei;
        for (i = 0, ei = array.length; i < ei; i++) {
            array[i] = array[i] ? true : false;
        }
        return array;
    };

    /** Apply a selection on the view given different arguments.
    *
    * @param {Matrix} selection
    *
    *  1. There is only one ND-Matrix containing either:
    *    a) `Booleans`: select all the corresponding indices,
    *    b) `Integers`: select the indices corresponding to the the integers.
    *  2. There is one or more 1D Matrix containing either:
    *    a) `Booleans`: select all the corresponding indices,
    *    b) `Integers`: select the indices corresponding to
    *                   the integer.
    *
    * @return {MatrixView}
    *
    * @private
    */
    const selectView = function (input, args) {
        const eMsg = "Matrix.get/set:";

        // Global selection
        let arg = args[0];
        if (arg instanceof Matrix && args.length === 1) {
            if (!arg.isreal()) {
                throw new Error(`${eMsg} Selection matrix must be real not complex.`);
            }
            const data = arg.getData();
            // Boolean selection
            if (arg.islogical() && Check.checkSizeEquals(input.getSize(), arg.getSize(), Matrix.ignoreTrailingDims)) {
                return new MatrixView([data.length, 1]).selectDimByBooleans(0, data);
            }
            // Indices selection
            return new MatrixView([input.numel(), 1]).selectDimByIndices(0, data);
        }

        // Dimension selection
        args = args.map(arg => {
            if (arg instanceof Matrix) {
                if (!arg.isvector()) {
                    throw new Error(`${eMsg} Selection must be a vector, got matrix with size = ${arg.size()}.`);
                }
                if (arg.islogical()) {
                    arg = arrayToBoolean(arg.getData());
                } else {
                    arg = [arg.getData()];
                }
            }
            return arg;
        });
        return input.getView().select(...args);
    };


    /** Allow to extract a subpart of the Matrix for each dimension
    * if no arguments is provided then it will return a new vector
    * with all the elements one after the others.
    * It acts like Matlab colon operator.
    *
    * @param {Integer[]} [select]
    *  For each dimension, can be an array-like formatted as:
    *
    *  - `[startValue]`
    *  - `[startValue, endValue]`
    *  - `[startValue, step, endValue]`
    *
    * @return {Matrix}
    *  Returns a new Matrix with containing selected values.
    *
    * @fixme when only one number is provided, should the function consider this
    * number as an indice and return the corresponding value ?
    */
    Matrix_prototype.get = function (...select) {
        if (select.length === 0) {
            return this.clone();
        }
        const view = selectView(this, select);
        if (select.length === 1 && select[0] instanceof Matrix) {
            if (view.getLength() === select[0].getLength()) {
                return this.extractViewFrom(view).reshape(select[0].getSize());
            }
            return this.extractViewFrom(view);
        }
        return this.extractViewFrom(view);
    };

    /** Return a copy of the Matrix with modified values according to the
    * input arguments.
    *
    * @return {Matrix}
    *
    * @fixme This function does not look very clean.
    */
    Matrix_prototype.set = function (...select) {
        const val = Matrix.from(select.pop());
        const view = selectView(this, select);
        if (select.length === 1 && select[0] instanceof Matrix) {
            const valSize = val.getSize(), initialSize = this.getSize();
            val.reshape().extractViewTo(view, this.reshape());
            val.reshape(valSize);
            // const initialSize = this.getSize();
            // val.extractViewTo(view, this.reshape());
            return this.reshape(initialSize);
        }
        return val.extractViewTo(view, this);
    };
    Matrix.set = function (mat, ...args) {
        if (!(mat instanceof Matrix)) {
            throw new Error("Matrix.set: Matrix to modify must be provided.");
        }
        return mat.clone().set(...args);
    };

    Matrix.reshape = function (mat, ...args) {
        if (!(mat instanceof Matrix)) {
            throw new Error("Matrix.reshape: Matrix to modify must be provided.");
        }
        return mat.clone().reshape(...args);
    };

    //////////////////////////////////////////////////////////////////
    //                      Matrix Manipulation                     //
    //////////////////////////////////////////////////////////////////

    /** Transpose operator transposed a 2D matrix.
    *
    * @return {Matrix}
    * @matlike
    */
    Matrix_prototype.transpose = function () {
        if (!this.ismatrix()) {
            throw new Error('Matrix.transpose: ' +
            'Transposition is only defined for matrix.');
        }
        const v = this.getView().swapDimensions(0, 1);
        return this.extractViewFrom(v);
    };
    Matrix.transpose = function (A) {
        return A.transpose();
    };

    /** Permutes the order of dimension.
    *
    * __Also see:__
    *  {@link Matrix#ipermute}.
    *
    * @param {Array} dimensionOrder
    *  Defines the order in which
    *  the dimensions are traversed.
    *
    * @return {Matrix} new Matrix with permuted dimensions.
    *
    * @matlike
    */
    Matrix_prototype.permute = function (dim) {
        const v = this.getView().permute(dim);
        return this.extractViewFrom(v);
    };
    Matrix.permute = function (input, ...args) {
        return input.permute(...args);
    };

    /** Inverse dimension permutation.
    *
    * @method ipermute
    *  {@link Matrix#permute}.
    *
    * @param {Integer[]} k
    *  Dim order to inverse.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.ipermute = function (dim) {
        const v = this.getView().ipermute(dim);
        return this.extractViewFrom(v);
    };
    Matrix.ipermute = function (input, ...args) {
        return input.ipermute(...args);
    };

    /** Inverse the scan order of two dimension.
    *
    * @param {Integer} [n=undefined]
    *  Shift argument :
    *
    *  + If n is undefined then remove all first singleton dimensions.
    *  + if n > 0 shift the n first dimension to the end.
    *  + if n < 0 then insert n singleton dimension at the start.
    *
    * @return {Array}
    *  Array containing:
    *
    *  - The new matrix,
    *  - the number of shift done.
    *
    * @matlike
    */
    Matrix_prototype.shiftdim = function (n) {
        const v = this.getView();
        const k = v.shiftDimension(n);
        if (typeof(k) === "number") {
            return [this.extractViewFrom(v), k];
        }
        return [this.extractViewFrom(v), n];
    };

    /** Circular shift on given dimensions.
    *
    * __Also see:__
    * {@link Matrix#permute},
    * {@link Matrix#shiftdim}.
    *
    * @param {Integer[]} shift Defines the shift on each dimension.
    *
    * @param {Integer[]}  [dimension] To be specified if shift argument
    *  is a scalar. Corresponds to which dimension must be shifted.
    *
    * @method circshift
    *
    * @chainable
    */
    Matrix_prototype.circshift = function (K, dim) {
        const v = this.getView().circshift(K, dim);
        return this.extractViewFrom(v);
    };

    /** Rotates Matrix counter-clockwise by a multiple of 90 degrees.
    *
    * @param {Integer} k
    *  Defines the number 90 degrees rotation.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.rot90 = function (k) {
        const v = this.getView().rot90(k);
        return this.extractViewFrom(v);
    };

    /** Flip matrix along a specific dimension.
    *
    * __Also see:__
    * {@link Matrix#fliplr},
    * {@link Matrix#flipud}.
    *
    * @param {Integer} dimension
    *  Dimension to flip.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.flipdim = function (d) {
        const v = this.getView().selectDimByColon(d, [-1, 0]);
        return this.extractViewFrom(v);
    };

    /** Flip matrix left to right.
    *
    * __Also see:__
    * {@link Matrix#flipdim},
    * {@link Matrix#flipud}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.fliplr = function () {
        return this.flipdim(1);
    };

    /** Flip matrix up to down.
    *
    * __Also see:__
    * {@link Matrix#fliplr},
    * {@link Matrix#flipdim}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.flipud = function () {
        return this.flipdim(0);
    };

    /** Repeat the matrix along multiple dimensions.
    * Matlab-like function repmat.
    *
    * @param {Integer[]} select
    *  For each dimension, specify the number of repetition
    *
    * @return {Matrix} new Matrix.
    *
    * @matlike
    */
    Matrix_prototype.repmat = function (...args) {
        // Check parameters
        const size = Check.checkSize(args, "square");

        // Output size computation
        const iSize = this.size(), sizeOut = [];
        let i, ie;
        for (i = 0, ie = Math.max(iSize.length, size.length); i < ie; i++) {
            sizeOut[i] = (iSize[i] || 1) * (size[i] || 1);
        }

        // Output matrix, view and data
        const om = new Matrix(sizeOut, this.getDataType(), !this.isreal());
        // Input and output data
        let ird, iid, ord, oid;
        if (this.isreal()) {
            ord = om.getData();
            ird = this.getData();
        } else {
            ord = om.getRealData();
            oid = om.getImagData();
            ird = this.getRealData();
            iid = this.getImagData();
        }

        // Output coarse view arrangement
        const ov = om.getView();
        for (i = 0, ie = iSize.length; i < ie; i++) {
            ov.selectDimByColon(i, [0, iSize[i], -1]);
        }

        // Input iterator
        const iv = this.getView();
        // Output coarse iterator
        const {iterator: ito, begin: bo, isEnd: eo, getPosition} = ov.getIterator(0);
        for (bo(); !eo(); ito()) {
            // Output sub iterator
            const sov = om.getView(), pos = getPosition();
            // Output fine view arrangement
            for (i = 0, ie = size.length; i < ie; i++) {
                const sTmp = iSize[i] || 1, tmp = pos[i] * sTmp;
                sov.selectDimByColon(i, [tmp, 1, tmp + sTmp - 1]);
            }
            // Copy to output
            iv.extract(ird, sov, ord);
            if (!this.isreal()) {
                iv.extract(iid, sov, oid);
            }
        }

        return om;
    };
    Matrix.repmat = function (input, ...args) {
        return input.repmat(...args);
    };

    /** Concatenate different Matrix along a given dimension.
    *
    * @param {Integer} dimension
    *  The dimension on which the matrix must be concatenate.
    *
    * @param {Matrix} m
    *  A list of matrices to concatenate. All dimension should be equals
    *  except the one corresponding to the parameter `dimension`.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.cat = function (dim, ...args) {

        // Ouptut Size
        const outputSize = this.getSize();
        if (outputSize[dim] === undefined) {
            outputSize[dim] = 1;
        }

        let i;
        const ie = args.length;
        for (i = 0; i < ie; i++) {
            args[i] = Matrix.from(args[i]);
            outputSize[dim] += args[i].getSize(dim);
        }

        // Output matrix
        const O = new Matrix(outputSize), v = O.getView();

        // Copy first Matrix
        let start = this.getSize(dim) - 1;
        v.selectDimByColon(dim, [0, start]);
        this.extractViewTo(v, O);
        v.restore();

        // Copy others matrix
        for (i = 0; i < ie; i++) {
            v.selectDimByColon(dim, [start + 1, start += args[i].getSize(dim)]);
            args[i].extractViewTo(v, O);
            v.restore();
        }
        return O;
    };
    Matrix.cat = function (dim, mat, ...args) {
        if (!Check.isInteger(dim)) {
            throw new Error(`Matrix.cat: Dim argument must be an integer, got ${dim}.`);
        }
        return Matrix.from(mat).cat(dim, ...args);
    };
}
