/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */

export default function arithmeticOperatorsExtension (Matrix, Matrix_prototype) {

    //////////////////////////////////////////////////////////////////
    //                     Arithmetic Operators                     //
    //////////////////////////////////////////////////////////////////


    /** Plus operator make an element wise addition.
    * This operation is done in place.
    *
    * __See also:__
    *  {@link Matrix#plus},
    *  {@link Matrix#minus},
    *  {@link Matrix#times},
    *  {@link Matrix#rdivide},
    *  {@link Matrix#ldivide}.
    *  {@link Matrix#ldivide}.
    *
    * @param {Number|Matrix} rightOp
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.plus = function (v) {
        v = Matrix.from(v);
        const n = this.numel();
        let u = this;
        if (v.isscalar()) { //  V SCALAR
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL SCALAR
                    const a = u.getData();
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] += c;
                    }
                } else { //          U REAL / V CPLX SCALAR
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    b.fill(d);
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] += c;
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V CPLX SCALAR
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] += c;
                    }
                } else { //          U CPLX / V CPLX SCALAR
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] += c;
                        b[x] += d;
                    }
                }
            }
        } else { //  V MATRIX
            Check.checkSizeEquals(u.getSize(), v.getSize(), Matrix.ignoreTrailingDims);
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL MATRIX
                    const a = u.getData();
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] += c[x];
                    }
                } else { //          U REAL / V CPLX MATRIX
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] += c[x];
                        b[x] = d[x];
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V REAL MATRIX
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] += c[x];
                    }
                } else { //          U CPLX / V CPLX MATRIX
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] += c[x];
                        b[x] += d[x];
                    }
                }
            }
        }
        return this;
    };

    Matrix_prototype['+='] = function (b) {
        return this.plus(b);
    };

    Matrix_prototype['+'] = function (b) {
        return this.clone().plus(b);
    };

    Matrix.plus = function (A, B) {
        A = Matrix.from(A);
        return A.isscalar() ? Matrix.from(B)['+'](A) : A['+'](B);
    };


    /** Minus operator make an element wise subtraction.
    *
    * __Also see:__
    * {@link Matrix#uminus},
    * {@link Matrix#plus},
    * {@link Matrix#times},
    * {@link Matrix#rdivide},
    * {@link Matrix#ldivide},
    *
    * @param {Number|Matrix} rightOp
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.minus = function (v) {
        v = Matrix.from(v);
        const n = this.numel();
        let u = this;
        if (v.isscalar()) { //  V SCALAR
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL SCALAR
                    const a = u.getData();
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] -= c;
                    }
                } else { //          U REAL / V CPLX SCALAR
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    b.fill(-d);
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] -= c;
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V CPLX SCALAR
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] -= c;
                    }
                } else { //          U CPLX / V CPLX SCALAR
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] -= c;
                        b[x] -= d;
                    }
                }
            }
        } else { //  V MATRIX
            Check.checkSizeEquals(u.getSize(), v.getSize(), Matrix.ignoreTrailingDims);
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL MATRIX
                    const a = u.getData();
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] -= c[x];
                    }
                } else { //          U REAL / V CPLX MATRIX
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] -= c[x];
                        b[x] = -d[x];
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V REAL MATRIX
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] -= c[x];
                    }
                } else { //          U CPLX / V CPLX MATRIX
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] -= c[x];
                        b[x] -= d[x];
                    }
                }
            }
        }
        return this;
    };

    Matrix_prototype['-='] = function (b) {
        return this.minus(b);
    };

    Matrix_prototype['-'] = function (b) {
        return this.clone().minus(b);
    };

    Matrix.minus = function (A, B) {
        A = Matrix.from(A);
        return A.isscalar() ? Matrix.from(B.uminus())['+'](A) : Matrix.from(A)['-'](B);
    };


    /** Uminus operator take the opposite of each Matrix value.
    *
    * @param {Number|Matrix} rightOp
    *
    * @chainable
    * @todo take into account the complex case.
    * @matlike
    */
    Matrix_prototype.uminus = function () {
        var x, ld = this.numel();
        if (this.isreal()) {
            var a = this.getData();
            for (x = 0; x < ld; x++) {
                a[x] = -a[x];
            }
        } else {
            var ar = this.getRealData(), ai = this.getImagData();
            for (x = 0; x < ld; x++) {
                ar[x] = -ar[x];
                ai[x] = -ai[x];
            }
        }
        return this;
    };

    Matrix.uminus = function (A) {
        return Matrix.from(A).clone().uminus();
    };


    /** Times operator make an element wise multiplication.
    *
    * __Also see:__
    * {@link Matrix#minus},
    * {@link Matrix#plus},
    * {@link Matrix#rdivide},
    * {@link Matrix#ldivide}.
    *
    * @param {Number|Matrix} rightOp
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.times = function (v) {
        v = Matrix.from(v);
        const n = this.numel();
        let u = this;
        if (v.isscalar()) { //  V SCALAR
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL SCALAR
                    const a = u.getData();
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] *= c;
                    }
                } else { //          U REAL / V CPLX SCALAR
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    let x;
                    for (x = 0; x < n; x++) {
                        b[x] = a[x] * d;
                        a[x] *= c;
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V CPLX SCALAR
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] *= c;
                        b[x] *= c;
                    }
                } else { //          U CPLX / V CPLX SCALAR
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    let x;
                    for (x = 0; x < n; x++) {
                        const r = a[x], i = b[x];
                        a[x] = r * c - i * d;
                        b[x] = r * d + i * c;
                    }
                }
            }
        } else { //  V MATRIX
            Check.checkSizeEquals(u.getSize(), v.getSize(), Matrix.ignoreTrailingDims);
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL MATRIX
                    const a = u.getData();
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] *= c[x];
                    }
                } else { //          U REAL / V CPLX MATRIX
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        b[x] = a[x] * d[x];
                        a[x] *= c[x];
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V REAL MATRIX
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] *= c[x];
                        b[x] *= c[x];
                    }
                } else { //          U CPLX / V CPLX MATRIX
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        const r1 = a[x], i1 = b[x], r2 = c[x], i2 = d[x];
                        a[x] = r1 * r2 - i1 * i2;
                        b[x] = i1 * r2 + r1 * i2;
                    }
                }
            }
        }
        return this;
    };

    Matrix_prototype['*='] = Matrix_prototype.times;

    Matrix_prototype['.*'] = function (b) {
        return this.clone().times(b);
    };

    Matrix.times = function (A, B) {
        A = Matrix.from(A);
        return A.isscalar() ? Matrix.from(B)['.*'](A) : Matrix.from(A)['.*'](B);
    };


    /** Rdivide operator make an element wise division,
    * The right term is the denominator.
    *
    * __Also see:__
    * {@link Matrix#minus},
    * {@link Matrix#plus},
    * {@link Matrix#rdivide},
    * {@link Matrix#ldivide}.
    *
    * @param {Number|Matrix} rightOp
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.rdivide = function (v) {
        v = Matrix.from(v);
        const n = this.numel();
        let u = this;
        if (v.isscalar()) { //  V SCALAR
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL SCALAR
                    const a = u.getData();
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] /= c;
                    }
                } else { //          U REAL / V CPLX SCALAR
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    b.set(a);
                    const tmp = 1 / (c * c + d * d);
                    const r2 = c * tmp;
                    const i2 = -d * tmp;
                    let x;
                    for (x = 0; x < n; x++) {
                        b[x] *= i2;
                        a[x] *= r2;
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V CPLX SCALAR
                    const c = v.asScalar();
                    const ic = 1 / c;
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] *= ic;
                        b[x] *= ic;
                    }
                } else { //          U CPLX / V CPLX SCALAR
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    const tmp = 1 / (c * c + d * d);
                    const r2 = c * tmp;
                    const i2 = d * tmp;
                    let x;
                    for (x = 0; x < n; x++) {
                        const r1 = a[x], i1 = b[x];
                        a[x] = r1 * r2 + i1 * i2;
                        b[x] = i1 * i2 - r1 * r2;
                    }
                }
            }
        } else { //  V MATRIX
            Check.checkSizeEquals(u.getSize(), v.getSize(), Matrix.ignoreTrailingDims);
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL MATRIX
                    const a = u.getData();
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] /= c[x];
                    }
                } else { //          U REAL / V CPLX MATRIX
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        const r1 = a[x], r2 = c[x], i2 = d[x];
                        const t = 1 / (r2 * r2 + i2 * i2);
                        a[x] = r1 * r2 * t;
                        b[x] = -r1 * i2 * t;
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V REAL MATRIX
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        const ic = 1 / c[x];
                        a[x] *= ic;
                        b[x] *= ic;
                    }
                } else { //          U CPLX / V CPLX MATRIX
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        const r1 = a[x], i1 = b[x], r2 = c[x], i2 = d[x];
                        const t = 1 / (r2 * r2 + i2 * i2);
                        a[x] = (r1 * r2 + i1 * i2) * t;
                        b[x] = (i1 * r2 - r1 * i2) * t;
                    }
                }
            }
        }
        return this;
    };

    Matrix_prototype['/='] = Matrix_prototype.rdivide;

    Matrix_prototype['./'] = function (V) {
        return Matrix.rdivide(this, V);
    };

    Matrix.rdivide = function (U, V) {
        U = Matrix.from(U);
        V = Matrix.from(V);
        if (V.isscalar()) {
            return U.clone().rdivide(V);
        }
        return V.clone().ldivide(U);
    };

    /** Ldivide operator make an element wise division,
    * The right term is the numerator.
    *
    * __Also see:__
    * {@link Matrix#minus},
    * {@link Matrix#plus},
    * {@link Matrix#rdivide},
    * {@link Matrix#ldivide}.
    *
    * @param {Number|Matrix} rightOp
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.ldivide = function (v) {
        v = Matrix.from(v);
        const n = this.numel();
        const u = this;
        if (v.isscalar()) { //  V SCALAR
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL SCALAR
                    const a = u.getData();
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] = c / a[x];
                    }
                } else { //          U REAL / V CPLX SCALAR
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    let x;
                    for (x = 0; x < n; x++) {
                        b[x] = d / a[x];
                        a[x] = c / a[x];
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V CPLX SCALAR
                    const c = v.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        const r1 = a[x],
                            i1 = b[x];
                        const tmp = c / (r1 * r1 + i1 * i1);
                        a[x] *= tmp;
                        b[x] *= -tmp;
                    }
                } else { //          U CPLX / V CPLX SCALAR
                    const c = v.getRealData()[0];
                    const d = v.getImagData()[0];
                    let x;
                    for (x = 0; x < n; x++) {
                        const r1 = a[x],
                            i1 = b[x];
                        const tmp = 1 / (r1 * r1 + i1 * i1);
                        a[x] = (r1 * c + i1 * d) * tmp;
                        b[x] = (r1 * d - i1 * c) * tmp;
                    }
                }
            }
        } else { //  V MATRIX
            Check.checkSizeEquals(u.getSize(), v.getSize(), Matrix.ignoreTrailingDims);
            if (u.isreal()) { //      U REAL
                if (v.isreal()) { //          U REAL / V REAL MATRIX
                    const a = u.getData();
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] = c[x] / a[x];
                    }
                } else { //          U REAL / V CPLX MATRIX
                    u.toComplex();
                    const a = u.getRealData();
                    const b = u.getImagData();
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        const ia = 1 / a[x];
                        a[x] = c[x] * ia;
                        b[x] = d[x] * ia;
                    }
                }
            } else { //      U CPLX
                const a = u.getRealData();
                const b = u.getImagData();
                if (v.isreal()) { //          U CPLX / V REAL MATRIX
                    const c = v.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        const r1 = a[x],
                            i1 = b[x];
                        const tmp = c[x] / (r1 * r1 + i1 * i1);
                        a[x] *= tmp;
                        b[x] *= -tmp;
                    }
                } else { //          U CPLX / V CPLX MATRIX
                    const c = v.getRealData();
                    const d = v.getImagData();
                    let x;
                    for (x = 0; x < n; x++) {
                        const r1 = c[x],
                            i1 = d[x],
                            r2 = a[x],
                            i2 = b[x];
                        const t = 1 / (r2 * r2 + i2 * i2);
                        a[x] = (r1 * r2 + i1 * i2) * t;
                        b[x] = (i1 * r2 - r1 * i2) * t;
                    }
                }
            }
        }
        return this;
    };

    Matrix_prototype['\\='] = Matrix_prototype.ldivide;

    Matrix_prototype['.\\'] = function (V) {
        return Matrix.ldivide(this, V);
    };

    Matrix.ldivide = function (U, V) {
        U = Matrix.from(U);
        V = Matrix.from(V);
        if (V.isscalar()) {
            return U.clone().ldivide(V);
        }
        return V.clone().rdivide(U);
    };


    /** Power operator make an element wise power operation,
    *
    * __Also see:__
    * {@link Matrix#minus},
    * {@link Matrix#plus},
    * {@link Matrix#rdivide},
    * {@link Matrix#ldivide}.
    *
    * @param {Number|Matrix} rightOp
    *
    * @chainable
    * @matlike
    */
    Matrix_prototype.power = function (b) {
        b = Matrix.from(b);
        const n = this.numel();
        const {pow, sqrt, cos, sin, atan2} = Math;
        if (b.isscalar()) {               // SCALAR
            if (this.isreal()) {             // REAL
                if (b.isreal()) {         // REAL / REAL
                    const a = this.getData();
                    b = b.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] = pow(a[x], b);
                    }
                } else {                  // REAL / IMAG
                    throw new Error('Matrix.power: This function has not been implemented yet for complex number.');
                }
            } else {                      // IMAG
                const ar = this.getRealData();
                const ai = this.getImagData();
                if (b.isreal()) {         // IMAG / REAL
                    b = b.asScalar();
                    let x;
                    for (x = 0; x < n; x++) {
                        const rb = pow(sqrt(ar[x] * ar[x] + ai[x] * ai[x]), b);
                        const tb = b * atan2(ai[x], ar[x]);
                        ar[x] = rb * cos(tb);
                        ai[x] = rb * sin(tb);
                    }
                } else {                  // IMAG / IMAG
                    throw new Error('Matrix.power: This function has not been implemented yet for complex number.');
                }
            }
        } else {                          // MATRIX
            Check.checkSizeEquals(this.getSize(), b.getSize(), Matrix.ignoreTrailingDims);
            if (this.isreal()) {             // MATRIX: REAL
                if (b.isreal()) {         // MATRIX: REAL / REAL
                    const a = this.getData();
                    b = b.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        a[x] = pow(a[x], b[x]);
                    }
                } else {                  // MATRIX: REAL / IMAG
                    throw new Error('Matrix.power: This function has not been implemented yet for complex number.');
                }
            } else {                      // MATRIX: IMAG
                const ar = this.getRealData();
                const ai = this.getImagData();
                if (b.isreal()) {         // MATRIX: IMAG / REAL
                    b = b.getData();
                    let x;
                    for (x = 0; x < n; x++) {
                        const rb = pow(sqrt(ar[x] * ar[x] + ai[x] * ai[x]), b[x]);
                        const tb = b[x] * atan2(ai[x], ar[x]);
                        ar[x] = rb * cos(tb);
                        ai[x] = rb * sin(tb);
                    }
                } else {                  // MATRIX: IMAG / IMAG
                    throw new Error('Matrix.power: This function has not been implemented yet for complex number.');
                }
            }
        }
        return this;
    };

    Matrix_prototype['.^'] = function (b) {
        return this.clone().power(b);
    };

    Matrix.power = function (A, B) {
        return Matrix.from(A)['.^'](B);
    };

}

/* Function generating automatically the arithmetic operators functions */
// const generateArithmeticOperators = function () {
//     const operators = {
//         // '+': {
//         //     "name": "plus",
//         //     "real/real": {
//         //         scalar: "a[x] += c;",
//         //         matrix: "a[x] += c[x];"
//         //     },
//         //     "real/imag": {
//         //         scalar_before: "b.fill(d);",
//         //         scalar: "a[x] += c;",
//         //         matrix: "a[x] += c[x]; b[x] = d[x];"
//         //     },
//         //     "imag/real": {
//         //         scalar: "a[x] += c;",
//         //         matrix: "a[x] += c[x];"
//         //     },
//         //     "imag/imag": {
//         //         scalar: "a[x] += c;    b[x] += d;",
//         //         matrix: "a[x] += c[x]; b[x] += d[x];"
//         //     },
//         //     UIsScalar: "V.clone().plus(U);",
//         //     UIsMatrix: "U.clone().plus(V);"
//         // },
//         // '-': {
//         //     "name": "minus",
//         //     "real/real": {
//         //         scalar: "a[x] -= c;",
//         //         matrix: "a[x] -= c[x];"
//         //     },
//         //     "real/imag": {
//         //         scalar_before: "b.fill(-d);",
//         //         scalar: "a[x] -= c;",
//         //         matrix: "a[x] -= c[x]; b[x] = -d[x];"
//         //     },
//         //     "imag/real": {
//         //         scalar: "a[x] -= c;",
//         //         matrix: "a[x] -= c[x];"
//         //     },
//         //     "imag/imag": {
//         //         scalar: "a[x] -= c;    b[x] -= d;",
//         //         matrix: "a[x] -= c[x]; b[x] -= d[x];"
//         //     },
//         //     UIsScalar: "V.clone().plus(U.uminus());",
//         //     UIsMatrix: "U.clone().minus(V);"
//         // },
//         // '.*': {
//         //     "name": "times",
//         //     "real/real": {
//         //         scalar: "a[x] *= c;",
//         //         matrix: "a[x] *= c[x];"
//         //     },
//         //     "real/imag": {
//         //         scalar: "b[x] = a[x] * d;    a[x] *= c;",
//         //         matrix: "b[x] = a[x] * d[x]; a[x] *= c[x];"
//         //     },
//         //     "imag/real": {
//         //         scalar: "a[x] *= c;    b[x] *= c;",
//         //         matrix: "a[x] *= c[x]; b[x] *= c[x];"
//         //     },
//         //     "imag/imag": {
//         //         scalar: "const r = a[x], i = b[x];" +
//         //             "a[x] = r * c - i * d;" +
//         //             "b[x] = r * d + i * c;",
//         //         matrix: "const r1 = a[x], i1 = b[x], r2 = c[x], i2 = d[x];" +
//         //             "a[x] = r1 * r2 - i1 * i2;" +
//         //             "b[x] = i1 * r2 + r1 * i2;"
//         //     },
//         //     UIsScalar: "V.clone().times(U);",
//         //     UIsMatrix: "U.clone().times(V);"
//         // },
//         // './': {
//         //     "name": "rdivide",
//         //     "real/real": {
//         //         scalar: "a[x] /= c;",
//         //         matrix: "a[x] /= c[x];"
//         //     },
//         //     "real/imag": {
//         //         scalar_before: "b.set(a);" +
//         //             "const tmp = 1 / (c * c + d * d);" +
//         //             "const r2 =  c * tmp;" +
//         //             "const i2 = -d * tmp;",
//         //         scalar: "b[x] *= i2;           a[x] *= r2;",
//         //         matrix: "const r1 = a[x], r2 = c[x], i2 = d[x];" +
//         //             "const t = 1 / (r2 * r2 + i2 * i2);" +
//         //             "a[x] = r1 * r2 * t;" +
//         //             "b[x] = -r1 * i2 * t;"
//         //     },
//         //     "imag/real": {
//         //         scalar_before: "const ic = 1 / c;",
//         //         scalar: "a[x] *= ic;    b[x] *= ic;",
//         //         matrix: "const ic = 1 / c[x]; a[x] *= ic; b[x] *= ic;"
//         //     },
//         //     "imag/imag": {
//         //         scalar_before: "const tmp = 1 / (c * c + d * d);" +
//         //             "const r2 = c * tmp;" +
//         //             "const i2 = d * tmp;",
//         //         scalar: "const r1 = a[x], i1 = b[x];" +
//         //             "a[x] = r1 * r2 + i1 * i2;" +
//         //             "b[x] = i1 * i2 - r1 * r2;",
//         //         matrix: "const r1 = a[x], i1 = b[x], r2 = c[x], i2 = d[x];" +
//         //             "const t = 1 / (r2 * r2 + i2 * i2);" +
//         //             "a[x] = (r1 * r2 + i1 * i2) * t;" +
//         //             "b[x] = (i1 * r2 - r1 * i2) * t;"
//         //     },
//         //     UIsScalar: "V.clone().rdivide(U);",
//         //     UIsMatrix: "U.clone().ldivide(V);"
//         // },
//         '.\\': {
//             "name": "ldivide",
//             "real/real": {
//                 scalar: "a[x] = c / a[x];",
//                 matrix: "a[x] = c[x] / a[x];"
//             },
//             "real/imag": {
//                 scalar: "b[x] = d / a[x];    a[x] = c / a[x];",
//                 matrix: "const ia = 1 / a[x]; a[x] = c[x] * ia; b[x] = d[x] * ia;"
//             },
//             "imag/real": {
//                 scalar: [
//                     "const r1 = a[x], i1 = b[x];",
//                     "const tmp = c / (r1 * r1 + i1 * i1);",
//                     "a[x] *=  tmp;",
//                     "b[x] *= -tmp;"
//                 ].join(" "),
//                 matrix: [
//                     "const r1 = a[x], i1 = b[x];",
//                     "const tmp = c[x] / (r1 * r1 + i1 * i1);",
//                     "a[x] *=  tmp;",
//                     "b[x] *= -tmp;"
//                 ].join(" ")
//             },
//             "imag/imag": {
//                 scalar: [
//                     "const r1 = a[x], i1 = b[x];",
//                     "const tmp = 1 / (r1 * r1 + i1 * i1);",
//                     "a[x] = (r1 * c + i1 * d) * tmp;",
//                     "b[x] = (r1 * d - i1 * c) * tmp;"
//                 ].join(" "),
//                 matrix: "const r1 = c[x], i1 = d[x], r2 = a[x], i2 = b[x];" +
//                     "const t = 1 / (r2 * r2 + i2 * i2);" +
//                     "a[x] = (r1 * r2 + i1 * i2) * t;" +
//                     "b[x] = (i1 * r2 - r1 * i2) * t;"
//             },
//             UIsScalar: "V.clone().ldivide(U);",
//             UIsMatrix: "U.clone().rdivide(V);"
//         },
//         // '.^': {
//         //     "name": "power",
//         //     "real/real": {
//         //         scalar: "a[x] = Math.pow(a[x], c);",
//         //         matrix: "a[x] = Math.pow(a[x], c[x]);"
//         //     },
//         //     "real/imag": {
//         //         scalar: "throw new Error('Matrix.power: This function has not " +
//         //             "been implemented yet for complex number.');",
//         //         matrix: "throw new Error('Matrix.power: This function has not " +
//         //             "been implemented yet for complex number.');"
//         //     },
//         //     "imag/real": {
//         //         scalar: "throw new Error('Matrix.power: This function has not " +
//         //             "been implemented yet for complex number.');",
//         //         matrix: "throw new Error('Matrix.power: This function has not " +
//         //             "been implemented yet for complex number.');"
//         //     },
//         //     "imag/imag": {
//         //         scalar: "throw new Error('Matrix.power: This function has not " +
//         //             "been implemented yet for complex number.');",
//         //         matrix: "throw new Error('Matrix.power: This function has not " +
//         //             "been implemented yet for complex number.');"
//         //     },
//         //     UIsScalar: "V.isscalar() ? U.clone().power(V) : undefined;",
//         //     UIsMatrix: "U.clone().power(V);"
//         // }
//     };
//
//     // Template function
//     const fct = (function (v) {
//         v = Matrix.from(v);
//         const n = this.numel();
//         let u = this;
//         if (v.isscalar()) {                         //  V SCALAR
//             if (u.isreal()) {                       //      U REAL
//                 if (v.isreal()) {                   //          U REAL / V REAL SCALAR
//                     const a = u.getData();
//                     const c = v.asScalar();
//                     "real/real-scalar";
//                 } else {                            //          U REAL / V CPLX SCALAR
//                     u.toComplex();
//                     const a = u.getRealData();
//                     const b = u.getImagData();
//                     const c = v.getRealData()[0];
//                     const d = v.getImagData()[0];
//                     "real/imag-scalar";
//                 }
//             } else {                                //      U CPLX
//                 const a = u.getRealData();
//                 const b = u.getImagData();
//                 if (v.isreal()) {                   //          U CPLX / V CPLX SCALAR
//                     const c = v.asScalar();
//                     "imag/real-scalar";
//                 } else {                            //          U CPLX / V CPLX SCALAR
//                     const c = v.getRealData()[0];
//                     const d = v.getImagData()[0];
//                     "imag/imag-scalar";
//                 }
//             }
//         } else {                                    //  V MATRIX
//             Check.checkSizeEquals(u.getSize(), v.getSize(), Matrix.ignoreTrailingDims);
//             if (u.isreal()) {                       //      U REAL
//                 if (v.isreal()) {                   //          U REAL / V REAL MATRIX
//                     const a = u.getData();
//                     const c = v.getData();
//                     "real/real-matrix";
//                 } else {                            //          U REAL / V CPLX MATRIX
//                     u.toComplex();
//                     const a = u.getRealData();
//                     const b = u.getImagData();
//                     const c = v.getRealData();
//                     const d = v.getImagData();
//                     "real/imag-matrix";
//                 }
//             } else {                                //      U CPLX
//                 const a = u.getRealData();
//                 const b = u.getImagData();
//                 if (v.isreal()) {                   //          U CPLX / V REAL MATRIX
//                     const c = v.getData();
//                     "imag/real-matrix";
//                 } else {                            //          U CPLX / V CPLX MATRIX
//                     const c = v.getRealData();
//                     const d = v.getImagData();
//                     "imag/imag-matrix";
//                 }
//             }
//         }
//         return this;
//     }).toString();
//
//     const fct2 = (function (U, V) {
//         U = Matrix.from(U);
//         V = Matrix.from(V);
//
//         if (U.isscalar()) {
//             return "UIsScalar";
//         } else {
//             return "UIsMatrix";
//         }
//     }).toString();
//
//     const addLoop = function (str) {
//         return "let x; for (x = 0; x < n; x++) {" + str + "}";
//     };
//
//     const replace = function (fun, op, c) {
//         let scalar = op[c].scalar_before ? op[c].scalar_before : "";
//         let matrix = op[c].matrix_before ? op[c].matrix_before : "";
//         scalar += addLoop(op[c].scalar);
//         matrix += addLoop(op[c].matrix);
//         fun = fun.replace("\"" + c + "-matrix\";", matrix);
//         fun = fun.replace("\"" + c + "-scalar\";", scalar);
//         return fun;
//     };
//
//     let o;
//     for (o in operators) {
//         const op = operators[o];
//         let fun = replace(fct, op, "real/real");
//         fun = replace(fun, op, "real/imag");
//         fun = replace(fun, op, "imag/real");
//         fun = replace(fun, op, "imag/imag");
//         console.log("Matrix_prototype." + op.name + " = " + fun + ";");
//
//         fun = fct2.replace("\"UIsScalar\";", op.UIsScalar);
//         fun = fun.replace("\"UIsMatrix\";", op.UIsMatrix);
//         console.log("Matrix." + op.name + " = " + fun + ";");
//     }
// };
// generateArithmeticOperators();
