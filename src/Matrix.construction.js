/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/
import MatrixView, {Check} from "@etsitpab/matrixview";

/*
* This module provides basic Matrix constructor interface
* such as `rand`, 'randi', 'eye', 'zeros', 'ones', etc.
* to build Matrix in matlab-like way.
*/

/** @class Matrix */

export default function constructionExtension (Matrix) {

    // Check if nodejs or browser
    const isNode = (typeof globalThis.module !== "undefined" && globalThis.module.exports) ? true : false;
    let Canvas, NewImage, fs;
    if (isNode) {
        import("fs").then(mod => {
            fs = mod;
            // Do not forget: export NODE_PATH=/usr/local/lib/node_modules
            import("canvas").then(canvasModule => {
                NewImage = canvasModule.Image;
                Canvas = canvasModule.Canvas;
            });
        });
    } else {
        NewImage = Image;
    }

    const createCanvas = function (width = 0, height = 0) {
        let canvas;
        if (isNode) {
            canvas = new Canvas();
        } else {
            canvas = document.createElement("canvas");
        }
        canvas.width = width;
        canvas.height = height;
        return canvas;
    };

    /** Creates a row vector filled ordered values.
    * Actually it acts like Matlab colon (:) operator.
    *
    * __Also see:__
    *  {@link Matrix#linspace}.
    *
    * @param {Array} colon
    *  Colon array can have 2, or 3 parameters:
    *
    *  - the first value indicates the first value of the output vector,
    *  - if there is two parameters, then they designate respectively
    *  - the first and the last value of the output vector (the step between
    *    two values is -1 or +1,
    *  - If there is three parameters, then they indicate respectively
    *    the first, the step and the last values.
    *
    * @param {String} [type=Matrix.dataType]
    *  Defined the numerical class of data.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.colon = function (...args) {
        const [first, step, last] = Check.checkColon(args);

        // Special cases
        if (!isFinite(first) || !isFinite(step) || !isFinite(last)) {
            throw new Error("Matrix.colon: Parameters are invalid number (Infitity or NaN).");
        }

        // Number.EPSILON: Minimum difference between 2 double precision values
        // Tolerance value
        const tol = 2.0 * Number.EPSILON * Math.max(Math.abs(first), Math.abs(last));
        // Step sign
        const stepSign = step > 0 ? 1 : -1;

        // Determine interval number
        const isInteger = Check.isInteger;

        let n;
        if (isInteger(first) && step === 1) {
            // Consecutive integers.
            n = Math.floor(last) - first;
        } else if (isInteger(first) && isInteger(step)) {
            // Integers with spacing > 1.
            const q = Math.floor(first / step);
            const r = first - q * step;
            n = Math.floor((last - r) / step) - q;
        } else {
            // General case.
            n =  Math.round((last - first) / step);
            if (stepSign * (first + n * step - last) > tol) {
                n = n - 1;
            }
        }

        let right = first + n * step;
        if (stepSign * (right - last) > -tol) {
            right = last;
        }
        const out = new Matrix(n + 1),
            dOut = out.getData(),
            k = Math.floor(n / 2) + 1;
        let x, v1 = first, v2 = right;
        for (x = 0; x < k; x++, v1 += step, v2 -= step) {
            dOut[x]     = v1;
            dOut[n - x] = v2;
        }

        if (n % 2 === 0) {
            dOut[n / 2] = (first + right) / 2;
        }

        return out;
    };

    /** Creates a column vector filled with linealy spaced values.
    * It acts similarly to the colon operator, but with a control on
    * the number of values.
    *
    * __Also see:__
    *  {@link Matrix#colon}, {@link Matrix#logspace}.
    *
    * @param {Number} min
    * Minimum value
    *
    * @param {Number} max
    * Maximum value
    *
    * @param {Integer} n
    * Number of elements
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.linspace = function (min, max, bins = 100) {
        if (!Check.isNumber(min)) {
            throw new Error("Matrix.linspace: Min must be a Number.");
        }
        if (!Check.isNumber(max)) {
            throw new Error("Matrix.linspace: Max must be a Number.");
        }
        if (!Check.isInteger(bins, 1)) {
            throw new Error("Matrix.linspace: Bins must be an integer > 1.");
        }

        var om = this.zeros(bins, 1), od = om.getData(), n = od.length - 1;
        for (var i = 0, ie = Math.floor(n / 2) + 1, v1 = min, v2 = max, step = (max - min) / (bins - 1); i < ie; i++, v1 += step, v2 -= step) {
            od[i] = v1;
            od[n - i] = v2;
        }
        if (n % 2 === 0) {
            od[n / 2] = (v1 + v2) / 2;
        }
        return om;
    };

    /** Creates a column vector filled with log spaced values.
    *
    * __Also see:__
    *  {@link Matrix#colon}, {@link Matrix#linspace}.
    *
    * @param {Number} min
    * Minimum value
    *
    * @param {Number} max
    * Maximum value
    *
    * @param {Integer} [n=50]
    * Number of elements
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.logspace = function (min, max, bins = 50) {
        if (!Check.isNumber(min)) {
            throw new Error("Matrix.logspace: Min must be a Number.");
        }
        if (!Check.isNumber(max)) {
            throw new Error("Matrix.logspace: Min must be a Number.");
        }
        if (!Check.isInteger(bins, 1)) {
            throw new Error("Matrix.linspace: Bins should be an integer > 1.");
        }
        const om = this.zeros(bins, 1), od = om.getData(), n = od.length - 1;
        const pow = Math.pow, step = (max - min) / (bins - 1);
        const ie = Math.floor(n / 2) + 1;
        let i, v1, v2;
        for (i = 0, v1 = min, v2 = max; i < ie; i++, v1 += step, v2 -= step) {
            od[i] = pow(10, v1);
            od[n - i] = pow(10, v2);
        }
        if (n % 2 === 0) {
            od[n / 2] = pow(10, (v1 + v2) / 2);
        }
        return om;
    };

    /** Creates a Matrix filled with zeros.
    *
    * __Also see:__
    *  {@link Matrix#true},
    *  {@link Matrix#false},
    *  {@link Matrix#ones}.
    *
    * @param {Integer[]} size
    * A sequence of integers indicating the size of the output matrix.
    *
    * @param {String} [type=Matrix.dataType]
    * Defined the numerical class of data.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.zeros = function (...args) {

        // Get type if any;
        let type;
        if (typeof args[args.length - 1] === "string") {
            type = args.pop();
        }

        // Check size
        const size = Check.checkSize((args.length === 0) ? [1] : args, "square");

        return new Matrix(size, type);
    };

    /** Creates a Matrix filled with ones.
    * Actually it acts like Matlab ones constructor.
    *
    * __Also see:__
    *  {@link Matrix#true},
    *  {@link Matrix#false},
    *  {@link Matrix#zeros}.
    *
    * @param {Integer[]} size A sequence of integers indicating
    * the size of the output matrix.
    *
    * @param {String} [type=Matrix.dataType] Defined the numerical
    * class of data.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.ones = function (...args) {
        // Get type;
        let type;
        if (typeof args[args.length - 1] === "string") {
            type = args.pop();
        }

        // Check size
        const size = Check.checkSize((args.length === 0) ? [1] : args, "square"),
            mat = new Matrix(size, type);
        mat.getData().fill(1);
        return mat;
    };

    /** Creates a logical Matrix filled with true.
    *
    * __Also see:__
    *  {@link Matrix#false},
    *  {@link Matrix#ones},
    *  {@link Matrix#zeros}.
    *
    * @param {Integer[]} size A sequence of integers indicating
    * the size of the output matrix.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.true = function (...args) {
        return this.ones(...args, "logical");
    };

    /** Creates a logical Matrix filled with false.
    *
    * __Also see:__
    *  {@link Matrix#true},
    *  {@link Matrix#ones},
    *  {@link Matrix#zeros}.
    *
    * @param {Integer[]} size A sequence of integers indicating
    * the size of the output matrix.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.false = function (...args) {
        return this.zeros(...args, "logical");
    };

    /** Creates a Matrix filled with random walues uniformely
    * distributed in range [0, 1].
    *
    * __Also see:__
    *  {@link Matrix#randn},
    *  {@link Matrix#randi}.
    *
    * @param {Integer[]} size
    *  A sequence of integers indicating the size of the
    *  output matrix.
    *
    * @param {String} [type=Matrix.dataType]
    *  Defined the numerical class of data.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.rand = function (...args) {
        // Get type;
        let type;
        if (typeof args[args.length - 1] === "string") {
            type = args.pop();
        }
        // Check size
        const size = Check.checkSize((args.length === 0) ? [1] : args, "square");
        const mat = new Matrix(size, type), data = mat.getData();
        const ie = data.length, random = Math.random;
        let i;
        for (i = 0; i < ie; ++i) {
            data[i] = random();
        }
        return mat;
    };

    /** Creates a Matrix filled with random walues following
    * the gaussian low of parameters (0, 1).
    *
    * __Also see:__
    *  {@link Matrix#rand},
    *  {@link Matrix#randi}.
    *
    * @param {Integer[]} size
    *  A sequence of integers indicating the size of the
    *  output matrix.
    *
    * @param {String} [type=Matrix.dataType]
    *  Defined the numerical class of data.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.randn = function (...args) {
        // Get type;
        let type;
        if (typeof args[args.length - 1] === "string") {
            type = args.pop();
        }

        // Check size
        const size = Check.checkSize((args.length === 0) ? [1] : args, "square");
        const mat = new Matrix(size, type), data = mat.getData();
        const {random, sqrt, sin, log} = Math, PI2 = Math.PI * 2;
        let i;
        const ie = data.length;
        for (i = 0; i < ie; ++i) {
            const t = PI2 * random();
            const r = sqrt(-2 * log(1 - random()));
            data[i] = r * sin(t);
        }

        return mat;
    };


    /** Creates a Matrix filled with random walues uniformely distributed
    * in a specified range.
    *
    * __Also see:__
    *  {@link Matrix#rand},
    *  {@link Matrix#randi}.
    *
    * @param {Integer|Integer[]} range
    *  Can be :
    *
    *  - An integer greater than 1, then the range for radom values will
    *    be [0, range],
    *  - An array-like of length 2, then the range for radom values will
    *    be [range[0], range[1]],
    *
    * @param {Integer[]} size
    *  A sequence of integers indicating the size of the output matrix.
    *
    * @param {String} [type=Matrix.dataType]
    * Defined the numerical class of data.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.randi = function (range, ...args) {
        // Check range
        range = Check.checkRange(range);

        // Get type
        let type;
        if (typeof args[args.length - 1] === "string") {
            type = args.pop();
        }

        // Check size
        const size = Check.checkSize((args.length === 0) ? [1] : args, "square");

        const mat = new Matrix(size, type), data = mat.getData();
        const min = range[0], c = range[1] - min + 1;
        const random = Math.random, floor = Math.floor;
        let i;
        const ie = data.length;
        for (i = 0; i < ie; ++i) {
            data[i] = floor(random() * c) + min;
        }
        return mat;
    };

    /** Creates a Matrix with ones the main diagonal.
    *
    * __Also see:__
    * {@link Matrix#zeros},
    * {@link Matrix#ones}.
    *
    * @param {Integer[]} size
    * A sequence of integers indicating the size of the output matrix.
    *
    * @param {String} [type=Matrix.dataType]
    * Defined the numerical class of data.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.eye = function (...args) {

        // Get type;
        let type;
        if (typeof args[args.length - 1] === "string") {
            type = args.pop();
        }

        // Check size
        const size = Check.checkSize((args.length === 0) ? [1] : args, "square");

        const mat = new Matrix(size, type);
        const data = mat.getData();

        // Scaning the from the second dimension (dim = 1)
        const {iterator: it, begin: b, isEnd: e} = mat.getIterator(2);

        const view = mat.getView();
        const fy = view.getFirst(1), dy = view.getStep(1);
        const fx = view.getFirst(0), dx = view.getStep(0);
        const N = Math.min(view.getSize(1), view.getSize(0));
        let i, y, n;
        for (i = b(); !e(); i = it()) {
            for (y = i + fy + fx, n = 0; n < N; y += dy, n++) {
                data[y + n * dx] = 1;
            }
        }
        return mat;
    };

    /** Creates a complex Matrix from a 2 Matrix.
    *
    * @method complex
    *
    * @param {Matrix} real
    * Matrix used are real part.
    *
    * @param {Matrix} imag
    * Matrix used are imaginary part.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.complex = function (real, imag) {

        real = Matrix.from(real);
        imag = Matrix.from(imag);

        if (!real.isreal()) {
            throw new Error("Matrix.complex: Real argument must be a real matrix.");
        }

        if (!imag.isreal()) {
            throw new Error("Matrix.complex: Imag argument must be a real matrix.");
        }
        if (!Check.checkSizeEquals(real.getSize(), imag.getSize(), Matrix.ignoreTrailingDims)) {
            throw new Error("Matrix.complex: Imag and real Matrix must have the same size.");
        }
        var realData = real.getData(), imagData = imag.getData();
        var od = new Matrix.dataType(realData.length * 2);
        od.subarray(0, realData.length).set(realData);
        od.subarray(realData.length).set(imagData);
        return new Matrix(real.getSize(), od, true);
    };

    /** Reads image data in order to creates a Matrix.
    *
    * @param {String|Image|HTMLCanvasElement} source
    *  Source of the image. It can be the image path, an Image element
    *  or a Canvas element respectively.
    *
    * @param {Function} [callback = undefined]
    *  Function to call once the image is loaded.
    *
    * @param {Function} [errorCallback = undefined]
    *  Function to call if an error occurs while loading.
    *
    * @return {Matrix}
    *
    * @matlike
    * @todo Should this function make use of Matrix.initialize ?
    */
    Matrix.imread = async function (source, callback, errCallback) {
        const errMsg = "Matrix.imread: ";

        // If source is a canvas
        if (!isNode && typeof source === "string" && document.getElementById(source)) {
            source = document.getElementById(source);
        }

        const readFromCanvas = canvas => {
            const ctx = canvas.getContext("2d");
            const width = canvas.width, height = canvas.height;
            let imageData;
            try {
                imageData = ctx.getImageData(0, 0, width, height);
            } catch (e) {
                if (errCallback !== undefined) {
                    errCallback.call(this, e);
                    return;
                }
                throw e;
            }
            const data = new Uint8ClampedArray(imageData.data);

            // TODO: Check Matrix transposition option
            const view = new MatrixView([4, width, height])
                .select([0, 2], [0, -1], [0, -1])
                // .permute([2, 1, 0]); // For transposition
                .permute([1, 2, 0]); // Without transposition
            const imOut = new Matrix(view.getSize(), view.extractFrom(data));
            if (callback) {
                callback.call(imOut, imOut);
            }
            return imOut;
        };

        const readFromVideo = video => {
            const canvas = createCanvas(video.videoWidth, video.videoHeight);
            canvas.getContext("2d").drawImage(video, 0, 0);
            return readFromCanvas(canvas);
        };

        const readFromImage = image => {
            if (!isNode) {
                image = image instanceof Event ? this : image;
            }
            const canvas = createCanvas(image.width, image.height);
            canvas.getContext("2d").drawImage(image, 0, 0, image.width, image.height);
            return readFromCanvas(canvas);
        };

        const readFromString = async name => {
            const im = new NewImage();
            im.onerror = errCallback || function () {
                throw new Error(errMsg + "Error occuring while loading image.");
            };
            if (isNode) {
                im.src = fs.readFileSync(name);
                return Promise.resolve(readFromImage(im));
            } else {
                im.src = name;
                return new Promise(
                    (resolve, reject) => {
                        im.onload = () => {
                            resolve(readFromImage(im));
                        };
                        im.onerror = reject;
                    }
                );
            }
        };

        const readFromFile = async file => {
            const imURL = URL.createObjectURL(file);
            const image = await readFromString(imURL);
            URL.revokeObjectURL(imURL);
            return image;
        };

        if (globalThis.File && source instanceof File && source.type.includes("image/")) {
            return await readFromFile(source);
        } else if (typeof source === "string") {
            return await readFromString(source);
        } else if (!isNode && source instanceof Image) {
            return await readFromImage(source);
        } else if (!isNode && source instanceof HTMLCanvasElement) {
            return await readFromCanvas(source);
        } else if (isNode && source instanceof Canvas) {
            return await readFromCanvas(source);
        } else if (!isNode && source instanceof HTMLVideoElement) {
            return readFromVideo(source);
        }
        throw new Error(errMsg + "invalid source argument");
    };

    /** Creates a square diagonal Matrix from a vector.
    *
    * @param {Matrix} vector
    *
    * @return {Matrix}
    *
    * @matlike
    * @todo This function must works with complex Matrix and array.
    */
    Matrix.diag = function (d) {
        d = Matrix.from(d);
        const l = d.numel();
        d = d.getData();
        const out = Matrix.zeros(l);
        const data = out.getData();
        const ei = data.length;
        let i, j;
        for (i = 0, j = 0; i < ei; i += l + 1, j++) {
            data[i] = d[j];
        }
        return out;
    };
}
