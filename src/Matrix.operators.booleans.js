/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */

export default function operatorsExtension (Matrix, Matrix_prototype) {


    //////////////////////////////////////////////////////////////////
    //        Boolean Operators functions defining the matrix       //
    //////////////////////////////////////////////////////////////////


    const booleanOperators = function (op, A, B) {

        A = Matrix.from(A);
        B = Matrix.from(B);
        if (!A.isreal() || !B.isreal()) {
            throw new Error("Matrix.booleanOperators: This function doesn't work with complex numbers.");
        }
        if (A.isscalar()) {
            [A, B] = [B, A];
        }

        var id = A.getData(), ld = id.length;

        var out, od, x;

        if (B.isscalar()) {
            B = B.asScalar();
            out = new Matrix(A.size(), 'logical');
            od = out.getData();
            if (op === '===' || op === '==') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] === B) ? 1 : 0;
                }
            } else if (op === '!==' || op === '!=') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] !== B) ? 1 : 0;
                }
            } else if (op === '&&') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] && B) ? 1 : 0;
                }
            } else if (op === '||') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] || B) ? 1 : 0;
                }
            } else if (op === '<') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] < B) ? 1 : 0;
                }
            } else if (op === '<=') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] <= B) ? 1 : 0;
                }
            } else if (op === '>') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] > B) ? 1 : 0;
                }
            } else if (op === '>=') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] >= B) ? 1 : 0;
                }
            } else {
                throw new Error('Matrix: Unknown operator \'' + op + '\'.');
            }

        } else if (B instanceof Matrix) {
            var size = Check.checkSizeEquals(A.size(), B.size(), Matrix.ignoreTrailingDims);
            out = new Matrix(size, 'boolean');
            od = out.getData();
            var i2d = B.getData();
            if (op === '===' || op === '==') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] === i2d[x]) ? 1 : 0;
                }
            } else if (op === '!==' || op === '!=') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] !== i2d[x]) ? 1 : 0;
                }
            } else if (op === '&&') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] && i2d[x]) ? 1 : 0;
                }
            } else if (op === '||') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] || i2d[x]) ? 1 : 0;
                }
            } else if (op === '<') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] < i2d[x]) ? 1 : 0;
                }
            } else if (op === '<=') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] <= i2d[x]) ? 1 : 0;
                }
            } else if (op === '>') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] > i2d[x]) ? 1 : 0;
                }
            } else if (op === '>=') {
                for (x = 0; x < ld; x++) {
                    od[x] = (id[x] >= i2d[x]) ? 1 : 0;
                }
            } else {
                throw new Error('Unknown operator \'' + op + '\'.');
            }
        } else {
            throw new Error('Argument must be a Matrix or a number');
        }

        return out;
    };

    /** Test equality between two arrays.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.eq = function (b) {
        return booleanOperators('===', this, b);
    };
    Matrix_prototype['==='] = Matrix_prototype.eq;
    Matrix.eq = function (a, b) {
        return booleanOperators('===', a, b);
    };

    /** Test inequality between two arrays.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.ne = function (b) {
        return booleanOperators('!==', this, b);
    };
    Matrix_prototype['!=='] = Matrix_prototype.ne;
    Matrix.ne = function (a, b) {
        return booleanOperators('!==', a, b);
    };

    /** Greater than operator.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.gt = function (b) {
        return booleanOperators('>', this, b);
    };
    Matrix_prototype['>'] = Matrix_prototype.gt;
    Matrix.gt = function (a, b) {
        return booleanOperators('>', a, b);
    };

    /** Greater or equal operator.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.ge = function (b) {
        return booleanOperators('>=', this, b);
    };
    Matrix_prototype['>='] = Matrix_prototype.ge;
    Matrix.ge = function (a, b) {
        return booleanOperators('>=', a, b);
    };

    /** Lower than operator.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.lt = function (b) {
        return booleanOperators('<', this, b);
    };
    Matrix_prototype['<'] = Matrix_prototype.lt;
    Matrix.lt = function (a, b) {
        return booleanOperators('<', a, b);
    };

    /** Lower or equal operator.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.le = function (b) {
        return booleanOperators('<=', this, b);
    };
    Matrix_prototype['<='] = Matrix_prototype.le;
    Matrix.le = function (a, b) {
        return booleanOperators('<=', a, b);
    };

    /** And operator.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.and = function (b) {
        return booleanOperators('&&', this, b);
    };
    Matrix_prototype['&&'] = Matrix_prototype.and;
    Matrix.and = function (a, b) {
        return booleanOperators('&&', a, b);
    };

    /** Or operator.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.or = function (b) {
        return booleanOperators('||', this, b);
    };
    Matrix_prototype['||'] = Matrix_prototype.or;
    Matrix.or = function (a, b) {
        return booleanOperators('||', a, b);
    };

    /** Return false if different of zero.
    *
    * @param {Number|Matrix} rightOp
    * @chainable
    * @matlike
    */
    Matrix_prototype.not = function () {
        var out = this.clone();
        var data = out.getData();
        var i, ie;
        for (i = 0, ie = data.length; i < ie; i++) {
            data[i] = !data[i] ? 1 : 0;
        }
        return out;
    };
    Matrix.not = function (a) {
        return Matrix.from(a).clone().not();
    };

    Matrix_prototype.isequal = function (...args) {
        args = args.map(arg => {
            arg = Matrix.from(arg);
            if (!Check.areSizeEquals(this.getSize(), arg.getSize(), Matrix.ignoreTrailingDims)) {
                return false;
            }
            return arg.getData();
        });

        const ref = this.getData();
        let i, ie, a, ae;
        for (i = 0, ie = ref.length; i < ie; i++) {
            const refi = ref[i];
            for (a = 0, ae = args.length; a < ae; a++) {
                if (args[a][i] !== refi) {
                    return false;
                }
            }
        }
        return true;
    };
    Matrix.isequal = function (arg0, ...args) {
        return Matrix.from(arg0).isequal(...args);
    };

    Matrix_prototype.isequaln = function (...args) {
        args = args.map(arg => {
            arg = Matrix.from(arg);
            if (!Check.areSizeEquals(this.getSize(), arg.getSize(), Matrix.ignoreTrailingDims)) {
                return false;
            }
            return arg.getData();
        });

        const ref = this.getData();
        let i, ie, a, ae;
        for (i = 0, ie = ref.length; i < ie; i++) {
            const refi = ref[i];
            for (a = 0, ae = args.length; a < ae; a++) {
                if (args[a][i] !== refi && (!isNaN(args[a][i]) || !isNaN(refi))) {
                    return false;
                }
            }
        }
        return true;
    };
    Matrix.isequaln = function (arg0, ...args) {
        return Matrix.from(arg0).isequaln(...args);
    };

}
