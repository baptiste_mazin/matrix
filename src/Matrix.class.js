/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
 * @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
 */
import MatrixView, {Check} from "@etsitpab/matrixview";

/** A Matrix or a ND-Array.
 *
 * This class implements ND-Arrays, i.e. arrays with N dimensions.
 * It is similar to Matlab's Matrix or NumPy's `ndarray`.
 *
 * Lot of vectorial operations are provided:
 *
 *  + element-wise operations, e.g. addition, search.
 *  + matrix operations, e.g. matrix multiplication or inversion.
 *  + array manipulation, e.g. concatenation, transposition.
 *
 *
 * @constructor Creates a Matrix.
 *
 * Unless data are provided, the matrix will be filled with zeros.
 *
 *     const A = new Matrix([3, 3]);                     // Create a 3x3 matrix
 *     const B = new Matrix([3, 2, 4], null, true);      // Create a 3x2x4 complex matrix
 *     const C = new Matrix([4, 2], Uint8Array);         // create a 4x2 matrix of uint8
 *     const D = new Matrix([3, 2], [3, 3, 3, 2, 2, 2]); // 3x2 matrix, columns are 3s and 2s
 *
 * @param {Array} size
 *  Size (dimensions) of the matrix.
 *
 * @param {String | Function | Array} [arg]
 *  Can be:
 *
 *  + a `String`: the name of the data constructor.
 *  + a `Function`: the constructor of the data.
 *  + an `Array`: the data of the matrix, columnwise.
 *
 * In the latest case, the length of the array must be:
 *
 *  + the product of the values in the `size` argument.
 *  + twice the above number for a complex matrix (argument `iscomplex`).
 *
 * @param {Boolean} [iscomplex=false]
 *  If true, creates a complex matrix.
 *
 * @todo size = N; data = x; import doc. from MatrixView; consistancy of name (isscalar or isScalar)?
 *
 *  + When the size is an integer: create a 1D vector?
 *  + When the data is a scalar: create an array filled with it.
 *  + When the data is an Array of Array: create a 2D array.
 */
export default function Matrix(size, Type, complex, bool) {

    //////////////////////////////////////////////////////////////////
    //                      Variables of the Matrix                 //
    //////////////////////////////////////////////////////////////////


    // Default View
    var view;
    // Where values are stored
    var data, realPart, imagPart;
    // if Matrix is boolean
    var isBoolean = false;


    //////////////////////////////////////////////////////////////////
    //                     Matrix Initialisation                    //
    //////////////////////////////////////////////////////////////////


    /** See {@link MatrixView#getIndex} @method */
    var getIndex;
    /** See {@link MatrixView#getDimLength} @method */
    var getDimLength;
    /** See {@link MatrixView#getLength} @method */
    var getLength;
    /** See {@link MatrixView#getSize} @method */
    var getSize;
    /** See {@link MatrixView#getIterator} @method */
    var getIterator;

    // Set the default View
    var setView = s => {
        if (s instanceof Matrix) {
            s = s.clone().getData();
        }
        size = Check.checkSize(s);
        view = new MatrixView(size);

        getIndex = view.getIndex;
        getDimLength = view.getDimLength;
        getLength = view.getLength;
        getSize = view.getSize;
        getIterator = view.getIterator.bind(view);
        this.getIndex = getIndex;
        this.getDimLength = getDimLength;
        this.getLength = getLength;
        this.getSize = getSize;
        this.getIterator = getIterator;
    };

    /** Matrix initialization function
     *
     * @private
     * @todo
     * - A function to check if an imaginary part exist?
     * - Make this function fully private.
     */
    const initialize = function (s, T, complex = false, bool = false) {
        setView(s);
        if (!Check.isBoolean(complex)) {
            throw new Error('Matrix: "complex" argument must be a boolean');
        }
        // If the type is an Array
        if (Check.isArrayLike(T)) {
            Type = Check.getTypeConstructor(T.constructor);
            data = Type === Array ? Matrix.dataType.from(T) : T;

            if (data.length !== view.getLength() * (complex ? 2 : 1)) {
                throw new Error('Matrix: data and size are incompatible.');
            }
            if (complex) {
                realPart = data.subarray(0, view.getLength());
                imagPart = data.subarray(view.getLength());
            }
        // The type is a data type
        } else {
            if (typeof T === "string" && ["logical", "bool", "boolean"].includes(T.toLowerCase())) {
                isBoolean = true;
            } else {
                isBoolean = false;
            }

            // Deal with the type
            Type = Check.getTypeConstructor(T || this.constructor.dataType);
            if (Type === Array) {
                Type = Matrix.dataType;
            }

            // Build the Matrix
            if (complex) {
                data = new Type(2 * view.getLength());
                realPart = data.subarray(0, view.getLength());
                imagPart = data.subarray(view.getLength());
            } else {
                data = new Type(view.getLength());
            }
        }

        // Set the boolean parameter
        if (!Check.isBoolean(bool)) {
            throw new Error(`Matrix: "bool" argument must be a boolean. Got: ${bool}.`);
        }
        if (bool) {
            isBoolean = true;
        }

        return this;
    }.bind(this);
    this.initialize = initialize;


    //////////////////////////////////////////////////////////////////
    //                    Basic getter functions                    //
    //////////////////////////////////////////////////////////////////


    /** Test whether the matrix has an imaginary part and if it's non-zero.
     *
     * Note: if the matrix has an imaginary part,
     *
     *  + all elements are tested one by one, and
     *  + if they are all zeros, the imaginary part is dropped.
     *
     * @return {Boolean}
     */
    var isreal = function () {
        if (!Check.isSet(imagPart)) {
            return true;
        }
        var i, ie;
        for (i = 0, ie = imagPart.length; i < ie; i++) {
            if (imagPart[i] !== 0) {
                return false;
            }
        }
        data = realPart;
        realPart = undefined;
        imagPart = undefined;
        return true;
    };

    /** Returns true if an imaginary part has been allocated.
     * @return {Boolean}
     */
    const hasImagPart = function () {
        if (Check.isSet(imagPart)) {
            return true;
        }
        return false;
    };

    /** Test whether the matrix is a scalar, i.e. has 1 element.
     *
     * @return {Boolean}
     */
    var isscalar = function () {
        return (getLength() === 1);
    };

    /** Test whether the matrix is empty, i.e. has 0 element.
     *
     * @return {Boolean}
     */
    var isempty = function () {
        return (data.length === 0);
    };

    /** Get or set a value in the matrix from its coordinates.
     *
     * See also:
     *  {@link Matrix#value},
     *  {@link Matrix#getIndex}.
     *
     * @param {Array | Number} coordinates
     *  Coordinate of the value to get/set.
     *
     * @param {Number} [value]
     *  If any, set this value.
     *
     * @return {Number | Array}
     *  The (new) value of the Matrix at the given coordinates.
     *  Complex values are returned as [real, imag].
     */
    var value = function (index, value) {
        index = Check.isArrayLike(index) ? getIndex(index) : index;
        if (!Check.isSet(value)) {
            return isreal() ? data[index] : [realPart[index], imagPart[index]];
        }
        if (isreal()) {
            data[index] = value;
        } else {
            realPart[index] = value[0];
            imagPart[index] = value[1];
        }
        return this;
    }.bind(this);

    /** Get (a reference to) the underlying data array.
     *
     * See also:
     *  {@link Matrix#asScalar},
     *  {@link Matrix#getRealData},
     *  {@link Matrix#getImagData},
     *  {@link Matrix#getView}.
     *
     * @return {Array}
     *  A reference to the array.
     */
    var getData = function () {
        return data;
    };

    /** Return value for real scalar Matrix.
     *
     * See also:
     *  {@link Matrix#getData},
     *  {@link Matrix#getRealData},
     *  {@link Matrix#getImagData},
     *  {@link Matrix#getView}.
     *
     * @return {Number}
     */
    const asScalar = function () {
        if (!isreal()) {
            throw new Error("Matrix.asScalar: Data must be real.");
        }
        if (getLength() !== 1) {
            throw new Error("Matrix.asScalar: Data length must be 1.");
        }
        return data[0];
    };
    this.getDataScalar = function () {
        console.warn("Matrix.getDataScalar: is deprecated, replace with asScalar.");
        return this.asScalar();
    };

    /** Get (a reference to) the underlying real data array.
     *
     * This is (a reference to) the first half of the data array.
     *
     * See also
     *  {@link Matrix#getData},
     *  {@link Matrix#getImagData},
     *  {@link Matrix#getView}.
     *
     * @return {Array}
     *  A reference to the array.
     */
    var getRealData = function () {
        if (!hasImagPart()) {
            this.toComplex();
        }
        return realPart;
    };

    /** Get (a reference to) the underlying imaginary data array.
     *
     * This is (a reference to) the second half of the data array.
     *
     * See also
     *  {@link Matrix#getData},
     *  {@link Matrix#getRealData},
     *  {@link Matrix#getView}.
     *
     * @return {Array}
     *  A reference to the array.
     */
    var getImagData = function () {
        if (!hasImagPart()) {
            this.toComplex();
        }
        return imagPart;
    };

    /** Add an imaginary part to a real Matrix.
     *
     * @param {Array | Matrix | Number} [imag]
     *  Imaginary part.
     *
     * @chainable
     *
     * @todo allow a number as second argument
     */
    const toComplex = imagNew => {
        const e = "Matrix.toComplex: ";

        if (imagNew instanceof Matrix) {
            if (!imagNew.isreal()) {
                throw new Error(`${e}Imaginary part cannot be complex.`);
            } else if (!Check.areSizeEquals(this.getSize(), imagNew.getSize(), Matrix.ignoreTrailingDims)) {
                throw new Error(`${e}Invalid imaginary part size. Got ${imagNew.getSize()} and expect ${this.getSize()}`);
            }
            imagNew = imagNew.getData();
        }

        if (imagPart === undefined) {
            const dataNew = new data.constructor(data.length * 2);
            realPart = dataNew.subarray(0, data.length);
            imagPart = dataNew.subarray(data.length);
            realPart.set(data);
            data = dataNew;
        }
        if (Check.isArrayLike(imagNew)) {
            if (imagNew.length !== this.getLength()) {
                throw new Error(`${e}Invalid imaginary part length.`);
            }
            imagPart.set(imagNew);
        }

        return this;
    };
    /** Keep only real part for complex matrix.
     *  @chainable
     */
    const real = () => {
        if (this.hasImagPart()) {
            data = realPart;
            realPart = undefined;
            imagPart = undefined;
        }
        return this;
    };

    /** Keep only imag part for complex matrix.
     * The matrix will be a one after that.
     *  @chainable
     */
    const imag = () => {
        if (this.hasImagPart()) {
            data = imagPart;
            realPart = undefined;
            imagPart = undefined;
        } else {
            data.fill(0);
        }
        return this;
    };

    /** Name of the data constructor.
     *
     * @return {String}
     *  Name of the constructor of the data array.
     */
    const getDataType = function () {
        return isBoolean ? 'logical' : data.constructor.name;
    };

    /** Get the default View.
     *
     * @return {MatrixView}
     *  Copy of the View on the Matrix.
     */
    const getView = function () {
        return new MatrixView(view);
    };

    this.value = value;
    this.getData = getData;
    this.asScalar = asScalar;
    this.getRealData = getRealData;
    this.getImagData = getImagData;
    this.getDataType = getDataType;
    this.getView = getView;
    this.isreal = isreal;
    this.isscalar = isscalar;
    this.isempty = isempty;
    this.toComplex = toComplex;
    this.real = real;
    this.imag = imag;
    this.hasImagPart = hasImagPart;


    //////////////////////////////////////////////////////////////////
    //                      Change the View                         //
    //////////////////////////////////////////////////////////////////

    /** Reshape the Matrix (on place).
     *
     * Change the dimensions of the Matrix while preserving the number of elements.
     * This is similar to Matlab's `reshape` function.
     *
     * Warning: this function updates the Matrix itself, not a copy!
     *
     * @param {Array} size
     *  New size of the Matrix.
     *
     * @chainable
     */
    const reshape = function () {
        var size = Array.prototype.slice.apply(arguments);
        if (Check.isArrayLike(size[0])) {
            size = size[0];
        }
        if (size.length === 0) {
            size = this.getLength();
        }
        size = Check.checkSize(size, 'column');
        var i, ie, l = 1;
        for (i = 0, ie = size.length; i < ie; i++) {
            l *= size[i];
        }
        if (l !== this.getLength()) {
            throw new Error('Matrix.reshape: number of elements must not change.');
        }
        setView(size);
        return this;
    }.bind(this);
    this.reshape = reshape;


    //////////////////////////////////////////////////////////////////
    //                        Copy Functions                        //
    //////////////////////////////////////////////////////////////////

    /** Get a copy of the Matrix.
     *
     * All the content of the Matrix is duplicated into a new Matrix.
     *
     * @return {Matrix}
     *  A copy of the Matrix.
     */
    const clone = function () {
        const r = !isreal(); // called first to remove a zero imaginary part
        const dataNew = new data.constructor(data);
        return new Matrix(size, dataNew, r, isBoolean);
    };
    this.clone = clone;

    // Constructor -- perform the initialization
    return initialize(size, Type, complex, bool);
}


//////////////////////////////////////////////////////////////////
//                      Static Values                           //
//////////////////////////////////////////////////////////////////


/** Default data constructor.
 *
 * @cfg {Function}
 */
Matrix.dataType = Float64Array;

/** Ignore trailing dimensions of size 1.
 *
 * Note: setting this option to `false` is more strict; resulting code is portable.
 *
 * @cfg {Boolean}
 */
Matrix.ignoreTrailingDims = true;
