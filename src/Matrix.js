/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
 * @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
 */

import Matrix from "./Matrix.class.js";
export default Matrix;

import MatrixView, {Check} from "@etsitpab/matrixview";
export {MatrixView, Check};

import toolsExtension from "./Matrix.tools.js";
toolsExtension(Matrix, Matrix.prototype);


import informationExtension from "./Matrix.information.js";
informationExtension(Matrix, Matrix.prototype);

import manipulationExtension from "./Matrix.manipulation.js";
manipulationExtension(Matrix, Matrix.prototype);

import constructionExtension from "./Matrix.construction.js";
constructionExtension(Matrix, Matrix.prototype);

import numericTypesExtension from "./Matrix.numeric_types.js";
numericTypesExtension(Matrix, Matrix.prototype);


import elementaryMath from "./Matrix.elementary_math.js";
elementaryMath(Matrix, Matrix.prototype);

import complexNumberExtension from "./Matrix.elementary_math.complex_numbers.js";
complexNumberExtension(Matrix, Matrix.prototype);

import specialFunctionExtension from "./Matrix.elementary_math.special_functions.js";
specialFunctionExtension(Matrix, Matrix.prototype);


import arithmeticOperatorsExtension from "./Matrix.operators.arithmetic.js";
arithmeticOperatorsExtension(Matrix, Matrix.prototype);

import booleansOperatorExtension from "./Matrix.operators.booleans.js";
booleansOperatorExtension(Matrix, Matrix.prototype);

import miscOperatorsExtension from "./Matrix.operators.misc.js";
miscOperatorsExtension(Matrix, Matrix.prototype);


import statisticsBaseExtension from "./Matrix.statistics.base.js";
statisticsBaseExtension(Matrix, Matrix.prototype);

import statisticsRandomExtension from "./Matrix.statistics.random.js";
statisticsRandomExtension(Matrix, Matrix.prototype);


import interpolationExtension from "./Matrix.interpolation.js";
interpolationExtension(Matrix, Matrix.prototype);

import linalgExtension from "./Linalg/Linalg.class.js";
linalgExtension(Matrix, Matrix.prototype);

import imageExtension from "./Image/Image.class.js";
imageExtension(Matrix, Matrix.prototype);

import fourierExtension from "./Fourier/Matrix.fourier.js";
fourierExtension(Matrix, Matrix.prototype);

import waveletExtension from "./Wavelets/Matrix.wavelets.js";
waveletExtension(Matrix, Matrix.prototype);


// import testExtension from "./Tests/Matrix.test_functions.js";
// testExtension(Matrix, Matrix.prototype);
