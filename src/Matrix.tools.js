/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/
/*global globalThis */

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */



export default function toolsExtension (Matrix, Matrix_prototype) {

    // Check if nodejs or browser
    const isNode = (typeof module !== "undefined" && module.exports) ? true : false;
    let fs;
    if (isNode) {
        import("fs").then(mod => fs = mod);
    }

    //////////////////////////////////////////////////////////////////
    //                   Matrix import/export functions             //
    //////////////////////////////////////////////////////////////////


    /** Convert 2D Matrix to 2D Array.
    *
    * __Also see:__
    * {@link Matrix#from}.
    *
    * @return {Number[][]} A 2D Array.
    *
    * @todo Tests.
    */
    Matrix_prototype.toArray = function () {
        const strErr = "Matrix.toArray: Function only available for";
        if (this.ndims() > 2) {
            throw new Error(`${strErr} 2D matrix.`);
        }
        if (!this.isreal()) {
            throw new Error(`${strErr} for real matrix.`);
        }
        const id = this.getData(), view = this.getView();
        const fy = view.getFirst(0), dy = view.getStep(0), ly = view.getEnd(0);
        const fx = view.getFirst(1), dx = view.getStep(1), lx = view.getEnd(1);

        const o = [];
        let y, ny, x, n;
        for (x = fx, n = lx; x !== n; x += dx) {
            const xTab = [];
            for (y = x + fy, ny = x + ly; y !== ny; y += dy) {
                xTab.push(id[y]);
            }
            o.push(xTab);
        }
        return o;
    };

    /** Convert 2D Matrix to formated string such like coma
    * separated values (CSV) strings.
    *
    * __Also see:__
    * {@link Matrix#dlmread}.
    *
    * @param {String} [delim=";"]  column delimiters.
    *
    * @return {String} A string.
    *
    * @matlike
    */
    Matrix_prototype.dlmwrite = function (d = ";") {

        if (typeof d !== "string") {
            throw new Error("Matrix.dlmread: Wrong delimiter specification.");
        }
        if (this.ndims() > 2) {
            throw new Error("Matrix.dlmwrite: function only available for 2D matrix.");
        }

        const view = this.getView(), td = this.getData();
        const dn = view.getStep(1), ln = view.getEnd(1);
        const m = view.getSize(0);

        let str = "", i, ij, ei, eij;
        for (i = 0, ei = m; i < ei; i++) {
            for (ij = i, eij = i + ln - dn; ij < eij; ij += dn) {
                str += td[ij] + d;
            }
            str += td[ij] + "\n";
        }

        return str;
    };

    /** Convert string with values delimited by characters
    * to Matrix.
    *
    * __Also see:__
    * {@link Matrix#dlmwrite}.
    *
    * @param {String} [delim] Column delimiters.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.dlmread = function (csv, d) {
        if (d !== undefined && typeof d !== "string") {
            throw new Error("Matrix.dlmread: Wrong delimiter specification.");
        }
        csv = csv.split("\n");
        if (csv[csv.length - 1] === "") {
            csv.pop();
        }
        // https://regex101.com/r/Qx8t5K/9
        const reg = /0b[01]+|0x[0-9a-f]+|[+-]?(?:\d*\.)?\d+(?:[e][+-]?\d+)?/gi;
        // const reg = /[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?/g;
        let i, ei, j, ej;
        for (i = 0, ei = csv.length; i < ei; i++) {
            const row = d ? csv[i].split(d) : csv[i].match(reg);
            for (j = 0, ej = row.length; j < ej; j++) {
                row[j] = parseFloat(row[j]);
            }
            csv[i] = row;
        }
        // Check that all rows have the same length
        if (!csv.every(v => v.length === csv[0].length)) {
            throw new Error("Matrix.dlmread: Inconsitent sizes in ND-array.");
        }

        return Matrix.from(csv).transpose();
    };

    /** Convert 2D Matrix to a string for copy/paste into Matlab.
    *
    * @return {String} A string.
    */
    Matrix_prototype.toMatlab = function () {
        if (this.ndims() > 2) {
            throw new Error("Matrix.toMatlab: function available only for 2D matrix.");
        }

        const pad = function (real, imag) {
            let str = real.toString();
            if (typeof imag === "number") {
                str += imag > 0 ? " + " : " - ";
                str += Math.abs(imag).toString();
                str += " * i";
            }
            return str;
        };

        const isReal = this.isreal();
        let o = "", id, ird, iid;
        if (isReal) {
            id = this.getData();
        } else {
            ird = this.getRealData();
            iid = this.getImagData();
        }
        const view = this.getView();
        const fy = view.getFirst(0), dy = view.getStep(0), ly = view.getEnd(0);
        const fx = view.getFirst(1), dx = view.getStep(1), lx = view.getEnd(1);

        let y, ny, x, n;
        o += "[";
        for (y = fy, ny = ly; y !== ny; y += dy) {
            if (y !== fy) {
                o += " ";
            }
            if (isReal) {
                for (x = y + fx, n = y + lx; x !== n; x += dx) {
                    o += pad(id[x]);
                    if (x !== n - dx) {
                        o += ",";
                    }
                }
            } else {
                for (x = y + fx, n = y + lx; x !== n; x += dx) {
                    o += pad(ird[x], iid[x]);
                    if (x !== n - dx) {
                        o += ",";
                    }
                }
            }
            if (y !== ny - dy) {
                o += ";";
            }
        }
        o += "]";
        return o;
    };

    /** Convert Matrix to string for display purposes.
    *
    * @param {String} [name=""] Name for the Matrix.
    *
    * @param {Number} [precision=4] Precision used to display the Matrix.
    *
    * @return {String} A string.
    *
    * @fixme There is some bugs when displaying complex Matrix.
    */
    Matrix_prototype.toString = function (name = "", precision = 4) {
        const length = precision + 5;
        const pad = function (real, imag) {
            let str = Number.isInteger(real) ? real + "" : real.toFixed(precision);
            str = str.padStart(length, " ");
            if (imag === undefined) {
                return str;
            }
            if (imag === 0) {
                return str.padEnd(2 * length + 1, " ");
            }
            str += imag > 0 ? " + " : " - ";
            str += Number.isInteger(real) ? Math.abs(imag) : Math.abs(imag).toFixed(precision);
            return str.padStart(length, " ") + "i";
        };

        if (this.isempty()) {
            return "Empty array: " + this.getSize().join("-by-");
        }
        if (this.getLength() > 10000) {
            return "Array: " + this.getSize().join("-by-");
        }

        const isReal = this.isreal();
        let id, ird, iid;
        if (isReal) {
            id = this.getData();
        } else {
            ird = this.getRealData();
            iid = this.getImagData();
        }

        const {iterator: it, begin: b, isEnd: e, getPosition} = this.getIterator(2);
        const z = this.getSize(2), view = this.getView();
        const fy = view.getFirst(0), dy = view.getStep(0), ly = view.getEnd(0);
        const fx = view.getFirst(1), dx = view.getStep(1), lx = view.getEnd(1);

        let i, y, ny, x, n;
        let o = "";
        for (i = b(); !e(); i = it()) {
            o += name;
            if (z > 1) {
                o += "(:,:," + getPosition() + ")";
            }
            o += " = [\n";
            for (y = i + fy, ny = i + ly; y !== ny; y += dy) {
                o += "\t";
                if (isReal) {
                    for (x = y + fx, n = y + lx; x !== n; x += dx) {
                        o += pad(id[x]) + " ";
                    }
                } else {
                    for (x = y + fx, n = y + lx; x !== n; x += dx) {
                        o += pad(ird[x], iid[x]) + " ";
                    }
                }
                o += "\n";
            }
            o += "]\n";
        }

        return o;
    };

    /** Cast data to Matrix. An Array is considered
    * as a column vector and an Array of Array as a set of
    * column vectors.
    *
    * If the input is a `Matrix` then it will be returned unchanged
    *
    * @param {Number|Number[]|Matrix} data
    *  Data to convert. If matrix, returns a copy, eventually with a new shape.
    *
    * @param {Integer[]} [shape]
    *  Shape of the returned matrix. it will be infered if ND-array and will be
    *  nx1 by default (n being the length of the provided array).
    *
    * @return {Matrix}
    */
    Matrix.from = function (data, shape) {
        if (data instanceof Matrix) {
            return shape ? data.reshape(shape) : data;
        }
        if (["number", "boolean"].includes(typeof(data))) {
            data = [data];
        }

        let d = data, size = [];
        if (Check.isArrayLike(d)) {
            while (d.every(v => Check.isArrayLike(v)) && d.length > 0) {
                size.unshift(d.length);
                if (!d.every(v => d[0].length === v.length)) {
                    throw new Error("Matrix.from: Inconsitent sizes in ND-array.");
                }
                d = d.map(v => Array.from(v)); // copy each array
                d = d.flat();
            }
            size.unshift(d.length);
            size = size.map((v, i) => v / (size[i + 1] || 1));
            Check.checkSize(size);
        } else {
            throw new Error(`Matrix.from: Invalid input, array-like expected. Got: ${d}.`);
        }
        let isBoolean = false;
        if (Check.isArrayOfBooleans(d)) {
            isBoolean = true;
        } else if (!Check.isArrayOfNumbers(d)) {
            throw new Error("Matrix.from: Array must only contain numbers or booleans.");
        }
        return new Matrix(shape ? shape : size, d, false, isBoolean);
    };

    /** Write a matrix on the disk using node. Be cautious, data are not
    * compressed.
    *
    * @param {String} name
    *  file name
    *
    * @param {Boolean}[littleEndian=false]
    *  Endianness
    *
    * @return {Matrix|ArrayBuffer}
    */
    Matrix_prototype.write = function (name, le = false) {
        const data = this.getData();
        let type;
        if (data instanceof Uint8Array) {
            type = 0;
        } else if (data instanceof Int8Array) {
            type = 1;
        } else if (data instanceof Uint8ClampedArray) {
            type = 2;
        } else if (data instanceof Uint16Array) {
            type = 3;
        } else if (data instanceof Int16Array) {
            type = 4;
        } else if (data instanceof Uint32Array) {
            type = 5;
        } else if (data instanceof Int32Array) {
            type = 6;
        } else if (data instanceof Float32Array) {
            type = 7;
        } else if (data instanceof Float64Array) {
            type = 8;
        }
        const buffer = data.buffer,
            sEl = data.BYTES_PER_ELEMENT,
            ndims = this.ndims(), size = this.getSize(),
            bufferOut = new ArrayBuffer(7 + ndims * 4 + sEl * data.length),
            dataView = new DataView(bufferOut);
        let pos = 0;
        dataView.setUint8(pos, type); pos += 1;
        dataView.setUint32(pos, data.length, le); pos += 4;
        dataView.setUint16(pos, ndims, le); pos += 2;
        let i;
        for (i = 0; i < ndims; i++) {
            dataView.setUint32(pos, size[i], le); pos += 4;
        }
        new Uint8Array(bufferOut).subarray(pos, pos + buffer.byteLength).set(new Uint8Array(buffer));
        if (typeof name === "string") {
            if (isNode) {
                fs.writeFileSync(name, new Buffer(new Uint8Array(bufferOut)));
            } else {
                Tools.download(bufferOut, name);
            }
        }
        return bufferOut;
    };

    /** Read a matrix from disk using node.
    *
    * @param {String} name
    *  File name
    *
    * @param {Boolean}[littleEndian=false]
    *  Endianness
    *
    * @return {Matrix}
    */
    Matrix.read = function (input, le = false) {
        const inputToDataView = function (input) {
            if (isNode && typeof input === "string") {
                input = fs.readFileSync(input);
            }
            if (input instanceof ArrayBuffer) {
                input = new DataView(input);
            } else if (input instanceof Uint8Array) {
                input = new DataView(input.buffer, input.byteOffset, input.byteLength);
            } else if (input instanceof Array || (globalThis.Buffer && input instanceof globalThis.Buffer)) {
                input = new DataView(new Uint8Array(input).buffer);
            } else if (!(input instanceof DataView)) {
                throw new Error("Matrix.read: Wrong type for input of type (" + input.constructor.name + ").");
            }
            return input;
        };
        const view = inputToDataView(input);
        const types = [
            "Uint8Array",
            "Int8Array",
            "Uint8ClampedArray",
            "Uint16Array",
            "Int16Array",
            "Uint32Array",
            "Int32Array",
            "Float32Array",
            "Float64Array"
        ];
        let pos = 0;
        const type  = types[view.getUint8(pos)]; pos += 1;
        const numel = view.getUint32(pos, le); pos += 4;
        const ndims = view.getUint16(pos, le); pos += 2;
        const size  = [];
        let i;
        for (i = 0; i < ndims; i++) {
            size[i] = view.getUint32(pos, le); pos += 4;
        }
        const TypeConstructor = Check.getTypeConstructor(type);
        const d = new TypeConstructor(numel);
        new Uint8Array(d.buffer).set(new Uint8Array(view.buffer, view.byteOffset, view.byteLength).subarray(pos));
        return new Matrix(size, d);
    };

    //////////////////////////////////////////////////////////////////
    //                   Matrix display functions                   //
    //////////////////////////////////////////////////////////////////


    /** Display the matrix in the console.
    *
    * @param {Number} precision
    *
    * @param {String} str
    *  String describing the content of the matrix.
    *  Used for display purpose
    *
    * @chainable
    */
    Matrix_prototype.display = function (...args) {
        console.log(this.toString(...args));
        return this;
    };
}
