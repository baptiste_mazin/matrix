/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

/** @class Matrix */

export default function interpolationExtension (Matrix) {
    /** Interpolate the input data at given values.
    *
    * __Also see:__
    *  {@link }.
    *
    * @param {Matrix} x
    * Abscissa of known points
    *
    * @param {Matrix} v
    * values of known points
    *
    * @param {Matrix} xq
    * values of requested points

    * @return {}
    *
    */
    const linear = function (X, V, Xq, O, extrapolation) {
        // V has shape numel(X) x (numel(V) / numel(X))
        // Let's s = numel(V) / numel(X)
        // O has a size of Xq x s
        const nx = X.length, no = O.length, nq = Xq.length;
        const first = X[0], last = X[nx - 1];
        // For each request r
        let q;
        for (q = 0; q < nq; q++) {
            const r = Xq[q];
            let x;
            // In the 2 first cases, we need to extrapolate
            if (r < first) {
                if (extrapolation !== "extrap") {
                    let o;
                    for (o = q; o < no; o += nq) {
                        O[o] = extrapolation;
                    }
                    continue;
                } else {
                    x = 0;
                }
            } else if (r > last) {
                if (extrapolation !== "extrap") {
                    let o;
                    for (o = q; o < no; o += nq) {
                        O[o] = extrapolation;
                    }
                    continue;
                } else {
                    x = nx - 2;
                }
            } else {
                // We find the closest smaller x < r
                const xe = nx - 2;
                for (x = 0; x < xe; x++) {
                    if (r <= X[x + 1]) {
                        break;
                    }
                }
            }

            // We interpolate for each v corresponding to x
            const cst = (r - X[x]) / (X[x + 1] - X[x]);
            let o, v;
            for (o = q, v = x; o < no; o += nq, v += nx) {
                O[o] = V[v] + (V[v + 1] - V[v]) * cst;
            }

        }
    };
    const nearest = function (x, v, xq, od) {
        var first = x[0], last = x[x.length - 1];
        for (var q = 0, qe = xq.length; q < qe; q++) {
            var r = xq[q];
            // In the following cases, we would have to extrapolate
            if (r < first || r > last) {
                od[q] = NaN;
                continue;
            }
            for (var i = 0, ie = x.length - 1; i < ie; i++) {
                if (r >= x[i] && r <= x[i + 1]) {
                    break;
                }
            }
            od[q] = r - x[i] < x[i + 1] - r ? v[i] : v[i + 1];
        }
    };
    const next = function (x, v, xq, od) {
        const first = x[0], last = x[x.length - 1];
        const qe = xq.length;
        let q;
        for (q = 0; q < qe; q++) {
            const r = xq[q];
            // In the following cases, we would have to extrapolate
            if (r < first || r > last) {
                od[q] = NaN;
                continue;
            }
            let i = -1;
            if (r !== x[0]) {
                const ie = x.length - 1;
                for (i = 0; i < ie; i++) {
                    if (r > x[i] && r <= x[i + 1]) {
                        break;
                    }
                }
            }
            od[q] = v[i + 1];
        }
    };
    const previous = function (x, v, xq, od) {
        const first = x[0], last = x[x.length - 1];
        const qe = xq.length;
        let q;
        for (q = 0; q < qe; q++) {
            const r = xq[q];
            // In the following cases, we would have to extrapolate
            if (r < first || r > last) {
                od[q] = NaN;
                continue;
            }
            const ie = x.length - 1;
            let i;
            for (i = 0; i < ie; i++) {
                if (r >= x[i] && r < x[i + 1]) {
                    break;
                }
            }
            od[q] = v[i];
        }
    };
    Matrix.interp1 = function (x, v, xq, method = "linear", extrapolation = NaN) {
        x = Matrix.from(x);
        v = Matrix.from(v);

        if (!x.iscolumn()) {
            throw new Error("Matrix.interp1: x must be a column vector.");
        }
        if (x.numel() !== v.size(0)) {
            throw new Error("Matrix.interp1: x and v must have the same size on the first dimension.");
        }

        let sorted = x.asort(0, "ascend");
        x = x.get(sorted);
        v = v.get(sorted, []);

        xq = Matrix.from(xq);
        const outSize = (xq.isvector() ? [xq.numel()] : xq.size()).concat(v.size().slice(1));
        const out = Matrix.zeros(outSize);

        if (method === "linear") {
            linear(x.getData(), v.getData(), xq.getData(), out.getData(), extrapolation);
        } else if (method === "nearest") {
            nearest(x.getData(), v.getData(), xq.getData(), out.getData());
        } else if (method === "next") {
            next(x.getData(), v.getData(), xq.getData(), out.getData());
        } else if (method === "previous") {
            previous(x.getData(), v.getData(), xq.getData(), out.getData());
        } else {
            throw new Error(`Matrix.interp1: method can be either 'linear', 'nearest', 'previous' or 'next'. Got ${method}.`);
        }
        return out;
    };
}
