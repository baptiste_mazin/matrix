/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

/** @class Matrix */

export default function complexNumberExtension (Matrix, Matrix_prototype) {

    /** Returns the Matrix real part.
    *
    * __Also see:__
    *  {@link Matrix#imag}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.real = function (m) {
        if (m.isreal()) {
            return m.clone();
        }
        const realData = m.getRealData().slice();
        return new Matrix(m.getSize(), realData);
    };

    /** Returns the Matrix imaginary part.
    *
    * __Also see:__
    * {@link Matrix#real}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix.imag = function (m) {
        if (m.isreal()) {
            return new Matrix(m.getSize());
        }
        const imagData = m.getImagData().slice();
        return new Matrix(m.getSize(), imagData);
    };

    /** Returns the phase angle for complex Matrix.
    *
    * When used as Matrix object method, this function acts in place.
    * Use the Matrix.angle property to work on a copy.
    *
    * __Also see:__
    *  {@link Matrix#abs}.
    *
    * @chainable
    * @matlike
    * @method angle
    */
    {
        const angle_real = function (data) {
            const ie = data.length;
            let i;
            for (i = 0; i < ie; i++) {
                data[i] = 0;
            }
        };

        const angle_cplx = function (datar, datai) {
            const ie = datar.length;
            let i;
            for (i = 0; i < ie; i++) {
                datar[i] = Math.atan2(datai[i], datar[i]);
                datai[i] = 0;
            }
        };

        Matrix_prototype.angle = function () {
            if (this.isreal()) {
                angle_real(this.getData());
            } else {
                angle_cplx(this.getRealData(), this.getImagData());
                this.isreal();
            }
            return this;
        };
    }
    Matrix.angle = function (m) {
        return m.clone().angle();
    };

    /** Returns the absolute value for real Matrix and
    * the complex magnitude for complex Matrix.
    *
    * When used as Matrix object method, this function acts in place.
    * Use the Matrix.abs property to work on a copy.
    *
    * __Also see:__
    *  {@link Matrix#angle}.
    *
    * @chainable
    * @matlike
    * @method abs
    */
    {
        const abs_real = function (data) {
            const ie = data.length;
            let i;
            for (i = 0; i < ie; i++) {
                if (data[i] < 0) {
                    data[i] = -data[i];
                }
            }
        };

        const abs_cplx = function (datar, datai) {
            const ie = datar.length;
            let i;
            for (i = 0; i < ie; i++) {
                datar[i] = Math.sqrt(datai[i] ** 2 + datar[i] ** 2);
                datai[i] = 0;
            }
        };

        Matrix_prototype.abs = function () {
            if (this.isreal()) {
                abs_real(this.getData());
            } else {
                abs_cplx(this.getRealData(), this.getImagData());
                this.isreal();
            }
            return this;
        };
    }
    Matrix.abs = function (m) {
        return m.clone().abs();
    };

    /** Returns the complex conjugate of each element of the Matrix.
    *
    * When used as Matrix object method, this function acts in place.
    * Use the Matrix.conj property to work on a copy.
    *
    * @matlike
    * @chainable
    */
    Matrix_prototype.conj = function () {
        if (this.isreal() === true) {
            return this;
        }
        const imag = this.getImagData(), ie = imag.length;
        let i;
        for (i = 0; i < ie; i++) {
            imag[i] = -imag[i];
        }
        return this;
    };
    Matrix.conj = function (m) {
        return m.clone().conj();
    };

    /** Complex conjugate transposition operator.
    *
    * @return {Matrix}
    * @matlike
    */
    Matrix_prototype.ctranspose = function () {
        return this.transpose().conj();
    };
    Matrix.ctranspose = function (m) {
        return m.ctranspose();
    };


}
