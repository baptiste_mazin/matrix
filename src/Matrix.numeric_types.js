/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */

export default function numericTypesExtension (Matrix, Matrix_prototype) {

    /** Returns a new Matrix with a data cast.
    *
    * __Also see:__
    *  {@link Matrix#type}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.cast = function (type) {
        let TypeConstructor = Check.getTypeConstructor(type);
        if (TypeConstructor === Array) {
            TypeConstructor = Float64Array;
        }

        let isBoolean = false;
        if (typeof(type) === "string") {
            isBoolean = ["boolean", "bool", "logical"].includes(type);
        }

        const od = new TypeConstructor(this.getData());
        return new Matrix(this.getSize(), od, !this.isreal(), isBoolean);
    };
    Matrix.cast = function (input, type) {
        return Matrix.from(input).cast(type);
    };

    /** Converts a Matrix to double.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.double = function () {
        return this.cast('double');
    };
    Matrix.double = function (input) {
        return Matrix.cast(input, "double");
    };

    /** Converts a Matrix to single.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.single = function () {
        return this.cast('single');
    };
    Matrix.single = function (input) {
        return Matrix.cast(input, "single");
    };

    /** Converts a Matrix to int8.
    *
    * Also see {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.int8 = function () {
        return this.cast('int8');
    };
    Matrix.int8 = function (input) {
        return Matrix.cast(input, "int8");
    };

    /** Converts a Matrix to int16.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.int16 = function () {
        return this.cast('int16');
    };
    Matrix.int16 = function (input) {
        return Matrix.cast(input, "int16");
    };

    /** Converts a Matrix to int32.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.int32 = function () {
        return this.cast('int32');
    };
    Matrix.int32 = function (input) {
        return Matrix.cast(input, "int32");
    };

    /** Converts a Matrix to uint8.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.uint8 = function () {
        return this.cast('uint8');
    };
    Matrix.uint8 = function (input) {
        return Matrix.cast(input, "uint8");
    };

    /** Converts a Matrix to logical.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.logical = function () {
        return this.cast('logical');
    };
    Matrix.logical = function (input) {
        return Matrix.cast(input, "logical");
    };

    /** Converts a Matrix to uint8c.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    */
    Matrix_prototype.uint8c = function () {
        return this.cast('uint8c');
    };
    Matrix.uint8c = function (input) {
        return Matrix.cast(input, "uint8c");
    };

    /** Converts a Matrix to uint16.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.uint16 = function () {
        return this.cast('uint16');
    };
    Matrix.uint16 = function (input) {
        return Matrix.cast(input, "uint16");
    };

    /** Converts a Matrix to uint32.
    *
    * __Also see:__
    *  {@link Matrix#cast}.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.uint32 = function () {
        return this.cast('uint32');
    };
    Matrix.uint32 = function (input) {
        return Matrix.cast(input, "uint32");
    };

    /** Returns a logical Matrix with 1 if value is NaN and 0 otherwise.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.isnan = function () {
        const oMat = new Matrix(this.getSize(), 'logical');
        const od = oMat.getData();
        let i;
        const ie = od.length;
        if (this.isreal()) {
            const id = this.getData();
            for (i = 0; i < ie; i++) {
                od[i] = isNaN(id[i]);
            }
        } else {
            const ird = this.getRealData(), iid = this.getImagData();
            for (i = 0; i < ie; i++) {
                od[i] = isNaN(ird[i]) || isNaN(iid[i]);
            }
        }
        return oMat;
    };

    /** Returns a logical Matrix with 1 if value is NaN and 0 otherwise.
    *
    * @return {Matrix}
    *
    * @matlike
    */
    Matrix_prototype.isinf = function () {
        const oMat = new Matrix(this.getSize(), 'logical');
        const od = oMat.getData();
        const ie = od.length;
        let i;
        if (this.isreal()) {
            const id = this.getData();
            for (i = 0; i < ie; i++) {
                const v = id[i];
                od[i] = (v === Infinity) || (v === -Infinity) ? 1 : 0;
            }
        } else {
            const ird = this.getRealData(), iid = this.getImagData();
            for (i = 0; i < ie; i++) {
                const vr = ird[i], vi = iid[i];
                od[i] = ((vr === Infinity) || (vr === -Infinity) || (vi === Infinity) || (vi === -Infinity)) ? 1 : 0;
            }
        }
        return oMat;
    };

    /** Returns a logical Matrix with 1 if value is NaN and 0 otherwise.
    *
    * @return {Matrix} New Matrix.
    *
    * @matlike
    */
    Matrix_prototype.isfinite = function () {
        const oMat = new Matrix(this.getSize(), 'logical');
        const od = oMat.getData();
        const ie = od.length;
        let i;
        if (this.isreal()) {
            const id = this.getData();
            for (i = 0; i < ie; i++) {
                od[i] = isFinite(id[i]) ? 1 : 0;
            }
        } else {
            const ird = this.getRealData(), iid = this.getImagData();
            for (i = 0; i < ie; i++) {
                od[i] = (isFinite(ird[i]) && isFinite(iid[i])) ? 1 : 0;
            }
        }
        return oMat;
    };
}
