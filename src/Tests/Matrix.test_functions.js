/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import getLogger from "@etsitpab/logger";

const Tools = {};
{
    const times = [], labels = {};

    Tools.tic = function (label) {
        if (label) {
            labels[label] = new Date().getTime();
        } else {
            times.push(new Date().getTime());
        }
    };

    Tools.toc = function (label) {
        var t = new Date().getTime();
        if (label) {
            return (t - labels[label]) || 0;
        }
        return (t - times.pop()) || 0;
    };
}

/** @class Matrix */

export default function testExtension (Matrix) {
    const logger = getLogger("Matrix", {level: "log"});

    const logPSNR = function (msg, psnr, time, log = logger) {
        if (psnr < 100 || isNaN(psnr)) {
            log.error(msg, "PSNR:", parseFloat(psnr.toFixed(2)), "dB", "Time:", time);
            return -1;
        } else if (psnr !== undefined && time !== undefined) {
            log.log(msg, "PSNR:", parseFloat(psnr.toFixed(2)), "dB", "Time:", time);
        } else if (psnr === undefined && time !== undefined) {
            log.log(msg, "Time:", time);
        } else if (psnr === undefined && time === undefined) {
            log.log(msg);
        }
    };

    Matrix._benchmarkColorspaces = function () {
        const logger = getLogger("Matrix tests - colorspaces", {level: "debug"});
        function rand(M) {
            const tab = new Float32Array(M);
            for (let i = 0; i < M; i++) {
                tab[i] = (Math.random() * 32 | 0) / 31;
            }
            return tab;
        }
        function error(a, b) {
            let err = 0, N = a.length;
            for (let i = 0; i < N; i++) {
                const tmp = a[i] - b[i];
                err += tmp * tmp;
            }
            return err / N;
        }
        const errors = [];
        const testcs = function (csin, csout, N = 1e5) {
            const test = function (nc, sc, sp, m) {
                const input = rand(nc * N);
                const inputCopy = new Float32Array(input);
                const t1 = new Date().getTime();
                Matrix.Colorspaces[csin + " to " + csout](input, N, sc, sp);
                const t2 = new Date().getTime();
                Matrix.Colorspaces[csout + " to " + csin](input, N, sc, sp);
                const t3 = new Date().getTime();
                const err = error(input, inputCopy);
                if (err < 1e-10) {
                    logger.log(m, t2 - t1, t3 - t2, "OK");
                    return true;
                } else {
                    errors.push([csin + " to " + csout, m].join(" - "));
                    logger.error(m, t2 - t1, t3 - t2, "Error: ", err);
                    return false;
                }
            };
            logger.groupCollapsed("Conversion from " +  csin + " to " + csout);
            const t1 = test(3, 1, 3, "RGB RGB        ");
            const t2 = test(4, 1, 3, "RGBA RGBA      ");
            const t3 = test(3, N, 1, "RRR GGG BBB    ");
            const t4 = test(4, N, 1, "RRR GGG BBB AAA");
            logger.groupEnd();
            return t1 + t2 + t3 + t4;
        };
        const testcsmatrix = function (mat, imat, N = 1e6) {
            const test = function (nc, sc, sp, mes) {
                const input = rand(nc * N);
                const inputCopy = new Float32Array(input);
                const t1 = new Date().getTime();
                Matrix.Colorspaces.matrix(input, mat, N, sc, sp);
                const t2 = new Date().getTime();
                Matrix.Colorspaces.matrix(input, imat, N, sc, sp);
                const t3 = new Date().getTime();
                const err = error(input, inputCopy);
                if (err < 1e-10) {
                    logger.log(mes, t2 - t1, t3 - t2, "OK");
                    return true;
                } else {
                    logger.error(mes, t2 - t1, t3 - t2, "Error: ", err);
                    return false;
                }
            };
            // CASE RGB RGB
            logger.groupCollapsed("Conversion using 3x3 matrix");
            const t1 = test(3, 1, 3, "RGB RGB        ");
            const t2 = test(4, 1, 3, "RGBA RGBA      ");
            const t3 = test(3, N, 1, "RRR GGG BBB    ");
            const t4 = test(4, N, 1, "RRR GGG BBB AAA");
            logger.groupEnd();
            return t1 + t2 + t3 + t4;
        };

        const tests = {
            "RGB": [
                "Lch",
                "Luv",
                "rgY",
                "xyY",
                "1960 uvY",
                "1976 u'v'Y",
                "Lab",
                "XYZ",
                "RGB",
                "HSV",
                "HSL",
                "HSI",
                "CMY",
                "Opponent",
                "Ohta",
                "XYZ"
            ],
            "LinearRGB": ["XYZ", "rgY", "GRGBG", "sRGB",],
            "Lab": ["Lch", "XYZ"],
            "xyY": ["1960 uvY", "1976 u'v'Y"],
            "1976 u'v'Y": ["1960 uvY"]
        };
        logger.groupCollapsed("Run tests");
        for (let [csin, csouts] of Object.entries(tests)) {
            for (let csout of csouts) {
                testcs(csin, csout);
            }
        }
        // For these functions the test function doesn't make sense
        // testcs("rgY", "xyY");
        // testcs("XYZ", "xyY");
        // testcs("XYZ", "Luv");
        // testcs("XYZ", "1960 uvY");
        // testcs("XYZ", "1976 u'v'Y");

        // Apply a linear 3x3 transformation
        let m1, m2;
        m1 = new Float32Array([1, 0, 0, 0, 1, 0, 0, 0, 1]);
        testcsmatrix(m1, m1);
        m1 = new Float32Array([1, 1, -1, 1, 0, 2, 1, -1, -1]);
        m2 = new Float32Array([1 / 3, 1 / 3, 1 / 3, 1 / 2, 0, -1 / 2, -1 / 6, 1 / 3, -1 / 6]);
        testcsmatrix(m1, m2);
        logger.groupEnd();
        if (errors.length) {
            logger.group("Following tests have failed");
            errors.forEach(e => logger.error(e));
            logger.groupEnd();
        } else {
            logger.info("No test has failed !");
        }

    };

    Matrix._benchmarkLinalg = function (sz = 300) {
        const logger = getLogger("Matrix tests - Linear algebra", {level: "debug"});

        const tic = Tools.tic, toc = Tools.toc;

        function display(type, time, residual) {
            if (residual > 1e-7 || isNaN(residual)) {
                logger.error("Case " + type + ", Residual: ", residual, "Time:", time);
                return true;
            }
            logger.log("Case " + type + ", Residual: ", residual, "Time:", time);
            return false;
        }

        const Bench = {

            "CHOLESKY Upper Decomposition": function (A) {
                tic();
                const G = A.chol('upper');
                const time = toc();
                const residual = G.ctranspose().mtimes(G).minus(A).norm();
                return [time, residual];
            },

            "CHOLESKY Lower Decomposition": function (A) {
                tic();
                const G = A.chol('lower');
                const time = toc();
                const residual = G.mtimes(G.ctranspose()).minus(A).norm();
                return [time, residual];
            },

            "LU Decomposition": function (A) {
                tic();
                const [L, U] = A.lu();
                const time = toc();
                const residual = L.mtimes(U).minus(A).norm();
                return [time, residual];
            },

            "LUP Decomposition": function (A) {
                tic();
                const [L, U, P] = A.lup();
                const time = toc();
                const residual = L.mtimes(U).minus(P.mtimes(A)).norm();
                return [time, residual];
            },

            "QR Decomposition": function (A) {
                tic();
                const [Q, R] = A.qr();
                const time = toc();
                const residual = Q.mtimes(R).minus(A).norm();
                return [time, residual];
            },

            "QRP Decomposition": function (A) {
                tic();
                const [Q, R, P] = A.qrp();
                const time = toc();
                const residual = Q.mtimes(R).minus(A.mtimes(P)).norm();
                return [time, residual];
            },
            /*
            "LU Inversion": function (A) {
                tic();
                const eye = Matrix.eye(A.getSize(0));
                const iA = solveLU(A, eye);
                const time = toc();
                const residual = A.mtimes(iA).minus(eye).norm();
                return [time, residual];
            },
             */
            "QR Inversion": function (A) {
                tic();
                const eye = Matrix.eye(A.getSize(0));
                const iA = A.mldivide(eye);
                const time = toc();
                const residual = A.mtimes(iA).minus(eye).norm();
                return [time, residual];
            },

            "BIDIAG Decomposition": function (A) {
                tic();
                const [U, B, V] = A.bidiag();
                const time = toc();
                const residual = U.mtimes(B).mtimes(V).minus(A).norm();
                return [time, residual];
            }

        };

        const C = Matrix.complex(Matrix.randi(9, sz), Matrix.randi(9, sz));
        const C_real = C.real();
        const CCt = C.mtimes(C.ctranspose());
        const CCt_real = C_real.mtimes(C_real.transpose());

        const errors = [];
        logger.groupCollapsed("Run tests");
        for (let [desc, func] of Object.entries(Bench)) {
            logger.groupCollapsed(desc);
            const err1 = display("REAL", ...func(CCt_real));
            if (err1) {
                errors.push([desc, "REAL"].join(" - "));
            }
            const err2 = display("CPLX", ...func(CCt));
            if (err2) {
                errors.push([desc, "CPLX"].join(" - "));
            }
            logger.groupEnd(desc);
        }
        logger.groupEnd();
        if (errors.length) {
            logger.group("Following tests have failed");
            errors.forEach(e => logger.error(e));
            logger.groupEnd();
        } else {
            logger.info("No test has failed !");
        }
    };

    Matrix._benchmarkSVD = function (sz = 300) {
        const t = [];
        let i;
        for (i = 0; i < 1; i++) {
            const A = Matrix.rand(sz);
            Tools.tic();
            const [U, S, V] = A.svd();
            t[i] = Tools.toc();
            const rec = U.mtimes(S).mtimes(V.transpose());
            logPSNR("SVD", Matrix.psnr(A, rec).asScalar(), t[i]);
        }
    };

    Matrix._benchmarkWavelets = function (wNames, wModes, testMode) {
        const logger = getLogger("Matrix tests - Wavelets", {level: "debug"});
        if (testMode === "fast") {
            wNames = ["haar", "sym2", "coif1", "sym4"];
            wModes = ["sym", "per"];
        }
        wNames = wNames || [
            'haar',
            'sym2', 'sym4', 'sym8',
            'db2', 'db4', 'db8',
            'coif1', 'coif2', 'coif4', 'coif4',
            'bi13', 'bi31', 'bi68',
            'rbio31', 'rbio33', 'rbio35', 'rbio39',
            'cdf97'
        ];
        wModes = wModes || [
            "sym", "symw", "per", "zpd", "nn"
        ];
        const test_wavedecrec = function (s, N, name) {
            const test = function (dim) {
                Tools.tic();
                const wt = Matrix.wavedec(s, N, name, dim);
                const iwt = Matrix.waverec(wt, name, dim);
                const time = Tools.toc();
                const psnr = Matrix.psnr(s, iwt).asScalar();
                return {
                    psnr: psnr,
                    time: time
                };
            };
            const res0 = test(0), res1 = test(1);
            return {
                psnr: Math.min(res0.psnr, res1.psnr),
                time: Math.round((res0.time + res1.time) / 2)
            };
        };
        const test_upwlev = function (s, N, name) {
            const test = function (dim) {
                Tools.tic();
                let wt = Matrix.wavedec(s, N, name, dim);
                for (let n = 0; n < N; n++) {
                    wt = Matrix.upwlev(wt, name, dim);
                }
                const time = Tools.toc();
                return {
                    psnr: Matrix.psnr(wt[0], s).asScalar(),
                    time: time
                };
            };
            const res0 = test(0), res1 = test(1);
            return {
                psnr: Math.min(res0.psnr, res1.psnr),
                time: Math.round((res0.time + res1.time) / 2)
            };
        };
        const test_wrcoef = function (s, N, name) {
            let M = 0;
            const test = function (dim) {
                Tools.tic();
                const wt = Matrix.wavedec(s, N, name, dim);
                let rec = Matrix.wrcoef('l', wt, name, dim, N - M);
                for (let n = N - M; n > 0; n--) {
                    rec["+="](Matrix.wrcoef('h', wt, name, dim, n));
                }
                const time = Tools.toc();
                return {
                    psnr: Matrix.psnr(s, rec).asScalar(),
                    time: time
                };
            };
            const res0 = test(0), res1 = test(1);
            return {
                psnr: Math.min(res0.psnr, res1.psnr),
                time: Math.round((res0.time + res1.time) / 2)
            };
        };
        const test_wavedecrec2 = function (s, N, name) {
            Tools.tic();
            const wt2 = Matrix.wavedec2(s, N, name);
            const iwt2 = Matrix.waverec2(wt2, name);
            const time = Tools.toc();
            const psnr = Matrix.psnr(s, iwt2).asScalar();
            return {
                psnr: psnr,
                time: time
            };
        };
        const test_upwlev2 = function (s, N, name) {
            Tools.tic();
            let wt = Matrix.wavedec2(s, N, name);
            for (let n = 0; n < N; n++) {
                wt = Matrix.upwlev2(wt, name);
            }
            const rec = Matrix.appcoef2(wt, name, 0);
            const time = Tools.toc();
            return {
                psnr: Matrix.psnr(rec, s).asScalar(),
                time: time
            };
        };
        const test_wrcoef2 = function (s, N, name) {
            let M = 0;
            Tools.tic();
            const wt = Matrix.wavedec2(s, N, name);
            let rec = Matrix.wrcoef2('a', wt, name, N - M);
            for (let n = N - M; n > 0; n--) {
                rec["+="](Matrix.wrcoef2('h', wt, name, n));
                rec["+="](Matrix.wrcoef2('v', wt, name, n));
                rec["+="](Matrix.wrcoef2('d', wt, name, n));
            }
            const time = Tools.toc();
            return {
                psnr: Matrix.psnr(s, rec).asScalar(),
                time: time
            };
        };
        let res, err;
        logger.groupCollapsed("Run tests");
        const errors = [];
        for (let n = 0; n < wNames.length; n++) {
            const name = wNames[n];
            for (let m = 0; m < wModes.length; m++) {
                Matrix.dwtmode(wModes[m]);
                for (let sz = 1; sz < 10; sz += 2) {
                    const s = Matrix.rand(sz, sz + 1, 2);
                    let N = Matrix.dwtmaxlev([sz, sz + 1], name);
                    N = N < 1 ? 1 : N;

                    logger.group(s.size() + " " + name + " " + wModes[m]);
                    // 1D tests
                    res = test_wavedecrec(s, N, name);
                    err = logPSNR("DWT 1D on " + N + " levels", res.psnr, res.time, logger);
                    if (err === -1) {
                        errors.push(`${name} ${s} - DWT 1D on ${N} levels.`);
                    }
                    if (testMode !== "fast") {
                        res = test_upwlev(s, N, name);
                        err = logPSNR("1D upwlev on " + N + " levels", res.psnr, res.time, logger);
                        if (err === -1) {
                            errors.push(`${name} ${s} - 1D upwlev on ${N} levels.`);
                        }
                        res = test_wrcoef(s, N, name);
                        err = logPSNR("Reconstruction with wrcoef on " + N + " levels", res.psnr, res.time, logger);
                        if (err === -1) {
                            errors.push(`${name} ${s} - Reconstruction with wrcoef on ${N} levels.`);
                        }
                    }
                    // 2D tests
                    res = test_wavedecrec2(s, N, name);
                    err = logPSNR("DWT 2D on " + N + " levels", res.psnr, res.time, logger);
                    if (err === -1) {
                        errors.push(`${name} ${s} - DWT 2D on ${N} levels.`);
                    }
                    if (testMode !== "fast") {
                        res = test_upwlev2(s, N, name);
                        err = logPSNR("2D upwlev on " + N + " levels", res.psnr, res.time, logger);
                        if (err === -1) {
                            errors.push(`${name} ${s} - 2D upwlev on ${N} levels.`);
                        }
                        res = test_wrcoef2(s, N, name);
                        err = logPSNR("Reconstruction with wrcoef2 on " + N + " levels", res.psnr, res.time, logger);
                        if (err === -1) {
                            errors.push(`${name} ${s} - Reconstruction with wrcoef2 on ${N} levels.`);
                        }
                    }
                    logger.groupEnd();
                }
            }
        }
        logger.groupEnd();
        if (errors.length) {
            logger.group("Following tests have failed");
            errors.forEach(e => logger.error(e));
            logger.groupEnd();
        } else {
            logger.info("No test has failed !");
        }
    };

    Matrix._benchmarkFourier = function () {
        const logger = getLogger("Matrix tests - Fourier", {level: "debug"});

        const test1 = function (s) {
            Tools.tic();
            const fft = Matrix.fft(s);
            const out = Matrix.ifft(fft);
            const time = Tools.toc();
            return {
                psnr: Matrix.psnr(s, out).asScalar(),
                time: time
            };
        };
        const test2 = function (s) {
            Tools.tic();
            const fft = Matrix.fft2(s);
            const out = Matrix.ifft2(fft);
            const time = Tools.toc();
            return {
                psnr: Matrix.psnr(s, out).asScalar(),
                time: time
            };
        };
        const errors = [];
        logger.groupCollapsed("Run tests");
        let res, err;
        for (let sz = 1; sz < 6; sz += 2) {
            const s = Matrix.rand(sz, sz + 1);
            res = test1(s);
            err = logPSNR("FFT 1D decomposition/reconstruction", res.psnr, res.time);
            if (err === -1) {
                errors.push(`FFT 1D decomposition/reconstruction - Size: ${sz}`);
            }
            res = test2(s);
            err = logPSNR("FFT 2D decomposition/reconstruction", res.psnr, res.time);
            if (err === -1) {
                errors.push(`FFT 2D decomposition/reconstruction - Size: ${sz}`);
            }
        }
        logger.groupEnd();

        if (errors.length) {
            logger.group("Following tests have failed");
            errors.forEach(e => logger.error(e));
            logger.groupEnd();
        } else {
            logger.info("No test has failed !");
        }
    };

    Matrix._benchmarkPolyfit = function () {
        const logger = getLogger("Matrix tests - Polyfit", {level: "debug"});

        const errors = [];
        logger.groupCollapsed("Run tests");
        for (let np = 1; np < 10; np++) {
            for (let i = 0; i < 10; i++) {
                const N = 400 + Math.floor(Math.random() * 200);
                const p = Matrix.randi([-100, 100], np, 1);
                const x = Matrix.colon(Math.floor(- N / 2), Math.ceil(N / 2)), y = Matrix.polyval(p, x);
                const pp = Matrix.polyfit(x, y, p.numel() - 1);
                const errMean = p['-'](pp).abs().mean().asScalar();
                if (errMean > 1e-7) {
                    logger.error(`Size: ${N}, degree: ${np}, ${errMean}`);
                    errors.push(`Size: ${N}, degree: ${np}`);
                } else {
                    logger.log(`Size: ${N}, degree: ${np}, ${errMean}`);
                }

            }
        }
        logger.groupEnd();
        if (errors.length) {
            logger.group("Following tests have failed");
            errors.forEach(e => logger.error(e));
            logger.groupEnd();
        } else {
            logger.info("No test has failed !");
        }
    };

    Matrix._benchmark = function () {
        for (let i in Matrix) {
            if (Matrix[i] && i.match(/benchmark/) && i !== "_benchmark") {
                Matrix[i]();
            }
        }
    };

}
