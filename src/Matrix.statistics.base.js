/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @author Baptiste Mazin     <baptiste.mazin@telecom-paristech.fr>
* @author Guillaume Tartavel <guillaume.tartavel@telecom-paristech.fr>
*/

import {Check} from "@etsitpab/matrixview";

/** @class Matrix */

export default function statisticsBaseExtension (Matrix, Matrix_prototype) {

    const min = function (data, s, d, N) {
        let i, e, m;
        for (i = s + d, e = s + N, m = data[s]; i < e; i += d) {
            if (data[i] < m) {
                m = data[i];
            }
        }
        return m;
    };

    const amin = function (data, s, d, N) {
        let i, e, m, im;
        for (i = s + d, e = s + N, m = data[s], im = s; i < e; i += d) {
            if (data[i] < m) {
                m = data[i];
                im = i;
            }
        }
        return im;
    };

    const max = function (data, s, d, N) {
        let i, e, m;
        for (i = s + d, e = s + N, m = data[s]; i < e; i += d) {
            if (data[i] > m) {
                m = data[i];
            }
        }
        return m;
    };

    const amax = function (data, s, d, N) {
        let i, e, m, im;
        for (i = s + d, e = s + N, m = data[s], im = s; i < e; i += d) {
            if (data[i] > m) {
                m = data[i];
                im = i;
            }
        }
        return im;
    };

    const sum = function (data, s, d, N) {
        let i, e, m;
        for (i = s, e = s + N, m = 0; i < e; i += d) {
            m += data[i];
        }
        return m;
    };

    const mean = function (data, s, d, N) {
        let i, e, m;
        for (i = s, e = s + N, m = 0; i < e; i += d) {
            m += data[i];
        }
        return m * d / N;
    };

    const prod = function (data, s, d, N) {
        let i, e, m;
        for (i = s, e = s + N, m = 1; i < e; i += d) {
            m *= data[i];
        }
        return m;
    };

    const variance = function (data, s, d, N) {
        const mu = mean(data, s, d, N);
        let i, e, m;
        for (i = s, e = s + N, m = 0; i < e; i += d) {
            m += (data[i] - mu) ** 2;
        }
        return m * d / (N - d);
    };

    const varianceBiased = function (data, s, d, N) {
        return variance(data, s, d, N) * (N - d) / N;
    };

    const cumsum = function (data, s, d, N) {
        let i, e;
        for (i = s + d, e = s + N; i < e; i += d) {
            data[i] += data[i - d];
        }
    };

    const cumprod = function (data, s, d, N) {
        let i, e;
        for (i = s + d, e = s + N; i < e; i += d) {
            data[i] *= data[i - d];
        }
    };

    const getPermutation = function (view, dim) {
        const ndims = view.ndims(), order = [dim];
        let i;
        for (i = 0; i < ndims; i++) {
            if (i !== dim) {
                order.push(i);
            }
        }
        return order;
    };

    // Do the function fun return a number or act in place ?
    const applyDim = function (mat, fun, dim, inplace = false, output = "Float64Array") {
        // Check parameter dim
        if (!Check.isSet(dim)) {
            if (inplace) {
                fun(mat.getData(), 0, 1, mat.numel());
                return mat;
            }
            if (typeof(output) === "string") {
                const v = fun(mat.clone().getData(), 0, 1, mat.numel());
                const TypeConstructor = Check.getTypeConstructor(output);
                return new Matrix([1, 1], TypeConstructor.from([v]));
            }
            if (output instanceof Matrix) {
                fun(mat.getData(), 0, 1, mat.numel(), output.getData());
                return output;
            }
            throw new Error("Matrix.applyDim: Invalid output.");
        }

        if (!Check.isInteger(dim, 0)) {
            throw new Error("Matrix.applyDim: Invalid dimension.");
        }

        const view = mat.getView(), order = getPermutation(view, dim);
        view.permute(order);

        // Input Matrix and data
        const id = mat.getData(), d = view.getStep(0), l = view.getEnd(0);
        const {iterator: it, begin:b, isEnd: e} = view.getIterator(1);
        if (!inplace) {
            // Output size and data
            if (typeof(output) === "string") {
                const sizeOut = mat.getSize();
                sizeOut[dim] = 1;
                let i, io;
                const om = new Matrix(sizeOut, output), od = new om.getData();
                for (i = b(), io = 0; !e(); i = it(), io++) {
                    od[io] = fun(id, i, d, l);
                }
                return om;
            }
            if (output instanceof Matrix) {
                const od = output.getData();
                let i;
                for (i = b(); !e(); i = it()) {
                    fun(id, i, d, l, od);
                }
                return output;
            }
        }
        if (output instanceof Matrix) {
            mat = output;
            output = output.getData();
        }
        let i;
        for (i = b(); !e(); i = it()) {
            fun(id, i, d, l, output);
        }
        return mat;
    };


    /** Return the argmin of a matrix.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the global argmin.
    * @return {Matrix}
    */
    Matrix_prototype.amin = function (dim) {
        const mat = this.isreal() ? this : Matrix.abs(this);
        return applyDim(mat, amin, dim, undefined, "uint32");
    };

    /** Return the minimum of a matrix.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the global minimum.
    * @return {Matrix}
    */
    Matrix_prototype.min = function (dim) {
        if (this.isreal()) {
            return applyDim(this, min, dim);
        }
        return this.get(this.amin(dim));
    };

    /** Return the argmax of a matrix.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the global argmax.
    * @return {Matrix}
    */
    Matrix_prototype.amax = function (dim) {
        const mat = this.isreal() ? this : Matrix.abs(this);
        return applyDim(mat, amax, dim, undefined, "uint32");
    };

    /** Return the maximum of a matrix.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the global maximum.
    * @return {Matrix}
    */
    Matrix_prototype.max = function (dim) {
        if (this.isreal()) {
            return applyDim(this, max, dim);
        }
        return this.get(this.amax(dim));
    };

    /** Return the sum of the matrix elements.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the global sum.
    * @return {Matrix}
    */
    Matrix_prototype.sum = function (dim) {
        if (this.isreal()) {
            return applyDim(this, sum, dim);
        }
        const size = this.getSize();
        return Matrix.complex(
            applyDim(new Matrix(size, this.getRealData()), sum, dim),
            applyDim(new Matrix(size, this.getImagData()), sum, dim)
        );
    };

    /** Return the product of the matrix elements.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the product of all the matrix elements.
    * @return {Matrix}
    */
    Matrix_prototype.prod = function (dim) {
        if (this.isreal) {
            return applyDim(this, prod, dim);
        }
        throw new Error("Matrix.prod: Is not yet implement for complex values.");
    };

    /** Return the average value of the matrix elements.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the average value of all the elements.
    * @return {Matrix}
    */
    Matrix_prototype.mean = function (dim) {
        if (this.isreal()) {
            return applyDim(this, mean, dim);
        }
        const size = this.getSize();
        return Matrix.complex(
            applyDim(new Matrix(size, this.getRealData()), mean, dim),
            applyDim(new Matrix(size, this.getImagData()), mean, dim)
        );
    };

    /** Return the variance of the matrix elements.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the variance of all the elements.
    * @param {Number} [norm=false]
    *  If false, use the non biased variance estimator (N - 1), and the
    *  biased one otherwise.
    * @return {Matrix}
    */
    Matrix_prototype.variance = function (dim, norm = false) {
        if (!this.isreal()) {
            throw new Error("Matrix.variance: Is not yet implement for complex values.");
        }
        let fun;
        switch (typeof norm) {
            case "boolean":
                fun = (norm === false) ? variance : varianceBiased;
                break;
            case "number":
                if (!Check.isInteger(norm, 0, 1)) {
                    throw new Error("Matrix.variance: Invalid argument.");
                }
                fun = [variance, varianceBiased][norm];
                break;
            default:
                throw new Error("Matrix.variance: Invalid argument.");
        }
        return applyDim(this, fun, dim);
    };
    Matrix_prototype.var = Matrix_prototype.variance;

    /** Return the standard deviation of the matrix elements.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the standard deviation of all the elements.
    * @param {Number} [norm=false]
    *  If false, use the non biased standard deviation estimator (N - 1),
    * and the biased one otherwise.
    * @return {Matrix}
    */
    Matrix_prototype.std = function (norm, dim) {
        if (!this.isreal()) {
            throw new Error("Matrix.std: Is not yet implement for complex values.");
        }
        return this.variance(norm, dim).arrayfun(Math.sqrt);
    };

    /** Return the cumulative sum of the matrix elements.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the cumulative sum of all the elements.
    * @return {Matrix}
    */
    Matrix_prototype.cumsum = function (dim) {
        const fun = cumsum;
        if (this.isreal()) {
            return applyDim(this, fun, dim, true);
        }
        const size = this.getSize();
        return Matrix.complex(
            applyDim(new Matrix(size, this.getRealData()), fun, dim, true),
            applyDim(new Matrix(size, this.getImagData()), fun, dim, true)
        );
    };
    Matrix.cumsum = function (mat, dim) {
        return Matrix.from(mat).clone().cumsum(dim);
    };


    /** Return the cumulative product of the matrix elements.
    * @param {Number} [dim=undefined]
    *  Dimension on which the computation must be performed. If undefined,
    *  return the cumulative product of all the elements.
    * @return {Matrix}
    */
    Matrix_prototype.cumprod = function (dim) {
        if (!this.isreal()) {
            throw new Error("Matrix.cumprod: Is not yet implemented for complex values.");
        }
        return applyDim(this, cumprod, dim, true);
    };
    Matrix.cumprod = function (mat, dim) {
        return Matrix.from(mat).clone().cumprod(dim);
    };

    (function () {
        // Tempplate function
        // const template = function (dataIn1, sI1, dI1, NI1, dataIn2, sI2, dI2, dataOut, sO, dO) {
        //     let i1, e, i2, o;
        //     for (i1 = sI1 + dI1, e = sI1 + NI1, i2 = sI2 + dI2, o = sO; i1 < e; i1 += dI1, i2 += dI2, o += dO) {
        //         dataOut[o] = dataIn1[i1] - dataIn2[i2];
        //     }
        // };
        const cov = function (dataIn1, sI1, dI1, NI1, mu1, dataIn2, sI2, dI2, mu2) {
            let i1, e, i2, m;
            for (i1 = sI1, e = sI1 + NI1, i2 = sI2, m = 0; i1 < e; i1 += dI1, i2 += dI2) {
                m += (dataIn1[i1] - mu1) * (dataIn2[i2] - mu2);
            }
            return m * dI1 / (NI1 - 1);
        };
        const corrcoef = function (dataIn1, sI1, dI1, NI1, mu1, sig1, dataIn2, sI2, dI2, mu2, sig2) {
            let i1, e, i2, m;
            for (i1 = sI1, e = sI1 + NI1, i2 = sI2, m = 0; i1 < e; i1 += dI1, i2 += dI2) {
                m += (dataIn1[i1] - mu1) * (dataIn2[i2] - mu2);
            }
            return m * dI1 / ((NI1 - 1) * sig1 * sig2);
        };
        /** Compute the covariance matrix.
        *
        * @param {Matrix} B
        *  Compute covariance between this matrix and B.
        *
        * @todo This function should be able to deal with complex
        * @matlike
        * @method cov
        */
        Matrix_prototype.cov = function (B) {
            if (!this.isreal()) {
                throw new Error("Matrix.cov: Is not yet implement for complex values.");
            }
            if (!this.ismatrix()) {
                throw new Error("Matrix.cov: Inputs must be a 2D matrices.");
            }

            const [ny, nx] = this.size();
            const om = Matrix.zeros(nx), od = om.getData();
            const d1 = this.getData(), mu1 = this.mean(0).getData();
            if (B === undefined) {
                let x1, x2, j, jj, j2, ij, ji;
                for (x1 = 0, j = 0, jj = 0; j < nx; x1 += ny, j++, jj += nx + 1) {
                    od[jj] = cov(d1, x1, 1, ny, mu1[j], d1, x1, 1, mu1[j]);
                    for (x2 = x1 + ny, j2 = j + 1, ij = j + j2 * nx, ji = j2 + j * nx; j2 < nx; x2 += ny, ij += nx, j2++, ji++) {
                        od[ij] = cov(d1, x1, 1, ny, mu1[j], d1, x2, 1, mu1[j2]);
                        od[ji] = od[ij];
                    }
                }
            } else {
                B = Matrix.from(B);
                if (!Check.checkSizeEquals(this.size(), B.size(), Matrix.ignoreTrailingDims)) {
                    throw new Error("Matrix.cov: Input sizes must match.");
                }
                const d2 = B.getData(), mu2 = B.mean(0).getData();
                let x1, x2, j, j2, ij;
                for (x1 = 0, j = 0; j < nx; x1 += ny, j++) {
                    for (x2 = 0, j2 = 0, ij = j + j2 * nx; j2 < nx; x2 += ny, ij += nx, j2++) {
                        od[ij] = cov(d1, x1, 1, ny, mu1[j], d2, x2, 1, mu2[j2]);
                    }
                }
            }
            return om;
        };

        /** Compute the correlation coefficients.
        *
        * @param {Matrix} B
        *  Compute correlation coefficients between this matrix and B.
        *
        * @todo This function should be able to deal with complex
        * @matlike
        * @method cov
        */
        Matrix_prototype.corrcoef = function (B) {
            if (!this.ismatrix()) {
                throw new Error("Matrix.corrcoef: Inputs must be a 2D matrices.");
            }

            const [ny, nx] = this.size();
            const om = Matrix.zeros(nx), od = om.getData();
            const d1 = this.getData(), mu1 = this.mean(0).getData(), sig1 = this.std(0).getData();
            if (B === undefined) {
                let x1, x2, j, jj, j2, ij, ji;
                for (x1 = 0, j = 0, jj = 0; j < nx; x1 += ny, j++, jj += nx + 1) {
                    od[jj] = corrcoef(d1, x1, 1, ny, mu1[j], sig1[j], d1, x1, 1, mu1[j], sig1[j]);
                    for (x2 = x1 + ny, j2 = j + 1, ij = j + j2 * nx, ji = j2 + j * nx; j2 < nx; x2 += ny, ij += nx, j2++, ji++) {
                        od[ij] = corrcoef(d1, x1, 1, ny, mu1[j], sig1[j], d1, x2, 1, mu1[j2], sig1[j2]);
                        od[ji] = od[ij];
                    }
                }
            } else {
                B = Matrix.from(B);
                if (!Check.checkSizeEquals(this.size(), B.size(), Matrix.ignoreTrailingDims)) {
                    throw new Error("Matrix.corrcoef: Input sizes must match.");
                }
                const d2 = B.getData(), mu2 = B.mean(0).getData(), sig2 = B.std(0).getData();
                let x1, x2, j, j2, ij;
                for (x1 = 0, j = 0; j < nx; x1 += ny, j++) {
                    for (x2 = 0, j2 = 0, ij = j + j2 * nx; j2 < nx; x2 += ny, ij += nx, j2++) {
                        od[ij] = corrcoef(d1, x1, 1, ny, mu1[j], sig1[j], d2, x2, 1, mu2[j2], sig2[j2]);
                    }
                }
            }
            return om;
        };
    })();
    Matrix.cov = function (A, B) {
        return this.from(A).cov(B);
    };
    Matrix.corrcoef = function (A, B) {
        return this.from(A).corrcoef(B);
    };

    {
        let tab, itab, fun;
        const asortAscend = function (a, b) {
            return tab[a] - tab[b];
        };
        const asortDescend = function (a, b) {
            return tab[b] - tab[a];
        };
        const sortAscend = function (a, b) {
            return a - b;
        };
        const sortDescend = function (a, b) {
            return b - a;
        };

        const sort = function (data, s, d, N) {
            let i, io, e;
            if (d === 1) {
                Array.prototype.sort.call(data.subarray(s, s + N), fun);
                return;
            }
            for (i = s, io = 0, e = s + N; i < e; i += d, io++) {
                tab[io] = data[i];
            }
            Array.prototype.sort.call(tab, fun);
            for (i = s, io = 0, e = s + N; i < e; i += d, io++) {
                data[i] = tab[io];
            }
        };
        const asort = function (data, s, d, N, out) {
            let i, io, e;
            for (i = s, io = 0, e = s + N; i < e; i += d, io++) {
                tab[io] = data[i];
                itab[io] = io;
            }
            Array.prototype.sort.call(itab, fun);
            for (i = s, io = 0, e = s + N; i < e; i += d, io++) {
                itab[io] = itab[io] * d + s;
                out[i] = itab[io];
            }
        };
        const median = function (data, s, d, N) {
            let tab2;
            if (d === 1) { // For fast computation on main dimension
                tab2 = data.slice(s, s + N);
            } else {
                let i, io, e;
                for (i = s, io = 0, e = s + N; i < e; i += d, io++) {
                    tab[io] = data[i];
                }
                tab2 = tab;
            }
            Array.prototype.sort.call(tab2, fun);
            const indice = Math.floor(tab.length / 2);
            return tab2.length % 2 === 0 ? 0.5 * (tab2[indice] + tab2[indice - 1]) : tab2[indice];
        };

        /** Sort the elements of the matrix.
        * @param {Number} [dim=undefined]
        *  Dimension on which the computation must be performed. If undefined,
        *  return all the elements sorted.
        * @param {String} [mode="ascend"]
        *  Sorting by increasing values ("ascend") or decreasing values ("descend")
        * @chainable
        */
        Matrix_prototype.sort = function (dim, mode = "ascend") {
            const size = typeof dim === "number" ? this.getSize(dim) : this.numel();
            tab = new Float64Array(size);
            if (mode === "ascend") {
                fun = sortAscend;
            } else if (mode === "descend") {
                fun = sortDescend;
            } else {
                throw new Error("Matrix.sort: Wrong mode selection: must be 'ascend' or 'descend'.");
            }
            return applyDim(this, sort, dim, true);
        };

        /** Compute the argsort of the elements of the matrix.
        * @param {Number} [dim=undefined]
        *  Dimension on which the computation must be performed. If undefined,
        *  return the global argsort.
        * @param {String} [mode="ascend"]
        *  Sorting by increasing values ("ascend") or decreasing values ("descend")
        * @return {Matrix}
        */
        Matrix_prototype.asort = function (dim, mode = "ascend") {
            const size = typeof dim === "number" ? this.getSize(dim) : this.numel();
            tab = new Float64Array(size);
            itab = new Uint32Array(size);
            if (mode === "ascend") {
                fun = asortAscend;
            } else if (mode === "descend") {
                fun = asortDescend;
            } else {
                throw new Error("Matrix.sort: Wrong mode selection: must be 'ascend' or 'descend'.");
            }
            const out = new Matrix(this.getSize(), "uint32");
            return applyDim(this, asort, dim, false, out);
        };

        /** Return the median value.
        * @param {Number} [dim=undefined]
        *  Dimension on which the computation must be performed. If undefined,
        *  return all the elements sorted.
        * @chainable
        */
        Matrix_prototype.median = function (dim) {
            const size = typeof dim === "number" ? this.getSize(dim) : this.numel();
            tab = new Float64Array(size);
            fun = sortAscend;
            return applyDim(this, median, dim);
        };
    }
    Matrix.sort = function (m, dim, mode) {
        return Matrix.from(m).clone().sort(dim, mode);
    };
    Matrix.asort = function (m, dim, mode) {
        return Matrix.from(m).asort(dim, mode);
    };
    Matrix.median = function (m, dim) {
        return Matrix.from(m).median(dim);
    };

    /** Return unique values in a matrix.
    * @return {Matrix}
    */
    Matrix_prototype.unique = function () {
        const sorted = this.clone().reshape().sort(0, "ascend"), sd = sorted.getData();
        const od = new Matrix.dataType(sd.length);
        od[0] = sd[0];
        const ie = sd.length;
        let i, j;
        for (i = 1, j = 1; i < ie; i++) {
            if (sd[i - 1] < sd[i]) {
                od[j++] = sd[i];
            }
        }
        return new Matrix.from(od.subarray(0, j));
    };
    Matrix.unique = function (A) {
        return Matrix.from(A).unique();
    };

    /** Return unique values in the union of two matrices.
    * @param {Matrix} A
    * @param {Matrix} B
    * @return {Matrix}
    */
    Matrix_prototype.union = function (B) {
        B = Matrix.from(B);
        return Matrix.cat(0, this.unique(), B.unique()).unique();
    };
    Matrix.union = function (A, B) {
        return Matrix.from(A).union(B);
    };

    /** Accumulate values in an array
    * @param {Array} subs
    *  Array of integers indicating subscript positions
    * @param {Array} val
    *  Values to be accumulated.
    * @param {Array} [size]
    *  Size of the output Array. Default is subs.max() + 1
    * @return {Matrix}
    */
    Matrix.accumarray = function (subs, val, size) {
        // Check subs argument
        subs = Matrix.from(subs);
        if (!subs.isreal()) {
            throw new Error("Matrix.accumarray: Is not yet implement for complex values.");
        }
        if (subs.ndims() > 2) {
            throw new Error("Matrix.accumarray: Subs must be a 2D Array.");
        }
        if (!Check.isArrayOfIntegers(subs.getData(), 0)) {
            throw new Error("Matrix.accumarray: Subs should be an array of positive integers.");
        }

        // Check size argument
        const max = subs.max(0).getData();
        let k, ek;
        if (Check.isArrayLike(size)) {
            for (k = 0, ek = max.length; k < ek; k++) {
                if (size[k] < max[k] + 1) {
                    throw new Error("Matrix.accumarray: Size and Subs values are unconsistent.");
                }
            }
        } else if (size === undefined) {
            size = [];
            for (k = 0, ek = max.length; k < ek; k++) {
                size[k] = max[k] + 1;
            }
        } else {
            throw new Error(`Matrix.accumarray: When defined, size must be an array-like object. Got ${size}`);
        }

        const steps = [1];
        for (k = 0, ek = size.length - 1; k < ek; k++) {
            steps[k + 1] = steps[k] * size[k];
        }

        // Scaning the from the second dimension (dim = 1)
        const sd = subs.getData(), N = subs.numel(), ni = subs.getSize(0);
        const ind = new Uint32Array(ni);
        let i, j, _j, ij;
        for (j = 0, _j = 0; _j < N; j++, _j += ni) {
            const s = steps[j];
            for (i = 0, ij = _j; i < ni; i++, ij++) {
                ind[i] += sd[ij] * s;
            }
        }

        const out = new Matrix(size), od = out.getData();
        val = Matrix.from(val);
        if (val.isscalar()) {
            val = val.asScalar();
            for (k = 0, ek = ind.length; k < ek; k++) {
                od[ind[k]] += val;
            }
        } else if (val.numel() === ind.length) {
            val = val.getData();
            for (k = 0, ek = ind.length; k < ek; k++) {
                od[ind[k]] += val[k];
            }
        } else {
            throw new Error("Matrix.accumarray: 'val' argument must either be a scalar or match 'subs' argument size.");
        }
        return out;
    };

    /** Perform PCA using SVD.
    * @param {Matrix} data
    *   MxN matrix of input data (M trials, N dimensions)
    * @return {Array} [coeff, scores, latent]
    *   coeff  - NxN matrix where each column is a principal component
    *   scores - MxN matrix of projected data
    *   latent - Nx1 matrix of variances
    */
    Matrix.pca = function (data) {
        const M = data.size(0);

        // subtract off the mean for each dimension
        const mn = data.mean(0);
        data = Matrix.minus(data, mn.repmat(M, 1));

        // construct the matrix Y
        const Y = Matrix.rdivide(data, Math.sqrt(M - 1));
        // SVD does it all
        let [, S, coeff] = Y.svd();

        // calculate the variances
        S = S.diag();
        const latent = S.times(S);

        // project the original data
        const scores = data.mtimes(coeff);
        return [coeff, scores, latent.reshape()];
    };
    
}
